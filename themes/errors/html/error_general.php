<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Error Page | Ông Bầu</title>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ruda:400,700,900">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style type="text/css">
        body { margin: 0; padding: 10px; background-color: #ed5565; font-family: "Ruda",sans-serif; color: #FFF; }
        .middle-box { margin: 0 auto; padding-top: 30px; z-index: 100; text-align: center; }
        .middle-box h3 { font-size: 25px; color:white; margin:20 auto; text-align: center;}
        /* If the screen size is 601px wide or more, set the font-size of <div> to 80px */
        @media screen and (min-width: 601px) {
          p.example {
            font-size: 25px;
          }
        }

        /* If the screen size is 600px wide or less, set the font-size of <div> to 30px */
        @media screen and (max-width: 600px) {
          p.example {
            font-size: 15px;
          }
        }

        @media screen and (max-width: 320px) {
          p.example {
            font-size: 13px;
          }
        }
    </style>
</head>

<body style="background: #FFBC10;">
    <div class="container">
        <div class="row">
              <img style="margin: 0 auto" src="<?php echo base_url('uploads/oblogo.png')?>" class="img-fluid" alt="Responsive image">           
        </div>
        <div class="row mt-3"> 
              <p class="example" style="margin: 0 auto;font-weight: bold;">Bạn không có quyền sử dụng chức năng này!</p>
        </div>
        
    </div>
</body>

</html>
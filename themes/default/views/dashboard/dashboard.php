<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<script src="<?= $assets ?>plugins/highchart/highcharts.js"></script>
<!-- <script src="<?= $assets ?>frontend/js/dashboard/index.js"></script> -->
<section class="content">
    <div class="row">
        <!-- ./col -->
        <div class="col-md-4 col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><span class="sales">0</span><sup style="font-size: 20px">đ</sup></h3>
                    <p>Doanh số</p>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-md-4 col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><span class="bill">0</span></h3>
                    <p>Số Bill</p>
                </div>
                <div class="icon">
                    <i class="fa fa-list"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-md-4 col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><span class="cup">0</span></h3>
                    <p>Số ly</p>
                </div>
                <div class="icon">
                    <i class="fa fa-coffee"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
    </div>

    <div class="row">
        <?php if (in_array($this->session->userdata('shopCode'), ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])): ?>
                <!-- <?php if (isset($countActive)): ?> -->
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-bank"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Đang hoạt động</span>
                            <span class="info-box-number countActive"><?= $countActive ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- <?php endif ?> -->
                <!-- <?php if (isset($countPending)): ?> -->
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-bank"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Sắp khai trương</span>
                            <span class="info-box-number countPending"><?= $countPending ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- <?php endif ?> -->
                <!-- <?php if (isset($countDeactive)): ?> -->
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-bank"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Đã đóng cửa</span>
                            <span class="info-box-number countDeactive"><?= $countDeactive ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- <?php endif ?> -->
        <?php endif ?>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y')), 'class="form-control datepicker" id="fromDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y')), 'class="form-control datepicker" id="toDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class='col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>'>
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("type"); ?></label>
                                        <?php
                                        echo form_dropdown('shopType', $shopTypeList, set_value('shopType', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class='col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>'>
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('model') ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class='col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>'>
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?= form_dropdown('provinceCode', $provinceList, set_value('provinceCode', $provinceCode), 'class="form-control select2 selectProvince" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class='col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>'>
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('districtCode', $districtList, set_value('districtCode', $districtCode), 'class="form-control select2 selectDistrict" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <?= form_checkbox('checkfull', 0, set_checkbox('checkfull', 0), 'class="check24h" id="check24h"') ?>
                                        <label>Hiển thị 24h </label>
                                    </div>
                                </div>
                            </div>
                            <?php if (in_array($this->controller . ':read', $this->session->userdata('permissions'))): ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="button" class="btn btn-warning"
                                                id="updateDataChart"><?= lang("view"); ?></button>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div id="chart"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        let loadTimeInterval;
        let originCategories, originData;
        let restClient = new $.RestClient('/api/dashboard/', {
            cache: 0,
            stripTrailingSlash: true,
            stringifyData: true,
            autoClearCache: true,
        });

        let timer = function () {
            loadTimeInterval = setInterval(function () {
                loadDashBoard();
            }, 1000 * 60 * 5);
        };
        restClient.add('summaryShop');
        restClient.add('summaryChart');
        loadDashBoard();


        $("#updateDataChart").click(function () {
            loadDashBoard();
        });

        $('input[type=checkbox].check24h').on('ifChecked', function (event) {
            buildChart();
        });

        $('input[type=checkbox].check24h').on('ifUnchecked', function (event) {
            buildChart();
        });



        function loadDashBoard() {
            if(typeof loadTimeInterval != 'undefined'){ clearInterval(loadTimeInterval); }
            timer();
            originCategories = null;
            originData = null;
            let fromDate = $("#fromDate").val();
            let toDate = $("#toDate").val();
            let shopCode = $("#selectShop").val();
            let shopType = $("#selectShopType").val();
            let shopModel = $("#selectShopModel").val();
            let provinceCode = $("#selectProvince").val();
            let districtCode = $("#selectDistrict").val();
            loadSummary(fromDate, toDate, shopCode, shopType, shopModel, provinceCode, districtCode);
            loadChart(fromDate, toDate, shopCode, shopType, shopModel, provinceCode, districtCode);
        }

        function buildChart(title = '') {
            let data = (originData != null) ? JSON.parse(JSON.stringify(originData)) : [];
            let categories = (originCategories != null) ? [...originCategories] : [];
            if (!$('#check24h').is(':checked')) {
                categories = categories.splice(6, 16);
                data.forEach(element => {
                    element.data = element.data.splice(6, 16);
                });
            }
            $('#chart').highcharts({
                chart: {
                    type: 'column',
                    style: {
                        fontFamily: 'Tahoma'
                    }
                },
                title: false,
                credits: {enabled: false},
                exporting: {enabled: false},

                subtitle: false, //{ text: 'Source: WorldClimate.com'},
                xAxis: {
                    categories: categories,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: false,
                    // tickInterval: 20
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                colors: ['#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce',
                    '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a',
                    '#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE',
                    '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'
                ],
                series: data
            });
        }

        function loadChart(fromDate = null, toDate = null, shopCode = null, shopType = null, shopModel = null, provinceCode = null, districtCode = null, check24h = null) {
            restClient.summaryChart.read({
                fromDate: fromDate,
                toDate: toDate,
                shopCode: shopCode,
                shopType: shopType,
                shopModel: shopModel,
                provinceCode: provinceCode,
                districtCode: districtCode,
                t: getTimeString()
            })
                .done(function (response, textStatus, xhrObject) {
                    if (textStatus == 'success' && response.success) {
                        originCategories = response.data.categories;
                        originData = response.data.data;
                        buildChart('');
                    }
                });
        }

        function loadSummary(fromDate = null, toDate = null, shopCode = null, shopType = null, shopModel = null, provinceCode = null, districtCode = null) {
            restClient.summaryShop.read({
                fromDate: fromDate,
                toDate: toDate,
                shopCode: shopCode,
                shopType: shopType,
                shopModel: shopModel,
                provinceCode: provinceCode,
                districtCode: districtCode,
                t: getTimeString()
            })
                .done(function (response, textStatus, xhrObject) {
                    if (textStatus == 'success' && response.success) {
                        $('.sales').text(formatNumber(response.data.totalAmount));
                        $('.bill').text(formatNumber(response.data.totalBill));
                        $('.cup').text(formatNumber(response.data.totalQuantity));
                        $('.countActive').text(formatNumber(response.data.totalActive));
                        $('.countPending').text(formatNumber(response.data.totalPending));
                        $('.countDeactive').text(formatNumber(response.data.totalStop));
                    }
                });
        }
    });
</script>

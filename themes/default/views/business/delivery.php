<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }

    .text-wrap {
        white-space: normal !important;
    }

    td.editable {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    /*.dataTables_scrollHeadInner {*/
    /*    width: 100% !important*/
    /*}*/
</style>
<script type="text/javascript">
    var table, detailTable;

    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        detailTable = $('#orderDetail').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            scrollX: true,
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            columnDefs: [
                {targets: 0, visible: false}
            ],
            fixedColumns: {
                leftColumns: 4
            }
        });
        var array_col_export = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
        <?php if( check_permission('delivery:Update')):?>
            array_col_export = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
        <?php endif;?>
        table = $('#orderTable').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            deferRender: true,
            scrollCollapse: true,
            select: {
                info: false,
                style: 'single'
            },
            order: [[7, "desc"]],
            ajax: {
                url: '<?=site_url('api/business/orderList');?>',
                type: 'GET',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    state: function () {
                        return $('#stateOrder').val();
                    },
                    shopModel: function () {
                        return $('#shopModel').val();
                    },
                    type: function () {
                        return 'delivery';
                    },
                    typeFilter: function () {
                        return $('#typeFilter').val();
                    }
                },
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            buttons: [
                <?php if(check_permission('delivery:Export')):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: {
                        columns: array_col_export,
                        // format: {
                        //     body: function (data, row, column, node) {
                        //         if (typeof data === 'string' || data instanceof String) {
                        //             data = data.replace(/<br\s*\/?>/ig, "\r\n");
                        //         }
                        //         return data;
                        //         // return data.replaceAll('<br>', "\r\n");
                        //     }
                        // }
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    }
                }
                <?php endif;?>
            ],
            fixedColumns: {
                leftColumns: 3
            },
            columns: [
                <?php if( check_permission('delivery:Update')):?>
                {
                    title: 'Thao tác',
                    data: null,
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row) {
                        <?php if( check_permission('delivery:Update')):?>
                        if (row.statusString == 'notdelivery') {
                            return '<div class="text-center"><div class="btn-group"><a class="btn btn-success btn-xs delivered" onclick="activeOrder(this);return false;" href="javascript:void(0);" >Giao hàng</a></div><div class="btn-group" style="margin-left:7px;"><a class="btn btn-warning btn-xs return" onclick="activeOrder(this);return false;" href="javascript:void(0);">Trả đơn</a></div></div>';
                        } else return '';
                        <?php else: ?>
                        return '';
                        <?php endif;?>
                    }
                },
                <?php endif;?>
                {title: 'Mã cửa hàng', data: "shopCode"},
                {title: 'Tên cửa hàng', data: "shopName"},
                {title: 'Địa chỉ', data: "shopAddress"},
                {title: 'Loại hình', data: "shopModel"},
                {title: 'Mô hình', data: "shopType"},
                {title: 'Ngày đặt', data: "approveDate1", render: formatFullDate, type: "date-euro"},
                {title: 'Ngày duyệt', data: "approveDate5", render: formatFullDate, type: "date-euro"},
                {title: 'Mã đơn hàng', data: "invCode", className: "dt-body-left"},
                {
                    title: 'Ghi chú',
                    data: "note",
                    className: "dt-body-left text-wrap",
                    render: function (data, type, row, meta) {
                        // let test = JSON.parse()
                        if (data != null) {
                            return data.replaceAll('\n', '<br>');
                            // return data.replace('\r\n', '<br>');
                        }
                        return data;
                    }
                },
                {
                    title: 'Ghi chú (duyệt)',
                    data: "approveNote",
                    className: "dt-body-left text-wrap",
                    render: function (data, type, row, meta) {
                        // let test = JSON.parse()
                        if (data != null) {
                            return data.replaceAll('\n', '<br>');
                            // return data.replace('\r\n', '<br>');
                        }
                        return data;
                    }
                },
                {title: 'Tổng duyệt', data: "totalAmountApprove", render: currencyFormat},
                {title: 'Tổng giao', data: "totalAmountDelivery", render: currencyFormat},
                {
                    title: 'Tình trạng', data: null, render: function (data, type, row) {
                        if (row.statusString == 'notdelivery') {
                            return 'Chưa giao';
                        } else {
                            return 'Đã giao';
                        }
                    }
                },
                {title: 'TT phân phối', data: "ttpp"},
                {title: 'Nhà phân phối', data: "distributor"},
                {title: 'Kho', data: "orgCode"},
                {title: 'Số SO', data: "soNumber"},
                {title: 'Bảng giá', data: "priceName"},
            ]

        });

        $('#search_table').on('keyup change', function (e) {
            let code = (e.keyCode ? e.keyCode : e.which);
            if (((code == 13 && table.search() !== this.value) || (table.search() !== '' && this.value === ''))) {
                table.search(this.value).draw();
            }
        });

        /**
         * [datadetail Table]
         * @type {[type]}
         */
        table.on('user-select', function (e, dt, type, cell, originalEvent) {
            if (cell.index().column == 0) return false;
            let row = dt.row(cell.index().row).node();
            $(row).removeClass('hover');
            if ($(row).hasClass('selected')) {
                e.preventDefault();
                // deselect
            } else {
                // $('input[type=checkbox]').iCheck('uncheck');
                // $('.checkbox').show();
                let rowData = table.row(row).data();
                let idParent = rowData.invId;
                detailTable.destroy();
                detailTable = $('#orderDetail').DataTable({
                    lengthMenu: [[50, 150, -1], [50, 150, "All"]],
                    dom: 'lBfrtip',
                    scrollX: true,
                    scrollY: 400,
                    deferRender: true,
                    scrollCollapse: true,
                    ajax: {
                        url: '<?=site_url('api/business/orderDetail');?>' + '?id=' + idParent + '&shopCode=' + rowData.shopCode,
                        type: 'GET',
                        "data": function (d) {
                            d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                        },
                        "dataSrc": function (json) {
                            let totalPrice = 0;
                            let responseData = json.data;
                            responseData.forEach(function print(element) {
                                totalPrice += element.QTY_DELIVERY * element.PRICE;
                            });
                            rowData.TOTAL_DELIVERY = totalPrice;
                            table.row(row).data(rowData).draw(false);
                            return responseData;
                        }
                    },
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                        "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                    },
                    buttons: [
                        <?php if( check_permission('delivery:Export')):?>
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                            className: 'btn btn-warning',
                            footer: true,
                            exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6]},
                            init: function (api, node, config) {
                                $(node).removeClass('btn-default')
                            }
                        }
                        <?php endif;?>
                    ],
                    fixedColumns: {
                        leftColumns: 3
                    },
                    columns: [
                        {data: "MTL_CODE", width: 100},
                        {data: "MTL_NAME"},
                        {data: "UNIT_CODE"},
                        {data: "QTY_APPROVE", className: "dt-body-right", render: formatIntegerSeparator},
                        {data: "QTY_DELIVERY", className: "dt-body-right editable", render: formatIntegerSeparator},
                        {data: "PRICE", className: "dt-body-right", render: formatIntegerSeparator},
                        {
                            data: null, className: "dt-body-right", render: function (data, type, row) {
                                row.TOTAL = row.PRICE * row.QTY_DELIVERY;
                                return formatIntegerSeparator(row.TOTAL);
                            }
                        },
                    ],
                    initComplete: function (settings, json) {
                        // if(detailTable.rows({filter: 'applied'}).data().length == 0){
                        //     $('input[type=checkbox]').iCheck('check');
                        // }else{
                        //     $('input[type=checkbox]').iCheck('uncheck');
                        // }
                    }
                });
            }
        });


        /**
         * Edit table
         *
         */
        <?php if( check_permission('delivery:Update') ):?>
        $("#orderDetail").on("click", "tbody tr td.editable", function (e) {
            let headerData = getDataSelectedRowDatatable(table)[0];

            if (headerData.statusString != 'notdelivery') {
                $(this).removeClass('editable');
                return;
            }
            if ($(this).find('input').length) return;
            let tr = $(this).closest('tr');
            let row = detailTable.row(tr);
            let rowData = row.data();
            let html = '<input style="height:33px" class="form-control input-text" min="0" onkeypress="validateNumber()" type="number">';
            $(this).html(html);
            $(this).find("input").focus().val(rowData.QTY_DELIVERY);
        });

        $('#orderDetail').on('focusout', 'tbody tr td input', async function (e) {
            let headerData = getDataSelectedRowDatatable(table)[0];
            let tr = $(this).closest('tr');
            let row = detailTable.row(tr);
            let rowData = row.data();
            let deliveryQty = parseInt($(this).val());
            let params = {
                shopCode: headerData.shopCode,
                invId: headerData.invId,
                detailId: rowData.IDL_ID,
                mtlCode: rowData.MTL_CODE,
                mtlConvertCode: rowData.MTL_CONVERT_CODE,
                mtlName: rowData.MTL_NAME,
                unit: rowData.UNIT_CODE,
                price: rowData.PRICE,
                qtyOrder: rowData.QTY_ORDER,
                qtyApprove: rowData.QTY_APPROVE,
                qtyDelivery: deliveryQty,
                state: headerData.statusString
            };
            if (params.qtyDelivery > rowData.QTY_APPROVE) params.qtyDelivery = rowData.QTY_APPROVE;
            if (params.qtyDelivery != rowData.QTY_DELIVERY) {
                let result = await saveOrderDetail(params);
                if (result.success) {
                    rowData.IDL_ID = result.data.detailId;
                    rowData.QTY_DELIVERY = params.qtyDelivery;
                    rowData.TOTAL = params.qtyDelivery * params.price;
                }
            }
            detailTable.row(row).data(rowData).draw();
            let newTotalAmount = sumByProperty(detailTable.data().toArray().filter(detail => detail.QTY_DELIVERY > 0), 'TOTAL')
            headerData.totalAmountDelivery = newTotalAmount;
            let trHead = table.row(table.rows({selected: true})[0]);
            table.row(trHead).data(headerData).draw();
        });

        function saveOrderDetail(params) {
            let url = '<?=site_url('api/business/orderDetail?t=');?>' + getTimeString();
            return new Promise((resolve, reject) => {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: params,
                    dataType: 'json',
                    // async:true
                    success: function (response) {
                        resolve(response)
                    },
                    error: function (error) {
                        reject(error)
                    },
                });
            });
        }
        <?php endif;?>




        $('#btnSearch').click(function () {
            detailTable.clear().draw();
            //let urlApi = '<?//=site_url('api/business/orderList?v=1');?>//'
            //let fromDate = $('#fromDate').val();
            //let toDate = $('#toDate').val();
            //let state = $('#stateOrder').val();
            //let shopModel = $('#shopModel').val();
            //urlApi += '&fromDate=' + fromDate + '&toDate=' + toDate + '&state=' + state + '&shopModel=' + shopModel + '&type=delivery';
            table.ajax.reload();
        });

    });

    <?php if( $type != 'delivered' && check_permission('delivery:Update')):?>
    function activeOrder(button) {
        let action = 'delivered';
        if ($(button).hasClass('return')) {
            action = 'return';
        }
        let tr = $(button).closest('tr');
        let row = table.row(tr);
        let rowData = row.data();
        let status = rowData.status;
        let parent = rowData.parentId;
        let invCode = rowData.invCode;
        let shopCode = rowData.shopCode;
        let shopName = rowData.shopName;
        let state = rowData.statusString;
        var msg = 'Số lượng thực giao không thể điều chỉnh sau khi thay đổi trạng thái.\nBạn có chắc chắn?';
        if (action == 'return') {
            status = 1;
            if (['kienlonggroup', 'dongtamgroup'].includes(parent)) {
                status = 2;
            }
            msg = 'Bạn muốn yêu cầu duyệt lại đơn hàng ' + invCode + ' \n(' + shopCode + ' - ' + shopName + ')?';
        } else {
            status = 6;
        }
        if (confirm(msg)) {
            $.ajax({
                url: '<?=site_url('api/business/activeOrder?t=');?>' + getTimeString(),
                type: 'PUT',
                data: JSON.stringify({id: rowData.invId, shopCode: rowData.shopCode, status: status, state: state}),
                // dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        if ($(tr).hasClass('selected')) {
                            detailTable.clear().draw();
                            $('.checkbox').hide();
                        }
                        row.remove().draw();
                        if (action == 'delete') {
                            alert('Hủy đơn hàng thành công.');
                        } else if (action == 'return') {
                            alert('Trả đơn hàng thành công.');
                        } else {
                            alert('Cập nhật trạng thái thành công');
                        }
                    }
                }
            });
        }
    }
    <?php endif;?>


</script>
<style type="text/css">
    .table td:first-child {
        padding: 1px;
    }

    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) {
        text-align: center;
    }

    .table td:nth-child(9) {
        text-align: right;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate" style=""'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate" style=""'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("status"); ?></label>
                                        <?php
                                        $a = array(
                                            'All' => 'Tất cả',
                                            'notdelivery' => 'Chưa giao',
                                            'delivered' => 'Đã giao'
                                        );
                                        echo form_dropdown('state', $a, set_value('state', $state), 'id="stateOrder" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= 'Loại hình'; ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="shopModel" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại duyệt</label>
                                        <?php
                                        $typeApprovedArray = array('Ngày đặt đơn', 'Ngày duyệt đơn');
                                        echo form_dropdown('typeFilter', $typeApprovedArray, set_value('typeFilter', $typeFilter), 'id="typeFilter" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button id="btnSearch" type="submit"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="orderTable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr class="active">
                                        <?php if (check_permission('delivery:Update')): ?>
                                            <th>Thao tác</th>
                                        <?php endif; ?>
                                        <th>Mã cửa hàng</th>
                                        <th><?= lang('name_store') ?></th>
                                        <th>Địa chỉ</th>
                                        <th><?= lang("model"); ?></th>
                                        <th>Loại</th>
                                        <th><?= lang('order_date') ?></th>
                                        <th>Thời gian duyệt</th>
                                        <th><?= lang("order_code"); ?></th>
                                        <th><?= lang("note"); ?></th>
                                        <th><?= lang("note"); ?></th>
                                        <th>Tổng duyệt</th>
                                        <th>Tổng giao</th>
                                        <th>Trạng thái</th>
                                        <th>TT Phân phối</th>
                                        <th>Nhà phân phối</th>
                                        <th>Kho</th>
                                        <th>Số SO</th>
                                        <th>Bảng giá</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12"
                                            class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dữ liệu chi tiết</h3>
                    <!-- <div class="box-tools">
                        <div class="form-group">
                            <div class="checkbox" style="display:none;">
                                <label>
                                    <input name="check_all" class="check_all" type="checkbox">
                                Tất cả
                                </label>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="box-body table-responsive">
                    <table id="orderDetail" class="table table-striped table-bordered processingOrder">
                        <thead>
                        <tr class="active">
                            <th>Mã sản phẩm</th>
                            <th>Tên sản phẩm</th>
                            <th>ĐVT</th>
                            <th>Số lượng duyệt</th>
                            <th>SL thực giao</th>
                            <th>Đơn giá</th>
                            <th>Thành tiền</th>
                            <!-- <th>Thao tác</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="post_msg">

                </div>
            </div>
        </div>
    </div>
</section>

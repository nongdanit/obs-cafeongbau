<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td{white-space: nowrap;}
    .background-color{ background-color: #FFBC10 !important; }
    .text-wrap{
        white-space:normal;
    }
    /*#UTable{
        table-layout: fixed;
        margin: 0 auto;
        clear: both;
        width: 100% !important;
    }*/
</style>
<?php
    $v = "?v=1";
    //$v .= "&shoptype=".$shopType;
    $v .= "&fromDate=".$fromDate;
    $v .= "&toDate=".$toDate;
    $v .= "&shopCode=".$shopCode;
    $v .= "&shopModel=".$shopModel;
    // $v .= "&status=".$status;
?>
<script type="text/javascript">
    var table = null;
    var baseApi = '<?=site_url('api/business/promotionShop');?>'
    var urlApi = baseApi + '<?php echo $v; ?>';
    
    function editApprovePromotion(index){
        if(table == null) return;

        var datarowClick = table.row(index).data();
        // console.log(datarowClick);
        showPromotionEdit(index, datarowClick.ID,datarowClick.PROMO_NAME,datarowClick.SHOP_NAME,datarowClick.SHOP_ADDRESS,datarowClick.START_DATE,datarowClick.END_DATE);
    }
    function showPromotionEdit(index, id, promotionName, shopName, shopAddress, startDate, endDate){
        $('#rowIndex').val(index);
        $('#promoIdEdit').val(id); 
        $('#promoNameEdit').val(promotionName);
        $('#shopNameEdit').val(shopName);
        $('#shopAddressEdit').val(shopAddress);
        $('#startDateEdit').val(startDate);  
        $('#endDateEdit').val(endDate);
        $('#btnBack').html('Đóng');
        $('#btnBack').addClass('promotionClose');
        $('#btnBack').removeClass('promotionBack');
        $('#formOrder').modal('show');
        $('#create').addClass('hidden');
        $('#edit').removeClass('hidden');
        $('#endDateEdit').data("DateTimePicker").minDate(startDate);
    }
    function fnAddRow(data) {
        $('#UTable').dataTable().fnAddData(data);
    }
    $(document).ready(function() {

        table = $('#UTable').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            //autoWidth: true,
            scrollX: true,
            scrollY: 400,
            //scroller: true,
            deferRender:    true,
            scrollCollapse: true,
            select: {
                info: false,
                style: 'single'
            },
            order: [[ 0, "desc" ]],
            ajax : {
                url: urlApi,
                type: 'GET',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            columnDefs : [
                {targets: 4, width: 250},
                {targets: 7, type:"date-eu"},
                {targets: 8, type:"date-eu"}
            ],
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            fixedColumns: {
                leftColumns: 4
            },
            buttons: [
            <?php if( check_permission( 'promotions:Export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: { columns: [2, 3, 4, 5, 6, 7, 8] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
            <?php endif;?>
            ],
            columns: [
                { "data": "ID", "visible": false, "searchable": false}, 
                { "data": null, "visible": true, "searchable": false, "orderable": false, "render":function ( data, type, row, meta ) {
                    // console.log(meta.row);
                        var btn = '<div class="text-center"><input type="button" class="btn btn-warning btn-xs align-middle" value="Cập nhật" onClick="editApprovePromotion('+ meta.row +')"></div>';
                        return btn;   
                    }
                }, 
                { "data": "SHOP_CODE"},
                { "data": "SHOP_NAME"},
                { "data": "SHOP_ADDRESS", className: ''},
                { "data": "PROMO_NAME", className: ''},
                { "data": "USE_VOUCHER", render: function(data){
                        if(data == 0){
                            return 'Không';
                        }else{
                            return 'Có';
                        }
                    }
                },
                { "data": "START_DATE" , render: hrsd},
                { "data": "END_DATE", render: hrsd},
                //{ "data": "NOTE", "visible": true},
            ]
        });
        table.on('user-select', function ( e, dt, type, cell, originalEvent ) {
            var row = dt.row( cell.index().row ).node();
            $(row).removeClass('hover');
            if ($(row).hasClass('selected') ) {
                e.preventDefault();
            }
            //var datarowClick = dt.row(0).data();
            //console.log(dt.row(row).index());
            //console.log(datarowClick);
            // showPromotionEdit(datarowClick.ID,datarowClick.PROMO_NAME,datarowClick.SHOP_NAME,datarowClick.START_DATE,datarowClick.END_DATE);
            //if(cell.index().column == 0) return false;
        });

        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });


        <?php if( check_permission( 'promotions:insert' )): ?>
        $("#promotionsForm").validate({
            rules: {
              promoId: "required",
              shopCode: "required",
              startDate: "required",
              endDate: "required",
            },
            messages: {
              promoId: "Chọn khuyến mãi",
              shopCode: "Chọn cửa hàng",
              startDate: "Chọn ngày bắt đầu khuyến mãi", 
              endDate: "Chọn ngày kết thúc khuyến mãi",             
            },
            submitHandler: function(form) {
                var params = $(form).serializeArray();
                var promoName = $('#selectPromotion option:selected').text();
                var shopCode = $('#selectShopOrder').val();
                var shopName = $('#selectShopOrder option:selected').text();
                var shopAddress = $('#selectShopAddress  option:selected').text();
                var startDate = $('#startDate').val();
                var endDate = $('#endDate').val();
                //console.log(params);
                $(form).ajaxSubmit({
                    type:"POST",
                    dataType: 'json',
                    data: params,
                    url:  $(form).attr('action'),
                    success: function(response) {
                        //console.log(response);
                        if(response.success){
                            alert('Thêm khuyến mã cho cửa hàng thành công.');
                            var promo = {
                                ID: response.data,
                                SHOP_CODE: shopCode,
                                SHOP_NAME: shopName,
                                SHOP_ADDRESS: shopAddress,
                                PROMO_NAME: promoName,
                                START_DATE: startDate,
                                END_DATE: endDate,
                                USE_VOUCHER: 0,
                            };
                            fnAddRow(promo);
                            $('#formOrder').modal('hide');

                        }else{
                            alert('Thêm khuyến mã cho cửa hàng không thành công.');
                        }
                    },
                    error: function(jqXHR, textStatus) {
                         console.log('error');
                    }
                });
            }
        });
        $("#promotionsFormEdit").validate({
            rules: {
              startDateEdit: "required",
              endDateEdit: "required",
            },
            messages: {
              startDate: "Chọn ngày bắt đầu khuyến mãi", 
              endDate: "Chọn ngày kết thúc khuyến mãi",             
            },
            submitHandler: function(form) {
                var rowIndex = $('#rowIndex').val();
                var startDate = $('#startDateEdit').val();
                var endDate = $('#endDateEdit').val();
                var rowData = table.row(rowIndex).data();
                var params = {id: rowData.ID, shopCode: rowData.SHOP_CODE, startDate: startDate, endDate: endDate};
                $.ajax({
                    url: $(form).attr('action'),
                    type: 'PUT',
                    data: JSON.stringify(params),
                    // dataType: 'json',
                    success: function(response) {
                        if(response.success){
                            alert('Cập nhật thành công.');
                            rowData.START_DATE = startDate;
                            rowData.END_DATE = endDate;
                            table.row(rowIndex).data(rowData).draw(true);
                            $('#formOrder').modal('hide');
                        }
                    },
                    error: function(jqXHR, textStatus) {
                        alert('Đã có lỗi xảy ra, vui lòng thử lại.')
                    }
                });
            }

        });
        $('#btnCreatePromotion').click(function(){
            $('#selectShopOrder').val(''); // Select the option with a value of '1'
            $('#selectShopOrder').trigger('change'); // Notify any JS components that the value changed

            $('#selectPromotion').val('');
            $('#selectPromotion').trigger('change'); // Select the option with a value of '1'
            $('#btnBack').removeClass('promotionClose');
            $('#btnBack').addClass('promotionBack');
        });

        $('#selectShopOrder').on('change', function(e) {
            let shop = $(this).val();
            $('#selectShopAddress').val(shop);
            $('#selectShopAddress').trigger('change'); // Notify any JS components that the value changed
        });

        $('#formOrder').on('click','.promotionBack', function(){
            $('#edit').addClass('hidden');
            $('#create').removeClass('hidden');
        });
        $('#formOrder').on("click",'.promotionClose', function(){
            $('#formOrder').modal('hide');
        });
        $('#endDate').datetimepicker({ format: 'DD-MM-YYYY', showClear: true, showClose: true, useCurrent: false, widgetPositioning: { horizontal: 'auto', vertical: 'auto' } });
        $('#endDateEdit').datetimepicker({ format: 'DD-MM-YYYY', showClear: true, showClose: true, useCurrent: false, widgetPositioning: { horizontal: 'auto', vertical: 'auto' } });
        $("#startDate").on("dp.change", function (e) {
            $('#endDate').data("DateTimePicker").minDate(e.date);
            if(e.date > $('#endDate').data("DateTimePicker").date()){
                $('#endDate').data("DateTimePicker").date(e.date);
            }
        });
        $("#startDateEdit").on("dp.change", function (e) {
            $('#endDateEdit').data("DateTimePicker").minDate(e.date);
            if(e.date > $('#endDateEdit').data("DateTimePicker").date()){
                $('#endDateEdit').data("DateTimePicker").date(e.date);
            }
        });
        $('#formOrder').on('show.bs.modal', function (event) {
            $('#edit').addClass('hidden');
            $('#create').removeClass('hidden');
        });
        <?php endif;?>
        $('#btnSearch').click(function(e){

            let v = "?v=1";
            v += "&fromDate=" + $('#fromDate').val();
            v += "&toDate=" + $('#toDate').val();
            v += "&shopCode=" + $('#selectShop').val();
            v += "&shopModel=" + $('#selectShopModel').val();
            urlApi = baseApi + v;
            table.ajax.url(urlApi).load();
        });
    });
         
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if( check_permission( 'promotions:insert' )): ?>
                    <button id="btnCreatePromotion" type="button" class="btn btn-warning" data-toggle="modal" data-target="#formOrder">Thêm cửa hàng</button>
                     <?php endif; ?>
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("reports/rptDaily", array('method'=>'get', 'id' => 'myform'));?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('model')?></label>
                                        <?php
                                            echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <button id="btnSearch" type="button" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                            <!-- <?= form_close();?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-striped table-bordered table-hover row-border" style="width:100%">
                                    <thead>
                                    <tr class="active">
                                        <th>ID</th>
                                        <th>Thao tác</th>
                                        <th><?=lang('shop_code'); ?></th>
                                        <th><?=lang('shop_name'); ?></th>
                                        <th><?=lang('address'); ?></th>
                                        <th>Khuyến mãi</th>
                                        <th>Voucher</th>
                                        <th>Ngày bắt đầu</th>
                                        <th>Ngày kết thúc</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if( check_permission( 'promotions:insert' )): ?>
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="formOrder" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="create" class="">
                <!-- approve promotion form -->
                <?= form_open("api/business/approvePromotion", array('method'=>'post','id' => 'promotionsForm'));?>
                <div class="modal-header">
                    <h3 class="modal-title">Thêm cửa hàng</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Khuyến mãi</label>
                                        <?php
                                        echo form_dropdown('promoId', $promotionsList, '', 'id="selectPromotion" data-placeholder="Chọn khuyến mãi" class="form-control select2" style="width:100%"');
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        unset($shopList['All']);
                                            echo form_dropdown('shopCode', $shopList, set_value('shopCode'), 'id="selectShopOrder" data-placeholder="Chọn cửa hàng" class="form-control select2" style="width:100%"');
                                        ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label" for="address"><?= lang("address"); ?></label>
                                        <?php
                                        echo form_dropdown('shopAddress', $shopAddress, set_value('shopAddress'), 'id="selectShopAddress" class="form-control select2" disabled style="width:100%"');
                                        ?>
                                    </div>                             
                                    <div class="form-group">
                                        <label class="control-label" for="date">Ngày bắt đầu</label>
                                        <?= form_input('startDate', set_value('date', date('d-m-Y')), 'class="form-control datepicker" id="startDate"');?>
                                    </div>
                                     <div class="form-group">
                                        <label class="control-label" for="date">Ngày kết thúc</label>
                                        <?= form_input('endDate', set_value('date', date('d-m-Y')), 'class="form-control datepicker" id="endDate"');?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-warning">Lưu</button>
                </div>
                <?= form_hidden('update', '0');?>
                <?= form_close();?>
            </div>
            <div id="edit" class="hidden">               
                <div class="modal-header">
                    <h5 class="modal-title">Cập nhật khuyến mãi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- <div class="col-xs-6 hidden" style="border-right: 1px solid #cdcdcd">
                            <div class="form-group">
                                 <label class="control-label" for="code">Khuyến mãi hiện có</label>
                            </div>
                            <div class="form-group">
                                 <h5 class="promotionName" for="code">Khuyến mãi</h5>                              
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="date">Ngày bắt đầu</label>
                                <?= form_input('startDate', set_value('date', date('d-m-Y')), 'class="form-control datepicker" id="startDateOrigin" readonly');?>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="date">Ngày kết thúc</label>
                                <?= form_input('endDate', set_value('date', date('d-m-Y')), 'class="form-control datepicker" id="endDateOrigin" readonly');?>
                            </div>                           
                        </div> -->
                        
                        <!-- update promotion form -->
                        <?= form_open("api/business/approvePromotion", array('method'=>'put','id' => 'promotionsFormEdit'));?>
                        <div class="col-xs-12">
                            <div class="form-group">
                                 <label class="control-label" for="code">Khuyến mãi</label>
                                 <input class="form-control" type="text" name="" readonly="" id="promoNameEdit">
                                 <input type="hidden" name="rowid" id="promoIdEdit">  
                                 <input type="hidden" name="rowIndex" id="rowIndex">                            
                            </div>
                            <div class="form-group">
                                 <label class="control-label" for="code">Tên cửa hàng</label>
                                 <input class="form-control" type="text" name="" readonly="" id="shopNameEdit">
                            </div>
                            <div class="form-group">
                                 <label class="control-label" for="code">Địa chỉ</label>
                                 <input class="form-control" type="text" name="" readonly="" id="shopAddressEdit">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="date">Ngày bắt đầu</label>
                                <?= form_input('startDate', set_value('date', date('d-m-Y')), 'class="form-control datepicker" id="startDateEdit"');?>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="date">Ngày kết thúc</label>
                                <?= form_input('endDate', set_value('date', date('d-m-Y')), 'class="form-control datepicker" id="endDateEdit"');?>
                            </div>   
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" id="btnBack" class="promotionBack">Quay lại</button>
                    <button type="submit" class="btn btn-warning">Cập nhật mới</button>
                </div>                  
                <?= form_hidden('update', '1');?>            
                <?= form_close();?>
            </div>
        </div>
        <!-- <?= form_hidden('formtype', 'create');?> -->
    </div>
</div>
<?php endif; ?>

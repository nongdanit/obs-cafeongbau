<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }

    .btn-group>.btn:last-child:not(:first-child){
        margin: 0px 10px !important;
    }

    td.editable {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }
</style>
<script type="text/javascript">
    var baseApiExport = '<?=site_url('api/business/businessApproveOrderExport');?>';
    var table, detailTable;
    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });

        table = $('#orderTable').DataTable({
            dom: 'lBfrtip',
            autoWidth: false,
            scrollX: true,
            scrollY: 400,
            deferRender: true,
            scrollCollapse: true,
            select: {
                info: false,
                style: 'single'
            },
            search: {
                "caseInsensitive": true,
            },
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            order: [[7, "desc"]],
            ajax: {
                url: '<?=site_url('api/business/orderList');?>',
                type: 'GET',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    shopModel: function () {
                        return $('#shopModel').val();
                    },
                    orderType: function () {
                        return $('#orderType').val();
                    },
                    state: function () {
                        return $('#stateOrder').val();
                    },
                    type: function () {
                        return 'approve';
                    },
                    typeFilter: function () {
                        return $('#typeFilter').val();
                    }
                }
            },
            columnDefs: [
                {"targets": 6, "type": "date-eu"},
                {"targets": 7, "type": "date-euro"}
            ],
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },

            buttons: [
                <?php if( check_permission('approve_order:Export')):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]},
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Chi Tiết Tất Cả',
                    className: 'btn btn-warning',
                    footer: false,
                    //exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ] },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        let tables = table.ajax.params();
                        var invId = '';
                        $.each(table.rows().data(), function( item, i ) {
                            invId += i.invId+",";
                        });
                        console.log(invId);
                        let fromDate = tables.fromDate();
                        let toDate = tables.toDate();
                        let exportUrl = `${baseApiExport}?invId=${invId}&fromDate=${fromDate}&toDate=${toDate}`;
                        downloadFile(exportUrl, 'Tất Cả Chi Tiết Duyệt ĐƠn Hàng.xlsx');

                    },

                }
                <?php endif;?>
            ],

            fixedColumns: {
                leftColumns: 3
            },
            columns: [
                <?php if(check_permission('approve_order:Update') || check_permission('approve_order:Delete')): ?>
                {
                    data: null, searchable: false,
                    "orderable": false, "render": function (data, type, row, meta) {
                        //console.log(meta.row);
                        if (row.statusString != 'pending') return '';
                        let btn = '';
                        <?php if( check_permission('approve_order:Update')):?>
                        btn += '<div class="text-center"><div class="btn-group"><a class="btn btn-success btn-xs approve" onclick="activeOrder(this);return false;" href="javascript:void(0);">Duyệt đơn</a></div><div class="btn-group" style="margin-left:7px;"><a class="btn btn-warning btn-xs return" onclick="activeOrder(this);return false;" href="javascript:void(0);">Trả đơn</a></div>';
                        <?php endif;?>
                        <?php if( check_permission('approve_order:Delete')):?>
                        btn += '<div class="btn-group" style="margin-left:7px;"><a class="btn btn-danger btn-xs delete" onclick="activeOrder(this);return false;" href="javascript:void(0);" >Hủy đơn</a></div></div>';
                        <?php endif;?>
                        return btn;

                    }
                },
                <?php endif;?>
                {data: "shopCode"},
                {data: "shopName"},
                {data: "shopModel", searchable: false},
                {data: "shopType"},
                {data: "orderType"},
                // {data: "invDate", render: formatDate, type: "date-eu"},
                {data: "approveDate1", render: formatFullDate, type: "date-euro"},
                {data: "approveDate5", render: formatFullDate, type: "date-euro"},
                {data: "invCode", className: "dt-body-left"},
                {data: "note", className: "dt-body-left"},
                {data: "totalAmount", className: "dt-body-right", render: formatIntegerSeparator},
                {data: "totalAmountApprove", className: "dt-body-right", render: formatIntegerSeparator},
                {
                    data: null, render: function (data, type, row) {
                        if (row.statusString == 'pending') {
                            return 'Chờ duyệt';
                        } else {
                            return 'Đã duyệt';
                        }
                    }
                },
                {data: "ttpp"},
                {data: "distributor"},
                {data: "orgCode"},
                {data: "priceName"},
            ],
            // "initComplete": function(settings, json) {
            //     //alert( 'DataTables has finished its initialisation.' );
            //     table.order([ 6, 'desc' ]).draw();
            // }

        });

        detailTable = $('#orderDetail').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            dom: 'lBfrtip',
            autoWidth: false,
            scrollX: true,
            scrollY: 400,
            deferRender: true,
            scrollCollapse: true,
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            buttons: [],
            fixedColumns: {
                leftColumns: 2
            },
        });

        table.on('user-select', function (e, dt, type, cell, originalEvent) {
            //console.log(cell.index().column == 11) return false;
            if (cell.index().column == 0 && (cell.data().status == 1 || cell.data().status == 2)) return false;
            let row = dt.row(cell.index().row).node();
            $(row).removeClass('hover');
            if ($(row).hasClass('selected')) {
                e.preventDefault();
                // deselect
            } else {
                $('input[type=checkbox]').iCheck('uncheck');
                $('.checkbox').show();
                let rowData = table.row(row).data();
                let idParent = rowData.invId;
                detailTable.destroy();
                detailTable = $('#orderDetail').DataTable({
                    lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                    dom: 'lBfrtip',
                    scrollX: true,
                    scrollY: 400,
                    deferRender: true,
                    scrollCollapse: true,
                    autoWidth: false,
                    pageLength: -1,
                    order: [[3, "desc"]],
                    ajax: {
                        url: '<?=site_url('api/business/orderDetail');?>',
                        type: 'GET',
                        data: {
                            id: function () {
                                return idParent;
                            },
                            shopCode: function () {
                                return rowData.shopCode;
                            }
                        },
                        dataSrc: function (json) {
                            let totalPrice = 0;
                            let responseData = json.data;
                            responseData.forEach(function print(element) {
                                totalPrice += element.TOTAL;
                            });
                            rowData.totalAmount = totalPrice;
                            table.row(row).data(rowData).draw(false);
                            return responseData;
                        }
                    },
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                        "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                    },
                    buttons: [
                        <?php if( check_permission('approve_order:Export')):?>
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                            className: 'btn btn-warning',
                            footer: false,
                            // exportOptions: { columns: [ 1, 2, 3, 4, 5, 6, 7 ] },
                            init: function (api, node, config) {
                                $(node).removeClass('btn-default')
                            }
                        }
                        <?php endif;?>
                    ],
                    fixedColumns: {
                        leftColumns: 2
                    },
                    columns: [
                        {data: "MTL_CODE", width: 100},
                        {data: "MTL_NAME"},
                        {data: "UNIT_CODE"},
                        {data: "QTY_ORDER", className: "dt-body-right", render: formatIntegerSeparator},
                        {data: "QTY_APPROVE", className: "dt-body-right editable", render: formatIntegerSeparator},
                        {data: "PRICE", render: currencyFormat},
                        {
                            data: null, className: "dt-body-right", render: function (data, type, row) {
                                row.TOTAL = row.PRICE * row.QTY_APPROVE;
                                return formatIntegerSeparator(row.TOTAL);
                            }
                        },
                    ],
                    // rowCallback: function(row, data, index) {
                    //     $(row).attr("idl_id" , data.IDL_ID);
                    //     $(row).attr("inv_id" , data.INV_ID);
                    //     $(row).attr("code" , data.MTL_CODE);
                    //     $(row).attr("name" , data.MTL_NAME);
                    //     $(row).attr("unit" , data.UNIT_CODE);
                    //     $(row).attr("qty_order" , data.QTY_ORDER);
                    //     $(row).attr("qty_approve" , data.QTY_APPROVE);
                    //     $(row).attr("price" , data.PRICE);
                    //     $(row).attr("type", 1);
                    // },
                    initComplete: function (settings, json) {
                        if (detailTable.rows({filter: 'applied'}).data().length == 0) {
                            $('input[type=checkbox]').iCheck('check');
                        } else {
                            $('input[type=checkbox]').iCheck('uncheck');
                        }
                    }
                });
                checkReload = true;
            }
        });
        // table.columns( 1 ).search( 1 ).draw();
        $('#search_table').on('keyup change', function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (((code == 13 && table.search() !== this.value) || (table.search() !== '' && this.value === ''))) {
                table.search(this.value).draw();
            }
        });
        <?php if( check_permission('approve_order:Update')):?>
        $("#frmApproveOrder").validate({
            rules: {
                approvePrice: {
                    required: true
                },
                approveOrg:{
                    required: true
                }
            },
            messages: {
                approvePrice: {
                    required: 'Chọn một Bảng Giá bán hàng'
                },
                approveOrg: {
                    required: 'Chọn một Kho Xuất'
                },
            },
            submitHandler: function (form) {
                let msg = 'Số lượng sẽ không thể thay đổi sau khi duyệt.\nBạn có chắc chắn duyệt đơn hàng này?';
                if (confirm(msg)) {
                    let invId = $('#approveOrderId').val();
                    let shopCode = $('#approveOrderShopCode').val();
                    let status = $('#approveOrderStatus').val();
                    let state = $('#approveOrderState').val();
                    let priceId = $('#selectApprovePrice').val();
                    let approveNote = $('#approveNote').val();
                    let approveOrgCode = $('#selectApproveOrg').val();

                    let data = {
                        id: invId,
                        shopCode: shopCode,
                        status: status,
                        state: state,
                        priceId: priceId,
                        approveNote: approveNote,
                        approveOrgCode: approveOrgCode,
                    }
                    // console.log(data);
                    // return;
                    let url = '<?=site_url('api/business/activeOrder?t=');?>' + getTimeString();
                    $.ajax({
                        url: url,
                        type: 'PUT',
                        data: JSON.stringify(data),
                        success: function (response) {
                            if (response.success) {
                                alert('Gửi đơn hàng thành công.');
                                $('#approveOrderModal').modal('hide');
                                table.ajax.reload();
                            } else {
                                alert(response.message);
                            }
                        }
                    });
                }

            }
        });

        $("#orderDetail").on("click", "tbody tr td.editable", function (e) {
            let headerData = getDataSelectedRowDatatable(table)[0];
            if (headerData.status > 2) {
                $(this).removeClass('editable');
                return;
            }
            if ($(this).find('input').length) return;

            let tr = $(this).closest('tr');
            let row = detailTable.row(tr);
            let rowData = row.data();
            let html = '<input style="height:33px" class="form-control input-text" min="0" onkeypress="validateNumber()" type="number">';
            $(this).html(html);
            $(this).find("input").focus().val(rowData.QTY_APPROVE);

        });

        $('#orderDetail').on('focusout', 'tbody tr td input', async function (e) {
            let headerData = getDataSelectedRowDatatable(table)[0];
            let tr = $(this).closest('tr');
            let row = detailTable.row(tr);
            let rowData = row.data();
            let params = {
                shopCode: headerData.shopCode,
                invId: headerData.invId,
                detailId: rowData.IDL_ID,
                mtlCode: rowData.MTL_CODE,
                mtlConvertCode: rowData.MTL_CONVERT_CODE,
                mtlName: rowData.MTL_NAME,
                unit: rowData.UNIT_CODE,
                price: rowData.PRICE,
                qtyOrder: rowData.QTY_ORDER,
                qtyApprove: parseInt($(this).val()),
                state: headerData.statusString
            };
            if (params.qtyApprove != rowData.QTY_APPROVE) {
                let result = await saveOrderDetail(params);
                if (result.success) {
                    rowData.IDL_ID = result.data.detailId;
                    rowData.QTY_APPROVE = params.qtyApprove;
                    rowData.TOTAL = params.qtyApprove * params.price;
                }
            }
            detailTable.row(row).data(rowData).draw();
            let newTotalAmount = sumByProperty(detailTable.data().toArray().filter(detail => detail.QTY_APPROVE > 0), 'TOTAL')
            headerData.totalAmountApprove = newTotalAmount;
            let trHead = table.row(table.rows({selected: true})[0]);
            table.row(trHead).data(headerData).draw();
        });

        function saveOrderDetail(params) {
            let url = '<?=site_url('api/business/orderDetail?t=');?>' + getTimeString();
            return new Promise((resolve, reject) => {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: params,
                    dataType: 'json',
                    // async:true
                    success: function (response) {
                        resolve(response)
                    },
                    error: function (error) {
                        reject(error)
                    },
                });
            });
        }

        function approveOrder(button) {
            let action = ($(button).hasClass('sendOrder')) ? 'send' : 'delete';
            let status = parseInt($(th).attr('status'));
            // var id = $(th).attr('id');
            // var model = $(th).attr('shop_model');
            let msg = 'Số lượng sẽ không thể thay đổi sau khi duyệt.\nBạn có chắc chắn duyệt đơn hàng này?';
            if (confirm(msg)) {
                $.getJSON("/business/approveOrder", {
                    id: $(th).attr('id'),
                    shop_model: $(th).attr('shop_model'),
                    status: status
                }, function (data, status, xhr) {
                    if (status == 'success') {
                        $('#staticModal').on('show.bs.modal', function (event) {
                            let modal = $(this);
                            modal.find('.modal-body').text(data.dataReturn.message);
                            modal.find('.href').hide();
                        });
                        $('#staticModal').modal('show');
                        if (data.dataReturn.error == false) {
                            if ($(th).parents('tr').hasClass('selected')) {
                                datadetail.clear().draw();
                            }
                            table.row($(th).parents('tr')).remove().draw();
                        }
                    } else if (status == 'timeout') {
                        console.log('connection timeout')
                    } else if (status == "error" || status == "parsererror") {
                        console.log("An error occured");
                    } else {
                        console.log("datatosend did not change");
                    }

                });
            }
        }
        <?php endif;?>




        // function saveGridTableDetail(arr,element,oldValue){
        //     $.ajax({
        //         type: 'POST',
        //         url: base_url + "/business/updateOrderDetail",
        //         data: {
        //             idl_id: arr.idl_id, inv_id: arr.inv_id, code: arr.code, name: arr.name, unit: arr.unit, qty_approve: arr.qty_approve, price: arr.price, type: 1
        //         },
        //         success: function(result){
        //             //var dataJson = $.parseJSON(result);
        //             if(result.dataReturn.error == false){
        //                 $('#updateOrderDetail').trigger("reset");
        //                 $('#modalformeditOrderDetail').modal('hide');
        //                 datadetail.ajax.reload();
        //                     // $(th).closest('tr').attr('qty_delivery', qty_delivery);
        //             }else{
        //                 console.log(result.dataReturn.message);
        //             }
        //         },
        //         error:function(error){
        //             element.text(oldValue);
        //         },
        //         dataType: 'json',
        //         async:true
        //     });
        // }


        /**
         * [detailTableTable]
         * @type {[type]}
         */
        var datarowClick;
        var dataSet = [];
        var idParent;


        $('input[type=checkbox]').on('ifChecked', function (event) {
            detailTable.draw();
            // detailTable.columns.adjust().draw();
            // $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
        });

        $('input[type=checkbox]').on('ifUnchecked', function (event) {
            detailTable.draw();
            // detailTable.columns.adjust().draw();
            // $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
        });


        $("#updateOrderDetail").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    type: "POST",
                    dataType: 'json',
                    data: $(form).serializeArray(),
                    url: $('#updateOrderDetail').attr('action'),
                    success: function (e) {
                        if (typeof e.dataReturn && e.dataReturn.error === true) {
                            $('#staticModal').on('show.bs.modal', function (event) {
                                var modal = $(this);
                                modal.find('.modal-body').text(e.dataReturn.message);
                                modal.find('.href').hide();
                            });
                            $('#staticModal').modal('show');
                            return true;
                        } else {
                            $('#updateOrderDetail').trigger("reset");
                            $('#modalformeditOrderDetail').modal('hide');
                            datadetail.ajax.reload();
                        }
                    },
                    error: function (jqXHR, textStatus) {
                    }
                })
            }
        });

        //Submit Order


        <?php if( check_permission('approve_order:insert')):?>
        $('#btnNewOrder').click(function () {
            var d = new Date();
            $('#selectShopOrder').val(''); // Select the option with a value of '1'
            $('#selectShopOrder').trigger('change'); // Notify any JS components that the value changed
            //$('#address').val('');
            $('#orderCode').val(d.yyyymmdd());
        });

        $('#selectShopOrder').on('change', function (e) {
            let shop = $(this).val();
            $('#selectShopAddress').val(shop);
            $('#selectShopAddress').trigger('change'); // Notify any JS components that the value changed
        });

        $("#formCreateOrder").validate({
            rules: {
                shopCode: {
                    required: true
                },
            },
            messages: {
                shopCode: {
                    required: 'Vui lòng chọn cửa hàng'
                },
            },
            submitHandler: function (form) {
                let shopCode = $('#selectShopOrder').val();
                let orderCode = $('#orderCode').val();
                let orderDate = $('#orderDate').val();
                let orderStatus = $('#orderStatus').val();
                let orderNote = $('#orderNote').val();
                let orderTypeInv = $('#orderTypeInv').val();

                let data = {
                    shopCode: shopCode,
                    orderCode: orderCode,
                    orderDate: orderDate,
                    orderStatus: orderStatus,
                    orderNote: orderNote,
                    orderType: orderTypeInv
                };
                $.post($(form).attr('action'), data, function (data) {
                    if (data.success) {
                        alert('Tạo đơn hàng mới thành công.');
                        $('#orderModal').modal('hide');
                        if (detailTable != null) detailTable.clear().draw();
                        table.ajax.reload();
                    } else {
                        alert(data.message);
                        return false;
                    }
                }, 'json');
            }
        });

        <?php endif;?>

        $('#btnSearch').click(function () {
            detailTable.clear().draw();
            table.ajax.reload();
        });

        Date.prototype.yyyymmdd = function () {
            let mm = this.getMonth() + 1; // getMonth() is zero-based
            let dd = this.getDate();
            let h = this.getHours();
            let m = this.getMinutes();
            let s = this.getSeconds();
            return ['BK', this.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd,
                '_',
                (h > 9 ? '' : '0') + h,
                (m > 9 ? '' : '0') + m,
                (s > 9 ? '' : '0') + s,
                '_OBS'
            ].join('');
        };

    });

    <?php if( $type != 'sended' && check_permission('approve_order:Update')):?>

    function activeOrder(button) {
        let action = 'approve';
        if ($(button).hasClass('return')) {
            action = 'return';
        } else if ($(button).hasClass('delete')) {
            action = 'delete';
        }
        let tr = $(button).closest('tr');
        let row = table.row(tr);
        let rowData = row.data();
        let status = rowData.status;
        let parent = rowData.parentId;
        let invCode = rowData.invCode;
        let shopCode = rowData.shopCode;
        let shopName = rowData.shopName;
        let orderTypeInvName = rowData.orderType;
        let state = rowData.statusString;
        let msg = 'Số lượng sẽ không thể thay đổi sau khi duyệt.\nBạn có chắc chắn duyệt đơn hàng này?';
        if (action == 'return') {
            status = 0;
            msg = 'Bạn có muốn TRẢ đơn hàng ' + invCode + ' \n(' + shopCode + ' - ' + shopName + ')?';
        } else if (action == 'delete') {
            status = 3;
            msg = 'Bạn có muốn HỦY đơn hàng ' + invCode + ' \n(' + shopCode + ' - ' + shopName + ')?';
        } else {
            status = 5;
            if (rowData.priceId == null) {
                if (rowData.shopModel == 'CSH') {
                    $('#selectApprovePrice').val(6822097);
                }
                if (rowData.shopModel == 'NQ') {
                    $('#selectApprovePrice').val(7005097);
                }
            } else {
                $('#selectApprovePrice').val(rowData.priceId);
            }
            // console.log(rowData);
            // Select the option with a value of '1'
            $('#selectApprovePrice').trigger('change'); // Notify any JS components that the value changed
            $('#selectApproveOrg').val(rowData.orgCode);
            $('#selectApproveOrg').trigger('change');
            $('#approveNote').val(rowData.approveNote);
            $('#approveShopName').val(shopName);
            $('#orderTypeInvName').val(orderTypeInvName);
            $('#approveOrderDate').val(formatDate(rowData.invDate));
            $('#approveOrderCode').val(invCode);
            $('#approveOrderId').val(rowData.invId);
            $('#approveOrderShopCode').val(shopCode);
            $('#approveOrderStatus').val(status);
            $('#approveOrderState').val(state);
            $('#approveOrderModal').modal('show');
            return;
        }
        if (confirm(msg)) {
            $.ajax({
                url: '<?=site_url('api/business/activeOrder?t=');?>' + getTimeString(),
                type: 'PUT',
                data: JSON.stringify({id: rowData.invId, shopCode: rowData.shopCode, status: status, state: state}),
                // dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        if ($(tr).hasClass('selected')) {
                            detailTable.clear().draw();
                            $('.checkbox').hide();
                        }
                        row.remove().draw();
                        if (action == 'delete') {
                            alert('Hủy đơn hàng thành công.');
                        } else if (action == 'return') {
                            alert('Trả đơn hàng thành công.');
                        } else {
                            alert('Gửi đơn hàng thành công.');
                        }
                    } else {
                        if (action == 'delete') {
                            alert('Hủy đơn hàng không thành công.');
                        } else if (action == 'return') {
                            alert('Trả đơn hàng không thành công.');
                        } else {
                            alert(response.message);
                        }
                    }
                }
            });
        }
    }
    <?php endif;?>


</script>
<style type="text/css">
    .table td:first-child {
        padding: 1px;
    }

    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) {
        text-align: center;
    }

    .table td:nth-child(9) {
        text-align: right;
    }
</style>
<section class="content">
    <?php //echo '<pre>'; print_r($_SESSION);
//    echo '<pre>';   print_r($this->session->userdata('permissions'));
    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if (check_permission('approve_order:insert')): ?>
                        <button id="btnNewOrder" type="button" class="btn btn-warning" data-toggle="modal"
                                data-target="#orderModal">Thêm đơn hàng
                        </button>
                    <?php endif; ?>
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label"
                                               for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate" style=""'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate" style=""'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("status"); ?></label>
                                        <?php
                                        $stateArray = array(
                                            'All' => 'Tất cả',
                                            'pending' => 'Chờ duyệt',
                                            'approved' => 'Đã duyệt',
                                        );
                                        echo form_dropdown('status', $stateArray, set_value('status', 'pending'), 'id="stateOrder" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= 'Loại hình'; ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="shopModel" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại duyệt</label>
                                        <?php
                                        $typeApprovedArray = array('Ngày đặt đơn', 'Ngày duyệt đơn');
                                        echo form_dropdown('typeFilter', $typeApprovedArray, set_value('typeFilter', $typeFilter), 'id="typeFilter" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <?php if(!empty($orderTypeInv)):?>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại đơn hàng</label>
                                        <?php
                                        $a = array('All' => 'Tất cả');
                                        $b = array_replace($a, $orderTypeInv);

                                        echo form_dropdown('orderType', $b, set_value('orderType'), 'id="orderType" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <?php endif;?>
                                <div class="col-sm-12">
                                    <button id="btnSearch"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="orderTable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr class="active">
                                        <?php if (check_permission('approve_order:Update') || check_permission('approve_order:Delete')): ?>
                                            <th>Thao tác</th>
                                        <?php endif; ?>
                                        <th>Mã</th>
                                        <th><?= lang('name_store') ?></th>
                                        <th><?= lang("model"); ?></th>
                                        <th>Loại</th>
                                        <th>Loại đơn hàng</th>
                                        <th>Ngày đặt</th>
                                        <th>Ngày duyệt</th>
                                        <th><?= lang("order_code"); ?></th>
                                        <th><?= lang("note"); ?></th>
                                        <th>Thành tiền(*)</th>
                                        <th>Tổng duyệt</th>
                                        <th>Trạng thái</th>
                                        <th>TT Phân phối</th>
                                        <th>Nhà phân phối</th>
                                        <th>Kho</th>
                                        <th>Bảng giá</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="13"
                                            class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dữ liệu chi tiết</h3>
                    <div class="box-tools">
                        <div class="form-group">
                            <div class="checkbox" style="display:none;">
                                <label>
                                    <input id="allItem" name="check_all" class="check_all" type="checkbox">
                                    Tất cả
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table id="orderDetail" class="table table-striped table-bordered pendingOrder"
                           style="width:100%">
                        <thead>
                        <tr class="active">
                            <th>Mã sản phẩm</th>
                            <th>Tên sản phẩm</th>
                            <th>ĐVT</th>
                            <th>Số lượng đặt</th>
                            <th>Số lượng duyệt</th>
                            <th>Đơn giá</th>
                            <th>Thành tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalformeditOrderDetail" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Cập nhật</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-warning">
                            <div class="box-header">
                            </div>
                            <div class="box-body">
                                <div class="col-lg-12">
                                    <?= form_open_multipart("business/updateOrderDetail", 'id="updateOrderDetail"'); ?>
                                    <input type="hidden" name="idl_id" id="idl_id" value="">
                                    <input type="hidden" name="inv_id" id="inv_id" value="">
                                    <input type="hidden" name="type" id="type" value="">
                                    <div class="row">
                                        <p class="text-red show-error"></p>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Mã sản phẩm
                                                <?= form_input('code', set_value('code'), 'class="form-control" id="code"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Tên sản phẩm
                                                <?= form_input('name', set_value('name'), 'class="form-control" id="name" '); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Đơn vị tính
                                                <?= form_input('unit', set_value('unit'), 'class="form-control" id="unit"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Số lượng đặt
                                                <?= form_input('qty_order', set_value('qty_order'), 'class="form-control" id="qty_order" '); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Số lượng duyệt
                                                <?= form_input('qty_approve', set_value('qty_approve'), 'class="form-control" id="qty_approve" '); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Đơn giá
                                                <?= form_input('price', set_value('price'), 'class="form-control" id="price" '); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal"><?= lang('cancel') ?></button>
                                        <?= form_submit('add', lang('update'), 'class="btn btn-warning"'); ?>
                                    </div>
                                    <?= form_close(); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (check_permission('approve_order:insert')): ?>
    <div class="modal fade" id="orderModal" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <?= form_open("api/business/createOrder", array('method' => 'post', 'id' => 'formCreateOrder')); ?>
                <div class="modal-header">
                    <h3 class="modal-title">Thêm đơn hàng</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        unset($shopList['All']);
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode'), 'id="selectShopOrder" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control select2" style="width:100%"');
                                        ?>
                                    </div>
                                    <?php if(!empty($orderTypeInv)):?>
                                    <div class="form-group">
                                        <label class="control-label" for="orderType">Loại đơn hàng</label>
                                        <?php
                                        echo form_dropdown('orderTypeInv', $orderTypeInv, set_value('orderTypeInv'), 'id="orderTypeInv" data-placeholder="Chọn loại đơn hàng" class="form-control select2" style="width:100%"');
                                        ?>
                                    </div>
                                    <?php endif;?>
                                    <div class="form-group">
                                        <label class="control-label" for="address"><?= lang("address"); ?></label>
                                        <?php
                                        echo form_dropdown('shopAddress', $shopAddress, set_value('shopAddress'), 'id="selectShopAddress" class="form-control select2" disabled style="width:100%"');
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="orderCode">Mã đơn hàng</label>
                                        <?= form_input('orderCode', set_value('orderCode', ''), 'class="form-control" id="orderCode" disabled '); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="date">Ngày</label>
                                        <?= form_input('orderDate', set_value('date', date('d-m-Y')), 'class="form-control datepicker" id="orderDate" disabled '); ?>
                                    </div>
                                    <div class="form-group hidden">
                                        <label class="control-label" for="status">Status</label>
                                        <?= form_input('orderStatus', set_value('status', 1), 'class="form-control" id="orderStatus" disabled '); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Ghi chú</label>
                                        <?= form_textarea('orderNote', set_value('note'), 'id="orderNote" class="form-control" ') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-warning">Lưu</button>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
<?php endif; ?>

<?php if (check_permission('approve_order:update')): ?>
    <div class="modal fade" id="approveOrderModal" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <?= form_open("api/business/createOrder", array('method' => 'post', 'id' => 'frmApproveOrder')); ?>
                <div class="modal-header">
                    <h3 class="modal-title">Duyệt đơn hàng</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Bảng giá</label>
                                        <?php
                                        echo form_dropdown('approvePrice', $erpPrices, set_value('approvePrice'), 'id="selectApprovePrice" data-placeholder="Chọn bảng giá" class="form-control select2" style="width:100%"');
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="code">Kho</label>
                                        <?php
                                        echo form_dropdown('approveOrg', $erpOrg, set_value('approveOrg'), 'id="selectApproveOrg" data-placeholder="Chọn kho xuất" class="form-control select2" style="width:100%"');
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="orderType">Loại đơn hàng</label>
                                        <?= form_input('orderTypeInvName', set_value('orderTypeInvName', ''), 'class="form-control" id="orderTypeInvName" disabled '); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="address">Cửa hàng</label>
                                        <?= form_input('approveShopName', set_value('approveShopName', ''), 'class="form-control" id="approveShopName" disabled '); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="orderCode">Mã đơn hàng</label>
                                        <?= form_input('approveOrderCode', set_value('approveOrderCode', ''), 'class="form-control" id="approveOrderCode" disabled '); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="date">Ngày đặt</label>
                                        <?= form_input('approveOrderDate', set_value('approveOrderDate', ''), 'class="form-control" id="approveOrderDate" disabled '); ?>
                                    </div>
                                    <div class="form-group hidden">
                                        <input class="form-control hidden" id="approveOrderId" disabled>
                                        <input class="form-control hidden" id="approveOrderShopCode" disabled>
                                        <input class="form-control hidden" id="approveOrderStatus" disabled>
                                        <input class="form-control hidden" id="approveOrderState" disabled>
                                        <!--                                        <input class="form-control hidden" id="approveOrderId" disabled>-->
                                        <!--                                        <input class="form-control hidden" id="approveOrderStatus" value="5" disabled>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Ghi chú</label>
                                        <?= form_textarea('approveNote', set_value('approveNote'), 'id="approveNote" class="form-control" style="max-width: 100%;min-width: 100%"') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-success">Duyệt đơn</button>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
<?php endif; ?>

<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>



<link rel="stylesheet" href="<?= $assets ?>plugins/bootstrap-long-multi-step-form/assets/css/style.css">
<link rel="stylesheet" href="<?= $assets ?>plugins/bootstrap-long-multi-step-form/assets/css/media-queries.css">

<script src="<?= $assets ?>plugins/bootstrap-long-multi-step-form/assets/js/jquery.backstretch.min.js"></script>
<script src="<?= $assets ?>plugins/bootstrap-long-multi-step-form/assets/js/scripts.js"></script>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?php if(isset($stores)): ?>
            <div class="box box-warning">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="col-lg-12">
                        <?= form_open_multipart("business/submit_qc", 'id="qc"');?>
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                    <?php
                                    echo form_dropdown('shop_id', $stores, set_value('shop_id', $dataShopFormulaQC['SHOP_ID']), 'id="shop_id" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control select2" ');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="start_date"><?= lang("start_date"); ?></label>
                                    <?= form_input('start_date', set_value('start_date', ($dataShopFormulaQC['START_TIME'] != '')?date('d-m-Y', strtotime($dataShopFormulaQC['START_TIME'])):'' ), 'class="form-control datepicker" id="start_date" ');?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="end_date"><?= lang("end_date"); ?></label>
                                    <?= form_input('end_date', set_value('end_date', ($dataShopFormulaQC['END_TIME'] != '')?date('d-m-Y', strtotime($dataShopFormulaQC['END_TIME'])):'' ), 'class="form-control datepicker" id="end_date" ');?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="code"><?= lang("template_formula"); ?></label>
                                    <?php
                                    echo form_dropdown('formula_id', $formula, set_value('formula_id', $dataShopFormulaQC['FORMULA_ID']), 'id="formula_id" data-placeholder="' . lang("select") . ' ' . lang("template_formula") . '" class="form-control select2" ');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label>Ghi chú</label>
                                    <?=form_textarea('note', set_value('note', $dataShopFormulaQC['NOTE']), 'id="note" class="form-control" ')?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Người thực hiện</label>
                                    <?= form_input('review_name', set_value('review_name', $dataShopFormulaQC['REVIEW_NAME']), 'class="form-control" id="review_name" '); ?>
                                </div>
                                <div class="form-group">
                                    <label>Ngày thực hiện</label>
                                    <?= form_input('review_date', set_value('review_date', ($dataShopFormulaQC['REVIEW_DATE'] != '')?date('d-m-Y', strtotime($dataShopFormulaQC['REVIEW_DATE'])):'' ), 'class="form-control datepicker" id="review_date"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php if( check_permission( 'qc:Update' )):?>
                            <?= form_submit('edit_shop', lang('edit'), 'class="btn btn-warning"'); ?>
                            <?php endif?>
                            <a href="<?=site_url('business/qc')?>" class="btn btn-default">Danh sách</a>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-warning show-error" style="display:none;"></div>
                            </div>
                        </div>
                        <input type="hidden" name="formtype" value="edit">
                        <input type="hidden" name="id" value="<?=$dataShopFormulaQC['SHOP_ID']?>">
                        <?= form_close();?>
                    </div>
                    <div class="clearfix"></div>
                    
                </div>
            </div>
            <?php else:?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        // $(".image_qc").filestyle('disabled', true);
                    })
                </script>
                <div class="box box-warning">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="row invoice-info">
                            <div class="col-sm-6 invoice-col">
                              <h2>Bảng đánh giá cửa hàng</h2>
                              <address>
                                <strong><?=$dataShopFormulaQC['SHOP_NAME']?></strong><br>
                                <b>Địa chỉ:</b> <?=$dataShopFormulaQC['ADDRESS'].' - '.$dataShopFormulaQC['WARD_NAME'].' - '. $dataShopFormulaQC['DISTRICT_NAME'].' - '. $dataShopFormulaQC['PROVINCE_NAME']?><br>
                                <b>Lần đánh giá:</b> <?=@$dataShopFormulaQC['VERSION']?><br>

                                <b>Bắt đầu:</b> <?= ($dataShopFormulaQC['START_TIME'] != '')?date('d-m-Y', strtotime($dataShopFormulaQC['START_TIME'])):'' ?><br>
                                <b>Kết thúc:</b> <?= ($dataShopFormulaQC['END_TIME'] != '')?date('d-m-Y', strtotime($dataShopFormulaQC['END_TIME'])):'' ?><br>
                              </address>
                            </div>
                            <div class="col-sm-6 invoice-col">
                              <h3>Ngày thực hiện: <?php if(isset($dataShopFormulaQC['REVIEW_DATE'])):?> <?=date('d-m-Y', strtotime($dataShopFormulaQC['REVIEW_DATE']))?><?php endif;?></h3>
                              <b>Người thực hiện:</b> <?=@$dataShopFormulaQC['REVIEW_NAME']?><br>
                              <b>Mục đích:</b> <?=@$dataShopFormulaQC['NOTE']?><br>
                              <b>Xem xét:</b> <br>
                              <b>Điểm đánh giá:</b> <?=!empty($dataShopFormulaQC['POINT'])?$dataShopFormulaQC['POINT'].' / '.$dataShopFormulaQC['MAX_POINT']:''?><br>
                              <b>Phần trăm điểm số:</b> <?=!empty($dataShopFormulaQC['PERCENT'])?$dataShopFormulaQC['PERCENT']:''?><br>
                            </div>
                        </div>

                        <div style="margin: 0 15px" class="row">
                            <?php if(!empty($dataShopFormulaQC['CATEs'])):?>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <?php foreach ($dataShopFormulaQC['CATEs'] as $key => $dataCate):?>
                                    <?php
                                        $in =  '';
                                        $class = 'collapsed';
                                        $aria_expanded = 'false';
                                        if($key == 0){
                                            $in = 'in';
                                            $class = '';
                                            $aria_expanded = 'true';
                                        }
                                    ?>
                                    <div class="panel panel-default">
                                        <a class="<?=$class?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#<?=$dataCate['CATE_CODE']?>" aria-expanded="<?=$aria_expanded?>" aria-controls="<?=$dataCate['CATE_CODE']?>">
                                            <div class="panel-heading <?=$dataCate['CATE_CODE']?>" role="tab">
                                                <h4 class="panel-title">
                                                <?=$dataCate['CATE_NAME']?>
                                                </h4>
                                                <p>Điểm chuẩn: <span class="max_point_cate"><?=$dataCate['MAX_POINT']?></span></p>
                                                <p>Đánh giá: <span valpoint_cate="<?=$dataCate['POINT']?>" class="point_cate"><?=$dataCate['POINT']?></span></p>
                                            </div>
                                        </a>
                                        <div id="<?=$dataCate['CATE_CODE']?>" class="panel-collapse collapse <?=$in?>" role="tabpanel">
                                            <div class="panel-body">
                                                <div class="msf-container">
                                                    <div class="row">
                                                        <div class="col-sm-12 msf-form">

                                                            <ul class="list-inline qc_list_step_by_step">
                                                                <?php if(!empty($dataCate['DETAILs'])):?>
                                                                <?php $count = count($dataCate['DETAILs']); foreach ($dataCate['DETAILs'] as $key2 => $dataQC):?>
                                                                <li><a data-toggle="tooltip" title="<?=$dataQC['ITEM_NAME']?>" position="<?=$dataCate['CATE_CODE']?>" class="btn btn-default btn-step_by_step" idx="<?=$dataQC['ROW_ID']?>" href="javascript:void(0)"><?=$key2+1?></a></li>
                                                                <?php endforeach;?>
                                                                <?php endif;?>
                                                            </ul>
                                                            <form action="" enctype="multipart/form-data" method="post" class="form-horizontal qc">
                                                                
                                                                <?php if(!empty($dataCate['DETAILs'])):?>
                                                                <?php $count = count($dataCate['DETAILs']); foreach ($dataCate['DETAILs'] as $key2 => $dataQC):?>
                                                                    <fieldset id="idx<?=$dataQC['ROW_ID']?>">
                                                                        <div class="col-sm-12 msf-title">
                                                                            <p>
                                                                                <?=$dataQC['ITEM_NAME']?>
                                                                            </p>
                                                                        </div>
                                                                        <h4><span class="step">(Tiêu chí <?=$key2 + 1?> / <?=$count?>)</span></h4>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Không đánh giá</label>
                                                                            <div class="col-sm-2">
                                                                              <input type="checkbox" <?=($dataQC['NON_SCORE'])?"checked":""?> name="non_score" class="form-control non_score<?=$dataQC['ROW_ID']?>" value="1">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Điểm chuẩn</label>
                                                                            <div class="col-sm-2">
                                                                              <input type="number" disabled value="<?=$dataQC['MAX_POINT']?>"class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Đánh giá</label>
                                                                            <div class="col-sm-2">
                                                                                <input type="number" min="0" max="<?=$dataQC['MAX_POINT']?>" position="<?=$dataCate['CATE_CODE']?>" valold="<?=$dataQC['POINT']?>" class="form-control input_point input_point<?=$dataQC['ROW_ID']?>" value="<?=$dataQC['POINT']?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Diễn giải</label>
                                                                            <div class="col-sm-6">
                                                                                <textarea rows="5" cols="25" class="form-control input_note input_note<?=$dataQC['ROW_ID']?>"><?=$dataQC['NOTE']?></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <!-- <div class="form-group">
                                                                            <label class="col-sm-2 control-label">Hình ảnh</label>
                                                                            <div class="col-sm-6">
                                                                                <input type="hidden" value="<?=$dataQC['IMAGES']?>" class="nameimage_qc<?=$dataQC['ROW_ID']?>">
                                                                                <input type="file" multiple  class="image_qc image_qc<?=$dataQC['ROW_ID']?>">
                                                                                <p id="message"></p>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <img data-action="zoom" class="img-fluid demo_image_qc demo_image_qc<?=$dataQC['ROW_ID']?>" style="height: 110px" src="<?=base_url().URL_QC.$dataQC['IMAGES']?>" />
                                                                            </div>
                                                                            <p id="message"></p>
                                                                        </div> -->

                                                                        <div class="form-group formuploadimg_post">
                                                                            <label class="col-sm-2 control-label">Hình ảnh</label>
                                                                            <div class="col-sm-6">
                                                                                <input id="files<?=$dataQC['ROW_ID']?>" qcid="<?=$dataQC['ROW_ID']?>" countimg='<?= (!empty($dataQC['IMAGES']))?@count(explode("#", $dataQC['IMAGES'])):0?>' type="file" multiple class="image_qc form-control-file">
                                                                                <p style="color: red;">Số lượng tối đa là 5 tấm ảnh</p>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="alert alert-danger" style="display: none;" id="message<?=$dataQC['ROW_ID']?>" role="alert">
                                                                                    This is a danger alert—check it out!
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div id="uploaded_images<?=$dataQC['ROW_ID']?>">
                                                                            <?php if( !empty($dataQC['IMAGES']) && $dataQC['IMAGES'] != 'null') :
                                                                                $image = explode("#", $dataQC['IMAGES']);
                                                                                if(is_array($image)):
                                                                                $dataImage = array(
                                                                                    'image_current_qc'.$dataQC['ROW_ID'] => $image
                                                                                );
                                                                                $this->session->set_userdata($dataImage);

                                                                                foreach($image as $k => $v):
                                                                                ?>
                                                                                <div class="col-sm-2">
                                                                                    <a style="float: right;" qcid="<?=$dataQC['ROW_ID']?>" onclick="del_image_qc(this)" name_image="<?=$v?>" class="del_image" title="Xóa">x</a>
                                                                                    <img src="<?= site_url().URL_QC.$v?>" class="img-responsive img-thumbnail" data-action="zoom">
                                                                                </div>
                                                                            <?php endforeach;?>
                                                                            <?php endif; endif;?>
                                                                            <?php
                                                                                if($this->session->has_userdata('imagenew_qc'.$dataQC['ROW_ID'])){
                                                                                    foreach ($this->session->userdata('imagenew_qc'.$dataQC['ROW_ID']) as $key => $value) {
                                                                                        @unlink(QC.$value);
                                                                                    }
                                                                                    $this->session->unset_userdata('imagenew_qc'.$dataQC['ROW_ID']);
                                                                                }
                                                                            ?>
                                                                            </div>
                                                                        </div>


                                                                        <br>
                                                                        <?php if( check_permission( 'qc:Update' )):?>
                                                                        <button type="button" position="<?=$dataCate['CATE_CODE']?>" row_id="<?=$dataQC['ROW_ID']?>" shop_formula_qc_id="<?=$dataQC['SHOP_FORMULA_QC_ID']?>" item_qc_id="<?=$dataQC['ITEM_QC_ID']?>" class="btn btn-primary btn-sm btn_edit">Cập nhật</button>
                                                                        <?php if($key2 != 0):?>
                                                                        <button type="button" position="<?=$dataCate['CATE_CODE']?>" class="btn btn-previous btn-warning btn-sm"><i class="fa fa-angle-left"></i> Trước</button>
                                                                        <?php endif?>
                                                                        <?php if($count != $key2 + 1):?>
                                                                        <button type="button" position="<?=$dataCate['CATE_CODE']?>" class="btn btn-next btn-warning btn-sm">Sau <i class="fa fa-angle-right"></i></button>
                                                                        <?php endif?>
                                                                        <?php endif?>
                                                                    </fieldset>
                                                                <?php endforeach;?>
                                                                    <input type="hidden" id="formtype" name="formtype" value="review">
                                                                    <input type="hidden" name="id" value="<?=$dataShopFormulaQC['SHOP_ID']?>">
                                                                <?php endif;?>
                                                                
                                                                
                                                            </form>

                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach;?>
                                </div>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>

</section>

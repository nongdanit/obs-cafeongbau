<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="col-lg-12">
                        <?= form_open_multipart("business/submit_qc", 'id="qc"');?>
                        <input type="hidden" name="formtype" value="create">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                    <?php
                                    echo form_dropdown('shop_id', $stores, set_value('shop_id'), 'id="shop_id" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control select2" ');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="start_date"><?= lang("start_date"); ?></label>
                                    <?= form_input('start_date', set_value('start_date'), 'class="form-control datepicker" id="start_date" ');?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="end_date"><?= lang("end_date"); ?></label>
                                    <?= form_input('end_date', set_value('end_date'), 'class="form-control datepicker" id="end_date" ');?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="code"><?= lang("template_formula"); ?></label>
                                    <?php
                                    echo form_dropdown('formula_id', $formula, set_value('formula_id'), 'id="formula_id" data-placeholder="' . lang("select") . ' ' . lang("template_formula") . '" class="form-control select2" ');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label>Ghi chú</label>
                                    <?=form_textarea('note', set_value('note'), 'id="note" class="form-control" ')?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Người thực hiện</label>
                                    <?= form_input('review_name', set_value('review_name'), 'class="form-control" id="review_name" '); ?>
                                </div>
                                <div class="form-group">
                                    <label>Ngày thực hiện</label>
                                    <?= form_input('review_date', set_value('review_date'), 'class="form-control datepicker" id="review_date"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?= form_submit('add_shop', lang('add'), 'class="btn btn-warning"'); ?>
                            <a href="<?=site_url('business/qc')?>" class="btn btn-default">Danh sách</a>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-warning show-error" style="display:none;"></div>
                            </div>
                        </div>
                        <?= form_close();?>
                    </div>
                    <div class="clearfix"></div>
                    <!-- <hr>
                    <div class="col-lg-12"><span class="label label-danger">(*) bắt buộc, không được bỏ trống</span></p></div> -->
                </div>
            </div>
        </div>
    </div>
</section>

<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    #UTable thead tr th{white-space: nowrap!important;}
    #UTable tbody tr td{white-space: normal!important;}
</style>
<?php
    $v = "?v=1";
    if ($this->input->get('shop_code')) {
        $v .= "&shop_code=".$this->input->get('shop_code');
    }
    if ($this->input->get('shoptype')) {
        $v .= "&shoptype=".$this->input->get('shoptype');
    }
    if ($this->input->get('province_code')) {
        $v .= "&province_code=".$this->input->get('province_code');
    }
    if ($this->input->get('district_code')) {
        $v .= "&district_code=".$this->input->get('district_code');
    }
?>
<script type="text/javascript">
    function pstatus(x) {
        if (x == 0) {
            return '<span class="label label-danger">Hủy</span>';
        } else if (x == 1) {
            return '<span class="label label-primary"><?= lang('active'); ?></span>';
        }else if (x == 5) {
            return '<span class="label label-info">Đang thực hiện</span>';
        } else if (x == 9) {
            return '<span class="label label-success">Hoàn thành</span>';
        }
    }
    $(document).ready(function() {
        var table = $('#UTable').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            ajax : {
                url: '<?=site_url('business/get_qc'. $v);?>',
                type: 'POST',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission( 'qc:Export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 9] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
                <?php endif?>
            ],
            columns: [
                { "data": "SHOP_CODE"},
                { "data": "SHOP_NAME"},
                { "data": "SHOP_TYPE"},
                { "data": null, "render": function ( data, type, row ) {
                        var t = row['ADDRESS'];
                            t += (row['WARD_NAME'] != '')?', ' +row['WARD_NAME']:'';
                            t += (row['DISTRICT_NAME'] != '')?', ' +row['DISTRICT_NAME']:'';
                            t += (row['PROVINCE_NAME'] != '')?', ' +row['PROVINCE_NAME']:'';
                        return t;

                    }
                },
                { "data": "FORMULA_NAME"},
                { "data": "REVIEW_NAME"},
                { "data": "STATUS", "orderable": false, render: pstatus},
                { "data": "PROVINCE_CODE", "visible": false},
                { "data": "DISTRICT_CODE", "visible": false},
                { "data": null, "render": function ( data, type, row ) {
                        return row['POINT'] +'/'+ row['MAX_POINT'];
                    }
                },
                { "data": "PERCENT"},
                { "data": null, "searchable": false, "orderable": false, "render" : function ( data, type, row ) {
                        return '<div class="text-center"><div class="btn-group"><a class="btn btn-success btn-xs" title="<?=lang('update')?>" href="<?php echo base_url('business/edit_qc/') ?>'+ row['ROW_ID']+'" ><i class="fa fa-edit"></i></a><a class="btn btn-warning btn-xs" title="<?=lang('update')?>" href="<?php echo base_url('business/review_qc/') ?>'+ row['ROW_ID']+'" ><i class="fa fa-star"></i></a></div></div>';
                    }
                }
            ]
        });

        <?php if ($this->input->get('shoptype') && $this->input->get('shoptype') != 'All' ):?>
            var val = '<?php echo $this->input->get('shoptype');?>'
            table.columns( 2 ).search( val ).draw();
        <?php endif;?>
        <?php if ($this->input->get('shop_code') && $this->input->get('shop_code') != 'HEAD_OFFICE' ):?>
            var val = '<?php echo $this->input->get('shop_code');?>'
            table.columns( 0 ).search( val ).draw();
        <?php endif;?>
        <?php if ($this->input->get('province_code') && $this->input->get('province_code') != 'All' ):?>
            var val = '<?php echo $this->input->get('province_code');?>'
            table.columns( 7 ).search( val ).draw();
        <?php endif;?>
        <?php if ($this->input->get('district_code') && $this->input->get('district_code') != 'All' ):?>
            var val = '<?php echo $this->input->get('district_code');?>'
            table.columns( 8 ).search( val ).draw();
        <?php endif;?>

        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <?php if( check_permission( 'qc:insert' )):?>
                    <a href="<?=site_url('business/create_qc')?>" class="btn btn-warning">Thêm mới</a>
                    <?php endif;?>
                </div>
                <div class="box-body">  
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <?= form_open("business/qc", array('method'=>'get','id' => 'myform'));?>
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shop_code', $shopList, set_value('shop_code', $shop_code), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('type')?></label>
                                        <?php
                                        echo form_dropdown('shoptype', $shopTypeList, set_value('shoptype', $shop_type), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?= form_dropdown('province_code', $provinceList, set_value('province_code', $province_code), 'class="form-control select2" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('district_code', $districtList, set_value('district_code', $district_code), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                    <a href="<?= base_url('business/qc')?>" class="btn btn btn-default"><?= lang("reset"); ?></a>
                                </div>
                            </div>
                            <?= form_close();?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr class="active">
                                        <th><?=lang("stores_code"); ?></th>
                                        <th><?=lang("name_store"); ?></th>
                                        <th><?=lang('type')?></th>
                                        <th><?=lang("address"); ?></th>
                                        <th>Mẫu đánh giá</th>
                                        <th>Người đánh giá</th>
                                        <th><?=lang('status'); ?></th>
                                        <th></th>
                                        <th></th>
                                        <th>Điểm</th>
                                        <th>%</th>
                                        <th><?=lang('actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

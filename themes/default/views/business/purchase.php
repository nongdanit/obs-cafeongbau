<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }

    .text-wrap {
        white-space: normal;
    }

    .modal-body {
        max-height: calc(100vh - 212px);
        overflow-y: auto;
    }

    .modal-lg {
        width: 85%;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    .select2-container--default .select2-results__option[aria-disabled=true] {
        display: none;
    }

</style>
<script type="text/javascript">
    var purchaseTable, purchaseDetailTable, ticketDetailTableEdit, selectedEditRow;
    var baseUrl = '<?=site_url('');?>';
    var purchaseExportBaseApi = baseUrl + 'api/business/purchaseExport';
    var fromDate = '<?php echo date("d-m-Y") ?>';
    var toDate = '<?php echo date("d-m-Y") ?>';
    <?php if(check_permission('purchase:insert')): ?>
    function saveStock(status = 0) {
        let url = '<?=site_url('api/business/purchaseCreate');?>';
        let toShop = $("#toShop option:selected").text();
        // console.log(ticketDetailTableEdit.data());
        let ticketDetails = ticketDetailTableEdit.data().toArray().map(detail => {
            let containerDetail = {};
            containerDetail.mtlCode = detail.mtlCode;
            containerDetail.mtlName = detail.mtlName;
            containerDetail.unitCode = detail.unitEven;
            // containerDetail.quantity = detail.qtyEven;
            containerDetail.quantity = detail.qtyEven * detail.spec + detail.qtyOdd;
            containerDetail.price = detail.price;
            // unitCode đổi lại truyên đơn lẻ unitOdd
            containerDetail.detailId = detail.detailId;
            containerDetail.invId = detail.invId;
            return containerDetail;
        }).filter(detail => detail.quantity > 0 || detail.detailId > 0);

        let purchaseData = {
            ticketId: $("#ticketId").text(),
            toShop: $("#toShop").val(),
            ticketDate: $("#ticketDate").val(),
            ticketType: "MN",
            ticketStatus: status,
            ticketNote: $("#ticketNote").val(),
            ticketCode: $("#ticketCode").val(),
            ticketDetails: ticketDetails
        }
        // console.log(purchaseData);return;
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(purchaseData),
            success: function (response) {
                console.log(response);
                if (response.success) {
                    if (status == 0) {
                        alert('Lưu phiếu thành công.')
                    } else {
                        alert('Lưu và hoàn tất phiếu thành công.')
                    }
                    if (purchaseData.ticketId == '') {
                        let data = [];
                        data.shopName   = toShop
                        data.invCode    = purchaseData.ticketCode
                        data.status     = status
                        data.invDate    = purchaseData.ticketDate
                        data.note       = purchaseData.ticketNote
                        data.shopCode   = purchaseData.toShop
                        // data.details    = purchaseData.ticketDetails
                        data.id         = response.data
                        purchaseTable.row.add(data).draw(false);
                    } else {
                        purchaseDetailTable.clear().draw();
                        purchaseTable.ajax.reload();
                    }
                    $('#ticketDetailModel').modal('hide');
                } else {
                    alert(response.message);
                }
            }
        });
    }
    <?php endif;?>

    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        $('#ticketDetailModel').css('z-index', '-1');
        $('#ticketDetailModel').modal('show');
        var slideInterval = setInterval(function () {
            $("#ticketDetailModel").modal('hide');
        }, 1);

        initpurchaseTable();
        initpurchaseDetailTable();

        purchaseTable.on('user-select', function (e, dt, type, cell, originalEvent) {
            if (cell.index().column == 0) return false;
            let dataRow;
            let row = dt.row(cell.index().row).node();
            $(row).removeClass('hover');
            $('#inventoryDetail').modal('show');
            if ($(row).hasClass('selected')) {
                e.preventDefault();
                // deselect
            } else {
                dataRow = purchaseTable.row(row).data();
                // console.log(dataRow)
                if (dataRow.details == null) {
                    purchaseDetailTable.destroy();
                    purchaseDetailTable = $('#purchaseDetailTable').DataTable({
                        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        order: [],
                        scrollX: true,
                        scrollY: 400,
                        deferRender: true,
                        scrollCollapse: true,
                        pageLength: -1,
                        ajax: {
                            url: '<?=site_url('api/business/purchaseDetail');?>' + '?id=' + dataRow.id + '&shopCode=' + dataRow.shopCode,
                            type: 'GET',
                            data: function (d) {
                                d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                            },
                            dataSrc: function (dataJson) {
                                console.log(dataJson)
                                dataRow.details = dataJson.data;
                                return dataJson.data.filter((detail) => detail.detailId > 0);
                            }
                        },
                        oLanguage: {
                            "sEmptyTable": "Không có dữ liệu",
                            "sSearch": "Tìm kiếm nhanh:",
                            "sLengthMenu": "Hiển thị _MENU_ dòng",
                            "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                            "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                        },
                        dom: 'lBfrtip',
                        buttons: [
                            <?php if( check_permission('purchase:export')):?>
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                                className: 'btn btn-warning',
                                title: 'Dữ liệu chi tiết phiếu - '+ dataRow.invCode,
                                footer: false,
                                init: function (api, node, config) {
                                    $(node).removeClass('btn-default')
                                }
                            },
                            <?php endif;?>
                        ],
                        fixedColumns: {
                            leftColumns: 2
                        },
                        columns: [
                            {data: "mtlCode"},
                            {data: "mtlName"},
                            {data: "spec", width: 130, render: currencyFormat, className: "dt-body-right"},
                            {data: "qtyEven", width: 130, sortable: true, className: "dt-body-right"},
                            {data: "unitEven", width: 130, className: "dt-body-left"},
                            {data: "qtyOdd", width: 130, sortable: true, className: "dt-body-right"},
                            {data: "unitOdd", width: 130, className: "dt-body-left"},
                            {data: "price", width: 130, sortable: true, className: "dt-body-right", render: formatIntegerSeparator},
                        ],
                    });
                } else {
                    purchaseDetailTable.clear().draw();
                    purchaseDetailTable.rows.add(dataRow.details.filter((detail) => detail.detailId > 0)); // Add new data
                    purchaseDetailTable.columns.adjust().draw();
                }
            }
        });
        $('#purchaseTable').on('click', 'button', function () {
            if (purchaseTable != null) {
                let row = $(this).closest('tr');
                let selectedData = purchaseTable.row(row).data();
                // console.log(selectedData);
                if ($(this).hasClass('update')) {
                    $('#ticketId').text(selectedData.id);
                    $("#ticketDate").val(hrsd(selectedData.invDate));
                    $("#ticketCode").val(selectedData.invCode);
                    $("#ticketNote").val(selectedData.note);
                    $('#toShop').val(selectedData.shopCode);
                    $('#toShop').trigger('change');
                    $('#toShop').prop('disabled', true);
                    $('#ticketDetailModel').modal('show');
                    if (selectedData.details == null) {
                        $.ajax({
                            url: '<?=site_url('api/business/purchaseDetail');?>' + '?id=' + selectedData.id + '&shopCode=' + selectedData.shopCode,
                            type: 'GET',
                            dataType: 'json',
                            success: function (dataJson) {
                                console.log(dataJson.data);
                                //selectedData.details = dataJson.data;
                                ticketDetailTableEdit.clear().draw();
                                ticketDetailTableEdit.rows.add(dataJson.data); // Add new data
                                ticketDetailTableEdit.columns.adjust().draw();
                            }
                        });
                    } else {
                        ticketDetailTableEdit.clear();
                        ticketDetailTableEdit.rows.add(selectedData.details); // Add new data
                        ticketDetailTableEdit.columns.adjust().draw();
                        ticketDetailTableEdit.draw();
                    }

                    inventorySaveToogle();
                } else if( $(this).hasClass('delete') ) {
                    let invCode     = selectedData.invCode;
                    let shopCode    = selectedData.shopCode;
                    let shopName    = selectedData.shopName;
                    let invId       = selectedData.id;
                    let msg         = 'Bạn có muốn HỦY phiếu ' + invCode + ' \n(' + shopCode + ' - ' + shopName + ')?';
                    if (confirm(msg)) {
                        $.ajax({
                            url: '<?=site_url('api/business/updateStatusPurchase?t=');?>' + getTimeString(),
                            type: 'PUT',
                            data: JSON.stringify({id: invId, status: 3}),
                            success: function (response) {
                                if (response.success) {
                                    alert('Đã HỦY đơn thành công!');
                                    if (row.hasClass('selected')) {
                                        purchaseDetailTable.clear().draw();
                                    }
                                    purchaseTable.row(row).remove().draw();
                                } else {
                                    console.log("An error occured");
                                }
                            }
                        });

                    }
                } else if ( $(this).hasClass('approve') ) {
                    console.log('action approve');
                    let invCode     = selectedData.invCode;
                    let shopCode    = selectedData.shopCode;
                    let shopName    = selectedData.shopName;
                    let invId       = selectedData.id;
                    let msg         = 'Bạn có muốn DUYỆT phiếu ' + invCode + ' \n(' + shopCode + ' - ' + shopName + ')?';
                    if (confirm(msg)) {
                        $.ajax({
                            url: '<?=site_url('api/business/updateStatusPurchase?t=');?>' + getTimeString(),
                            type: 'PUT',
                            data: JSON.stringify({id: invId, status: 9}),
                            success: function (response) {
                                if (response.success) {
                                    alert('Đã DUYỆT đơn thành công!');
                                    if (row.hasClass('selected')) {
                                        purchaseDetailTable.clear().draw();
                                    }
                                    purchaseTable.ajax.reload();
                                } else {
                                    console.log("An error occured");
                                }
                            }
                        });

                    }
                    return;
                } else {
                    console.log('no action');
                }
                //console.log(currentData);
            }
        });
        $('#ticketDetailTableEdit').on('change', 'input', function () {
            //Get the cell of the input
            if (ticketDetailTableEdit != null) {
                var cell = $(this).closest('td');
                var row = $(this).closest('tr');
                var currentData = ticketDetailTableEdit.row(row).data();
                if ($(this).hasClass("qtyEven")) {
                    currentData.qtyEven = parseInt($(this).val());
                } else if( $(this).hasClass("qtyOdd") ) {
                    currentData.qtyOdd = parseInt($(this).val());
                } else if( $(this).hasClass("price") ) {
                    currentData.price = parseInt($(this).val());
                }
                ticketDetailTableEdit.row(row).data(currentData);
            }
        });

        $('#btnSearch').click(function (e) {
            //if(table2 != null) table2.destroy();
            purchaseTable.ajax.reload();
        });
        $('#ticketDetailModel').on('show.bs.modal', function (e) {
            $('#ticketDetailModel ').css('z-index', '1050');
            $('.modal .modal-body').css('overflow-y', 'auto');
            $('.modal .modal-body').css('min-height', $(window).height() * 0.7);
            if (slideInterval != null) {
                clearInterval(slideInterval);
                slideInterval = null;
            }
            initTicketDetailTableEdit();
        });

        $('#btnCreatePurchase').on('click', function (e) {
            var currenTime = new Date();
            $('#ticketId').text('');
            $('#toShop').val('');
            $('#toShop').trigger('change');
            $('#ticketCode').val(currenTime.yyyymmdd());
            ;
            $('#ticketDetailModel').modal('show');
            inventorySaveToogle();
        });


        $('#toShop').on('change', function (e) {
            if ($(this).val() != null && $(this).val() != '') {
                if ($('#ticketId').text() == '') {
                    let urlApi = '<?=site_url('api/business/materialList');?>' + '?type=3';
                    ticketDetailTableEditDraw(urlApi);
                }
            }
            inventorySaveToogle();
        });

        Date.prototype.yyyymmdd = function () {
            let mm = this.getMonth() + 1; // getMonth() is zero-based
            let dd = this.getDate();
            let h = this.getHours();
            let m = this.getMinutes();
            let s = this.getSeconds();
            return ['MN', this.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd,
                '_',
                (h > 9 ? '' : '0') + h,
                (m > 9 ? '' : '0') + m,
                (s > 9 ? '' : '0') + s,
                '_OBS'
            ].join('');
        };

        function initpurchaseDetailTable() {
            if (purchaseDetailTable != null)
                purchaseDetailTable.destroy();
            purchaseDetailTable = $('#purchaseDetailTable').DataTable({
                lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                //dom: 'lBfrtip',
                scrollX: true,
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                    "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                },
                fixedColumns: {
                    leftColumns: 2
                }
            });
        }

        function initpurchaseTable() {
            purchaseTable = $('#purchaseTable').DataTable({
                lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                dom: 'lBfrtip',
                scrollX: true,
                scrollY: 400,
                //scroller: true,
                deferRender: true,
                scrollCollapse: true,
                select: {
                    info: false,
                    style: 'single'
                },
                ajax: {
                    url: '<?=site_url('api/business/purchaseList');?>',
                    type: 'GET',
                    data: {
                        fromDate: function () {
                            return $('#fromDate').val();
                        },
                        toDate: function () {
                            return $('#toDate').val();
                        },
                        shopCode: function () {
                            return $('#selectShop').val();
                        },
                        status: function () {
                            return $('#invStatus').val();
                        },
                    }
                },
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                    "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                },
                buttons: [
                    <?php if( check_permission('purchase:export')):?>
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                        className: 'btn btn-warning',
                        footer: true,
                        //exportOptions: { columns: [ 1,2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ] },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-default')
                        },
                        action: function (e, dt, node, config) {
                            //alert(transferParams);
                            $('#overlay, #ajaxCall').show();
                            let tableParams = purchaseTable.ajax.params();
                            let fromDate = tableParams.fromDate();
                            let toDate = tableParams.toDate();
                            let shopCode = tableParams.shopCode();
                            let status = tableParams.status();
                            let exportUrl = `${purchaseExportBaseApi}?fromDate=${fromDate}&toDate=${toDate}&status=${status}`;
                            downloadFile(exportUrl, 'Báo cáo nhập hàng mua ngoài - ' + fromDate + '_' + toDate + '.xlsx')
                        }
                    }
                    <?php endif;?>
                ],
                order: [[5, "desc"], [2, "asc"]],

                fixedColumns: {
                    leftColumns: 2
                },
                columnDefs: [
                    {"targets": 4, "type": "date-eu"},
                ],
                columns: [
                    {
                        data: null,
                        searchable: false,
                        sortable: false,
                        className: "dt-body-center",
                        render: function (data, type, row, meta) {
                            var atc='<div style="margin: 0px 5px">';
                            var flagUpdate, flagApprove, flagDelete = false;
                            <?php if( check_permission('purchase:update')):?>
                                flagUpdate = true;
                            <?php endif;?>
                            <?php if( check_permission('purchase:approve')):?>
                                flagApprove = true;
                            <?php endif;?>
                            <?php if( check_permission('purchase:delete')):?>
                                flagDelete = true;
                            <?php endif;?>
                                if (row.status == 9) {
                                    atc +='<a class="btn btn-primary btn-xs" href="javascript:void(0);">Đã Duyệt</a> ';
                                }
                                if(row.status == 1 && flagApprove) {
                                    atc += '<button class="btn btn-success btn-xs approve">Duyệt</button> ';
                                }
                                if(row.status == 1 && flagDelete) {
                                    atc += '<button class="btn btn-danger btn-xs delete" invId="' + row.id + '">Hủy</button> '
                                }
                                if (row.status == 0 && flagUpdate) {
                                    atc +='<button class="btn btn-warning btn-xs update" invId="' + row.id + '">Cập nhật</button> ';
                                }
                                if (row.status == 0 && flagDelete) {
                                    atc +='<button class="btn btn-danger btn-xs delete" invId="' + row.id + '">Hủy</button> ';
                                }
                                atc += '</div>';

                            return atc;
                        }
                    },
                    {data: "invCode"},
                    {data: "shopCode", visible: false},
                    {data: "shopName"},
                    //{ data: "shopType", visible: false },
                    {data: "invDate", render: hrsd},
                    {
                        data: "status", render: function (data) {
                            if (data == 9) {
                                return 'Hoàn thành';
                            } else if (data == 1) {
                                return 'Chờ duyệt';
                            } else if (data == 3) {
                                return 'Đã hủy';
                            } else if(data == 0) {
                                return 'Đơn nháp';
                            }
                        }
                    },
                    {data: "note", className: "dt-body-left"}
                ],
            });
        }

        function ticketDetailTableEditDraw(urlApi = '') {
            if (ticketDetailTableEdit != null) {
                ticketDetailTableEdit.destroy();
            }
            ticketDetailTableEdit = $('#ticketDetailTableEdit').DataTable({

                dom: 'lBfrtip',
                //scrollCollapse : true,
                paging: false,
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                    "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                },
                ajax: {
                    url: urlApi,
                    type: 'GET',
                    data: function (d) {
                        d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                    },
                    dataSrc: function (dataJson) {
                        return dataJson.data;
                    }
                },
                order: [[1, "asc"], [0, "asc"]],
                buttons: [],
                columns: [
                    {data: "mtlCode"},
                    {
                        data: "mtlName", render: function (data, type, row, meta) {
                            return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                        }
                    },
                    {data: "spec", width: 130, render: formatIntegerSeparator, className: "dt-body-right"},
                    {
                        data: "qtyEven", width: 130, sortable: false, render: function (data, type, row, meta) {
                            return '<input class="form-control qtyEven text-right" type="number" value="' + data + '" min="0" style="width:120px;margin:0 auto;">';
                        },
                    },
                    {data: "unitEven", width: 130, className: "dt-body-center"},
                    {
                        data: "qtyOdd", width: 130, sortable: false, render: function (data, type, row, meta) {
                            if(row.unitOdd == null || row.unitOdd == '' || row.unitOdd == row.unitEven){
                                return '<input class="form-control qtyOdd text-right" disabled type="number" value="' + data + '" min="0" style="width: 120px; margin: 0 auto;">';
                            }else{
                                return '<input class="form-control qtyOdd text-right" type="number" value="' + data + '" min="0" style="width: 120px; margin: 0 auto;">';
                            }
                        },
                    },
                    {data: "unitOdd", width: 130, className: "dt-body-center"},
                    {
                        data: "price", width: 130, sortable: false, render: function (data, type, row, meta) {
                            return '<input class="form-control price text-right" type="number" value="' + data + '" min="0" style="width:120px;margin:0 auto;">';
                        },
                    },
                ],
                initComplete: function (settings, json) {
                }

            });
        }

        function initTicketDetailTableEdit(reset = false) {
            if (ticketDetailTableEdit == null) {
                ticketDetailTableEdit = $('#ticketDetailTableEdit').DataTable({
                    paging: false,
                    destroy: true,
                    dom: 'lBfrtip',
                    order: [[1, "asc"], [0, "asc"]],
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                        "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                    },
                    buttons: [],
                    columns: [
                        {data: "mtlCode"},
                        {
                            data: "mtlName", render: function (data, type, full, meta) {
                                return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                            }
                        },
                        {data: "spec", render: currencyFormat, className: "text-right"},
                        {
                            data: "qtyEven", sortable: false, render: function (data, type, full, meta) {
                                return '<input class="form-control qtyEven text-right" type="number" value="' + data + '" min="0" style="width: 120px; margin: 0 auto;">';
                            },
                        },
                        {data: "unitEven", width: 130, className: "dt-body-center"},
                        {
                            data: "qtyOdd", width: 130, sortable: false, render: function (data, type, row, meta) {
                                if(row.unitOdd == null || row.unitOdd == '' || row.unitOdd == row.unitEven){
                                    return '<input class="form-control qtyOdd text-right" disabled type="number" value="' + data + '" min="0" style="width: 120px; margin: 0 auto;">';
                                }else{
                                    return '<input class="form-control qtyOdd text-right" type="number" value="' + data + '" min="0" style="width: 120px; margin: 0 auto;">';
                                }
                            },
                        },
                        {data: "unitOdd", width: 130, className: "dt-body-center"},
                        {
                            data: "price", width: 130, sortable: false, render: function (data, type, row, meta) {
                                return '<input class="form-control price text-right" type="number" value="' + data + '" style="width:120px;margin:0 auto;">';
                            },
                        },
                    ],
                });
            } else {
                ticketDetailTableEdit.clear().draw();

            }
        }


        function inventorySaveToogle() {
            let toShop = $("#toShop option:selected").text();
            if (toShop != '') {
                $('#btnSave').prop('disabled', false);
                $('#btnDone').prop('disabled', false);
            } else {
                $('#btnSave').prop('disabled', true);
                $('#btnDone').prop('disabled', true);
            }
        }


        


    });
</script>
<style type="text/css">
    .table td:first-child {
        padding: 1px;
    }

    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) {
        text-align: center;
    }

    .table td:nth-child(9) {
        text-align: right;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if (check_permission('purchase:insert')): ?>
                        <button id="btnCreatePurchase" type="button" class="btn btn-warning">Thêm mới</button>
                    <?php endif; ?>
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shop_code', $shopList, set_value('shop_code', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Trạng thái</label>
                                        <?php
                                        $invStatus = array(
                                            'All' => 'Tất cả',
                                            '0' => 'Đơn nháp',
                                            '1' => 'Chờ duyệt',
                                            '9' => 'Hoàn thành',
                                        );
                                        echo form_dropdown('invStatus', $invStatus, set_value('invStatus', 'completed'), 'id="invStatus" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <button id="btnSearch" type="button"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="purchaseTable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr class="active">
                                        <th>Thao tác</th>
                                        <th>Mã phiếu</th>
                                        <th>Mã cửa hàng</th>
                                        <th>Cửa hàng nhập</th>
                                        <th>Ngày tạo</th>
                                        <th>Trạng thái</th>
                                        <th>Ghi chú</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12"
                                            class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dữ liệu chi tiết</h3>
                    <!-- <div class="box-tools">
                        <div class="form-group">
                            <div class="checkbox" style="display:none;">
                                <label>
                                    <input name="check_all" class="check_all" type="checkbox">
                                Tất cả
                                </label>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="box-body table-responsive">
                    <table id="purchaseDetailTable" class="table table-striped table-bordered order-detail">
                        <thead>
                        <tr class="active">
                            <th>Mã NVL</th>
                            <th>Tên NVL</th>
                            <th>Quy cách</th>
                            <th>SL chẵn</th>
                            <th>ĐVT chẵn</th>
                            <th>SL lẻ</th>
                            <th>ĐVT lẻ</th>
                            <th>Giá</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="post_msg">

                </div>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
    <div class="modal fade" id="ticketDetailModel" data-keyboard="false" data-backdrop="static" role="dialog"
         aria-labelledby="modelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modelLabel">Thông tin phiếu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group hidden">
                            <label id="ticketId" class="col-sm-2 control-label"></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Cửa hàng nhập</label>
                            <div class="col-sm-4">
                                <?php
                                echo form_dropdown('toShop', $allShopList, set_value('toShop', null), 'id="toShop" data-placeholder="Chọn cửa hàng" class="form-control input-tip select2" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày tạo</label>
                            <div class="col-sm-4">
                                <?= form_input('ticketDate', set_value('ticketDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="ticketDate" readonly disabled'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mã phiếu</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="ticketCode" readonly="" disabled="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ghi chú</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" id="ticketNote" rows="3"
                                          style="resize: vertical;"></textarea>
                            </div>
                        </div>
                    </form>
                    <div class="box-body table-responsive">
                        <table id="ticketDetailTableEdit" style="width:100% !important;" width="100%"
                               class="table table-striped table-bordered">
                            <thead>
                            <tr class="active">
                                <th>Mã NVL</th>
                                <th>Tên NVL</th>
                                <th>Quy cách</th>
                                <th>SL chẵn</th>
                                <th>ĐVT chẵn</th>
                                <th>SL lẻ</th>
                                <th>ĐVT lẻ</th>
                                <th>Giá</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button id="btnSave" onclick="saveStock(0)" type="button" class="btn btn-warning" disabled="">Lưu
                    </button>
                    <button id="btnDone" onclick="saveStock(1)" type="button" class="btn btn-warning" disabled="">Lưu và
                        hoàn tất
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>


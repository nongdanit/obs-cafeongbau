<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td{white-space: nowrap;}
    .background-color{ background-color: #FFBC10 !important; }
    .text-wrap{
        white-space:normal;
    }
    .modal-body {
        max-height: calc(100vh - 212px);
        overflow-y: auto;
    }
    .modal-lg {
        width: 85%;
    }
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }
    .text-wrap{
        white-space:normal;
    }

</style>
<?php
    $v = "?v=1";
    $v .= '&fromDate='.$fromDate;
    $v .= '&toDate='.$toDate;
    $v .= '&shopCode='.$shopCode;
    $v .= '&stockType='.$stockType;
    $v .= '&status=0';
    $v .= '&shopModel='.$shopModel;
?>
<script type="text/javascript">
    var stockTable, stockDetailTable, stockDetailTableEdit, selectedEditRow;
    var stockParams = '<?php echo $v; ?>'
    var baseUrl = '<?=site_url('');?>';
    var stockBaseApi = baseUrl + 'api/report/reportInventory';
    var stockExportBaseApi = baseUrl + 'api/report/reportInventoryExport';
    var baseApiMaterialList = baseUrl + 'api/business/materialList';
    
    function saveStock(status = 0){
        var url = baseUrl + 'api/business/stockCreate';
        let stockDetails = stockDetailTableEdit.data().toArray().map(detail => {
            let containerDetail = {};
            containerDetail.detailId = detail.stockDetailId || 0;
            containerDetail.mtlCode = detail.mtlCode;
            containerDetail.mtlName = detail.mtlName;
            containerDetail.unitCode = detail.unitEven;
            containerDetail.quantity = detail.qtyEven * detail.spec + detail.qtyOdd;
            return containerDetail;
        });
        //.filter(detail => detail.quantity > 0 || detail.detailId > 0);
        var stockData = {
            stockId: $("#stockId").text(),
            shopCode: $("#stockShop").val(),
            stockDate: $("#stockDate").val(),
            stockType: $("#stockTypes").val(),
            stockStatus: status,
            stockNote: $("#stockNote").val(),
            stockCode: $("#stockCode").val(),
            stockDetails: stockDetails
        }
        // console.log(stockData);
        // return;
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(stockData),
            success: function(response) {
                if(response.success){
                    if(status == 0){
                        alert('Lưu phiếu thành công.')
                    }else{
                        alert('Lưu và hoàn tất phiếu thành công.')                      
                    }
                    if(stockData.stockId == ''){
                        //console.log(data.message);
                        // stockTable.row.add(response.data).draw(false);
                    }else{
                        stockDetailTable.clear().draw();
                        stockTable.ajax.reload();
                    }
                    $('#stockDetailModel').modal('hide');
                }else{
                    alert(response.message);
                }
            }
        });
    }
    $(document).ready(function() {
        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });
        $('#stockDetailModel').css('z-index', '-1');
        $('#stockDetailModel').modal('show');
        var slideInterval = setInterval(function(){
            $("#stockDetailModel").modal('hide');
        }, 1);

        stockDetailTable = $('#stockDetailTable').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            scrollX: true,
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            fixedColumns: {
                leftColumns: 3
            }
        });
        //stockDetailTableEditInit();
        stockTable = $('#stockTable').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            //scroller: true,
            deferRender:    true,
            scrollCollapse: true,
            // autoWidth: true,
            select: {
                info: false,
                style: 'single'
            },
            ajax : { 
                url: stockBaseApi + stockParams,
                type: 'GET', 
                data: function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            buttons: [
                <?php if( check_permission( 'inventory:Export' )):?>
                { 
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    //exportOptions: { columns: [ 1,2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    },
                    action: function(e, dt, node, config) {                     
                        $('#overlay, #ajaxCall').show();
                        var request = new XMLHttpRequest();
                        var _OBJECT_URL;
                      
                        request.addEventListener('readystatechange', function(e) {
                            if(request.readyState == 2 && request.status == 200) {
                                // Download is being started
                            }
                            else if(request.readyState == 3) {
                                // Download is under progress
                            }
                            else if(request.readyState == 4) {
                                // Downloaing has finished
                                $('#overlay, #ajaxCall').hide();
                                _OBJECT_URL = URL.createObjectURL(request.response);
                                // Set href as a local object URL
                                $('#hiddenDownload').attr('href', _OBJECT_URL);
                                
                                // Set name of download
                                $('#hiddenDownload').attr('download', 'Báo cáo kiểm kê.xlsx');
                                $('#hiddenDownload')[0].click();
                                setTimeout(function() {
                                    window.URL.revokeObjectURL(_OBJECT_URL);
                                }, 60 * 1000);
                            }
                        });
                        request.addEventListener('progress', function(e) {
                            if (e.lengthComputable) {
                                var percent_complete = (e.loaded / e.total) * 100;
                                //console.log(percent_complete);
                            }
                            
                        });
                        request.responseType = 'blob'; 
                        // Downloading excel file
                        request.open('get', stockExportBaseApi + stockParams);                        
                        request.send();                    
                    }
                }
                <?php endif;?>
            ],
            fixedColumns: {
                leftColumns: 3
            },
            order: [[ 5, "desc" ], [ 2, "asc" ]],
            columnDefs: [
                {"targets": 5, "type":"date-euro"},
            ],
            columns: [           
                { data: null, searchable: false, visible: true, className: "dt-body-center", render: function( data, type, row, meta){
                        if(row.stockStatus == 9){
                            return '';
                        }
                        return '<button class="btn btn-warning btn-xs" stockId="' + row.stockId +'" href="javascript:void(0);">Cập nhật</button>';
                    } 
                },
                { data: "stockCode" },
                { data: "shopCode"},
                { data: "shopName"},
                { data: "shopType" },
                { data: "stockDate", render: formatDate },
                { data: "stockType", render: function(data){
                        if(data == 1){
                            return 'Tháng';
                        }
                        return 'Ngày';
                    }
                },
                { data: "stockStatus", render: function(data){
                        if(data == 9){
                            return 'Hoàn thành';
                        }
                        return 'Đang kiểm kê';
                    }
                },
                { data: "stockNote", className: "dt-body-left" }
            ]
        });
        var dataRow;
        stockTable.on('user-select', function ( e, dt, type, cell, originalEvent ) {
            if(cell.index().column == 0) return false;
            var row = dt.row( cell.index().row ).node();
            $(row).removeClass('hover');
            $('#inventoryDetail').modal('show');
            if ( $(row).hasClass('selected') ) {
                e.preventDefault();
                // deselect
            }else{
                //table.$('tr.selected').removeClass('selected');
                //$(this).addClass('selected');
                // $('.checkbox').show();
                dataRow = stockTable.row(row).data();
                let params = 'stockId='+ dataRow.stockId + '&shopCode=' + dataRow.shopCode + '&t=' + new Date().getTime();
                //console.log(dataRow);
                if(dataRow.stockDetails == null){
                    stockDetailTable.destroy();
                    stockDetailTable = $('#stockDetailTable').DataTable({
                        lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
                        dom: 'lBfrtip',
                        scrollX: true,
                        scrollY: 400,
                        //scroller: true,
                        deferRender:    true,
                        scrollCollapse: true,
                        pageLength: -1,
                        ajax : { 
                            url: '<?=site_url('api/report/reportInventoryDetail?');?>' + params,
                            type: 'GET', 
                            data: function ( d ) {
                                d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                            },
                            dataSrc: function (dataJson) {                 
                                dataRow.stockDetails = dataJson.data;                          
                                return dataJson.data;
                            }
                        },
                        oLanguage: {
                            "sEmptyTable": "Không có dữ liệu",
                            "sSearch": "Tìm kiếm nhanh:",
                            "sLengthMenu": "Hiển thị _MENU_ dòng",
                            "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                            "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                        },
                        
                        buttons: [
                            <?php if( check_permission(  'inventory:Export' )):?>
                                { 
                                    extend: 'excelHtml5',
                                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                                    className: 'btn btn-warning',
                                    title: 'Kiểm kê ' + ((dataRow.stockType == 0) ? 'Ngày' : 'Tháng') + '_' + dataRow.shopName + '_' + formatDate(dataRow.stockDate),
                                    footer: false,
                                        //exportOptions: { columns: [ 1, 2, 3, 4,5, 6, 7 ] },
                                    init: function(api, node, config) {
                                        $(node).removeClass('btn-default')
                                    }
                                },
                            <?php endif;?>
                        ],
                        fixedColumns: {
                            leftColumns: 3,
                        },
                        order: [[ 1, "asc" ], [ 0, "asc" ]],
                        columns: [
                            { data: "mtlCode"},
                            { data: "mtlName", width: 350, className: "text-wrap"},
                            { data: "spec", render: currencyFormat, className: "text-right"},
                            { data: "mtlType"},
                            { data: "qtyEven", render: currencyFormat, className: "text-right"},
                            { data: "unitEven", className: "dt-body-left"},
                            { data: "qtyOdd", render: currencyFormat, className: "text-right"},
                            { data: "unitOdd", className: "dt-body-left"},
                        ],
                    });
                }else{
                    stockDetailTable.clear().draw();
                    stockDetailTable.rows.add(dataRow.stockDetails); // Add new data
                    stockDetailTable.columns.adjust().draw();
                }                                    
            }
        });
        $('#stockTable').on( 'click', 'button', function () {
            if(stockTable != null){
                var row = $(this).closest('tr');
                selectedEditRow = stockTable.row(row);
                var currentData = selectedEditRow.data();
                
                //console.log(selectedEditRow);
                $('#stockTypes').prop('disabled', true);
                $('#stockShop').prop('disabled', true);
                $('#stockId').text(currentData.stockId);
                $('#stockTypes').select2('val', currentData.stockType);
                $('#stockShop').select2('val', currentData.shopCode);
                $("#stockDate" ).val(formatDate(currentData.stockDate));
                $("#stockCode" ).val(currentData.stockCode);
                $("#stockStatus2" ).val('Đang kiểm kê');
                $("#stockNote" ).val(currentData.stockNote);
                $('#stockDetailModel').modal('show');
                if(currentData.stockDetails == null){
                    let params = 'stockId='+ currentData.stockId + '&shopCode=' + currentData.shopCode + '&t=' + new Date().getTime();
                    $.ajax({
                        url: baseUrl + 'api/report/reportInventoryDetail?' + params,
                        type: 'GET',
                        dataType: 'json',
                        success: function(dataJson){
                            //console.log(dataJson);
                            currentData.stockDetails = dataJson.data;
                            stockDetailTableEdit.clear().draw();
                            stockDetailTableEdit.rows.add(dataJson.data); // Add new data
                            stockDetailTableEdit.columns.adjust().draw();
                        }
                    });
                }else{
                    stockDetailTableEdit.clear().draw();
                    stockDetailTableEdit.rows.add(currentData.stockDetails); // Add new data
                    stockDetailTableEdit.columns.adjust().draw();
                    stockDetailTableEdit.draw();
                }
                        
                //console.log(currentData);
            }      
        });
        $('#stockDetailTableEdit').on( 'change', 'input', function () {
            //Get the cell of the input
            if(stockDetailTableEdit != null){
                var cell = $(this).closest('td');
                var row = $(this).closest('tr');
                var currentData = stockDetailTableEdit.row(row).data();
                if($(this).hasClass("qtyEven")){
                    currentData.qtyEven = parseInt($(this).val());
                }else{
                    currentData.qtyOdd = parseInt($(this).val());
                }
                stockDetailTableEdit.row( row ).data(currentData);
            }      
        });
        
        $('#btnSearch').click(function(e){
            //if(table2 != null) table2.destroy();
            stockParams = "?v=1";
            stockParams += "&fromDate=" + $('#fromDate').val();
            stockParams += "&toDate=" + $('#toDate').val();
            stockParams += "&shopCode=" + $('#shop_code').val();
            stockParams += "&stockType=" + $('#stockType').val();
            stockParams += "&status=" + $('#stockStatus').val();
            stockParams += "&shopModel=" + $('#shopmodel').val();
            stockDetailTable.clear().draw();
            stockTable.ajax.url(stockBaseApi + stockParams).load();
        });
        $('#stockDetailModel').on('show.bs.modal', function (e) {
            $('#stockDetailModel ').css('z-index', '1050');
            $('.modal .modal-body').css('overflow-y', 'auto'); 
            $('.modal .modal-body').css('min-height', $(window).height() * 0.7);
            if(slideInterval != null) {
                clearInterval(slideInterval);
                slideInterval = null;
            }
            stockDetailTableEditInit();
        });
        $('#btnCreateStock').on('click', function(e){
            var currenTime = new Date();
            $("#stockId").text('');
            $("#stockTypes").select2("val", null);
            $("#stockShop").select2("val", null);
            $('#stockCode').val(currenTime.yyyymmdd());
            $('#stockStatus2').val("Đang kiểm kê");
            $('#stockTypes').prop('disabled', false);
            $('#stockShop').prop('disabled', false);
            $('#stockDetailModel').modal('show');
            //stockDetailTableEditInit();
            inventorySaveToogle();
        });
        $('#stockTypes').on('change', function (e) {
            if($(this).prop("disabled") == false){
                var optionSelected = $("option:selected", this);
                var valueSelected = this.value;           
                //alert(valueSelected);
                if(valueSelected != '' && stockDetailTableEdit != null){
                    if(valueSelected < 0){
                        stockDetailTableEdit.clear().draw();
                    }else{
                        stockDetailTableEditDraw(baseApiMaterialList + '?type=' + valueSelected);
                    }  
                }
                inventorySaveToogle();
            }
            
        });
        $('#stockShop').on('change', function (e) {
            //console.log('hello');
            inventorySaveToogle();          
        });

        Date.prototype.yyyymmdd = function() {
          var mm = this.getMonth() + 1; // getMonth() is zero-based
          var dd = this.getDate();
          var h = this.getHours();
          var m = this.getMinutes();
          var s = this.getSeconds();
          return ['KK',this.getFullYear(),
                  (mm>9 ? '' : '0') + mm,
                  (dd>9 ? '' : '0') + dd,
                  '_',
                  (h>9 ? '' : '0') + h,
                  (m>9 ? '' : '0') + m,
                  (s>9 ? '' : '0') + s,
                  '_OBS'
                 ].join('');
        };
        function stockDetailTableEditDraw(urlApi = ''){
            // if(inventoryList != null){
            if(stockDetailTableEdit != null){
                stockDetailTableEdit.destroy();
            }
            stockDetailTableEdit = $('#stockDetailTableEdit').DataTable({
                //scrollY : 300,
                //scrollX : true,
                //deferLoading: 0,
                //dom: 'lBfrtip',
                scrollCollapse : true,
                paging : false,
                fixedColumns:   {
                    leftColumns: 2
                },
                ajax: { 
                    url: urlApi,
                    type: 'GET', 
                    data: function ( d ) {
                        d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                    },
                    dataSrc: function (dataJson) {                                          
                        return dataJson.data;
                    }
                },
                order: [[ 1, "asc" ], [ 0, "asc" ]],
                // columnDefs: [
                //     {"targets": 4, "width": "20%"},
                //     {"targets": 6, "width": "150"},
                // ],
                columns: [
                    { data: "mtlCode"},
                    { data: "mtlName", render: function(data, type, row, meta){
                            return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                        }
                    },
                    { data: "spec", width:120, render: formatIntegerSeparator, className: "dt-body-right"},
                    { data: "mtlType"},
                    { data: "qtyEven", width:120, className: "dt-body-center", sortable: false, render: function(data, type, full, meta){
                            return '<input class="form-control qtyEven text-right" type="number" value="' + data +'" min="0" style="width:120px;margin:0 auto;">';
                        }, 
                    },
                    { data: "unitEven", width:100, className: "dt-body-left"},
                    { data: "qtyOdd", width:120, sortable: false, render: function(data, type, row, meta){
                            return '<input class="form-control qtyOdd text-right" type="number" value="' + data +'" min="0" style="width:120px;margin:0 auto;">';
                            }, 
                    },
                    { data: "unitOdd", width:100, className: "dt-body-left"},
                ],
                initComplete: function( settings, json ) {
                }

            });          
        }      
        function stockDetailTableEditInit(reset = false){
            if(stockDetailTableEdit == null){

                stockDetailTableEdit = $('#stockDetailTableEdit').DataTable({
                    paging: false,
                    destroy: true,
                    dom: 'lBfrtip',
                    order: [[ 1, "asc" ], [ 0, "asc" ]],
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                        "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                    },
                    buttons: [],
                    columns: [
                        { data: "mtlCode"},
                        { data: "mtlName", render: function(data, type, full, meta){
                                return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                            }
                        },
                        { data: "spec", width:120, render: currencyFormat, className: "text-right"},
                        { data: "mtlType"},
                        { data: "qtyEven", width:120, className: "dt-body-center", sortable: false, render: function(data, type, full, meta){
                                return '<input class="form-control qtyEven text-right" type="number" value="' + data +'" min="0" style="width: 120px;margin:0 auto;">';
                            }, 
                        },
                        { data: "unitEven", width:100, className: "dt-body-left"},
                        { data: "qtyOdd", width:120, sortable: false, render: function(data, type, full, meta){
                                return '<input class="form-control qtyOdd text-right" type="number" value="' + data +'" min="0" style="width: 120px;margin:0 auto;">';
                            }, 
                        },
                        { data: "unitOdd", width:100, className: "dt-body-left"},
                    ],
                });
            }else{
                stockDetailTableEdit.clear().draw();
                // stockDetailTableEdit.destroy();
                // stockDetailTableEdit = $('#stockDetailTableEdit').DataTable({
                //     paging: false,
                //     destroy: true,
                //     order: [[ 1, "asc" ], [ 0, "asc" ]],
                //     columns: [
                //         { data: "mtlCode"},
                //         { data: "mtlName", render: function(data, type, full, meta){
                //                 return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                //             }
                //         },
                //         { data: "spec", width:120, render: currencyFormat, className: "text-right"},
                //         { data: "mtlType"},
                //         { data: "qtyEven", width:120, className: "dt-body-center", sortable: false, render: function(data, type, full, meta){
                //                 return '<input class="form-control qtyEven text-right" type="number" value="' + data +'" min="0" style="width: 120px;margin:0 auto;">';
                //             }, 
                //         },
                //         { data: "unitEven", width:100, className: "dt-body-left"},
                //         { data: "qtyOdd", width:120, sortable: false, render: function(data, type, full, meta){
                //                 return '<input class="form-control qtyOdd text-right" type="number" value="' + data +'" min="0" style="width: 120px;margin:0 auto;">';
                //             }, 
                //         },
                //         { data: "unitOdd", width:100, className: "dt-body-left"},
                //     ],
                //     //deferLoading: 0
                // });
            } 
        }




        function inventorySaveToogle(){
            var valueShop = $("#stockShop option:selected").text();
            var valueType = $("#stockTypes option:selected").text();
            if(valueShop != '' && valueShop != 'Chọn cửa hàng' && valueType != '' && valueType != 'Chọn loại phiếu'){
                //alert(valueShop);
                $('#btnInventorySave').prop('disabled', false);
                $('#btnInventoryDone').prop('disabled', false);
            }else{
                $('#btnInventorySave').prop('disabled', true);
                $('#btnInventoryDone').prop('disabled', true);
            }
        }

    });
</script>
<style type="text/css">
    .table td:first-child { padding: 1px; }
    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) { text-align: center; }
    .table td:nth-child(9) { text-align: right; }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if( check_permission( 'inventory:insert' )):?>
                    <button id="btnCreateStock" type="button" class="btn btn-warning">Thêm mới</button>
                    <?php endif;?>
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("business/delivery", array('method'=>'get','id' => ''));?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shop_code', $shopList, set_value('shop_code', $shopCode), 'id="shop_code" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại phiếu</label>
                                        <?php
                                        $stockTypes = array(
                                            -1 => 'Tất cả',
                                            0 => 'Ngày',
                                            1 => 'Tháng',
                                        );
                                        echo form_dropdown('stockType', $stockTypes, set_value('stockType', -1), 'id="stockType" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Trạng thái</label>
                                        <?php
                                        $stockStatus = array(
                                            -1 => 'Tất cả',
                                            0 => 'Đang kiểm kê',
                                            9 => 'Hoàn thành',
                                        );
                                        echo form_dropdown('stockStatus', $stockStatus, set_value('stockStatus', 0), 'id="stockStatus" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : ""?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= 'Loại hình'; ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('model', $shopModel), 'id="shopmodel" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button id="btnSearch" type="button" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                            <!--  --><?= form_close();?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="stockTable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr class="active">
                                            <th>Thao tác</th>
                                            <th>Mã phiếu kiểm kê</th>
                                            <th>Mã cửa hàng</th>
                                            <th>Tên cửa hàng</th>
                                            <th>Mô hình</th>
                                            <th>Ngày kiểm kê</th>
                                            <th>Loại phiếu</th>
                                            <th>Trạng thái</th>
                                            <th>Ghi chú</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="12" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dữ liệu chi tiết</h3>
                    <!-- <div class="box-tools">
                        <div class="form-group">
                            <div class="checkbox" style="display:none;">
                                <label>
                                    <input name="check_all" class="check_all" type="checkbox">
                                Tất cả
                                </label>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="box-body table-responsive">
                    <table id="stockDetailTable" class="table table-striped table-bordered order-detail">
                        <thead>
                            <tr class="active">
                                <th>Mã NVL</th>
                                <th>Tên NVL</th>
                                <th>Quy cách</th>
                                <th>Loại</th>
                                <th>Tồn chẵn</th>
                                <th>ĐVT chẵn</th>
                                <th>Tồn lẻ</th>
                                <th>ĐVT lẻ</th>
                                <!-- <th>Thao tác</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="post_msg">
                        
                </div>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
    <div class="modal fade"  id="stockDetailModel" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="modelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modelLabel">Thông tin kiểm kê</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group hidden">
                            <label id="stockId" class="col-sm-2 control-label"></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Loại phiếu</label>
                            <div class="col-sm-4">
                                <?php
                                    $arr = array(
                                        // -1 => 'Chọn loại phiếu',
                                        0 => 'Ngày (Nguyên vật liệu)',
                                        1 => 'Tháng (TTB, CCDC, NVL, ...)',
                                    );
                                    echo form_dropdown('stockTypes', $arr, set_value('stockTypes', null), 'id="stockTypes" data-placeholder="Chọn loại phiếu" class="form-control input-tip select2" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Cửa hàng</label>
                            <div class="col-sm-4">
                                <?php
                                    if($shopCode !== 'HEAD_OFFICE' && $shopCode !== 'kienlonggroup' && $shopCode !== 'dongtamgroup'){
                                        if (($key = array_search('Tất cả', $shopList)) !== false) {
                                            unset($shopList[$key]);
                                        }
                                    }else{
                                        if (($key = array_search('Tất cả', $shopList)) !== false) {
                                            unset($shopList[$key]);
                                        }
                                    }
                                    echo form_dropdown('stockShop', $shopList, set_value('stockShop', null), 'id="stockShop" data-placeholder="Chọn cửa hàng" class="form-control input-tip select2" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày kiểm kê</label>
                            <div class="col-sm-4">
                                <?= form_input('stockDate', set_value('stockDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="stockDate" readonly disabled');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Số phiếu</label>
                            <div class="col-sm-4">
                            <input type="text" class="form-control" id="stockCode" readonly="" disabled="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Trạng thái</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="stockStatus2" readonly="" disabled="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ghi chú</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" id="stockNote" rows="3" style="resize: vertical;"></textarea>
                            </div>
                        </div>
                    </form>
                    <!-- <div class="box-body table-responsive"> -->
                        <table id="stockDetailTableEdit" style="width:100% !important;" class="table table-striped table-bordered">
                            <thead>
                                <tr class="active">
                                    <th>Mã NVL</th>
                                    <th>Tên NVL</th>
                                    <th>Quy cách</th>
                                    <th>Loại</th>
                                    <th>Tồn chẵn</th>
                                    <th>ĐVT chẵn</th>
                                    <th>Tồn lẻ</th>
                                    <th>ĐVT lẻ</th>
                                    <!-- <th>Thao tác</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button id="btnInventorySave" onclick="saveStock(0)" type="button" class="btn btn-warning" disabled="">Lưu</button>
                    <button id="btnInventoryDone" onclick="saveStock(9)" type="button" class="btn btn-warning" disabled="">Lưu và hoàn tất</button>
                  </div>
            </div>
        </div>
    </div>
</section>


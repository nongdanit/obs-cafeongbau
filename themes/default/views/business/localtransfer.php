<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }

    .text-wrap {
        white-space: normal;
    }

    .modal-body {
        max-height: calc(100vh - 212px);
        overflow-y: auto;
    }

    .modal-lg {
        width: 85%;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    .select2-container--default .select2-results__option[aria-disabled=true] {
        display: none;
    }

</style>
<script type="text/javascript">
    var transferTable, transferDetailTable, ticketDetailTableEdit, selectedEditRow;
    var baseUrl = '<?=site_url('');?>';
    var transferExportBaseApi = baseUrl + 'api/business/transferExport';
    var fromDate = '<?php echo date("d-m-Y") ?>';
    var toDate = '<?php echo date("d-m-Y") ?>';
    <?php if(check_permission('localtransfer:insert')): ?>
    function saveStock(status = 0) {
        let url = '<?=site_url('api/business/transferCreate');?>';
        let fromShop = $("#fromShop option:selected").text();
        let toShop = $("#toShop option:selected").text();
        let ticketDetails = ticketDetailTableEdit.data().toArray().map(detail => {
            let containerDetail = {};
            containerDetail.detailId = detail.detailId;
            containerDetail.invId = detail.invId;
            containerDetail.mtlCode = detail.mtlCode;
            containerDetail.mtlName = detail.mtlName;
            containerDetail.unitCode = detail.unitEven;
            containerDetail.quantity = detail.qtyEven * detail.spec + detail.qtyOdd;
            containerDetail.price = detail.price;
            return containerDetail;
        }).filter(detail => detail.quantity > 0 || detail.detailId > 0);
        let transferData = {
            ticketId: $("#ticketId").text(),
            fromShop: $("#fromShop").val(),
            toShop: $("#toShop").val(),
            ticketDate: $("#ticketDate").val(),
            ticketType: "NB",
            ticketStatus: status,
            ticketNote: $("#ticketNote").val(),
            ticketCode: $("#ticketCode").val(),
            ticketDetails: ticketDetails
        }
        // console.log(transferData);return;
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(transferData),
            success: function (response) {
                console.log(response);
                if (response.success) {
                    if (status == 0) {
                        alert('Lưu phiếu thành công.')
                    } else {
                        alert('Lưu và hoàn tất phiếu thành công.')
                    }
                    if (transferData.ticketId == '') {
                        let data = response.data;
                        data.shopName = fromShop;
                        data.linkShop = toShop;
                        // transferTable.row.add(data).draw(false);
                    } else {
                        transferDetailTable.clear().draw();
                        transferTable.ajax.reload();
                    }
                    $('#ticketDetailModel').modal('hide');
                } else {
                    alert(response.message);
                }
            }
        });
    }
    <?php endif;?>

    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        $('#ticketDetailModel').css('z-index', '-1');
        $('#ticketDetailModel').modal('show');
        var slideInterval = setInterval(function () {
            $("#ticketDetailModel").modal('hide');
        }, 1);

        initTransferTable();
        initTransferDetailTable();
        // initTicketDetailTableEdit();
        transferTable.on('user-select', function (e, dt, type, cell, originalEvent) {
            if (cell.index().column == 0) return false;
            let dataRow;
            let row = dt.row(cell.index().row).node();
            $(row).removeClass('hover');
            $('#inventoryDetail').modal('show');
            if ($(row).hasClass('selected')) {
                e.preventDefault();
                // deselect
            } else {
                dataRow = transferTable.row(row).data();
                if (dataRow.details == null) {
                    transferDetailTable.destroy();
                    transferDetailTable = $('#transferDetailTable').DataTable({
                        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        order: [],
                        scrollX: true,
                        scrollY: 400,
                        //scroller: true,
                        deferRender: true,
                        scrollCollapse: true,
                        pageLength: -1,
                        ajax: {
                            url: '<?=site_url('api/business/transferDetail');?>' + '?id=' + dataRow.id + '&shopCode=' + dataRow.shopCode,
                            type: 'GET',
                            data: function (d) {
                                d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                            },
                            dataSrc: function (dataJson) {
                                dataRow.details = dataJson.data;
                                return dataJson.data.filter((detail) => detail.detailId > 0);
                            }
                        },
                        oLanguage: {
                            "sEmptyTable": "Không có dữ liệu",
                            "sSearch": "Tìm kiếm nhanh:",
                            "sLengthMenu": "Hiển thị _MENU_ dòng",
                            "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                            "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                        },
                        dom: 'lBfrtip',
                        buttons: [
                            <?php if( check_permission('localtransfer:Export')):?>
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                                className: 'btn btn-warning',
                                title: 'Điều chuyển HH nội bộ từ "' + dataRow.shopName + '" đến "' + dataRow.linkShop + '"_' + hrsd(dataRow.invDate),
                                footer: false,
                                //exportOptions: { columns: [ 1, 2, 3, 4,5, 6, 7 ] },
                                init: function (api, node, config) {
                                    $(node).removeClass('btn-default')
                                }
                            },
                            <?php endif;?>
                        ],
                        fixedColumns: {
                            leftColumns: 2
                        },
                        columns: [
                            {data: "mtlCode"},
                            {data: "mtlName"},
                            {data: "spec", width: 130, render: currencyFormat, className: "dt-body-right"},
                            //{ data: "mtlType"},
                            {data: "qtyEven", width: 130, sortable: true, className: "dt-body-right"},
                            {data: "unitEven", width: 130, className: "dt-body-left"},
                            {data: "qtyOdd", width: 130, sortable: true, className: "dt-body-right"},
                            {data: "unitOdd", width: 130, className: "dt-body-left"},
                        ],
                    });
                } else {
                    transferDetailTable.clear().draw();
                    transferDetailTable.rows.add(dataRow.details.filter((detail) => detail.detailId > 0)); // Add new data
                    transferDetailTable.columns.adjust().draw();
                }
            }
        });
        $('#transferTable').on('click', 'button', function () {
            if (transferTable != null) {
                let row = $(this).closest('tr');
                let selectedData = transferTable.row(row).data();
                if ($(this).hasClass('update')) {
                    $('#ticketId').text(selectedData.id);
                    $("#ticketDate").val(hrsd(selectedData.invDate));
                    $("#ticketCode").val(selectedData.invCode);
                    $("#ticketNote").val(selectedData.note);

                    $('#toShop').val(selectedData.linkShopCode);
                    $('#toShop').trigger('change');
                    $('#fromShop').val(selectedData.shopCode);
                    $('#fromShop').trigger('change');
                    $('#fromShop').prop('disabled', true);
                    $('#toShop').prop('disabled', true);
                    $('#ticketDetailModel').modal('show');
                    if (selectedData.details == null) {
                        $.ajax({
                            url: '<?=site_url('api/business/transferDetail');?>' + '?id=' + selectedData.id + '&shopCode=' + selectedData.shopCode,
                            type: 'GET',
                            dataType: 'json',
                            success: function (dataJson) {
                                //console.log(dataJson);
                                //selectedData.details = dataJson.data;
                                ticketDetailTableEdit.clear().draw();
                                ticketDetailTableEdit.rows.add(dataJson.data); // Add new data
                                ticketDetailTableEdit.columns.adjust().draw();
                            }
                        });
                    } else {
                        ticketDetailTableEdit.clear();
                        ticketDetailTableEdit.rows.add(selectedData.details); // Add new data
                        ticketDetailTableEdit.columns.adjust().draw();
                        ticketDetailTableEdit.draw();
                    }

                    inventorySaveToogle();
                } else {
                    let invCode = selectedData.invCode;
                    let shopCode = selectedData.shopCode;
                    let shopName = selectedData.shopName;
                    let invId = selectedData.id;
                    let msg = 'Bạn có muốn HỦY phiếu ' + invCode + ' \n(' + shopCode + ' - ' + shopName + ')?';
                    if (confirm(msg)) {
                        $.getJSON("/business/active_order", {id: invId, status: 3}, function (data, status, xhr) {
                            if (status == 'success') {
                                if (data.dataReturn.error == false) {
                                    if (row.hasClass('selected')) {
                                        transferDetailTable.clear().draw();
                                    }
                                    transferTable.row(row).remove().draw();
                                }
                            } else if (status == 'timeout') {
                                console.log('connection timeout')
                            } else if (status == "error" || status == "parsererror") {
                                console.log("An error occured");
                            } else {
                                console.log("datatosend did not change");
                            }

                        });
                    }
                }


                //console.log(currentData);
            }
        });
        $('#ticketDetailTableEdit').on('change', 'input', function () {
            //Get the cell of the input
            if (ticketDetailTableEdit != null) {
                var cell = $(this).closest('td');
                var row = $(this).closest('tr');
                var currentData = ticketDetailTableEdit.row(row).data();
                if ($(this).hasClass("qtyEven")) {
                    currentData.qtyEven = parseInt($(this).val());
                } else {
                    currentData.qtyOdd = parseInt($(this).val());
                }
                ticketDetailTableEdit.row(row).data(currentData);
            }
        });

        $('#btnSearch').click(function (e) {
            //if(table2 != null) table2.destroy();
            transferTable.ajax.reload();
        });
        $('#ticketDetailModel').on('show.bs.modal', function (e) {
            $('#ticketDetailModel ').css('z-index', '1050');
            $('.modal .modal-body').css('overflow-y', 'auto');
            $('.modal .modal-body').css('min-height', $(window).height() * 0.7);
            if (slideInterval != null) {
                clearInterval(slideInterval);
                slideInterval = null;
            }
            initTicketDetailTableEdit();
        });
        $('#btnCreateTransfer').on('click', function (e) {
            $('#fromShop').prop('disabled', false);
            $('#toShop').prop('disabled', true);
            $("#fromShop").val('');
            $('#fromShop').trigger('change');
            $("#toShop").val('');
            $('#toShop').trigger('change');
            var currenTime = new Date();
            $('#ticketId').text('');
            $('#ticketCode').val(currenTime.yyyymmdd());
            ;
            $('#ticketDetailModel').modal('show');

            // if(ticketDetailTableEdit != null){
            //     ticketDetailTableEdit.clear().draw();
            //     //ticketDetailTableEdit.rows.add(currentData.details); // Add new data
            //     //ticketDetailTableEdit.columns.adjust().draw();
            // }else{
            //     initTicketDetailTableEdit();
            // }
            inventorySaveToogle();
            // initTicketDetailTableEdit();
        });


        $('#fromShop').on('change', function (e) {
            // console.log($(this).val());
            if ($(this).val() != null && $(this).val() != '') {
                $('#toShop').prop('disabled', false);
            } else {
                $('#toShop').prop('disabled', true);
            }
            if ($(this).val() == $('#toShop').val()) {
                $(this).val('')
                $(this).trigger('change');
            }

            inventorySaveToogle();
        });
        $('#toShop').on('change', function (e) {
            //console.log($(this).val());
            if ($(this).val() != null && $(this).val() != '') {
                let fromShopCode = $('#fromShop').val();
                if (fromShopCode != '' && fromShopCode != null) {
                    if ($(this).val() == fromShopCode) {
                        $(this).val('');
                    } else {
                        if ($('#ticketId').text() == '') {
                            let urlApi = '<?=site_url('api/business/transferDetail');?>' + '?shopCode=' + fromShopCode;
                            ticketDetailTableEditDraw(urlApi);
                        }

                    }
                }
                //           
            }
            inventorySaveToogle();
        });

        Date.prototype.yyyymmdd = function () {
            let mm = this.getMonth() + 1; // getMonth() is zero-based
            let dd = this.getDate();
            let h = this.getHours();
            let m = this.getMinutes();
            let s = this.getSeconds();
            return ['NB', this.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd,
                '_',
                (h > 9 ? '' : '0') + h,
                (m > 9 ? '' : '0') + m,
                (s > 9 ? '' : '0') + s,
                '_OBS'
            ].join('');
        };

        function initTransferDetailTable() {
            if (transferDetailTable != null)
                transferDetailTable.destroy();
            transferDetailTable = $('#transferDetailTable').DataTable({
                lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                //dom: 'lBfrtip',
                scrollX: true,
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                    "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                },
                fixedColumns: {
                    leftColumns: 2
                }
            });
        }

        function initTransferTable() {
            transferTable = $('#transferTable').DataTable({
                lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                dom: 'lBfrtip',
                scrollX: true,
                scrollY: 400,
                //scroller: true,
                deferRender: true,
                scrollCollapse: true,
                select: {
                    info: false,
                    style: 'single'
                },
                ajax: {
                    url: '<?=site_url('api/business/transferList');?>',
                    type: 'GET',
                    data: {
                        fromDate: function () {
                            return $('#fromDate').val();
                        },
                        toDate: function () {
                            return $('#toDate').val();
                        },
                        shopCode: function () {
                            return $('#selectShop').val();
                        },
                        status: function () {
                            return $('#invStatus').val();
                        },
                    }
                },
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                    "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                },
                buttons: [
                    <?php if( check_permission('localtransfer:Export')):?>
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                        className: 'btn btn-warning',
                        footer: true,
                        //exportOptions: { columns: [ 1,2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ] },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-default')
                        },
                        action: function (e, dt, node, config) {
                            //alert(transferParams);             
                            $('#overlay, #ajaxCall').show();
                            let tableParams = transferTable.ajax.params();
                            let fromDate = tableParams.fromDate();
                            let toDate = tableParams.toDate();
                            let shopCode = tableParams.shopCode();
                            let status = tableParams.status();
                            let exportUrl = `${transferExportBaseApi}?fromDate=${fromDate}&toDate=${toDate}&shopCode=${shopCode}&status=${status}`;
                            downloadFile(exportUrl, 'Báo cáo điều chuyển hàng_' + fromDate + '_' + toDate + '.xlsx')
                        }
                    }
                    <?php endif;?>
                ],
                // fixedColumns: {
                //     leftColumns: 3
                // },
                order: [[5, "desc"], [2, "asc"]],

                fixedColumns: {
                    leftColumns: 2
                },
                columnDefs: [
                    {"targets": 4, "type": "date-eu"},
                ],
                columns: [
                    {
                        data: null,
                        searchable: false,
                        sortable: false,
                        className: "dt-body-center",
                        render: function (data, type, row, meta) {
                            if (row.status == 9) {
                                return '';
                            }
                            return '<div style="margin: 0px 5px"><button class="btn btn-warning btn-xs update" invId="' + row.id + '">Cập nhật</button> <button class="btn btn-danger btn-xs delete" invId="' + row.id + '">Hủy</button></div>';
                        }
                    },
                    {data: "invCode"},
                    {data: "shopCode", visible: false},
                    {data: "shopName"},
                    //{ data: "shopType", visible: false },
                    {data: "invDate", render: hrsd},
                    {
                        data: "status", render: function (data) {
                            if (data == 9) {
                                return 'Hoàn thành';
                            }
                            return 'Đang xử lý';
                        }
                    },
                    {data: "linkShop", className: "dt-body-left"},
                    {data: "note", className: "dt-body-left"}
                ],
                // rowCallback: function( row, data, index ) {
                //     transferTable.columns(0).hide();
                //     // if ($('#invStatus').val() == 9) {
                //     //     $(row).hide();
                //     // }
                // },
            });
        }

        function ticketDetailTableEditDraw(urlApi = '') {
            // if(inventoryList != null){
            if (ticketDetailTableEdit != null) {
                ticketDetailTableEdit.destroy();
            }
            ticketDetailTableEdit = $('#ticketDetailTableEdit').DataTable({

                //scrollY : 300,
                //scrollX : true,
                //deferLoading: 0,
                dom: 'lBfrtip',
                //scrollCollapse : true,
                paging: false,
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                    "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                },
                // fixedColumns:   {
                //     leftColumns: 2
                // },
                ajax: {
                    url: urlApi,
                    type: 'GET',
                    data: function (d) {
                        d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                    },
                    dataSrc: function (dataJson) {
                        return dataJson.data;
                    }
                },
                order: [[1, "asc"], [0, "asc"]],
                // columnDefs: [
                //     {"targets": 4, "width": "20%"},
                //     {"targets": 6, "width": "150"},
                // ],
                buttons: [],
                columns: [
                    {data: "mtlCode"},
                    {
                        data: "mtlName", render: function (data, type, row, meta) {
                            return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                        }
                    },
                    {data: "spec", width: 130, render: formatIntegerSeparator, className: "dt-body-right"},
                    //{ data: "mtlType"},
                    {
                        data: "qtyEven", width: 130, sortable: false, render: function (data, type, row, meta) {
                            return '<input class="form-control qtyEven text-right" type="number" value="' + data + '" min="0" style="width:120px;margin:0 auto;">';
                        },
                    },
                    {data: "unitEven", width: 130, className: "dt-body-left"},
                    {
                        data: "qtyOdd", width: 130, sortable: false, render: function (data, type, row, meta) {
                            if (row.unitOdd == null || row.unitOdd == '') {
                                return '<input class="form-control qtyOdd text-right" disabled type="number" value="' + data + '" min="0" style="width:120px;margin:0 auto;">';
                            } else {
                                return '<input class="form-control qtyOdd text-right" type="number" value="' + data + '" min="0" style="width:120px;margin:0 auto;">';
                            }

                        },
                    },
                    {data: "unitOdd", width: 130, className: "dt-body-left"},
                ],
                initComplete: function (settings, json) {
                }

            });
        }

        function initTicketDetailTableEdit(reset = false) {
            if (ticketDetailTableEdit == null) {
                ticketDetailTableEdit = $('#ticketDetailTableEdit').DataTable({
                    paging: false,
                    destroy: true,
                    dom: 'lBfrtip',
                    order: [[1, "asc"], [0, "asc"]],
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                        "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                    },
                    buttons: [],
                    columns: [
                        {data: "mtlCode"},
                        {
                            data: "mtlName", render: function (data, type, full, meta) {
                                return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                            }
                        },
                        {data: "spec", render: currencyFormat, className: "text-right"},
                        //{ data: "mtlType"},
                        {
                            data: "qtyEven", sortable: false, render: function (data, type, full, meta) {
                                return '<input class="form-control qtyEven text-right" type="number" value="' + data + '" min="0" style="width: 120px; margin: 0 auto;">';
                            },
                        },
                        {data: "unitEven", className: "dt-body-left"},
                        {
                            data: "qtyOdd", sortable: false, render: function (data, type, full, meta) {
                                return '<input class="form-control qtyOdd text-right" type="number" value="' + data + '" min="0" style="width: 120px; margin: 0 auto;">';
                            },
                        },
                        {data: "unitOdd", className: "dt-body-left"},
                    ],
                });
            } else {
                ticketDetailTableEdit.clear().draw();

                // ticketDetailTableEdit = $('#ticketDetailTableEdit').DataTable({
                //     paging: false,
                //     destroy: true,
                //     dom: 'lBfrtip',
                //     order: [[ 1, "asc" ], [ 0, "asc" ]],
                //     oLanguage: {
                //         "sEmptyTable": "Không có dữ liệu",
                //         "sSearch": "Tìm kiếm nhanh:",
                //         "sLengthMenu": "Hiển thị _MENU_ dòng",
                //         "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                //         "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                //     },
                //     buttons: [],
                //     columns: [
                //         { data: "mtlCode"},
                //         { data: "mtlName", render: function(data, type, full, meta){
                //                 return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                //             }
                //         },
                //         { data: "spec", render: currencyFormat, className: "text-right"},
                //         { data: "mtlType"},
                //         { data: "qtyEven", sortable: false, render: function(data, type, full, meta){
                //                 return '<input class="form-control qtyEven text-right" type="number" value="' + data +'" min="0" style="width: 120px;">';
                //             }, 
                //         },
                //         { data: "unitEven", className: "dt-body-left"},
                //         { data: "qtyOdd", sortable: false, render: function(data, type, full, meta){
                //                 return '<input class="form-control qtyOdd text-right" type="number" value="' + data +'" min="0" style="width: 120px;">';
                //             }, 
                //         },
                //         { data: "unitOdd", className: "dt-body-left"},
                //     ],
                // });
            }
        }


        function inventorySaveToogle() {
            let fromShop = $("#fromShop option:selected").text();
            let toShop = $("#toShop option:selected").text();
            if (fromShop != '' && toShop != '') {
                //alert(valueShop);
                $('#btnSave').prop('disabled', false);
                $('#btnDone').prop('disabled', false);
            } else {
                $('#btnSave').prop('disabled', true);
                $('#btnDone').prop('disabled', true);
            }
        }
    });
</script>
<style type="text/css">
    .table td:first-child {
        padding: 1px;
    }

    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) {
        text-align: center;
    }

    .table td:nth-child(9) {
        text-align: right;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if (check_permission('localtransfer:insert')): ?>
                        <button id="btnCreateTransfer" type="button" class="btn btn-warning">Thêm mới</button>
                    <?php endif; ?>
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("business/delivery", array('method' => 'get', 'id' => '')); ?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shop_code', $shopList, set_value('shop_code', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Trạng thái</label>
                                        <?php
                                        $invStatus = array(
                                            'All' => 'Tất cả',
                                            '0' => 'Đang xử lý',
                                            '9' => 'Hoàn thành',
                                        );
                                        echo form_dropdown('invStatus', $invStatus, set_value('invStatus', 'completed'), 'id="invStatus" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <button id="btnSearch" type="button"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                            <!--  --><?= form_close(); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="transferTable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr class="active">
                                        <th>Thao tác</th>
                                        <th>Mã phiếu</th>
                                        <th>Mã cửa hàng</th>
                                        <th>Cửa hàng xuất</th>
                                        <!-- <th>Mô hình</th> -->
                                        <th>Ngày tạo</th>
                                        <!-- <th>Loại phiếu</th> -->
                                        <th>Trạng thái</th>
                                        <th>Cửa hàng nhập</th>
                                        <th>Ghi chú</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12"
                                            class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dữ liệu chi tiết</h3>
                    <!-- <div class="box-tools">
                        <div class="form-group">
                            <div class="checkbox" style="display:none;">
                                <label>
                                    <input name="check_all" class="check_all" type="checkbox">
                                Tất cả
                                </label>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="box-body table-responsive">
                    <table id="transferDetailTable" class="table table-striped table-bordered order-detail">
                        <thead>
                        <tr class="active">
                            <th>Mã NVL</th>
                            <th>Tên NVL</th>
                            <th>Quy cách</th>
                            <!-- <th>Loại</th> -->
                            <th>SL Xuất chẵn</th>
                            <th>ĐVT chẵn</th>
                            <th>SL xuất lẻ</th>
                            <th>ĐVT lẻ</th>
                            <!-- <th>Thao tác</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="post_msg">

                </div>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
    <div class="modal fade" id="ticketDetailModel" data-keyboard="false" data-backdrop="static" role="dialog"
         aria-labelledby="modelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modelLabel">Thông tin phiếu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group hidden">
                            <label id="ticketId" class="col-sm-2 control-label"></label>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Cửa hàng xuất</label>
                            <div class="col-sm-4">
                                <?php
                                unset($shopList['All']);
                                echo form_dropdown('fromShop', $shopList, set_value('fromShop', null), 'id="fromShop" data-placeholder="Chọn cửa hàng" class="form-control input-tip select2" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Cửa hàng nhập</label>
                            <div class="col-sm-4">
                                <?php
                                echo form_dropdown('toShop', $allShopList, set_value('toShop', null), 'id="toShop" data-placeholder="Chọn cửa hàng" class="form-control input-tip select2" style="width:100%;" disabled');
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày tạo</label>
                            <div class="col-sm-4">
                                <?= form_input('ticketDate', set_value('ticketDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="ticketDate" readonly disabled'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mã phiếu</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="ticketCode" readonly="" disabled="">
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-sm-2 control-label">Trạng thái</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="stockStatus2" readonly="" disabled="">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ghi chú</label>
                            <div class="col-sm-4">
                                <textarea class="form-control" id="ticketNote" rows="3"
                                          style="resize: vertical;"></textarea>
                            </div>
                        </div>
                    </form>
                    <div class="box-body table-responsive">
                        <table id="ticketDetailTableEdit" style="width:100% !important;" width="100%"
                               class="table table-striped table-bordered">
                            <thead>
                            <tr class="active">
                                <th>Mã NVL</th>
                                <th>Tên NVL</th>
                                <th>Quy cách</th>
                                <!-- <th>Loại</th> -->
                                <th>SL Xuất chẵn</th>
                                <th>ĐVT chẵn</th>
                                <th>SL xuất lẻ</th>
                                <th>ĐVT lẻ</th>
                                <!-- <th>Thao tác</th> -->
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button id="btnSave" onclick="saveStock(0)" type="button" class="btn btn-warning" disabled="">Lưu
                    </button>
                    <button id="btnDone" onclick="saveStock(9)" type="button" class="btn btn-warning" disabled="">Lưu và
                        hoàn tất
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>


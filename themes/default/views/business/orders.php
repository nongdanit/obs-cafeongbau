<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }

    #note {
        resize: vertical;
        min-height: 110px;
    }

    td.editable {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }
</style>
<style type="text/css">
    .table td:first-child {
        padding: 1px;
    }

    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) {
        text-align: center;
    }

    .table td:nth-child(9) {
        text-align: right;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if (check_permission('orders:insert')): ?>
                        <button id="btnNewOrder" type="button" class="btn btn-warning" data-toggle="modal"
                                data-target="#orderModal">Thêm đơn hàng
                        </button>
                    <?php endif; ?>
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("business/orders", array('method' => 'get', 'id' => '')); ?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"
                                               for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate" style=""'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate" style=""'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("status"); ?></label>
                                        <?php
                                        $a = [];
                                        //if($this->session->userdata('group_id') == 8){
                                        $a['All'] = 'Tất cả';
                                        $a['processing'] = 'Đang xử lý';
                                        $a['sended'] = 'Đã gửi';
                                        //}
                                        echo form_dropdown('state', $a, set_value('state', $state), 'id="stateOrder" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= 'Loại hình'; ?></label>
                                        <?php
                                $a = [];
                                $a[''] = 'Tất cả';
                                $a['NQ'] = 'Nhượng Quyền';
                                $a['CSH'] = 'Chủ Sở Hữu';
                                echo form_dropdown('model', $a, set_value('model', $model), 'id="model" class="form-control input-tip select2"');
                                ?>
                                    </div>
                                </div> -->
                                <?php if(!empty($orderTypeInv)):?>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại đơn hàng</label>
                                        <?php
                                        $a = array('All' => 'Tất cả');
                                        $b = array_replace($a, $orderTypeInv);

                                        echo form_dropdown('orderType', $b, set_value('orderType'), 'id="orderType" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <?php endif;?>
                                <div class="col-sm-12">
                                    <button id="btnSearch" type="submit"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                            <!-- <?= form_close(); ?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="orderTable" width="100%"
                                       class="table table-bordered table-striped no-footer">
                                    <thead>
                                    <tr class="active">
                                        <?php if ($type != 'sended' && check_permission('orders:Update')): ?>
                                            <th>Thao tác</th>
                                        <?php endif; ?>
                                        <th><?= lang("shop_code"); ?></th>
                                        <th><?= lang('name_store') ?></th>
                                        <th><?= lang("model"); ?></th>
                                        <th>Loại</th>
                                        <th>Loại đơn hàng</th>
                                        <th><?= lang('order_date') ?></th>
                                        <th><?= lang("order_code"); ?></th>
                                        <th><?= lang("note"); ?></th>
                                        <th>Thành tiền(*)</th>
                                        <th>Trạng thái</th>
                                        <th>TT Phân phối</th>
                                        <th>Nhà phân phối</th>
                                        <th>Kho</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12"
                                            class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dữ liệu chi tiết</h3>
                    <div class="box-tools">
                        <div class="form-group">
                            <div class="checkbox" style="display:none;">
                                <label>
                                    <input id="allItem" name="check_all" class="check_all" type="checkbox">
                                    Tất cả
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table id="orderDetail" class="table table-striped table-bordered display processingOrder"
                           style="width:100%">
                        <thead>
                        <tr class="active">
                            <th>Mã sản phẩm</th>
                            <th>Tên sản phẩm</th>
                            <th>ĐVT</th>
                            <th>Số lượng đặt</th>
                            <th>Đơn giá</th>
                            <th>Thành tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalformeditOrderDetail" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Cập nhật</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-warning">
                            <div class="box-header">
                            </div>
                            <div class="box-body">
                                <div class="col-lg-12">
                                    <?= form_open_multipart("business/updateOrderDetail", 'id="updateOrderDetail"'); ?>
                                    <input type="hidden" name="idl_id" id="idl_id" value="">
                                    <input type="hidden" name="inv_id" id="inv_id" value="">
                                    <input type="hidden" name="type" id="type" value="1">
                                    <div class="row">
                                        <p class="text-red show-error"></p>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Mã sản phẩm
                                                <?= form_input('code', set_value('code'), 'class="form-control" id="code"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Tên sản phẩm
                                                <?= form_input('name', set_value('name'), 'class="form-control" id="name" '); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Đơn vị tính
                                                <?= form_input('unit', set_value('unit'), 'class="form-control" id="unit"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Số lượng đặt
                                                <?= form_input('qty_order', set_value('qty_order'), 'class="form-control" id="qty_order" '); ?>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4">
                                            <div class="form-group">
                                                Số lượng duyệt
                                                <?= form_input('qty_approve', set_value('qty_approve'), 'class="form-control" id="qty_approve" '); ?>
                                            </div>
                                        </div> -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Đơn giá
                                                <?= form_input('price', set_value('price'), 'class="form-control" id="price" '); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal"><?= lang('cancel') ?></button>
                                        <?= form_submit('add', lang('update'), 'class="btn btn-warning"'); ?>
                                    </div>
                                    <?= form_close(); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (check_permission('orders:insert')): ?>
    <div class="modal fade" id="orderModal" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <?= form_open('api/business/createOrder', array('method' => 'post', 'id' => 'formCreateOrder')); ?>
                <div class="modal-header">
                    <h3 class="modal-title">Thêm đơn hàng</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- <form id="formCreateOrder"> -->
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        unset($shopList['All']);
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode'), 'id="selectShopOrder" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control select2" style="width:100%"');
                                        ?>
                                    </div>
                                    <?php if(!empty($orderTypeInv)):?>
                                    <div class="form-group">
                                        <label class="control-label" for="orderType">Loại đơn hàng</label>
                                        <?php
                                        echo form_dropdown('orderTypeInv', $orderTypeInv, set_value('orderTypeInv'), 'id="orderTypeInv" data-placeholder="Chọn loại đơn hàng" class="form-control select2" style="width:100%"');
                                        ?>
                                    </div>
                                    <?php endif;?>
                                    <div class="form-group">
                                        <label class="control-label" for="address"><?= lang("address"); ?></label>
                                        <?php
                                        echo form_dropdown('shopAddress', $shopAddress, set_value('shopAddress'), 'id="selectShopAddress" class="form-control select2" disabled style="width:100%"');
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="orderCode">Mã đơn hàng</label>
                                        <?= form_input('orderCode', set_value('orderCode', ''), 'class="form-control" id="orderCode" disabled '); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="date">Ngày</label>
                                        <?= form_input('orderDate', set_value('date', date('d-m-Y')), 'class="form-control datepicker" id="orderDate" disabled '); ?>
                                    </div>
                                    <div class="form-group hidden">
                                        <label class="control-label" for="status">Status</label>
                                        <?= form_input('orderStatus', set_value('status', 0), 'class="form-control" id="orderStatus" disabled '); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Ghi chú</label>
                                        <?= form_textarea('orderNote', set_value('note'), 'id="orderNote" class="form-control" ') ?>
                                    </div>
                                    <!-- </form> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-warning">Lưu</button>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript">

    var headerTable, detailTable;
    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        headerTable = $('#orderTable').DataTable({
            lengthMenu: [[50, 100, 200, -1], [50, 100, 200, "All"]],
            dom: 'lBfrtip',
            autoWidth: false,
            scrollX: true,
            scrollY: 400,
            deferRender: true,
            scrollCollapse: true,
            select: {
                info: false,
                style: 'single'
            },
            search: {
                "caseInsensitive": true,
            },
            ajax: {
                url: '<?=site_url('api/business/orderList');?>',
                type: 'GET',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    state: function () {
                        return $('#stateOrder').val();
                    },
                    type: function () {
                        return 'create';
                    },
                    orderType: function () {
                        return $('#orderType').val();
                    }
                },
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            buttons: [
                <?php if( check_permission('orders:Export') ):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    exportOptions: {
                        <?php if( check_permission('orders:Update')):?>
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
                        <?php else:?>
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                        <?php endif;?>
                        modifier: {
                            selected: null
                        }
                    },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    }
                }
                <?php endif;?>
            ],
            order: [[6, "desc"]],
            fixedColumns: {
                leftColumns: 3
            },
            columns: [
                <?php if( $type != 'sended' && (check_permission('orders:Update') || check_permission('orders:Delete')) ):?>
                {
                    "data": null, "searchable": false, "orderable": false,
                    render: function (data, type, row, meta) {
                        if (row.statusString == 'sended') return '';
                        var btn = '<div class="text-center">';

                        <?php if( check_permission('orders:Update')):?>
                        btn += '<a class="btn btn-success btn-xs sendOrder" onclick="activeOrder(this);return false;" href="javascript:void(0);" >Gửi đơn</a>';
                        <?php endif;?>
                        <?php if( check_permission('orders:Delete')):?>
                        btn += '<a class="btn btn-danger btn-xs deleteOrder" onclick="activeOrder(this);return false;" href="javascript:void(0);" style="margin-left:5px">Hủy đơn</a>';
                        <?php endif;?>
                        btn += '</div>';

                        return btn;
                    }
                },
                <?php endif;?>
                {data: "shopCode"},
                {data: "shopName"},
                {data: "shopModel", searchable: false},
                {data: "shopType"},
                {data: "orderType"},
                {data: "invDate", render: formatDate, type: "date-eu"},
                {data: "invCode", className: "dt-body-left"},
                {data: "note", className: "dt-body-left"},
                {data: "totalAmount", className: "dt-body-right", render: formatIntegerSeparator},
                {
                    data: null, render: function (data, type, row) {
                        if (row.statusString == 'processing') {
                            return 'Đang xử lý';
                        } else {
                            return 'Đã gửi';
                        }

                    }
                },
                {data: "ttpp",},
                {data: "distributor"},
                {data: "orgCode"},
            ]

        });

        detailTable = $('#orderDetail').DataTable({
            lengthMenu: [[50, 100, 200, -1], [50, 100, 200, "All"]],
            dom: 'lBfrtip',
            autoWidth: false,
            scrollX: true,
            scrollY: 400,
            deferRender: true,
            scrollCollapse: true,
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            buttons: [],
            fixedColumns: {
                leftColumns: 2
            },
        });
        $('#search_table').on('keyup change', function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (((code == 13 && headerTable.search() !== this.value) || (headerTable.search() !== '' && this.value === ''))) {
                headerTable.search(this.value).draw();
            }
        });


        /**
         * [datadetail Table]
         * @type {[type]}
         */
        headerTable.on('user-select', function (e, dt, type, cell, originalEvent) {
            if (cell.index().column == 0 && (cell.data().status == 0 || cell.data().status == 1)) return false;
            let row = dt.row(cell.index().row).node();
            $(row).removeClass('hover');
            if ($(row).hasClass('selected')) {
                e.preventDefault();
                // deselect
            } else {
                $('input[type=checkbox]').iCheck('uncheck');
                $('.checkbox').show();
                let rowData = headerTable.row(row).data();
                let idParent = rowData.invId;
                detailTable.destroy();
                detailTable = $('#orderDetail').DataTable({
                    lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                    dom: 'lBfrtip',
                    scrollX: true,
                    scrollY: 400,
                    deferRender: true,
                    scrollCollapse: true,
                    pageLength: -1,
                    ajax: {
                        url: '<?=site_url('api/business/orderDetail');?>' + '?id=' + idParent + '&shopCode=' + rowData.shopCode,
                        type: 'GET',
                        data: function (d) {
                            d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                        },
                        dataSrc: function (json) {
                            let totalPrice = 0;
                            let responseData = json.data;
                            responseData.forEach(function print(element) {
                                totalPrice += element.TOTAL;
                            });
                            rowData.totalAmount = totalPrice;
                            headerTable.row(row).data(rowData).draw();
                            return responseData;

                        }
                    },
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sIfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                        "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                    },
                    buttons: [
                        <?php if( check_permission('orders:Export')):?>
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                            className: 'btn btn-warning',
                            footer: false,
                            filename: function () {
                                let headerData = getDataSelectedRowDatatable(headerTable)[0];
                                return 'Đơn đặt hàng(' + headerData.invCode + ')_(' + headerData.shopCode + ')' + headerData.shopName;
                            },
                            title: function () {
                                let headerData = getDataSelectedRowDatatable(headerTable)[0];
                                return 'Cửa hàng: ' + '(' + headerData.shopCode + ')' + headerData.shopName;
                            },
                            messageTop: function () {
                                let headerData = getDataSelectedRowDatatable(headerTable)[0];
                                return 'Mã đơn: ' + headerData.invCode + ' | Ngày tạo: ' + formatDate(headerData.invDate);
                            },
                            // exportOptions: { columns: [ 1, 2, 3, 4, 6, 7 ] },
                            init: function (api, node, config) {
                                $(node).removeClass('btn-default')
                            }
                        }
                        <?php endif;?>
                    ],
                    fixedColumns: {
                        leftColumns: 2
                    },
                    columns: [
                        {data: "MTL_CODE", width: 100},
                        {data: "MTL_NAME"},
                        {data: "UNIT_CODE"},
                        {data: "QTY_ORDER", className: "dt-body-right editable", render: formatIntegerSeparator},
                        {data: "PRICE", className: "dt-body-right", render: formatIntegerSeparator},
                        {
                            data: null, className: "dt-body-right", render: function (data, type, row) {
                                // return currencyFormat(row['PRICE']*row['QTY_ORDER'])
                                row.TOTAL = row.PRICE * row.QTY_ORDER;
                                return formatIntegerSeparator(row.TOTAL);
                            }
                        },
                    ],
                    rowCallback: function (row, data, index, cells) {
                        // console.log(cells);
                        // $(row).attr("idl_id" , data.IDL_ID);
                        // $(row).attr("inv_id" , data.INV_ID);
                        // $(row).attr("code" , data.MTL_CODE);
                        // $(row).attr("name" , data.MTL_NAME);
                        // $(row).attr("unit" , data.UNIT_CODE);
                        // $(row).attr("qty_order" , data.QTY_ORDER);
                        // $(row).attr("qty_delivery" , data.QTY_DELIVERY);
                        // $(row).attr("price" , data.PRICE);
                        // $(row).attr("type", 1);
                    },
                    initComplete: function (settings, json) {
                        // console.log(detailTable.rows({filter: 'applied'}).data().length);
                        if (detailTable.rows({filter: 'applied'}).data().length == 0) {
                            $('input[type=checkbox]').iCheck('check');
                        } else {
                            $('input[type=checkbox]').iCheck('uncheck');
                        }
                    }

                });
            }
        });

        $('input[type=checkbox]').on('ifChecked', function (event) {
            // $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().fixedColumns().relayout();
            detailTable.draw();
        });

        $('input[type=checkbox]').on('ifUnchecked', function (event) {
            // $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().fixedColumns().relayout();
            detailTable.draw();
        });


        $("#updateOrderDetail").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    type: "POST",
                    dataType: 'json',
                    data: $(form).serializeArray(),
                    url: $('#updateOrderDetail').attr('action'),
                    success: function (e) {
                        if (typeof e.dataReturn && e.dataReturn.error === true) {
                            $('#staticModal').on('show.bs.modal', function (event) {
                                var modal = $(this);
                                modal.find('.modal-body').text(e.dataReturn.message);
                                modal.find('.href').hide();
                            });
                            $('#staticModal').modal('show');
                            return true;
                        } else {
                            $('#updateOrderDetail').trigger("reset");
                            $('#modalformeditOrderDetail').modal('hide');
                            detailTable.ajax.reload();
                        }
                    },
                    error: function (jqXHR, textStatus) {
                    }
                })
            }
        });


        <?php if( $type != 'sended' && check_permission('orders:Update')):?>
        $("#orderDetail").on("click", "tbody tr td.editable", function (e) {
            let headerData = getDataSelectedRowDatatable(headerTable)[0];
            if (headerData.statusString !== 'processing') {
                $(this).removeClass('editable');
                return
            }
            // if( ['kienlonggroup', 'dongtamgroup'].includes(headerData.parentId) && headerData.status > 1){
            //     $(this).removeClass('editable');
            //     return;
            // }else if(headerData.status > 0){
            //     $(this).removeClass('editable');
            //     return;
            // }
            if ($(this).find('input').length) return;
            let tr = $(this).closest('tr');
            let row = detailTable.row(tr);
            let rowData = row.data();
            let html = '<input class="form-control input-text" min="0" onkeypress="validateNumber()" type="number">';
            $(this).html("").append(html);
            $(this).find("input").focus().val(rowData.QTY_ORDER);

        });
        $('#orderDetail').on('focusout', 'tbody tr td input', async function (e) {
            let headerData = getDataSelectedRowDatatable(headerTable)[0];
            let tr = $(this).closest('tr');
            let row = detailTable.row(tr);
            let rowData = row.data();
            let params = {
                shopCode: headerData.shopCode,
                invId: headerData.invId,
                detailId: rowData.IDL_ID,
                mtlCode: rowData.MTL_CODE,
                mtlConvertCode: rowData.MTL_CONVERT_CODE,
                mtlName: rowData.MTL_NAME,
                unit: rowData.UNIT_CODE,
                price: rowData.PRICE,
                qtyOrder: parseInt($(this).val()),
                qtyApprove: rowData.QTY_APPROVE,
                state: headerData.statusString
            };
            if (params.qtyOrder != rowData.QTY_ORDER) {
                let result = await saveOrderDetail(params);
                if (result.success) {
                    rowData.IDL_ID = result.data.detailId;
                    rowData.QTY_ORDER = params.qtyOrder;
                    rowData.TOTAL = params.qtyOrder * params.price;
                }
            }
            detailTable.row(row).data(rowData).draw();
            let newTotalAmount = sumByProperty(detailTable.data().toArray().filter(detail => detail.QTY_ORDER > 0), 'TOTAL')
            headerData.totalAmount = newTotalAmount;
            let trHead = headerTable.row(headerTable.rows({selected: true})[0]);
            headerTable.row(trHead).data(headerData).draw();
            // table.row(headerData).data(headerData).draw()
            // console.log(headerData);
            // console.log();
            // console.log(detailTable.data().toArray().filter(detail => detail.QTY_ORDER > 0).reduce((prev, cur) => prev + cur['TOTAL'], 0));
        });

        function saveOrderDetail(params) {
            let url = '<?=site_url('api/business/orderDetail?t=');?>' + getTimeString();
            return new Promise((resolve, reject) => {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: params,
                    dataType: 'json',
                    // async:true
                    success: function (response) {
                        resolve(response)
                    },
                    error: function (error) {
                        reject(error)
                    },
                });
            });
        }
        <?php endif;?>



        <?php if( check_permission('orders:insert')):?>
        $('#btnNewOrder').click(function () {
            let d = new Date();
            $('#selectShopOrder').val(''); // Select the option with a value of '1'
            $('#selectShopOrder').trigger('change'); // Notify any JS components that the value changed
            $('#orderCode').val(d.yyyymmdd());
        });

        $('#selectShopOrder').on('change', function (e) {
            let shop = $(this).val();
            $('#selectShopAddress').val(shop);
            $('#selectShopAddress').trigger('change'); // Notify any JS components that the value changed
        });

        //Submit Order
        $("#formCreateOrder").validate({
            rules: {
                shopCode: {
                    required: true
                },
            },
            messages: {
                shopCode: {
                    required: 'Vui lòng chọn cửa hàng'
                },
            },
            submitHandler: function (form) {
                let shopCode = $('#selectShopOrder').val();
                let orderCode = $('#orderCode').val();
                let orderDate = $('#orderDate').val();
                let orderStatus = $('#orderStatus').val();
                let orderNote = $('#orderNote').val();
                let orderTypeInv = $('#orderTypeInv').val();
                let data = {
                    shopCode: shopCode,
                    orderCode: orderCode,
                    orderDate: orderDate,
                    orderStatus: orderStatus,
                    orderNote: orderNote,
                    orderType: orderTypeInv
                };
                $.post($(form).attr('action'), data, function (data) {
                    if (data.success) {
                        alert('Tạo đơn hàng mới thành công.');
                        $('#orderModal').modal('hide');
                        if (detailTable != null) detailTable.clear().draw();
                        headerTable.ajax.reload();
                    } else {
                        alert(data.message);
                        return false;
                    }
                }, 'json');
            }
        });

        <?php endif;?>
        $('#btnSearch').click(function () {
            detailTable.clear().draw();
            headerTable.ajax.reload();
        });


    });


    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    <?php if( $type != 'sended' && check_permission('orders:Update')):?>
    function activeOrder(button) {
        let action = ($(button).hasClass('sendOrder')) ? 'send' : 'delete';
        let tr = $(button).closest('tr');
        let row = headerTable.row(tr);
        let rowData = row.data();
        let status = rowData.status;
        let parent = rowData.parentId;
        let invCode = rowData.invCode;
        let shopCode = rowData.shopCode;
        let shopName = rowData.shopName;
        let state = rowData.statusString;
        let msg = ''
        // console.log(rowData);
        if (action != 'delete') {
            msg = 'Số lượng sẽ không thể thay đổi sau khi gửi đơn.\nBạn có chắc chắn gửi đơn hàng \n' + invCode + ' (' + shopCode + ' - ' + shopName + ')?';
            if (['kienlonggroup', 'dongtamgroup', 'DAOTAO'].includes(parent)) {
                status = <?= $this->session->userdata('userId') != 'TNC0100000' ? 2 : 1 ?>;
            } else {
                status = 1;
            }
        } else {
            status = 3;
            msg = 'Bạn có muốn HỦY đơn hàng\n' + invCode + ' (' + shopCode + ' - ' + shopName + ')?';
        }
        if (confirm(msg)) {
            $.ajax({
                url: '<?=site_url('api/business/activeOrder?t=');?>' + getTimeString(),
                type: 'PUT',
                data: JSON.stringify({id: rowData.invId, shopCode: rowData.shopCode, status: status, state: state}),
                // dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        if ($(tr).hasClass('selected')) {
                            detailTable.clear().draw();
                            $('.checkbox').hide();
                        }
                        row.remove().draw();
                        if (action == 'delete') {
                            alert('Hủy đơn hàng thành công.');
                        } else {
                            alert('Gửi đơn hàng thành công.');
                        }
                    } else {
                        if (action == 'delete') {
                            alert('Hủy đơn hàng không thành công.');
                        } else {
                            alert(response.message);
                        }
                    }
                }
            });
        }
    }
    <?php endif;?>
</script>

<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
  .table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #FFBC10;
  }
  .table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td, .table-bordered>thead>tr>td{
    text-align: center;
  }
</style>
<section class="content">
<?php if(!empty($qtyStore)):?>
  <div class="row reporttable">
  <div class="col-md-4 col-lg-4 col-xs-12">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><span class="sales"><?=$qtyStore['TOTAL']?></span></h3>
        <p>Tổng số</p>
      </div>
      <div class="icon">
        <i class="fa fa-university"></i>
      </div>
    </div>
  </div>

  <?php if(!empty($qtyStore['DETAILs'])):?>
    <?php foreach ($qtyStore['DETAILs'] as $key => $qtyStoreDetail) : ?>
      <div class="col-md-4 col-lg-4 col-xs-12">
        <p class="lead"><?=$qtyStoreDetail['SHOP_MODEL']?>: <?=$qtyStoreDetail['TOTAL']?></p>
        <div class="table-responsive">
          <table class="table table-bordered table-bordered-report">
            <tbody>
             <?php foreach ($qtyStoreDetail['DETAILs'] as $key => $qtyStoreStatus) :?>
              <tr>
                <th class="dt-body-left"><?=$qtyStoreStatus['STATUS_NAME']?>:</th>
                <td><?=$qtyStoreStatus['TOTAL']?></td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    <?php endforeach;?>
  <?php endif;?>
  </div>
<?php endif;?>
</section>
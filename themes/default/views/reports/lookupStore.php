<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td{white-space: nowrap;}
    .background-color{ background-color: #FFBC10 !important; }
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }
</style>
<?php
$v = "?v=1";
$v .= '&shopType='.$shopType;
$v .= '&provinceCode='.$provinceCode;
$v .= '&districtCode='.$districtCode;
$v .= '&shopModel='.$shopModel;
$v .= '&status='.$status;
?>
<script type="text/javascript">
    $(document).ready(function() {
        var tableConfig = {
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            scrollCollapse: true,
            scroller: true,
            deferLoading: false, // here
            // ajax : {
            //     url: '<?=site_url('api/report/lookupStore');?>',
            //     type: 'GET',
            //     "data": function ( d ) {
            //         d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
            //     }
            // },
            columnDefs : [
                // {"targets": 12, "type":"date-eu"},
                // {"targets": 13, "type":"date-eu"},
                // {"targets": 14, "type":"date-euro"},
                // {"targets": 15, "type":"date-eu"},
            ],
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng <b>(_TOTAL_)</b>"
            },
            order: [[ 1, "asc" ]],
            fixedColumns: {
                leftColumns: 2
            },
            buttons: [
            <?php if( check_permission(  'lookupStore:Export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    //exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
            <?php endif;?>  
            ],
            columns: [
                { "data": "SHOP_CODE"},
                { "data": "SHOP_NAME"},
                { "data": "ADDRESS_FULL"},
                { "data": "SHOP_TYPE"},
                { "data": "LAT"},
                { "data": "LNG"},
                { "data": "SHOP_TEL"},
                { "data": "STATUS_NAME"},
                { "data": "DISTANCE"},
            ]
        };
        var table = $('#UTable').DataTable(tableConfig);
       

        // $('#UTable').wrap('<div id="scrooll_div"></div>');
        // $('#scrooll_div').doubleScroll();

        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });

        $('#btnSearch').click(function(){
            var latitude = document.getElementById('searchLat').value;
            var longitude = document.getElementById('searchLng').value;
            var address = document.getElementById('searchAddress').value;
            var shopType = document.getElementById('selectShopType').value;
            var shopModel = document.getElementById('selectShopModel').value;
            var distance = document.getElementById('searchDistance').value;
            if(!validatelatlng(latitude)){
                alert('Giá trị vĩ độ không hợp lệ.');
                return;
            }
            if(!validatelatlng(longitude)){
                alert('Giá trị kinh độ không hợp lệ.');
                return;
            }
            if(shopModel == '00') shopModel = 'All';
            let v = "?v=1";
            v += "&address=" + address;
            v += "&lat=" + latitude;
            v += "&lng=" + longitude;
            v += "&shopType=" + shopType;
            v += "&shopModel=" + shopModel;
            v += "&distance=" + distance;
            let urlApi = '<?=site_url('api/report/lookupStore');?>' + v;
            tableConfig.ajax = {
                url: urlApi,
                type: 'GET',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            };
            table.clear().draw();
            table.destroy();
            table = $('#UTable').DataTable( tableConfig );

        });

        function validatelatlng(latlng){
          //var latlngArray = latlng.split(",");
          //for(var i = 0; i < latlngArray.length; i++) {
            if(isNaN(latlng)){
              
              return false;
            }
          //}
          return true;
        }

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("reports/rptShop_info", array('method'=>'get','id' => 'myform'));?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Địa chỉ</label>
                                        <input id="searchAddress" type="search" class="form-control form-control-sm" name="">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Kinh độ (longitude)</label>
                                        <input id="searchLng" value="0" type="number" class="form-control form-control-sm" name="">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Vĩ độ (latitude)</label>
                                        <input id="searchLat" value="0" type="number" class="form-control form-control-sm" name="">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Bán kính (m)</label>
                                        <input id="searchDistance" value="1000" type="number" min="0" class="form-control form-control-sm" name="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('type')?></label>
                                        <?php
                                        echo form_dropdown('shopType', $shopTypeList, set_value('shoptype', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('model')?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopmodel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 hidden">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?= form_dropdown('province_code', $provinceList, set_value('province_code', $provinceCode), 'class="form-control select2" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 hidden">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('district_code', $districtList, set_value('district_code', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 hidden">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('status'); ?></label>
                                        <?php
                                        $statusArr['All'] = "Tất cả";
                                        $statusArr[1] = "Đang hoạt động";
                                        $statusArr[2] = "Sắp khai trương";
                                        $statusArr[3] = "Đã đóng cửa";
                                        //$statusArr['Không xác định'] = "Không xác định";
                                        ?>
                                        <?= form_dropdown('status', $statusArr, set_value('status', $status), 'class="form-control" id="status" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button id="btnSearch" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                    
                                </div>
                            </div>
                            <!-- <?= form_close();?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th>Mã cửa hàng</th>
                                            <th>Tên cửa hàng</th>
                                            <th>Địa chỉ</th>
                                            <th>Mô hình</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>SĐT</th>
                                            <th>Trạng thái</th>
                                            <th>Khoảng cách (m)</th>
                                            <!-- <th>SĐT chủ CH</th>
                                            <th>Loại hình</th>
                                            <th><?=lang('type')?></th>
                                            <th>Thuộc TTPP</th>
                                            <th>Thuộc Cung Ứng</th>
                                            <th>Nhà phân phối</th>
                                            <th>Ngày khai trương</th>
                                            <th>Ngày đóng</th>
                                            <th>Lần đăng nhập gần nhất</th>
                                            <th>Ngày BH gần nhất</th>
                                            <th><?=lang('status'); ?></th>
                                            <th></th>
                                            <th></th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

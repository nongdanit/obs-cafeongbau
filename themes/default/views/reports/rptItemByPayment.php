<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td{white-space: nowrap;}
    .background-color{ background-color: #FFBC10 !important; }
</style>

<script type="text/javascript">
    var baseApi = '<?=site_url('api/report/reportItemPayment');?>';
    var baseApiExport = '<?=site_url('api/report/reportItemPaymentExport');?>';
    var totalRecords = 0;
    var reportTable ;
    var currentFilter = {
        fromDate: '<?=date('d-m-Y', strtotime($fromDate))?>',
        toDate: '<?=date('d-m-Y', strtotime($toDate))?>',
        shopCode: 'All',
        shopType: 'All',
        provinceCode: 'All',
        districtCode: 'All',
        shopModel: 'All'
    }

    $(document).ready(function() {
        totalRecords = 0;
        
        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });
        reportTable = $('#SLRData').DataTable({
            // searchDelay: 3000,
            processing: false,
            serverSide: true,
            pageLength: 100,
            lengthMenu: [ [100, 200, 300, 400], [100, 200, 300, 400] ],
            dom: 'lBfrtip',
            scrollY: 400,
            scrollX: true,
            //scroller: true,
            deferRender:    true,
            scrollCollapse: true,
            searching: false,
            ajax: getReportData(),
            order: [[ 0, "desc" ]],
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if(check_permission( 'rptItemByPayment:export' )):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    },
                    action: function(e, dt, node, config) {
                        // let tableParams = reportTable.ajax.params();
                        // console.log(tableParams)
                        // let fromDate = tableParams.fromDate();
                        // let toDate = tableParams.toDate();
                        // let shopCode = tableParams.shopCode();
                        // let shopType = tableParams.shopType();
                        // let provinceCode = tableParams.provinceCode();
                        // let districtCode = tableParams.districtCode();
                        // let shopModel = tableParams.shopModel();

                        currentFilter.fromDate      = $('#fromDate').val()
                        currentFilter.toDate        = $('#toDate').val()
                        currentFilter.shopCode      = $('#selectShop').val()
                        currentFilter.shopType      = $('#selectShopType').val()
                        currentFilter.provinceCode  = $('#selectProvince').val()
                        currentFilter.districtCode  = $('#selectDistrict').val()
                        currentFilter.shopModel     = $('#selectShopModel').val()

                        // let status = tableParams.status();
                        // let search = reportTable.search();
                        // let exportUrl = `${baseApiExport}?fromDate=${fromDate}&toDate=${toDate}&shopCode=${shopCode}&shopType=${shopType}&provinceCode=${provinceCode}&districtCode=${districtCode}&shopModel=${shopModel}`;
                        let exportUrl = `${baseApiExport}?` + getParams();
                        downloadFile(exportUrl, "Chi tiết SP theo HT thanh toán_" + currentFilter.fromDate + '_' + currentFilter.toDate + '.xlsx');
                    }
                }
                <?php endif;?>
            ],
            columns: [  
                { data: "transDate", render: formatFullDate, type: "date-euro"},
                { data: "transId"},
                { data: "shopCode"},
                { data: "shopName"},
                { data: "shopAddress"},
                { data: "shopType"},
                { data: "itemGroup"},
                { data: "itemCode"},
                { data: "itemName"},
                { data: "quantity", className: 'dt-body-right', render: formatNumber},
                { data: "unit"},
                { data: "price", className: 'dt-body-right', render: formatNumber},
                { data: "discount", className: 'dt-body-right', render: formatNumber},
                { data: "amount", className: 'dt-body-right', render: formatNumber},
                { data: "ShopeePay", className: 'dt-body-right', render: formatNumber},
                { data: "Beamin", className: 'dt-body-right', render: formatNumber},
                { data: "Grab", className: 'dt-body-right', render: formatNumber},
                { data: "Momo", className: 'dt-body-right', render: formatNumber},
                { data: "NowFoody", className: 'dt-body-right', render: formatNumber},
                { data: "VNPay", className: 'dt-body-right', render: formatNumber},
                { data: "ZaloPay", className: 'dt-body-right', render: formatNumber},
                { data: "LoShip", className: 'dt-body-right', render: formatNumber},
                { data: "GoFood", className: 'dt-body-right', render: formatNumber},
                { data: "beFood", className: 'dt-body-right', render: formatNumber},
                { data: "Tiki", className: 'dt-body-right', render: formatNumber},
                { data: "Lazada", className: 'dt-body-right', render: formatNumber},
                { data: "Shopee", className: 'dt-body-right', render: formatNumber},
                { data: "SenMall", className: 'dt-body-right', render: formatNumber},
                { data: "TikTokShop", className: 'dt-body-right', render: formatNumber},
                { data: "Momo-QR", className: 'dt-body-right', render: formatNumber},
            ],
            initComplete: function(settings, json) {
                 console.log(json)
            },
            
        });
		$('#SLRData').on( 'length.dt', function ( e, settings, len ) {
		    document.getElementById('limit').value = len;
		});

        $('#btnSearch').click(function(e){
            // reportTable.clearPipeline();
            // reportTable.ajax.reload();

            currentFilter.fromDate      = $('#fromDate').val()
            currentFilter.toDate        = $('#toDate').val()
            currentFilter.shopCode      = $('#selectShop').val()
            currentFilter.shopType      = $('#selectShopType').val()
            currentFilter.provinceCode  = $('#selectProvince').val()
            currentFilter.districtCode  = $('#selectDistrict').val()
            currentFilter.shopModel     = $('#selectShopModel').val()

            reportTable.search('');
            reportTable.clearPipeline();
            reportTable.ajax.url(baseApi + '?' + getParams()).load();
        });

        function getParams() {
            let params = 'fromDate=' + currentFilter.fromDate
                + '&toDate=' + currentFilter.toDate
                + '&shopCode=' + currentFilter.shopCode
                + '&shopType=' + currentFilter.shopType
                + '&provinceCode=' + currentFilter.provinceCode
                + '&districtCode=' + currentFilter.districtCode
                + '&shopModel=' + currentFilter.shopModel;
            return params;
        }

        function resultCallback(json){
            totalRecords = json.recordsTotal;
        }

        function getReportData() {
            return $.fn.dataTable.pipeline({
                url: baseApi,
                pages: 10, // number of pages to cache,
                method: 'get',
                // data: {
                //     fromDate: function () {
                //         return $('#fromDate').val();
                //     },
                //     toDate: function () {
                //         return $('#toDate').val();
                //     },
                //     shopCode: function () {
                //         return $('#selectShop').val();
                //     },
                //     shopType: function () {
                //         return $('#selectShopType').val();
                //     },
                //     provinceCode: function () {
                //         return $('#selectProvince').val();
                //     },
                //     districtCode: function () {
                //         return $('#selectDistrict').val();
                //     },
                //     shopModel: function () {
                //         return $('#selectShopModel').val();
                //     },
                // },
                data: currentFilter
            }, resultCallback);
        }
    });
</script>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("reports/rptItemByPayment", array('method'=>'get', 'id' => 'myform'));?> -->
                            <div class="row">
                            	
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('type')?></label>
                                        <?php
                                        echo form_dropdown('shopType', $shopTypeList, set_value('shopType', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('model')?></label>
                                        <?php
                                            echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?= form_dropdown('provinceCode', $provinceList, set_value('provinceCode', $provinceCode), 'class="form-control select2 selectProvince" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('districtCode', $districtList, set_value('districtCode', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                               
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <input type="hidden" name="limit" id="limit" readonly value="<?php echo $limit; ?>">                                      
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button id="btnSearch" type="button" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                            <!-- <?= form_close();?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="SLRData" class="table table-striped table-bordered table-condensed">
                                    <thead>
                                        <tr class="hidden">
                                          <!--   <th class="background-color"><?=(!empty($addressShop))?"Địa chỉ:":""?></th>
                                            <th class="background-color" colspan="5"><?=$addressShop?></th> -->
                                            <th class="background-color" colspan="7"></th>
                                            <th class="background-color">Tổng:</th>
                                            <th class="background-color"></th>
                                            <th class="background-color"></th>
                                            <th class="background-color"></th>
                                            <th class="background-color"></th>
                                            <th class="background-color"></th>
                                            <th class="background-color"></th>
                                            <th class="background-color" id="totalAirPay"></th>
                                            <th class="background-color" id="totalBeamin"></th>
                                            <th class="background-color" id="totalGrab"></th>
                                            <th class="background-color" id="totalMomo"></th>
                                            <th class="background-color" id="totalNowFoody"></th>
                                            <th class="background-color" id="totalVNPay"></th>
                                            <th class="background-color" id="totalZaloPay"></th>
                                            <th class="background-color" id="totalGoFood"></th>
                                            <th class="background-color" id="totalbeFood"></th>
                                            <th class="background-color" id="totalTiki"></th>
                                            <th class="background-color" id="totalLazada"></th>
                                            <th class="background-color" id="totalShopee"></th>
                                            <th class="background-color" id="totalSenMall"></th>
                                            <th class="background-color" id="totalTikTokShop"></th>
                                            <th class="background-color" id="totalMomoQR"></th>
                                        </tr>
                                        <tr class="active">
                                            <th>Ngày BH</th>
                                            <th>Mã bill</th>  
                                            <th>Mã cửa hàng</th>
                                            <th>Cửa hàng</th>
                                            <th>Địa chỉ</th>                                           
                                            <th>Mô hình</th>
                                            <th>Nhóm</th>
                                            <th>Mã sản phẩm</th>
                                            <th>Tên sản phẩm</th>
                                            <th>SL</th>
                                            <th>ĐVT</th>
                                            <th>Giá</th>
                                            <th>Giảm giá</th>
                                            <th>Thành tiền</th>                             
                                            <th>ShopeePay</th>
                                            <th>Beamin</th>
                                            <th>Grab</th>
                                            <th>Momo</th>
                                            <th>NowFoody</th>
                                            <th>VNPay</th>
                                            <th>ZaloPay</th>
                                            <th>LoShip</th>
                                            <th>GoFood</th>
                                            <th>beFood</th>
                                            <th>Tiki</th>
                                            <th>Lazada</th>
                                            <th>Shopee</th>
                                            <th>SenMall</th>
                                            <th>TikTokShop</th>
                                            <th>Momo-QR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- <tr>
                                            <td colspan="12" class="dataTables_empty" style="text-align: center;"><?= lang('loading_data_from_server'); ?></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
</section>

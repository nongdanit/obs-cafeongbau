<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td{white-space: nowrap;}
    .background-color{ background-color: #FFBC10 !important; }
    
    .text-wrap{
        white-space:normal;
    }
    .modal-body {
        max-height: calc(100vh - 212px);
        overflow-y: auto;
    }
    .modal-lg {
        width: 85%;
    }
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }
    .select2-container--default .select2-results__option[aria-disabled=true] {
        display: none;
    }

    .dataTables_scrollHeadInner {
        width: 100% !important
    }
</style>
<?php
    $v = "?v=1";
    $v .= '&fromDate='.$fromDate;
    $v .= '&toDate='.$toDate;
    $v .= '&shopCode='.$shopCode;
    //$v .= '&stockType='.$stockType;
    $v .= '&status=0';
    //$v .= '&shopModel='.$model;
?>
<script type="text/javascript">
    var transferTable, transferDetailTable, selectedEditRow;
    var transferParams = '<?php echo $v; ?>'
    var baseUrl = '<?=site_url('');?>';
    var transferBaseApi = baseUrl + 'api/report/reportLocalTransfer';
    $(document).ready(function() {
        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });
        transferTable = $('#transferTable').DataTable({
            lengthMenu: [ [100, 200, 300, 400], [100, 200, 300, 400] ],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            //scroller: true,
            deferRender:    true,
            scrollCollapse: true,
            select: {
                info: false,
                style: 'single'
            },
            // ajax : { 
            //     url: transferBaseApi + transferParams,
            //     type: 'GET', 
            //     data: function ( d ) {
            //         d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
            //     }
            // },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            buttons: [
                <?php if( check_permission( 'rptlocaltransfer:export' )):?>
                { 
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    filename: function(){
                        return 'Báo cáo xuất nhập tồn_' + $('#selectShop').select2('data')[0].text + '_' + $('#fromDate').val() + '_' + $('#toDate').val();
                    },
                    exportOptions: { 
                        columns: [':visible'], 
                        format: {
                            body: function (data, row, column, node) {
                                let cellData;
                                cellData = data.indexOf("<") < 0 ? data : $(data).text();   // Some cells contains html elements. need to strip off
                                return cellData;
                                // return column === 10 ? '\u200C' + cellData : cellData;

                            }
                        },
                    },
                    customize: function( xlsx ) {
                        let sheet = xlsx.xl.worksheets['sheet1.xml'];
                        // Loop over the cells in column `C`
                        let datas = transferTable.data().toArray();
                        let i = 0;
                        $('row c[r^="K"]', sheet).each( function () {
                            // Get the value
                            //console.log($('is t', this).text());
                            if ( i > 0 ) {
                                //console.log(datas[i - 1]);
                                $(this).attr( 's', '52' );
                            }
                            i++;
                        });
                    },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    },
                }
                <?php endif;?>
            ],
            // fixedColumns: {
            //     leftColumns: 3
            // },
            order: [[ 0, "asc" ], [ 1, "asc" ]],
            // columnDefs: [
            //     {"targets": 5, "type":"date-eu"},
            // ],
            columns: [           
                { data: "SHOP_CODE"},
                { data: "MTL_CONVERT_CODE", className: "dt-body-left" },
                { data: "MTL_NAME", visible: true},
                { data: "SPEC", render: currencyFormat, className:'dt-body-right'},
                { data: "UNIT", visible: true },
                { data: "OPEN_QTY_VALUE", render: renderQTY, className:'dt-body-right'},
                { data: "INPUT_QTY_VALUE", render: renderQTY, className:'dt-body-right'},
                { data: "OUTPUT_QTY_VALUE", render: renderQTY, className:'dt-body-right'},
                { data: "STOCK_QTY_VALUE", render: renderQTY, className:'dt-body-right'},
                { data: "XUAT_QTY_VALUE", render: renderQTY, className:'dt-body-right'},
                { data: "USING_QTY_VALUE", render: renderQTY, className:'dt-body-right'},
                { data: "VARIANT", render: renderVariant, className:'dt-body-right'},
            ],
            // rowCallback: function( row, data, index ) {
            //     transferTable.columns(0).hide();
            //     // if ($('#invStatus').val() == 9) {
            //     //     $(row).hide();
            //     // }
            // },
        });
        function renderQTY(data){
            if(!Number.isInteger(data)){
                return $.fn.dataTable.render.number(',', '.', 2, '').display(data);
            }else{
                return $.fn.dataTable.render.number(',', '.', 0, '').display(data);
            }
        }

        function renderVariant(data, type, row){
            var variant = 0;
            if(row.USING_QTY != 0){
                variant = ((row.USING_QTY - row.XUAT_QTY) / row.USING_QTY) * 100;
                //variant = (row.USING_QTY_VALUE - row.XUAT_QTY_VALUE) / row.USING_QTY_VALUE;
                //variant = varian
            }
            //console.log(variant);
            var color = (variant < 0) ? 'text-red' : 'text-dark';
            if(!Number.isInteger(variant)){
                return '<div class="' + color + '">' + $.fn.dataTable.render.number(',', '.', 2, '').display(variant) + '%' + '</div>';
            }else{
                return '<div class="' + color + '">' + $.fn.dataTable.render.number(',', '.', 0, '').display(variant) + '%' + '</div>';
            }
        }
        
        //var dataRow;
        transferTable.on('user-select', function ( e, dt, type, cell, originalEvent ) {
            var row = dt.row( cell.index().row ).node();
            $(row).removeClass('hover');
            if ( $(row).hasClass('selected') ) {
                e.preventDefault();
                // deselect
            }else{
                //table.$('tr.selected').removeClass('selected');
                //$(this).addClass('selected');
                // $('.checkbox').show();
                //dataRow = transferTable.row(row).data();
                //console.log(dataRow);
                
            }
        });
        $('#btnSearch').click(function(e){
            // if($('#shop_code').val() == 'HEAD_OFFICE'){
            //     alert('Chọn cửa hàng để xem báo cáo.');
            //     return;
            // }
            if(transferTable.ajax.url() == null){
                transferTable.ajax.url({
                    url:transferBaseApi,
                    method: 'GET',
                    data:{
                        fromDate: function () {
                            return $('#fromDate').val();
                        },
                        toDate: function () {
                            return $('#toDate').val();
                        },
                        shopCode: function () {
                            return $('#selectShop').val();
                        },
                        shopModel: function () {
                            return $('#selectShopModel').val();
                        },
                        shopStatus: function () {
                            return $('#selectShopStatus').val();
                        },
                        type: function () {
                            return $('#rptType').val();
                        },
                    }
                });
            }
            transferTable.ajax.reload();
        });
    });
</script>
<style type="text/css">
    .table td:first-child { padding: 1px; }
    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) { text-align: center; }
    .table td:nth-child(9) { text-align: right; }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại báo cáo</label>
                                        <?php
                                        $rptType = array(
                                            0 => 'Ngày',
                                            1 => 'Tháng',
                                        );
                                        echo form_dropdown('rptType', $rptType, set_value('rptType', 1), 'id="rptType" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang("name_store"); ?></label>
                                        <?php
                                        // $listShop = array_merge(array('HEAD_OFFICE' => 'Tất cả'), $listShop);
                                        echo form_dropdown('shop_code', $shopList, set_value('shop_code', -1), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"');?>
                                    </div>
                                </div>
                                
                                
                                <!-- <div class="col-sm-6 col-md-3 hidden">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('type')?></label>
                                        <?php
                                            // $arr['All'] = 'Tất cả';
                                            // foreach($listShopType as $key => $value) {
                                            //     $arr[$value['TITLE']] = $value['TITLE'];
                                            // }
                                        echo form_dropdown('shopType', $shopTypeList, set_value('shopType', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div> -->
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= 'Loại hình'; ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('model', $shopModel), 'id="selectShopModel" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('status'); ?></label>
                                        <?php
                                        $statusArr['All'] = "Tất cả";
                                        $statusArr['1'] = "Đang hoạt động";
                                        $statusArr['3'] = "Đã đóng cửa";
                                        //$statusArr['4'] = "Không xác định";
                                        ?>
                                        <?= form_dropdown('status', $statusArr, set_value('status', $statusArr['All']), 'class="form-control" id="selectShopStatus" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                </div>
                                
                                <button id="btnSearch" type="button" class="btn btn btn-warning"><?= lang("search"); ?></button>
                               
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="transferTable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th>Mã cửa hàng</th>
                                            <th>Mã NVL</th>
                                            <th>Tên NVL</th>
                                            <th>Quy cách</th>
                                            <th>ĐVT</th>
                                            <th>Tồn ĐK</th>
                                            <th>Nhập TK</th>
                                            <th>Xuất TK</th>
                                            <th>Tồn CK</th>
                                            <th>Tiêu hao TT</th>
                                            <th>Tiêu hao TCT</th>
                                            <th>Chênh lệch</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- <tr>
                                            <td colspan="12" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
</section>


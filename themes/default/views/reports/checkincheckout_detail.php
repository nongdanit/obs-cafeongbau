<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }
    textarea {
        resize: vertical; /* user can resize vertically, but width is fixed */
    }

</style>
<?php
$v = "?v=1";
$v .= "&month=" . $month;
$v .= "&shopCode=" . $shopCode;
?>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <?= form_open("reports/checkincheckout_detail", array('method' => 'get', 'id' => 'myform')); ?>
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="month">Tháng</label>
                                        <?= form_input('month', set_value('month', date('m-Y', strtotime($month))), 'class="form-control monthpicker" id="month" '); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shop_code', $shopCode), 'id="shop_code" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                    <a href="<?= base_url('reports/checkincheckout_detail') ?>"
                                       class="btn btn btn-default"><?= lang("reset"); ?></a>
                                </div>
                            </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="tableCheckInCheckOut"
                                       class="table table-striped table-bordered table-condensed table-hover">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var reason  = JSON.parse('<?php echo $reason;?>');
    var URL_IMG_CHECK_IN_OUT = '<?php echo URL_IMG_CHECK_IN_OUT;?>';
    var table = null;

    function btnSaveTimeCheck(th, rowIndex, colIndex, rowId) {
        let tbl_row = $(th).closest('tr');
        let timeCheckout = tbl_row.find('input[type="text"]').val(),
            userName = tbl_row.attr('username'),
            shopCode = tbl_row.attr('shopcode'),
            date = tbl_row.attr('date'),
            start_time = tbl_row.attr('start_time'),
            note = tbl_row.find('textarea').val();
        if (note === '') {
            alert('Vui lòng nhập ghi chú');
            tbl_row.find('textarea').focus();
            return false;
        }

        const a = moment(date + ' ' + timeCheckout, 'DD-MM-YYYY HH:mm:ss')
        const b = moment(start_time, 'DD-MM-YYYY HH:mm:ss')
        // Tính khoảng cách giữa hai ngày theo phút
        // console.log(a.diff(b, 'minutes'))
        if (a.diff(b, 'minutes') > 0) {
            $.post("<?= base_url('api/report/updateEmpCheckOut')?>", {
                timecheckout: timeCheckout,
                note: note,
                username: userName,
                shopcode: shopCode,
                date: date,
                rowid: rowId
            }, function (e) {
                alert(e.dataReturn.message);
                //console.log(e.dataReturn.dataResult.totalTime);
                if (table != null) {
                    // table.cell({row: rowIndex, column: colIndex}).data(e.dataReturn.dataResult.totalTime).draw(true);
                }
                $('#staticModal').modal('hide');

            }, 'json');
        } else {
            alert('Thời gian checkout phải lớn hơn thời gian checkin');
            return false;
        }
    }

    function btnSaveHoliday(th, rowIndex, colIndex, rowId, userID) {
        let tbl_row = $(th).closest('tr');
        let timeCheckout = tbl_row.find('input[type="text"]').val(),
            userName = tbl_row.attr('username'),
            shopCode = tbl_row.attr('shopcode'),
            date = tbl_row.attr('date'),
            note = $.trim(tbl_row.find('textarea').val()),
            reason = tbl_row.find('select').val(),
            reason_title = tbl_row.find('select#reason option:selected').text(),
            code = tbl_row.find('select#reason option:selected').attr('code'),
            data = {
                note: note,
                userName: userName,
                shopCode: shopCode,
                user_id: userID,
                rowId: rowId,
                date: date,
                reason: reason,
                reason_title: reason_title,
                code: code
            };
        let text_show_talbe = code;
        if (reason == 2 && note.length == 0) {
            alert('Nhập lý do nghỉ.');
            return;
        }

        console.log(data);
        console.log(text_show_talbe)

        $.post("<?= base_url('api/report/addAnnualLeave')?>", data, function (e) {
            alert(e.dataReturn.message);
            if (e.dataReturn.error == false) {
                if (table != null) {
                    table.cell({row: rowIndex, column: colIndex}).data(text_show_talbe).draw(true);
                }
            }

            $('#staticModal').modal('hide');
        }, 'json');
    }

    function loadTable() {
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: '<?=site_url('api/report/checkincheckoutdetail' . $v);?>',
            success: function (response) {
                // console.log(response);
                if (response.data.length > 0) {
                    table = $('#tableCheckInCheckOut').removeAttr('width').DataTable({
                        lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        dom: 'Blfrtip',
                        select: {
                            info: false,
                            style: 'single'
                        },
                        data: response.data,
                        columns: response.columns,
                        oLanguage: {
                            "sEmptyTable": "Không có dữ liệu",
                            "sSearch": "Tìm kiếm nhanh:",
                            "sLengthMenu": "Hiển thị _MENU_ dòng",
                            "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                            "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                        },
                        search: {
                            "caseInsensitive": true,
                        },
                        // select: true,
                        // searching: false,
                        scrollX: true,
                        scrollCollapse: true,
                        fixedColumns: {
                            leftColumns: 3
                        },
                        buttons: [
                            <?php if( check_permission('checkincheckout:Export')):?>
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                                className: 'btn btn-warning m-r-sm',
                                init: function (api, node, config) {
                                    $(node).removeClass('btn-default')
                                }
                            },
                            {
                                text: '<i class="fa fa-file-excel-o"></i> Xuất Dữ liệu chi tiết',
                                className: 'btn btn-danger',
                                footer: false,
                                exportOptions: {columns: [1, 2, 3, 4, 5, 6]},
                                init: function (api, node, config) {
                                    $(node).removeClass('btn-default')
                                },
                                action: function (e, dt, node, config) {
                                    let exportUrl = '<?=site_url('api/report/reportAttendanceDetailExport' . $v);?>';
                                    // let toDate = tableParams.toDate();
                                    // let shopCode = tableParams.shopCode();
                                    // let search = table.search();
                                    // let exportUrl = `<?=site_url('api/report/reportTransDetailExport');?>?fromDate=${fromDate}&toDate=${toDate}&shopCode=${shopCode}&search=${search}`;
                                    downloadFile(exportUrl, 'Báo cáo chấm công chi tiết.xlsx');
                                }
                            }
                            <?php endif;?>
                        ]
                    });
                    $('div.dataTables_length select').addClass('form-control');

                    table.on('click', 'tr td', function (e) {
                        let td = $(this);
                        let cell = table.cell(td);
                        let colIndex = td.index();
                        let rowIndex = table.row(td.closest('tr')).index();
                        //visible col 1
                        colIndex = colIndex + 1
                        if (colIndex < 9) return;
                        console.log(colIndex);
                        let rowData = getDataSelectedRowDatatable(table)[0];
                        let cellData = cell.data();
                        console.log(cellData)
                        // Kiểm tra
                        console.log(reason.some(number => number == cellData)) // true
                        let checkCellData = reason.some(number => number == cellData)

                        // if (cellData != '' && cellData != 'P' && cellData != 'PSN' && cellData != 'POD' && cellData != 'L' && cellData != 'PK') {
                        if (cellData != '' && !checkCellData) {
                            console.log(1)
                            let username = rowData['Số điện thoại'];
                            let user_id  = rowData['user_id'];
                            let shopcode = rowData['Mã cửa hàng'];
                            let full_name = rowData['Họ và tên'];
                            let numday = 2;
                            let edit = true;
                            var currentTime = '<?php echo $currentDate;?>'.split("-");
                            var currentDate = new Date(parseInt(currentTime[2]), parseInt(currentTime[1]) - 1, parseInt(currentTime[0]) - numday);

                            let title = table.column(colIndex).header();
                            let header_title = $(title).html();
                            var dob = new Date(header_title.substring(6, 10), header_title.substring(3, 5) - 1, header_title.substring(0, 2)).toLocaleString("en-US", {timeZone: "Asia/Ho_Chi_Minh"});
                            dob = new Date(dob);
                            if ( currentDate.getTime() > dob.getTime() ) {
                                edit = false;
                            }
                            $.ajax({
                                url: '<?=site_url('api/report/getInfoEmployeeWithGridTable' . $v);?>',
                                type: 'post',
                                data: {header_title: header_title, username: username, shopcode: shopcode, user_id: user_id, type: 1},
                                dataType: 'json',
                                success: function (returnedData) {
                                    if (returnedData.data.length > 0) {
                                        let data = returnedData.data;
                                        console.log(data)
                                        let s = '<div class="post">' +
                                            '<div class="user-block">' +
                                            '<h3>' + full_name + '</h3>(' + username + ')' +
                                            '</div></div>';

                                        s += '<table class="table table-bordered">' +
                                            '<tbody><tr>' +
                                            '<th style="width: 10px">#</th>' +
                                            '<th style="width: 100px">Thời gian <br> check in</th>' +
                                            '<th>Ảnh check in</th>' +
                                            '<th style="width: 100px">Thời gian <br> check out</th>' +
                                            '<th>Ảnh check out</th>';
                                        s += '<th>Ghi chú</th>';
                                        s += '</tr>';

                                        for (const i in data) {
                                            let end_date;
                                            if (formatDate(data[i]['check_out']) != '') {
                                                end_date = formatDate(data[i]['check_out'])
                                            } else end_date = header_title;
                                            if (edit) {
                                                s += '<tr start_time="' + formatFullDate(data[i]['check_in']) + '" date="' + end_date + '" shopcode="' + data[i]['shop_code'] + '" username="' + username + '" >' +
                                                    '<td>' + (Number(i) + 1) + '</td>' +
                                                    '<td>' + formatFullDate(data[i]['check_in']) + '</td>' +
                                                    '<td style="text-align: center;"> <img data-action="zoom" src="' + URL_IMG_CHECK_IN_OUT + data[i]['img_in'] + '" style="width: 100px;" alt="Avatar"> </td>' +
                                                    '<td>' + end_date + ' <input type="text" name="start_time" value="' + formattime(data[i]['check_out']) + '" class="form-control datetimepicker start_time" style="width: 90px;display: initial;"></td>' +
                                                    '<td style="text-align: center;"> <img data-action="zoom" src="' + URL_IMG_CHECK_IN_OUT + data[i]['img_out'] + '" style="width: 100px;" alt="Avatar"> </td>';

                                                let note = data[i]['description'];
                                                if(note == undefined || note == 'undefined' || note == null){
                                                    note = '';
                                                }
                                                s += '<td><textarea class="form-control" name="note">' + note + '</textarea></td>';

                                                if (data[0]['phone'] != '<?= $this->session->userdata('userId') ?>') {
                                                    s += '<td><button type="button" onclick="btnSaveTimeCheck(this,' + rowIndex + ',' + colIndex + ',' + data[i]['id'] + ')" class="btn btn-warning bt-save">Lưu</button></td>';
                                                }
                                                s += '</tr>';
                                            } else {
                                                let note = data[i]['description'];
                                                if(note == undefined || note == 'undefined' || note == null){
                                                    note = '';
                                                }
                                                s += '<tr>' +
                                                    '<td>' + (Number(i) + 1) + '</td>' +
                                                    '<td>' + formatFullDate(data[i]['check_in']) + '</td>' +
                                                    '<td style="text-align: center;"> <img data-action="zoom" src="' + URL_IMG_CHECK_IN_OUT + data[i]['img_in'] + '" style="width: 100px;" alt="Avatar"> </td>' +
                                                    '<td>' + formatFullDate(data[i]['check_out']) + '</td>' +
                                                    '<td style="text-align: center;"> <img data-action="zoom" src="' + URL_IMG_CHECK_IN_OUT + data[i]['img_out'] + '" style="width: 100px;" alt="Avatar"> </td>' +
                                                    '<td>' + note + '</td>' +
                                                    '</tr>';
                                            }
                                        }


                                        s += '</tbody></table>';

                                        $('#staticModal').on('show.bs.modal', function (event) {
                                            let modal = $(this);
                                            modal.find('.modal-title').text('Thông tin chi tiết')
                                            modal.find('.modal-dialog').removeClass('modal-sm').addClass('modal-lg');
                                            modal.find('.modal-body').html(s);
                                            modal.find('.bt-action').text('Đóng');
                                            modal.find('.href').hide();
                                        });
                                        $('#staticModal').on('hidden.bs.modal', function () {
                                            // do something…
                                            if (table != null) table.rows('.selected').deselect();
                                        });
                                        $('#staticModal').modal('show');
                                    }
                                }
                            });
                        // } else if ( (cellData != '' && (cellData == 'P' || cellData == 'L' || cellData == 'PSN' || cellData == 'POD') ) ||
                        //     (cellData == '') ||
                        //     (cellData == 'PK') ) {

                        } else if ( (cellData != '' && checkCellData ) ||
                            (cellData == '')
                             ) {

                            console.log(2)
                            // let rowData = table.row(row).data();
                            let username = rowData['Số điện thoại'];
                            let user_id  = rowData['user_id'];
                            let shopcode = rowData['Mã cửa hàng'];
                            let full_name = rowData['Họ và tên'];
                            let title = table.column(colIndex).header();
                            let header_title = $(title).html();

                            var currentTime = '<?php echo $currentDate;?>'.split("-");
                            var currentDate = new Date(parseInt(currentTime[2]), parseInt(currentTime[1]) - 1, parseInt(currentTime[0]));
                            var currentYear = currentDate.getFullYear();
                            var currentMonth = currentDate.getMonth();
                            var currentDate = currentDate.getDate();
                            var currentStartDate = null;
                            var currentEndDate = null;

                            var dob = new Date(header_title.substring(6, 10), header_title.substring(3, 5) - 1, header_title.substring(0, 2)).toLocaleString("en-US", {timeZone: "Asia/Ho_Chi_Minh"});
                            dob = new Date(dob);
                            if (currentDate > 25) {
                                    var currentMonth_step_startDate = 0;
                                    var currentMonth_step_endDate = 1;
                                if (currentMonth == 11) {// Tháng 12
                                    currentEndDate = new Date(currentYear + 1, 0, 25);
                                    currentStartDate = new Date(currentYear, 11, 26);
                                } else if (currentMonth == 0) { // Tháng 1
                                    currentEndDate = new Date(currentYear, currentMonth + 1, 25);
                                    currentStartDate = new Date(currentYear, currentMonth, 26);
                                } else {
                                    currentEndDate = new Date(currentYear, currentMonth + currentMonth_step_endDate, 25);
                                    currentStartDate = new Date(currentYear, currentMonth + currentMonth_step_startDate, 26);
                                }
                            } else {
                                var currentMonth_step_startDate = -1;
                                var currentMonth_step_endDate = 0;
                                currentEndDate = new Date(currentYear, currentMonth + currentMonth_step_endDate, 25);
                                if (currentMonth == 0) {
                                    currentStartDate = new Date(currentYear - 1, 11, 26);
                                } else {
                                    currentStartDate = new Date(currentYear, currentMonth +  currentMonth_step_startDate, 26);
                                }
                            }

                            // console.log(dob)
                            // console.log(currentStartDate)
                            // console.log(currentEndDate)
                            // console.log(dob.getTime())
                            // console.log(currentStartDate.getTime())
                            // console.log(currentEndDate.getTime())

                            if (currentStartDate.getTime() <= dob.getTime() && dob.getTime() <= currentEndDate.getTime()) {
                                $.ajax({
                                    url: '<?=site_url('api/report/getInfoEmployeeWithGridTable' . $v);?>',
                                    type: 'post',
                                    data: {header_title: header_title, username: username, shopcode: shopcode, user_id: user_id, type: 2},
                                    dataType: 'json',
                                    success: function (returnedData) {
                                        console.log(returnedData);
                                        if (returnedData.data.reason.length > 0) {
                                            var data = returnedData.data.reason;
                                            var balance = returnedData.data.balance;
                                            let rowId = balance.id == undefined ? null : balance.id;
                                            var s = '<div class="post">' +
                                                '<div class="user-block">' +
                                                '<h3>' + full_name + '</h3>(' + username + ')' +
                                                '</div></div>';

                                            s += '<div style="overflow: auto"><table class="table table-bordered">' +
                                                '<thead>' +

                                                '<tr>' +
                                                '<th>Ngày</th>' +
                                                '<th>Duyệt phép</th>';
                                            s += '<th>Lý do</th>';
                                            s += '</tr>';
                                            s += '</thead>';
                                            let end_date = header_title;
                                            s += '<tr date="' + end_date + '" rowid="' + user_id + '" shopcode="' + shopcode + '" username="' + username + '" >' +
                                                        '<td>' + end_date + '</td>' +
                                                        '<td>' +
                                                        '<select class="form-control select2" name="reason" id="reason" style="width:100%">';

                                            if (data.length > 0) {
                                                for (const i in data) {
                                                    let selected = '';
                                                    if ( data[i]['id'] == balance.reason_id) {
                                                        selected = 'selected';
                                                    }
                                                    s += '<option '+selected+' code="'+data[i]['code']+'" value="'+data[i]['id']+'">'+data[i]['name']+'</option>';

                                                }

                                                s += '</select></td>';
                                                let note = balance.description;
                                                if(balance.description == undefined || balance.description == 'undefined'){
                                                    note = '';
                                                }
                                                s += '<td><textarea rows="5" class="form-control" name="note" style="max-width: 100%">' +  note + '</textarea></td>';

                                                if (username != '<?= $this->session->userdata('userId') ?>') {
                                                    s += '<td><button type="button" onclick="btnSaveHoliday(this,' + rowIndex + ',' + colIndex + ',' + rowId + ',' + user_id + ')" class="btn btn-warning bt-save">Lưu</button></td>';
                                                }
                                                s += '</tr>';

                                            }

                                            s += '</tbody></table></div>';

                                            $('#staticModal').on('show.bs.modal', function (event) {
                                                var modal = $(this);
                                                modal.find('.modal-title').text('Duyệt phép')
                                                modal.find('.modal-dialog').removeClass('modal-sm').addClass('modal-lg');
                                                modal.find('.modal-body').html(s);
                                                modal.find('.bt-action').text('Đóng');
                                                modal.find('.href').hide();
                                            });
                                            $('#staticModal').on('hidden.bs.modal', function () {
                                                // do something…
                                                if (table != null) table.rows('.selected').deselect();
                                            });
                                            $('#staticModal').modal('show');
                                        }
                                    }
                                });
                            } else {
                                alert('Dữ liệu đã chốt tính lương, không thể duyệt phép nữa');
                                return false;
                            }
                        }
                        // alert(td.index());
                    });

                    table.on('user-select', function (e, dt, type, cell, originalEvent) {
                        // if (cell.index().column == 0 && (cell.data().status == 1 || cell.data().status == 2)) return false;
                        let row = dt.row(cell.index().row).node();
                        $(row).removeClass('hover');
                        if ($(row).hasClass('selected')) {
                            e.preventDefault();
                            // deselect
                        }
                    });

                    // table.on('user-select', function (e, dt, type, cell, originalEvent) {
                    //     let rowIndex = cell.index().row;
                    //     let colIndex = cell.index().column;
                    //     if (colIndex < 10) return;
                    //     let row = dt.row(cell.index().row).node();
                    //     //get the initialization options
                    //     let columns = table.settings().init().columns;
                    //     if ($(row).hasClass('selected')) {
                    //         e.preventDefault();
                    //     } else {
                    //
                    //
                    //     }
                    // });
                } else {
                    $('#tableCheckInCheckOut').html('<thead><tr><th>Không có dữ liệu</th></tr></thead>');
                }
            }
        });
    }

    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });

        loadTable();


    });
</script>

<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td{white-space: nowrap;}
    .background-color{ background-color: #FFBC10 !important; }
   
    #priceDetailModel .modal-dialog {
        height: 90% !important; /* = 90% of the .modal-backdrop block = %90 of the screen */
    }

    #priceDetailModel .modal-content {
        height: 100%; /* = 100% of the .modal-dialog block */
    }

    #priceDetailModel .modal-lg {
        width: 85%;
    }

    #priceDetailModel .modal-body {
        /* 100% = dialog height, 120px = header + footer */
        height: calc(100% - 120px);
        min-height: calc(100% - 120px);
        /*overflow-y: auto;*/
    }

    .modal-title {
        font-weight: 700;
    }
    .dataTables_scrollHeadInner, .table{
        width:100%!important
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        const restClient = new $.RestClient('/api/report/');
        restClient.add('rptWareHouseDetail');
        var currentInventory;   
        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });
        var table = $('#SLRData').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            scroller: true,
            deferRender: true,
            select: {
                info: false,
                style: 'single'
            },
            search: {
                "caseInsensitive": true,
            },           
            ajax: {
                url: '<?=site_url('api/report/rptWareHouse');?>',
                type: 'GET',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    shopCode: function () {
                        return $('#selectShop').val();
                    },
                    shopType: function () {
                        return $('#selectShopType').val();
                    },
                    provinceCode: function () {
                        return $('#selectProvince').val();
                    },
                    districtCode: function () {
                        return $('#selectDistrict').val();
                    },
                    shopModel: function () {
                        return $('#selectShopModel').val();
                    },
                    groupCode: function () {
                        if ($('#selectGroup').val() === undefined) {
                            return 'All';
                        } else {
                            return $('#selectGroup').val();
                        }
                    },
                },
                dataSrc: function(response){
                    if(response.success){
                        return response.data;
                    }else if(response.message != null){
                        console.log( "error: " + response.message);
                    }
                    return [];
                }
            },
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission( 'rptWareHouse:Export' )):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    action: function (e, dt, node, config) {
                        let tableParams = table.ajax.params();
                        let fromDate = tableParams.fromDate();
                        let toDate = tableParams.toDate();
                        let shopCode = tableParams.shopCode();
                        let shopType = tableParams.shopType();
                        let provinceCode = tableParams.provinceCode();
                        let districtCode = tableParams.districtCode();
                        let shopModel = tableParams.shopModel();
                        let groupCode = tableParams.groupCode();
                        let search = table.search();
                        let exportUrl = `<?=site_url('api/report/rptWareHouse');?>?fromDate=${fromDate}&toDate=${toDate}&shopCode=${shopCode}&shopType=${shopType}&provinceCode=${provinceCode}&districtCode=${districtCode}&shopModel=${shopModel}&search=${search}&groupCode=${groupCode}&isExport=true`;
                        downloadFile(exportUrl, 'Nhập kho OBS.xlsx');
                    },
                    // exportOptions: {
                    //     columns: ':visible'
                    // },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    },
                }
                <?php endif;?>
            ],
            order: [[ 1, "desc" ]],
            columnDefs : [{"targets": 1, "type":"date-eu"}],
            // fixedColumns: {
            //     leftColumns: 2
            // },
            columns: [
                { data: "shopCode"},
                { data: "invDate", render: formatDate },
                { data: "invCode" },
                { data: "status", render: function ( data, type, row ){
                        return (data == 9 || data == 7) ? 'Hoàn thành' : '';
                    } 
                },
                { data: "note"},
                { data: "bkInvCode" },
                { data: "createBy" },
            ],
        });

        table.on('user-select', function ( e, dt, type, cell, originalEvent ) {
            // console.log(cell.data());
            // if(cell.index().column == 0 && (cell.data().STATUS == 0 || cell.data().STATUS == 1)) return false;
            let row = dt.row( cell.index().row ).node();
            $(row).removeClass('hover');
            if ( $(row).hasClass('selected') ) {
                e.preventDefault();
                    // deselect
                $('#priceDetailModel').modal().show();
            } else {
                currentInventory = table.row(row).data();    
                if(currentInventory != null && currentInventory.details == null){
                    restClient.rptWareHouseDetail.read({invId: currentInventory.id, shopCode: currentInventory.shopCode}).done(function (response, textStatus, xhrObject) {
                        //console.log(textStatus);
                        if (textStatus == 'success') {
                            if (response.success) {
                                    currentInventory.details = response.data;
                                    $('#priceDetailModel').modal().show();
                            } else {
                                alert(response.message);
                            }
                        }
                    });      
                }else{
                    $('#priceDetailModel').modal().show();
                }                     
            }
              
        });

        $('#priceDetailModel').on('shown.bs.modal', function (e) {
            detailInit();
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
            //$('.dataTables_info').addClass('pull-left');
        });

        $('#btnSearch').click(function(e){
            table.ajax.reload();
        });

        var detaiTable;
        function detailInit(){
            // $('#priceDetailModel .modal-title').html(cu.priceName);
            //console.log(currentInventory);
            const height = $('.modal-body').height() - 120;
            if(detaiTable != null) detaiTable.destroy();
            detaiTable = $('#detailTable').DataTable({
                paging: false,
                destroy: true,
                scrollX: true,
                scrollY: height,
                dom: 'lBfrtip',
                oLanguage: {
                    sEmptyTable: "Không có dữ liệu",
                    sSearch: "Tìm kiếm nhanh:",
                    sLengthMenu: "Hiển thị _MENU_ dòng",
                    sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                },
                order: [[ 1, "asc" ], [ 0, "asc" ]],
                buttons: [
                    <?php if( check_permission( 'rptWareHouse:Export' )):?>
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                        className: 'btn btn-warning',
                        title: 'Dữ liệu chi tiết "' + currentInventory.invCode,
                        footer: false,
                        exportOptions: { 
                            columns: ':visible' 
                        },
                        init: function(api, node, config) {
                           $(node).removeClass('btn-default')
                        },
                    }
                    <?php endif;?>
                ],
                columns: [
                    { data: "mtlCode"},
                    { data: "mtlName", render: function(data, type, full, meta){
                            return '<div class="text-wrap" style="width: 250px;">' + data + '</div>';
                        }
                    },
                    { data: "spec", render: formatNumber, className: "dt-body-right"},
                    //{ data: "mtlType"},
                    { data: "qtyEven", className: "dt-body-right",},
                    { data: "unitEven", className: "dt-body-left"},
                    { data: "qtyOdd", className: "dt-body-right",},
                    { data: "unitOdd", className: "dt-body-left"},
                ],
                //deferLoading: 0
            });
            detaiTable.clear();
            if (currentInventory != null && currentInventory.details != null) detaiTable.rows.add(currentInventory.details).draw()
        }
        

    });
</script>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?=
                                        form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('type')?></label>
                                        <?=
                                        form_dropdown('shopType', $shopTypeList, set_value('shopType', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('model')?></label>
                                        <?=  
                                        form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?= 
                                        form_dropdown('provinceCode', $provinceList, set_value('provinceCode', $provinceCode), 'class="form-control select2" id="selectProvince" style="width:100%;"'); 
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('districtCode', $districtList, set_value('districtCode', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <?php if($this->session->userdata('shopCode') == 'HEAD_OFFICE'):?>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Group</label>
                                        <?php
                                            $group['All'] = "Tất cả";
                                            $group['kienlonggroup'] = 'Kiên Long Group';
                                            $group['dongtamgroup'] = 'Đồng Tâm Group';
                                        ?>
                                        <?= form_dropdown('groupCode', $group, set_value('group_code', $groupCode), 'class="form-control select2" id="selectGroup" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <?php endif?>

                                <div class="col-sm-12">
                                    <button id="btnSearch" type="button" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="SLRData" class="table table-striped table-bordered">
                                    <thead>
                                       
                                        <tr class="active">
                                            <th>Mã cửa hàng</th>
                                            <th>Ngày nhập</th>
                                            <th>Mã phiếu nhập</th>
                                            <th>Trạng thái</th>
                                            <th>Ghi chú</th>
                                            <th>Mã phiếu đặt</th>
                                            <th>Người nhận</th>
                                            <!-- <th>Số ly</th>
                                            <th>Tiền mặt</th>
                                            <th>CK</th>
                                            <th>Momo</th>
                                            <th>AirPay</th>
                                            <th>VNPay</th>
                                            <th>Beamin</th>
                                            <th>Now/Foody</th>
                                            <th>Grab</th>
                                            <th>ZaloPay</th>
                                            <th>Doanh số</th>
                                            <th></th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="12" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade" id="priceDetailModel" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Dữ liệu chi tiết</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 15px">
                        <li role="presentation" class="active"><a href="#itemTab" aria-controls="itemTab" role="tab"
                                                                  data-toggle="tab">Nguyên vật liệu</a></li>
                        <li role="presentation" class="hidden"><a href="#shopTab" aria-controls="shopTab" role="tab" data-toggle="tab">Cửa
                                hàng</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="itemTab">
                            <!-- <div class="table-responsive"> -->
                            <table id="detailTable" style="width:100% !important;" class="table table-striped table-bordered">
                                <thead>
                                    <tr class="active">
                                        <th>Mã NVL</th>
                                        <th>Tên NVL</th>
                                        <th>Quy cách</th>
                                        <th>SL chẵn</th>
                                        <th>ĐVT chẵn</th>
                                        <th>SL lẻ</th>
                                        <th>ĐVT lẻ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- </div> -->
                        </div>
                        <div role="tabpanel" class="tab-pane hidden" id="shopTab">
                            <table id="shopTable" class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                <tr class="active">
                                    <!-- <th></th> -->
                                    <th>Mã cửa hàng</th>
                                    <th>Tên cửa hàng</th>
                                    <th>Mô hình</th>
                                    <th>Địa chỉ</th>
                                    <!-- <th>Giá mặc định</th>
                                    <th>Người tạo</th>
                                    <th>Ngày tạo</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>-->
                <!--                <button id="btnSave" type="button" class="btn btn-warning">Cài đặt</button>-->
                <!--                <button id="btnDone" type="button" class="btn btn-warning" disabled="">Lưu và hoàn tất</button>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
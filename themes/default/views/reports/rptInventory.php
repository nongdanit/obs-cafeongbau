<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }

    .text-wrap {
        white-space: normal;
    }

    .dataTables_scrollHeadInner {
        width: 100% !important
    }


</style>
<script type="text/javascript">
    var table, datadetail;
    var baseApiExport = '<?=site_url('api/report/reportInventoryExport');?>';
    var baseApi = '<?=site_url('api/report/reportInventory');?>';
    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        datadetail = $('#data-detail').DataTable();
        table = $('#listOrder').DataTable({
            lengthMenu: [[10, 50, 150, -1], [10, 50, 150, "All"]],
            dom: 'lBfrtip',
            scrollX: true,
            // autoWidth: true,
            scrollY: 400,
            //scroller: true,
            deferRender: true,
            scrollCollapse: true,
            select: {
                info: false,
                style: 'single'
            },
            ajax: {
                url: baseApi,
                type: 'GET',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    shopCode: function () {
                        return $('#selectShop').val();
                    },
                    shopModel: function () {
                        return $('#selectShopModel').val();
                    },
                    stockType: function () {
                        return $('#stockType').val();
                    },
                    status: function () {
                        return 9;
                    },
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
            buttons: [
                <?php if( check_permission('rptInventory:Export')):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    //exportOptions: { columns: [ 1,2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ] },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        let tableParams = table.ajax.params();
                        let fromDate = tableParams.fromDate();
                        let toDate = tableParams.toDate();
                        let shopCode = tableParams.shopCode();
                        let shopModel = tableParams.shopModel();
                        let stockType = tableParams.stockType();
                        let status = tableParams.status();
                        let exportUrl = `${baseApiExport}?fromDate=${fromDate}&toDate=${toDate}&shopCode=${shopCode}&shopMode=${shopModel}&stockType=${stockType}&status=${status}`;
                        downloadFile(exportUrl, 'Báo cáo kiểm kê.xlsx');
                    }
                }
                <?php endif;?>
            ],
            // fixedColumns: {
            //     leftColumns: 2
            // },
            order: [[5, "asc"], [2, "desc"]],
            columnDefs: [
                {"targets": 5, "type": "date-eu"},
            ],
            columns: [
                // { "data": "stockId", "visible": true, "searchable": false },
                <?php if( check_permission('rptInventory:Update')):?>
                {
                    data: "stockId",
                    searchable: false,
                    sortable: false,
                    visible: true,
                    className: "dt-body-center",
                    render: function (data, type, row, meta) {
                        return '<button class="btn btn-warning btn-xs" stockId="' + data + '" href="javascript:void(0);">Trả phiếu</button>';
                    }
                },
                <?php else:?>
                {data: null, visible: false},
                <?php endif;?>

                {"data": "stockCode"},
                {"data": "shopCode"},
                {"data": "shopName"},
                {"data": "shopType"},
                {"data": "stockDate", "render": formatDate},
                {
                    "data": "stockType", render: function (data) {
                        if (data == 1) {
                            return 'Tháng';
                        }
                        return 'Ngày';
                    }
                },
                {
                    "data": "stockStatus", render: function (data) {
                        if (data == 9) {
                            return 'Hoàn thành';
                        }
                        return 'Đang kiểm kê';
                    }
                },
                {"data": "stockNote"}
            ]
        });
        var currentInventory;
        table.on('user-select', function (e, dt, type, cell, originalEvent) {
            if (cell.index().column == 0) return false;
            var row = dt.row(cell.index().row).node();
            $(row).removeClass('hover');
            if ($(row).hasClass('selected')) {
                e.preventDefault();
                // deselect
            } else {
                currentInventory = table.row(row).data();
                let params = 'stockId=' + currentInventory.stockId + '&shopCode=' + currentInventory.shopCode + '&t=' + new Date().getTime();
                if (currentInventory.details == null) {
                    datadetail.destroy();
                    datadetail = $('#data-detail').DataTable({
                        lengthMenu: [[50, 150, -1], [50, 150, "All"]],
                        order: [[1, "asc"], [0, "asc"]],
                        scrollX: true,
                        scrollY: 400,
                        deferRender: true,
                        scrollCollapse: true,
                        select: {
                            info: false,
                            style: 'single'
                        },
                        ajax: {
                            url: '<?=site_url('api/report/reportInventoryDetail?');?>' + params,
                            type: 'GET',
                            data: function (d) {
                                d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                            },
                            "dataSrc": function (dataJson) {
                                currentInventory.details = dataJson;
                                return dataJson.data;
                            }
                        },
                        oLanguage: {
                            "sEmptyTable": "Không có dữ liệu",
                            "sSearch": "Tìm kiếm nhanh:",
                            "sLengthMenu": "Hiển thị _MENU_ dòng",
                            "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                            "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
                        },
                        dom: 'lBfrtip',
                        buttons: [
                            <?php if( check_permission('rptInventory:Export')):?>
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                                className: 'btn btn-warning',
                                // title: 'Kiểm kê ' + ((dataRow.stockType == 0) ? 'Ngày' : 'Tháng') + '_' + dataRow.shopName + '_' + formatDate(dataRow.stockDate),
                                filename: function () {
                                    return 'Kiểm kê ' + ((currentInventory.stockType == 0) ? 'Ngày' : 'Tháng') + '_' + currentInventory.shopName + '_' + formatDate(currentInventory.stockDate);
                                },
                                footer: false,
                                //exportOptions: { columns: [ 1, 2, 3, 4,5, 6, 7 ] },
                                init: function (api, node, config) {
                                    $(node).removeClass('btn-default')
                                }
                            }
                            <?php endif;?>
                        ],
                        columns: [
                            {"data": "mtlCode"},
                            {"data": "mtlName"},
                            {"data": "spec", "render": currencyFormat, className: "text-right"},
                            {"data": "mtlType"},
                            {"data": "qtyEven", "render": currencyFormat, className: "text-right"},
                            {"data": "unitEven"},
                            {"data": "qtyOdd", "render": currencyFormat, className: "text-right"},
                            {"data": "unitOdd"},
                        ],
                    });
                } else {
                    datadetail.clear().draw();
                    datadetail.rows.add(currentInventory.details.data); // Add new data
                    datadetail.columns.adjust().draw();

                }
                // datadetail.on('user-select', function ( e, dt, type, cell, originalEvent ) {
                //     //if(cell.index().column == 0) return false;
                //     var row = dt.row( cell.index().row ).node();
                //     $(row).removeClass('hover');
                //     if ( $(row).hasClass('selected') ) {
                //         e.preventDefault();
                //             // deselect
                //     }
                // })
            }
        });
        <?php if( check_permission('rptInventory:Update')):?>
        table.on('click', 'button', function () {
            var row = $(this).closest('tr');
            var rowData = table.row(row).data();
            //console.log(currentData);
            if (confirm('Trả phiếu kiểm kê ' + rowData.stockCode + "?")) {
                $.ajax({
                    url: '<?=site_url('api/report/updateStockStatus');?>',
                    type: 'PUT',
                    dataType: 'json',
                    data: JSON.stringify({shopCode: rowData.shopCode, stockId: rowData.stockId, status: 0}),
                    success: function (dataJson) {
                        //console.log(dataJson);
                        if (dataJson.success) {
                            table.row(row).remove().draw();
                            alert('Trả phiếu thành công.');
                        } else {
                            alert(dataJson.message);
                        }
                    }
                });
            }
        });
        <?php endif;?>
        $('#btnSearch').click(function (e) {
            //if(table2 != null) table2.destroy();
            datadetail.clear().draw();
            table.ajax.reload();
        });
    });

</script>
<style type="text/css">
    .table td:first-child {
        padding: 1px;
    }

    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) {
        text-align: center;
    }

    .table td:nth-child(9) {
        text-align: right;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("business/delivery", array('method' => 'get', 'id' => '')); ?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shop_code', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại phiếu</label>
                                        <?php
                                        $stockTypes = array(
                                            'All' => 'Tất cả',
                                            0 => 'Ngày',
                                            1 => 'Tháng',
                                        );
                                        echo form_dropdown('stockType', $stockTypes, set_value('stockType', $stockType), 'id="stockType" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= 'Loại hình'; ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('model', $shopModel), 'id="selectShopModel" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button id="btnSearch" type="button"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                            <!--  --><?= form_close(); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="listOrder" class="table table-striped table-bordered">
                                    <thead>
                                    <tr class="active">

                                        <th>Thao tác</th>
                                        <th>Mã phiếu kiểm kê</th>
                                        <th>Mã cửa hàng</th>
                                        <th>Tên cửa hàng</th>
                                        <th>Mô hình</th>
                                        <th>Ngày kiểm kê</th>
                                        <th>Loại phiếu</th>
                                        <th>Trạng thái</th>
                                        <th>Ghi chú</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12"
                                            class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Dữ liệu chi tiết</h3>
                    <!-- <div class="box-tools">
                        <div class="form-group">
                            <div class="checkbox" style="display:none;">
                                <label>
                                    <input name="check_all" class="check_all" type="checkbox">
                                Tất cả
                                </label>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="box-body table-responsive edittable">
                    <table id="data-detail" class="table table-striped table-bordered table-hover order-detail">
                        <thead>
                        <tr>
                            <th>Mã NVL</th>
                            <th>Tên NVL</th>
                            <th>Quy cách</th>
                            <th>Loại</th>
                            <th>Tồn chẵn</th>
                            <th>ĐVT chẵn</th>
                            <th>Tồn lẽ</th>
                            <th>ĐVT lẽ</th>
                            <!-- <th>Thao tác</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="post_msg">

                </div>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
</section>

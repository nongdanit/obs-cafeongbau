<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    #SLRData thead tr th {
        white-space: nowrap !important;
    }

    .dataTables_scrollHeadInner {
        width: 100% !important
    }

    /*#SLRData tbody tr td{white-space: normal!important;}*/
</style>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate" '); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate" '); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("type"); ?></label>
                                        <?php
                                        echo form_dropdown('shopType', $shopTypeList, set_value('shopType', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('model') ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?= form_dropdown('provinceCode', $provinceList, set_value('provinceCode', $provinceCode), 'class="form-control select2" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('districtCode', $districtList, set_value('districtCode', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại bill</label>
                                        <?php
                                        $options = array(
                                            0 => 'Hoàn thành',
                                            3 => 'Đã hủy'
                                        );
                                        ?>
                                        <?= form_dropdown('transType', $options, set_value('transType', 0), 'class="form-control select2" id="selectTransStatus" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12 row">
                                    <div class="col-sm-6 col-md-9">
                                        <button id="btnSearch" type="button"
                                                class="btn btn btn-warning"><?= lang("search"); ?></button>
                                        <!-- <a href="<?= base_url('reports/rptTransDetails') ?>" class="btn btn btn-default"><?= lang("reset"); ?></a> -->
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group text-right" id="clickSync">
                                            <?= form_checkbox('compareSync', 1, set_checkbox('compareSync', FALSE), 'id="compareSync"') ?>
                                            <label for="compareSync">Đồng bộ trễ ngày</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <!--                            <div class="table-responsive">-->
                            <table id="SLRData" style="width: 100% !important;"
                                   class="table table-striped table-bordered table-condensed table-hover display">
                                <thead>
                                <tr style="background-color: #FFBC10;">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th colspan="2">Tổng:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th id="totalDiscount" style="text-align: right; padding-right: 5px;"></th>
                                    <th id="totalAmount" style="text-align: right; padding-right: 5px;"></th>
                                    <th></th>

                                </tr>
                                <tr class="active">
                                    <th></th>
                                    <th class="col-sm-2">Ngày BH</th>
                                    <th class="col-sm-2">Ngày Đồng bộ</th>
                                    <th><?= lang("stores_code"); ?></th>
                                    <th class="col-sm-2"><?= lang("name_store"); ?></th>
                                    <th><?= lang("type"); ?></th>
                                    <th>Mã số bill</th>
                                    <th>Lý do hủy</th>
                                    <th>Hình thức TT</th>
                                    <th>Giảm giá</th>
                                    <th>Thành tiền</th>
                                    <th>CTKM</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="12"><?= lang('loading_data_from_server'); ?></td>
                                </tr>
                                </tbody>
                            </table>
                            <!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row tableCheckInCheckOut">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table id="tableCheckInCheckOut" class="table table-striped table-bordered table-condensed table-hover">
                </table>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
</section>

<script type="text/javascript">
    /* Formatting function for row details - modify as you need */
    var totalRecords = 0;
    var transType = 0;
    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        var reportTable = $('#SLRData').DataTable({
            processing: false,
            serverSide: true,
            // pageLength: 100,
            lengthMenu: [[100, 200, 300, 400], [100, 200, 300, 400]],
            dom: 'lBfrtip',
            scrollY: 400,
            scrollX: true,
            deferRender: true,
            scrollCollapse: true,
            searching: true,
            search: {
                "caseInsensitive": true,
            },
            ajax: getReportData(),
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons:
                [
                    <?php if( $permissions['export'] ):?>
                    {
                        //extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                        className: 'btn btn-warning',
                        footer: false,
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7]},
                        init: function (api, node, config) {
                            $(node).removeClass('btn-default')
                        },
                        action: function (e, dt, node, config) {

                            let tableParams = reportTable.ajax.params();
                            let fromDate = tableParams.fromDate();
                            let toDate = tableParams.toDate();
                            let shopCode = tableParams.shopCode();
                            let shopType = tableParams.shopType();
                            let provinceCode = tableParams.provinceCode();
                            let districtCode = tableParams.districtCode();
                            let compareSync = tableParams.compareSync();
                            let shopModel = tableParams.shopModel();
                            let status = tableParams.status();
                            let search = reportTable.search();
                            console.log(compareSync);
                            let exportUrl = `<?=site_url('api/report/reportTransDetailExport');?>?fromDate=${fromDate}&toDate=${toDate}&shopCode=${shopCode}&shopType=${shopType}&provinceCode=${provinceCode}&districtCode=${districtCode}&compareSync=${compareSync}&shopModel=${shopModel}&search=${search}&status=${status}`;
                            downloadFile(exportUrl, 'Doanh số theo bill.xlsx');
                        }
                    }
                    <?php endif;?>
                ],
            order: [[1, 'desc']],
            columnDefs: [
                {"targets": 1, "type": "date-eu"}
            ],
            columns: [
                {
                    searchable: false,
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    data: "createDate",
                    className: "dt-body-center",
                    render: formatFullDate,
                    type: "date-euro",
                    width: 130
                },
                {
                    data: "serverDate",
                    className: "dt-body-center",
                    render: formatFullDate,
                    type: "date-euro",
                    width: 130
                },
                {data: "shopCode", width: 100},
                {data: "shopName"},
                {data: "shopType", width: 100},
                {data: "transNum", width: 100, className: "dt-body-right"},
                {data: "cancelReason", orderable: false, visible: false},
                {
                    data: "payType",
                    width: 100,
                    className: 'payType dt-body-center',
                    render: function (data, type, row) {
                        <?php if( $permissions['update']):?>
                        return '<select id="' + row.transId + '" class="selectPayType" style="margin 0 auto; width:120px !important;">'
                            + '<option value="ShopeePay"' + ((data == 'ShopeePay') ? ' selected' : '') + '">ShopeePay</option>'
                            + '<option value="Beamin"' + ((data == 'Beamin') ? ' selected' : '') + '>Beamin</option>'
                            + '<option value="CK"' + ((data == 'CK') ? ' selected' : '') + '>CK</option>'
                            + '<option value="Grab"' + ((data == 'Grab') ? ' selected' : '') + '>Grab</option>'
                            + '<option value="Momo"' + ((data == 'Momo') ? ' selected' : '') + '>Momo</option>'
                            + '<option value="NowFoody"' + ((data == 'NowFoody') ? ' selected' : '') + '>NowFoody</option>'
                            + '<option value="TM"' + ((data == 'TM') ? ' selected' : '') + '>TM</option>'
                            + '<option value="VNPay"' + ((data == 'VNPay') ? ' selected' : '') + '>VNPay</option>'
                            + '<option value="ZaloPay"' + ((data == 'ZaloPay') ? ' selected' : '') + '>ZaloPay</option>'
                            + '<option value="Loship"' + ((data == 'LoShip' || data == 'Loship') ? ' selected' : '') + '>Loship</option>'
                            + '<option value="GoFood"' + ((data == 'GoFood') ? ' selected' : '') + '>GoFood</option>'
                            + '<option value="beFood"' + ((data == 'beFood' || data == 'beFood') ? ' selected' : '') + '>beFood</option>'
                            + '<option value="Tiki"' + ((data == 'Tiki' || data == 'Tiki') ? ' selected' : '') + '>Tiki</option>'
                            + '<option value="Lazada"' + ((data == 'Lazada' || data == 'Lazada') ? ' selected' : '') + '>Lazada</option>'
                            + '<option value="Shopee"' + ((data == 'Shopee' || data == 'Shopee') ? ' selected' : '') + '>Shopee</option>'
                            + '<option value="GrabMart"' + ((data == 'GrabMart') ? ' selected' : '') + '>GrabMart</option>'
                            + '<option value="TikiNgon"' + ((data == 'TikiNgon') ? ' selected' : '') + '>TikiNgon</option>'
                            + '<option value="SenMall"' + ((data == 'SenMall') ? ' selected' : '') + '>SenMall</option>'
                            + '<option value="TikTokShop"' + ((data == 'TikTokShop') ? ' selected' : '') + '>TikTokShop</option>'
                            + '<option value="Momo-QR"' + ((data == 'Momo-QR') ? ' selected' : '') + '>Momo-QR</option>'
                            + '</select>';
                        <?php else:?>
                        return data;
                        <?php endif;?>

                    }
                },
                {data: "discount", className: "dt-body-right", render: formatNumber},
                {data: "amount", className: "dt-body-right", render: formatNumber},
                {data: "promotionName", width: 200, className: "dt-body-left"},
            ],
            rowCallback: function (row, data) {
                // $('.selectPayType').select2({
                //     //     minimumResultsForSearch: -1,
                //     //     width: 'element',
                //     //     height: 'element',
                //     // });
                // console.log(data.transId);
                // $('#trans' + data.transId).select2({
                //     minimumResultsForSearch: -1,
                //     width: 'element',
                //     height: 'element',
                // });
            },
            <?php if( $permissions['update']):?>
            drawCallback: function () {
                $('.selectPayType').select2({
                    minimumResultsForSearch: -1,
                    width: 'element',
                    height: 'element',
                });
                $('.selectPayType').on('select2:select', function (e) {
                    let tr = $(this).closest('tr');
                    let rowData = reportTable.row(tr).data();
                    let transId = rowData.transId;
                    let oldPayType = rowData.payType;
                    let newPayType = $(this).select2('data')[0].text;
                    if (confirm('Thay đổi hình thức thanh toán từ ' + oldPayType + ' -> ' + newPayType)) {
                        let params = {shopCode: rowData.shopCode, transId: rowData.transId, payType: newPayType};
                        let url = '<?=site_url('api/report/updatePayType');?>';
                        $.ajax({
                            url: url,
                            type: 'PUT',
                            dataType: 'json',
                            contentType: 'application/json',
                            data: JSON.stringify(params),
                            success: function (data) {
                                if (data.success) {
                                    reportTable.row(tr).data().payType = newPayType;
                                    alert('Thay đổi hình thức thanh toán thành công.');
                                } else {
                                    $(this).select2().val(rowData.payType).trigger('change');
                                    alert('Thay đổi hình thức thanh toán không thành công.');

                                }
                            }
                        });
                    } else {
                        $(this).select2().val(rowData.payType).trigger('change');
                    }
                });
            },
            <?php endif;?>
            initComplete: function (settings, json) {
                $('#SLRData_filter input').unbind();
                $('#SLRData_filter input').bind('keyup', function (e) {
                    if (e.keyCode == 13) {
                        reportTable.search(this.value).draw();
                    }
                });
            },
            // headerCallback: function( thead, data, start, end, display ) {
            //     var api = this.api(), data;
            //     $(thead).find('th').eq(6).html( cf(api.column(7, {'filter': 'applied'}).data().reduce( function (a, b) { return pf(a) + pf(b); }, 0)) );
            //     $(thead).find('th').eq(7).html( cf(api.column(8, {'filter': 'applied'}).data().reduce( function (a, b) { return pf(a) + pf(b); }, 0)) );
            // }
        });

        reportTable.on('click', '> tbody > tr > td:not(.payType)', function () {
            drawDetail(this);
        });


        $('#btnSearch').click(function (e) {
            let column = reportTable.column(7);
            transType = $('#selectTransStatus').val();
            if (transType == 3) {
                column.visible(true);
            } else {
                column.visible(false);
            }
            reportTable.clearPipeline();
            reportTable.ajax.reload();

        });

        function resultCallback(json) {
            totalRecords = json.recordsTotal;
            document.getElementById('totalDiscount').innerHTML = formatNumber(json.totalDiscount);
            document.getElementById('totalAmount').innerHTML = formatNumber(json.totalAmount);
        }

        function drawDetail(elm) {
            let tr = $(elm).closest('tr');
            let row = reportTable.row(tr);
            let d = row.data();
            if (d.detail == null) {
                let params = 'transId=' + d.transId + '&shopCode=' + d.shopCode + '&t=' + new Date().getTime();
                $.get('<?=site_url('api/report/reportTransDetail?');?>' + params, function (data) {
                    if (data['success']) {
                        d.detail = data['data'];
                    }

                    // Open this row
                    row.child(drawTable(d)).show();
                    tr.addClass('shown');
                });
            } else {
                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(drawTable(d)).show();
                    tr.addClass('shown');
                }
            }

        }

        function drawTable(d) {
            let detail = d.detail;
            let trans_code = '';
            let voucher = '';
            if (d.transId != null) trans_code = d.transId;
            if (d.voucher != null) voucher = d.voucher;
            let t = '';
            for (var i in detail) {
                t += '<tr>' +
                    '<td>' + detail[i]['itemCode'] + '</td>' +
                    '<td class="text-left">' + detail[i]['itemName'] + '</td>' +
                    '<td>' + currencyFormat(detail[i]['price']) + '</td>' +
                    '<td class="text-left">' + detail[i]['unit'] + '</td>' +
                    '<td class="text-left">' + detail[i]['unitSize'] + '</td>' +
                    '<td>' + currencyFormat(detail[i]['quantity']) + '</td>' +
                    '<td>' + currencyFormat(detail[i]['discount']) + '</td>' +
                    '<td>' + currencyFormat(detail[i]['amount']) + '</td>' +
                    '</tr>';
            }
            return '<table class="tablechild table table-bordered" style="width:88%">' +
                '<thead>' +
                '<tr style="background-color: gray;color: #fff;">' +
                '<td colspan="2" style="text-align: left;">Mã giao dịch: ' + trans_code + '</td>' +
                //'<td>'+ trans_code +'</td>'+
                '<td colspan="2" style="text-align: left;">Voucher: ' + voucher + ' </td>' +
                //'<td colspan="2">'+ voucher +'</td>'+
                '</tr>' +
                '<tr>' +
                '<td>Mã sản phẩm</td>' +
                '<td>Tên sản phẩm</td>' +
                '<td>Giá</td>' +
                '<td>ĐVT</td>' +
                '<td>Size</td>' +
                '<td>Số lượng</td>' +
                '<td>Giảm giá</td>' +
                '<td>Thành tiền</td>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                t
            '</tbody>' +
            '</table>';
        }


        function getReportData() {
            var check;
            $('#compareSync').on('ifChecked', function (event) {
                check = 1;
            });
            $('#compareSync').on('ifUnchecked', function (event) {
                check = 0;
            });
            return $.fn.dataTable.pipeline({
                url: '<?=site_url('api/report/reportTransDetail')?>',
                pages: 10, // number of pages to cache,
                method: 'POST',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    shopCode: function () {
                        return $('#selectShop').val();
                    },
                    shopType: function () {
                        return $('#selectShopType').val();
                    },
                    provinceCode: function () {
                        return $('#selectProvince').val();
                    },
                    districtCode: function () {
                        return $('#selectDistrict').val();
                    },
                    compareSync: function () {
                        return check;
                    },
                    shopModel: function () {
                        return $('#selectShopModel').val();
                    },
                    status: function () {
                        return $('#selectTransStatus').val();
                    },
                },
            }, resultCallback);
        }
    });
</script>

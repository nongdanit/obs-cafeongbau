<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var reportTable = $('#SLRDataItemsHours').DataTable({
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            scroller: true,
            deferRender: true,
            ordering: false,
            //paging: false,
            searching: false,
            ajax: getReportData(),
            fixedColumns: {
                leftColumns: 2
            },
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission($this->action . ':export')):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    // exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17] },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    }
                }
                <?php endif;?>
            ],
            columns: [
                {data: "Loại / Món", className: "dt-body-left"},// className: "width20"
                {data: "Nhóm"},
                {data: "00-01", visible: false, render: formatNumber, className: "text-right"},
                {data: "01-02", visible: false, render: formatNumber, className: "text-right"},
                {data: "02-03", visible: false, render: formatNumber, className: "text-right"},
                {data: "03-04", visible: false, render: formatNumber, className: "text-right"},
                {data: "04-05", visible: false, render: formatNumber, className: "text-right"},
                {data: "05-06", visible: false, render: formatNumber, className: "text-right"},
                {data: "06-07", render: formatNumber, className: "text-right"},
                {data: "07-08", render: formatNumber, className: "text-right"},
                {data: "08-09", render: formatNumber, className: "text-right"},
                {data: "09-10", render: formatNumber, className: "text-right"},
                {data: "10-11", render: formatNumber, className: "text-right"},
                {data: "11-12", render: formatNumber, className: "text-right"},
                {data: "12-13", render: formatNumber, className: "text-right"},
                {data: "13-14", render: formatNumber, className: "text-right"},
                {data: "14-15", render: formatNumber, className: "text-right"},
                {data: "15-16", render: formatNumber, className: "text-right"},
                {data: "16-17", render: formatNumber, className: "text-right"},
                {data: "17-18", render: formatNumber, className: "text-right"},
                {data: "18-19", render: formatNumber, className: "text-right"},
                {data: "19-20", render: formatNumber, className: "text-right"},
                {data: "20-21", render: formatNumber, className: "text-right"},
                {data: "21-22", render: formatNumber, className: "text-right"},
                {data: "22-23", visible: false, render: formatNumber, className: "text-right"},
                {data: "23-24", visible: false, render: formatNumber, className: "text-right"}

            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                if (iDisplayIndex == 0 || iDisplayIndex == 1 || iDisplayIndex == 2) $(nRow).css({
                    'background-color': '#cce5ff',
                    'color': '#004085'
                });
            },
        });

        $('#btnSearch').click(function (e) {
            reportTable.ajax.reload();
        });

        $('#check24').on('ifChecked', function (event) {
            reportTable.columns(2).visible(true);
            reportTable.columns(3).visible(true);
            reportTable.columns(4).visible(true);
            reportTable.columns(5).visible(true);
            reportTable.columns(6).visible(true);
            reportTable.columns(7).visible(true);
            reportTable.columns(24).visible(true);
            reportTable.columns(25).visible(true);
        });
        $('#check24').on('ifUnchecked', function (event) {
            reportTable.columns(2).visible(false);
            reportTable.columns(3).visible(false);
            reportTable.columns(4).visible(false);
            reportTable.columns(5).visible(false);
            reportTable.columns(6).visible(false);
            reportTable.columns(7).visible(false);
            reportTable.columns(24).visible(false);
            reportTable.columns(25).visible(false);
        });

        function getReportData() {
            return {
                url: '<?=site_url('api/report/reportHour');?>',
                type: 'GET',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    shopCode: function () {
                        return $('#selectShop').val();
                    },
                    shopType: function () {
                        return $('#selectShopType').val();
                    },
                    provinceCode: function () {
                        return $('#selectProvince').val();
                    },
                    districtCode: function () {
                        return $('#selectDistrict').val();
                    },
                    shopModel: function () {
                        return $('#selectShopModel').val();
                    },
                },
                dataSrc: function (response) {
                    if (response.success) {
                        return response.data;
                    } else if (response.message != null) {
                        console.log("error: " + response.message);
                    }
                    return [];
                }
            }
        }

    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
    });
</script>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("reports/rptHour", array('method' => 'get', 'id' => 'myform')); ?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate" '); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate" '); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("type"); ?></label>
                                        <?php
                                        // $arr['All'] = 'Tất cả';
                                        // foreach($listShopType as $key => $value) {
                                        //     $arr[$value['TITLE']] = $value['TITLE'];
                                        // }
                                        echo form_dropdown('shopType', $shopTypeList, set_value('shoptype', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('model') ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopmodel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <!--  <?php
                                        $province['All'] = "Tất cả";
                                        foreach ($listProvince as $key => $value) {
                                            $province[$value['PROVINCE_CODE']] = $value['PROVINCE_NAME'];
                                        }
                                        ?> -->
                                        <?= form_dropdown('provinceCode', $provinceList, set_value('province_code', $provinceCode), 'class="form-control select2 selectProvince" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('districtCode', $districtList, set_value('district_code', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3" <?php echo count($shopList) == 1 ? "style='margin-top:30px;'" : "" ?>>
                                    <div class="form-group">
                                        <label>Hiển thị 24h </label>

                                        <?= form_checkbox('checkFull', 1, ($checkFull == 1) ? TRUE : FALSE, 'id="check24"') ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button id="btnSearch" type="submit"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                    <!-- <a href="<?= base_url('reports/rptHour') ?>" class="btn btn btn-default"><?= lang("reset"); ?></a> -->
                                </div>
                            </div>
                            <!-- <?= form_close(); ?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="SLRDataItemsHours"
                                       class="table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr class="active">
                                        <th>Loại/Món</th>
                                        <th>Nhóm</th>
                                        <th>00-01</th>
                                        <th>01-02</th>
                                        <th>02-03</th>
                                        <th>03-04</th>
                                        <th>04-05</th>
                                        <th>05-06</th>
                                        <th>06-07</th>
                                        <th>07-08</th>
                                        <th>08-09</th>
                                        <th>09-10</th>
                                        <th>10-11</th>
                                        <th>11-12</th>
                                        <th>12-13</th>
                                        <th>13-14</th>
                                        <th>14-15</th>
                                        <th>15-16</th>
                                        <th>16-17</th>
                                        <th>17-18</th>
                                        <th>18-19</th>
                                        <th>19-20</th>
                                        <th>20-21</th>
                                        <th>21-22</th>
                                        <th>22-23</th>
                                        <th>23-24</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <!-- <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td> -->
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

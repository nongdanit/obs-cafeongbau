<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
/*    #UTable thead tr th{white-space: nowrap!important;}*/
    /*#SLRDataItems tbody tr td{white-space: normal!important;}*/
</style>
<?php
$v = "?v=1";
$v .= "&shopType=All";
$v .= "&provinceCode=All";
$v .= "&districtCode=All";
$v .= "&shopModel=CSH";
$v .= "&status=1";
if ($this->input->get('date')) {
    $v .= "&date=".$this->input->get('date');
}
if ($this->input->get('shopCode')) {
    $shopCode = ( $this->input->get('shopCode') == 'HEAD_OFFICE' ) ? 'ALL ':$this->input->get('shopCode');
    $v .= "&shopCode=" . $shopCode;
}

?>

<script type="text/javascript">
    $(document).ready(function() {
        var month = $('#date').val();
        var table = $('#UTable').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            // scrollY: 400,
            //scroller: true,
            // deferRender:    true,
            // scrollCollapse: true,
            ajax : {
                // url: '<?=site_url('/api/report/checkincheckout'. $v);?>',
                url: '<?=site_url('api/categories/stores'. $v);?>',
                type: 'GET',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission( 'checkincheckout:export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: { columns: [ 0, 1, 2] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
                <?php endif;?>
            ],
            columns: [
                { "data": "shopCode", "render": function ( data, type, row ) {
                        if(row['shopCode']!= null){
                            return '<a href="<?php echo base_url('reports/checkincheckout_detail?shopCode=') ?>'+ row['shopCode'] +'&month='+ hrsd(month) +'" >'+row['shopCode']+'</a>';
                        }
                        return '';
                    }
                },
                { "data": "shopName", "render": function ( data, type, row ) {
                        if(row['shopName']!= null){
                            return '<a href="<?php echo base_url('reports/checkincheckout_detail?shopCode=') ?>'+ row['shopCode'] +'&month='+ hrsd(month) +'" >'+row['shopName']+'</a>';
                        }
                        return '';
                    }
                },
                { "data": 'shopAddress'},
                // { "data": "shopModel"},
                // { "data": "CAL_OPEN_TIME", "render": formatTimeExtra},
                // { "data": "CAL_CLOSE_TIME", "render": formatTimeExtra},
                // { "data": "OPEN_TIME","orderable": false, "render": formattime},
                // { "data": "CLOSE_TIME", "orderable": false, "render": formattime}
            ],
            initComplete: function () {
                this.api()
                    .columns(1)
                    .every(function () {
                        let column = this;
                        // Create select element
                        let select = document.createElement('select');
                        select.add(new Option(''));
                        select.setAttribute("class","select2");
                        column.footer().replaceChildren(select);

                        // Apply listener for user change in value
                        select.addEventListener('change', function () {
                            column
                                .search(select.value, {exact: true})
                                .draw();
                        });
                        // Add list of options
                        column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                select.add(new Option(d));
                            });
                    });
            },
        });

        <?php
            if ($this->input->get('model')) {
        ?>
            var valmodel = '<?php echo $this->input->get('model');?>'
            switch(valmodel) {
              case '00':
                valmodel = '';
                break;
              case '01':
                valmodel = 'Chủ sở hữu';
                break;
            case '02':
                valmodel = 'Nhượng quyền';
                break;
              default:
                valmodel = '';
            }
            table.columns( 3 ).search( valmodel ).draw();
        <?php }
        ?>

    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });
    });
</script>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <?= form_open("reports/checkincheckout", array('method'=>'get','id' => 'myform'));?>
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="date">Chọn ngày</label>
                                        <?= form_input('date', set_value('date', $date), 'class="form-control date_max_current" id="date" ');?>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shop_code', $shopCode), 'id="shop_code" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div> -->
                                <!-- <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("model"); ?></label>
                                        <?php
                                        $a = [];
                                        $a['00']      = 'Tất cả';
                                        $a['01']   = 'Chủ Sở Hữu';
                                        $a['02']    = 'Nhượng Quyền';
                                        echo form_dropdown('shopModel', $a, set_value('model', $shopModel), 'id="shopmodel" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div> -->
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                    <a href="<?= base_url('reports/checkincheckout')?>" class="btn btn btn-default"><?= lang("reset"); ?></a>
                                </div>
                            </div>
                            <?= form_close();?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th><?=lang("stores_code"); ?></th>
                                            <th><?=lang("name_store"); ?></th>
                                            <th><?=lang("address"); ?></th>
                                            <!-- <th></th>
                                            <th>Giờ mở(theo lịch) </th>
                                            <th>Giờ đóng(theo lịch) </th>
                                            <th>Giờ mở</th>
                                            <th>Giờ đóng</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th><?=lang("name_store"); ?></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

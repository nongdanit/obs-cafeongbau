<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    #SLRDataItems thead tr th {
        white-space: nowrap !important;
    }
    .dataTables_scrollHeadInner, .table{
        /*width:100%!important*/
    }
    /*#SLRDataItems td{white-space: nowrap!important;}*/
</style>

<script type="text/javascript">
    var baseApiExport = '<?=site_url('api/report/reportPromotionByVipCardExport');?>';
    var baseApi = '<?=site_url('api/report/reportPromotionByVipCard');?>';
    var totalRecords = 0;
    $(document).ready(function () {
        var totalCup = 0;
        var totalAmount = 0;
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });

        var reportTable = $('#SLRDataItems').DataTable({
            lengthMenu: [[100, 200, 300, 400], [100, 200, 300, 400]],
            dom: 'lBfrtip',
            processing: false,
            serverSide: true,
            scrollX: true,
            scrollY: 400,
            // scroller: true,
            scrollCollapse: true,
            deferRender: true,
            // searching: false,
            search: {
                "caseInsensitive": true,
            },
            ajax: getReportData(),
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission($this->action . ':export')):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    //exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ] },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        let tableParams = reportTable.ajax.params();
                        let fromDate = tableParams.fromDate();
                        let toDate = tableParams.toDate();
                        let shopCode = tableParams.shopCode();
                        let shopType = tableParams.shopType();
                        let provinceCode = tableParams.provinceCode();
                        let districtCode = tableParams.districtCode();
                        let shopModel = tableParams.shopModel();
                        let search = reportTable.search();
                        let exportUrl = `${baseApiExport}?fromDate=${fromDate}&toDate=${toDate}&shopCode=${shopCode}&shopType=${shopType}&provinceCode=${provinceCode}&districtCode=${districtCode}&shopModel=${shopModel}&search=${search}&type=1`;
                        downloadFile(exportUrl, 'Doanh số theo thẻ Vip.xlsx');

                    }
                }
                <?php endif;?>
            ],
            columnDefs: [{"targets": 0, "type": "date-eu"}],
            order: [[0, "desc"]],
            columns: [
                {title: 'Ngày BH', width: 120, data: "transDate", className: 'text-nowrap'},
                {title: 'Mã CH', width: 120, data: "shopCode"},
                {title: 'Cửa hàng', data: "shopName"},
                {title: 'Mã nhân viên', data: "employeeCode"},
                {title: 'Tên nhân vên', data: "employeeName"},
                {title: 'Bộ phận', data: "position"},
                {title: 'Mã thẻ', data: "cardNumber"},
                {title: 'Mã thức uống', data: "itemCode", orderable: true},
                {title: 'Tên thức uống', data: "itemName"},
                {title: 'Mã số bill', data: "transNum", width: 100, className: "dt-body-right", render: formatNumber},
                {title: 'SL', data: "quantity", width: 100, className: "dt-body-right", render: formatNumber},
                {title: 'Giá', data: "price", className: "dt-body-right", render: formatNumber},
                {title: 'Giảm giá', data: "discount", className: "dt-body-right", render: formatNumber},
                {title: 'Thành tiền', data: "amount", className: "dt-body-right", render: formatNumber},
            ],
            initComplete: function (settings, json) {
                $('#SLRDataItems_filter input').unbind();
                $('#SLRDataItems_filter input').bind('keyup', function (e) {
                    if (e.keyCode == 13) {
                        reportTable.search(this.value).draw();
                    }
                });
            },
        });

        $('#btnSearch').click(function (e) {
            reportTable.search('');
            reportTable.clearPipeline();
            reportTable.ajax.reload();
        });

        function resultCallback(json) {
            totalRecords = json.recordsTotal;
            $('#totalAmount').html(formatNumber(json.totalAmount));
            $('#totalQuantity').html(formatNumber(json.totalQuantity));
        }

        function getReportData() {
            return $.fn.dataTable.pipeline({
                url: baseApi,
                pages: 10, // number of pages to cache,
                method: 'POST',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    shopCode: function () {
                        return $('#selectShop').val();
                    },
                    shopType: function () {
                        return $('#selectShopType').val();
                    },
                    provinceCode: function () {
                        return $('#selectProvince').val();
                    },
                    districtCode: function () {
                        return $('#selectDistrict').val();
                    },
                    shopModel: function () {
                        return $('#selectShopModel').val();
                    },
                },
            }, resultCallback);
        }
    });

</script>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("reports/rptPromotionByVipCard", array('method' => 'get', 'id' => 'myform')); ?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate" '); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate" '); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("type"); ?></label>
                                        <?php
                                        echo form_dropdown('shopType', $shopTypeList, set_value('shopType', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('model') ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>

                                        <?= form_dropdown('provinceCode', $provinceList, set_value('provinceCode', $provinceCode), 'class="form-control select2 selectProvince" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('districtCode', $districtList, set_value('districtCode', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button id="btnSearch" type="submit"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                    <!--  <a href="<?= base_url('reports/rptPromotionByVipCard') ?>" class="btn btn btn-default"><?= lang("reset"); ?></a> -->
                                </div>
                            </div>
                            <!-- <?= form_close(); ?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="SLRDataItems"
                                       class="table table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr style="background-color: #FFBC10;">
                                        <th><?= (!empty($addressShop)) ? "Địa chỉ:" : "" ?></th>
                                        <th colspan="4"><?= (!empty($addressShop)) ? "Địa chỉ:" : "" ?></th>
                                        <th>Tổng:</th>
                                        <th></th>
                                        <th id="totalQuantity"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th id="totalAmount"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr class="active">
                                        <th class="col-sm-1">Ngày BH</th>
                                        <th >Mã CH</th>
                                        <th class="col-sm-2">Cửa hàng</th>
                                        <th class="col-sm-1">Mã nhân viên</th>
                                        <th class="col-sm-1">Tên nhân viên</th>
                                        <th class="col-sm-1">Bộ phận</th>
                                        <th class="col-sm-1">Mã thẻ</th>
                                        <th class="col-sm-1">Mã thức uống</th>
                                        <th class="col-sm-1">Tên thức uống</th>
                                        <th class="col-sm-1">Mã số bill</th>
                                        <th class="col-sm-1">SL</th>
                                        <th class="col-sm-1">Giá</th>
                                        <th>Giảm giá</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
</section>

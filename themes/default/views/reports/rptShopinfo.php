<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>dev/js/jquery.doubleScroll.js"></script> -->
<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#UTable').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            //scroller: true,
            deferRender: true,
            scrollCollapse: true,
            scroller: true,
            ajax: {
                url: '<?=site_url('api/report/reportShopinfo');?>',
                type: 'GET',
                data: {
                    shopType: function () {
                        return $('#selectShopType').val();
                    },
                    provinceCode: function () {
                        return $('#selectProvince').val();
                    },
                    districtCode: function () {
                        return $('#selectDistrict').val();
                    },
                    shopModel: function () {
                        return $('#selectShopModel').val();
                    },
                    status: function () {
                        return $('#selectStatus').val();
                    },
                    t: function () {
                        return getTimeString();
                    },
                },
            },
            columnDefs: [
                {"targets": 12, "type": "date-eu"},
                {"targets": 13, "type": "date-eu"},
                {"targets": 14, "type": "date-euro"},
                {"targets": 15, "type": "date-eu"},
            ],
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng <b>(_TOTAL_)</b>"
            },
            order: [[1, "asc"]],
            fixedColumns: {
                leftColumns: 2
            },
            buttons: [
                <?php if( check_permission('rptShop_info:Export')):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]},
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    }
                }
                <?php endif;?>
            ],
            columns: [
                {"data": "shopCode"},
                {"data": "shopName"},
                {"data": "shopAddress"},
                {"data": "lat"},
                {"data": "lng"},
                {"data": "shopTel"},
                {"data": "ownerTel"},
                {"data": "shopModel"},
                {"data": "shopType"},
                {"data": "ttpp"},
                {"data": "am"},
                {"data": "distributor"},
                {"data": "startDate", "render": formatDate},
                {"data": "endDate", "render": formatDate},
                {"data": "lastLogin", "render": formatFullDate},
                {"data": "lastTrans", "render": formatDate},
                {"data": "activeStatus", "orderable": false},
            ]
        });

        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });

        $('#btnSearch').click(function () {
            table.ajax.reload();
        });

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("reports/rptShop_info", array('method' => 'get', 'id' => 'myform')); ?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('type') ?></label>
                                        <?php

                                        echo form_dropdown('shoptype', $shopTypeList, set_value('shoptype', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('model') ?></label>
                                        <?php
                                        echo form_dropdown('shopmodel', $shopModelList, set_value('shopmodel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?= form_dropdown('province_code', $provinceList, set_value('province_code', $provinceCode), 'class="form-control select2" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('district_code', $districtList, set_value('district_code', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('status'); ?></label>
                                        <?php
                                        $statusArr['All'] = "Tất cả";
                                        $statusArr['1'] = "Đang hoạt động";
                                        $statusArr['2'] = "Sắp khai trương";
                                        $statusArr['3'] = "Đã đóng cửa";
                                        //$statusArr['Không xác định'] = "Không xác định";
                                        ?>
                                        <?= form_dropdown('status', $statusArr, set_value('status', $status), 'class="form-control" id="selectStatus" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn btn-warning"
                                            id="btnSearch"><?= lang("search"); ?></button>
                                    <!-- <a href="<?= base_url('reports/rptShop_info') ?>" class="btn btn btn-default"><?= lang("reset"); ?></a> -->
                                </div>
                            </div>
                            <!-- <?= form_close(); ?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr class="active">
                                        <th><?= lang("stores_code"); ?></th>
                                        <th><?= lang("name_store"); ?></th>
                                        <th><?= lang("address"); ?></th>
                                        <th>Latitude</th>
                                        <th>Longitude</th>
                                        <th>SĐT</th>
                                        <th>SĐT chủ CH</th>
                                        <th>Loại hình</th>
                                        <th><?= lang('type') ?></th>
                                        <th>Thuộc TTPP</th>
                                        <th>Thuộc Cung Ứng</th>
                                        <th>Nhà phân phối</th>
                                        <th>Ngày khai trương</th>
                                        <th>Ngày đóng</th>
                                        <th>Lần đăng nhập gần nhất</th>
                                        <th>Ngày BH gần nhất</th>
                                        <th><?= lang('status'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

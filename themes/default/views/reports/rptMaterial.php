<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }
    .dataTables_scrollHeadInner, .table{
        width:100%!important
    }
</style>

<script type="text/javascript">
    var baseApi = '<?=site_url('api/report/reportMaterial');?>';
    var baseApiDetail = '<?=site_url('api/report/reportMaterialDetail');?>';
    var baseApiExport = '<?=site_url('api/report/reportMaterialDetailExport');?>';
    var table, tableDetail;
    var totalRecords = 0;
    $(document).ready(function () {
        var totalRecords = 0;
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        table = $('#SLRData').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            dom: 'lBfrtip',
            scrollY: 400,
            scroller: true,
            deferRender: true,
            scrollCollapse: true,
            ajax: {
                url: baseApi,
                type: 'GET',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    shopCode: function () {
                        return $('#selectShop').val();
                    },
                    shopModel: function () {
                        return $('#selectShopModel').val();
                    },
                    type: function () {
                        return $('#reportType').val();
                    },
                },
                dataSrc: function (json) {
                    //alert($('#shop_code').select2('data')[0].text);  
                    $('#title').html('Tiêu hao nguyên vật liệu: ' + $('#selectShop').select2('data')[0].text);
                    return json.data;
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission('materials:export')):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    title: function () {
                        let shopName = $('#selectShop').select2('data')[0].text;
                        let shopModel = $('#selectShopModel').select2('data')[0].text;
                        let exportTitle = '';
                        if (shopName == 'Tất cả') {
                            exportTitle = 'Tất cả cửa hàng';
                            if (shopModel !== 'Tất cả') {
                                exportTitle += ' ' + shopModel;
                            }
                        } else {
                            exportTitle = shopName;
                        }
                        return 'Tiêu hao NVL_' + exportTitle + '_' + $('#fromDate').val() + '_' + $('#toDate').val();
                    },
                    footer: false,
                    ///exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 , 15] },
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                }
                <?php endif;?>
            ],
            columns: [
                {"data": "MTL_CODE", className: "dt-body-left"},
                {"data": "MTL_NAME"},
                {"data": "QUANTITY", render: numberWithCommas, className: "text-right"},
                {"data": "UNIT"},
                {"data": "QTY", render: numberWithCommas, className: "text-right"},
                {"data": "UNIT_PRI"},

            ]
        });

        $('#btnSearch').click(function (e) {
            if ($('#reportType').val() == 0) {
                $('#rptMaterials').removeClass('hidden');
                $('#rptMaterialsDetail').addClass('hidden');
                table.ajax.reload();
            } else {
                $('#rptMaterials').addClass('hidden');
                $('#rptMaterialsDetail').removeClass('hidden');
                if (tableDetail == null) {
                    tableDetailInit();
                } else {
                    tableDetail.clearPipeline();
                    tableDetail.ajax.reload();
                }

            }

        });

        function tableDetailInit() {
            tableDetail = $('#SLRDataDetail').DataTable({
                lengthMenu: [[100, 200, 300, 400], [100, 200, 300, 400]],
                dom: 'lBfrtip',
                processing: false,
                serverSide: true,
                pageLength: 100,
                scrollX: true,
                scrollY: 400,
                //scroller: true,
                deferRender: true,
                scrollCollapse: true,
                oLanguage: {
                    sEmptyTable: "Không có dữ liệu",
                    sSearch: "Tìm kiếm nhanh:",
                    sLengthMenu: "Hiển thị _MENU_ dòng",
                    sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                },
                // searching: false,
                ajax: $.fn.dataTable.pipeline({
                    url: baseApiDetail,
                    pages: 10, // number of pages to cache,
                    method: 'POST',
                    data: {
                        fromDate: function () {
                            return $('#fromDate').val();
                        },
                        toDate: function () {
                            return $('#toDate').val();
                        },
                        shopCode: function () {
                            return $('#selectShop').val();
                        },
                        shopModel: function () {
                            return $('#selectShopModel').val();
                        },
                        type: function () {
                            return $('#reportType').val();
                        },
                        shopName: function () {
                            return $('#selectShop').select2('data')[0].text;
                        },
                    }
                }, resultCallback),
                buttons: [
                    <?php if( check_permission('materials:export')):?>
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                        className: 'btn btn-warning',
                        action: function (e, dt, node, config) {
                            let tableParams = tableDetail.ajax.params();
                            let fromDate = tableParams.fromDate();
                            let toDate = tableParams.toDate();
                            let shopCode = tableParams.shopCode();
                            let shopModel = tableParams.shopModel();
                            let search = tableDetail.search();
                            let shopName = $('#selectShop').select2('data')[0].text;
                            let shopModelText = $('#selectShopModel').select2('data')[0].text;
                            let exportTitle = '';
                            if (shopName == 'Tất cả') {
                                exportTitle = 'Tất cả cửa hàng';
                                if (shopModelText !== 'Tất cả') {
                                    exportTitle += ' ' + shopModel;
                                }
                            } else {
                                exportTitle = shopName;
                            }
                            let fileName = 'Tiêu hao NVL_' + exportTitle + '_' + $('#fromDate').val() + '_' + $('#toDate').val();
                            let exportUrl = `${baseApiExport}?fromDate=${fromDate}&toDate=${toDate}&shopCode=${shopCode}&shopModel=${shopModel}&search=${search}`;
                            // let exportUrl = baseApiExport + v + '&search=' + tableDetail.search() + '&start=0&limit=' + totalRecords + '&type=1';
                            downloadFile(exportUrl, fileName + '.xlsx');
                        },
                        footer: false,
                        ///exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 , 15] },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-default')
                        },
                    }
                    <?php endif;?>
                ],
                order: [[0, "desc"]],
                columnDefs: [
                    {"targets": 0, "type": "date-eu"},
                ],
                columns: [
                    {"data": "transDate", render: formatDate},
                    {"data": "shopCode"},
                    {"data": "shopName"},
                    {"data": "shopType"},
                    {"data": "mtlCode"},
                    {"data": "mtlName"},
                    {"data": "quantity", render: formatNumber, className: "dt-body-right"},
                    {"data": "mtlUnit"},
                    {"data": "qty", render: numberWithCommas, className: "dt-body-right"},
                    {"data": "unitPri"},
                ],
                initComplete: function (settings, json) {
                    $('#SLRDataDetail_filter input').unbind();
                    $('#SLRDataDetail_filter input').bind('keyup', function (e) {
                        if (e.keyCode == 13) {
                            tableDetail.search(this.value).draw();
                        }
                    });
                },
            });
        }

        function resultCallback(json) {
            totalRecords = json.recordsTotal;
            $('#titleDetail').html('Tiêu hao nguyên vật liệu: ' + $('#selectShop').select2('data')[0].text);
        }
    });
</script>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("reports/materials", array('method' => 'get', 'id' => 'myform')); ?> -->
                            <div class="row">

                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shop_code', $shopList, set_value('shop_code', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= 'Loại hình'; ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= 'Loại báo cáo'; ?></label>
                                        <?php
                                        $arr = array(
                                            '0' => 'Tổng hợp',
                                            '1' => 'Chi tiết'
                                        );
                                        echo form_dropdown('reportType', $arr, set_value('reportType', 0), 'id="reportType" class="form-control input-tip select2"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <button id="btnSearch" type="submit"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                    <!-- <a href="<?= base_url('reports/rptItemByPayment') ?>" class="btn btn btn-default hidden"><?= lang("reset"); ?></a> -->
                                </div>
                            </div>
                            <!-- <?= form_close(); ?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive" id="rptMaterials">
                                <table id="SLRData"
                                       class="table table-striped table-bordered table-condensed table-hover dataTable no-footer">
                                    <thead>
                                    <tr>
                                        <th id="title" class="background-color" colspan="7" style="text-align: left;">
                                            Tiêu hao nguyên vật liệu
                                        </th>
                                    </tr>
                                    <tr class="active">
                                        <th>Mã NVL</th>
                                        <th>Tên NVL</th>
                                        <th>Số lượng sử dụng</th>
                                        <th>Đơn vị sử dụng</th>
                                        <th>Số lượng</th>
                                        <th>Đơn vị tính</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12"
                                            class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive hidden" id="rptMaterialsDetail">
                                <table id="SLRDataDetail"
                                       class="table table-striped table-bordered table-condensed table-hover dataTable no-footer">
                                    <thead>
                                    <tr>
                                        <th id="titleDetail" class="background-color" colspan="10"
                                            style="text-align: left;">Tiêu hao nguyên vật liệu
                                        </th>
                                    </tr>
                                    <tr class="active">
                                        <th>Ngày</th>
                                        <th>Mã cửa hàng</th>
                                        <th>Tên cửa hàng</th>
                                        <th>Mô hình</th>
                                        <th>Mã NVL</th>
                                        <th>Tên NVL</th>
                                        <th>Số lượng sử dụng</th>
                                        <th>Đơn vị sử dụng</th>
                                        <th>Số lượng</th>
                                        <th>Đơn vị tính</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!-- <tr>
                                            <td colspan="12" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id="hiddenDownload" style="display: none;"></a>
</section>

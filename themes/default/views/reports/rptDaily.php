<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <!-- <h3 class="box-title"><?= lang('customize_report'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <!-- <?= form_open("reports/rptDaily", array('method' => 'get', 'id' => 'myform')); ?> -->
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="fromDate"><?= lang("start_date"); ?></label>
                                        <?= form_input('fromDate', set_value('fromDate', date('d-m-Y', strtotime($fromDate))), 'class="form-control datepicker" id="fromDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="toDate"><?= lang("end_date"); ?></label>
                                        <?= form_input('toDate', set_value('toDate', date('d-m-Y', strtotime($toDate))), 'class="form-control datepicker" id="toDate"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('type') ?></label>
                                        <?php
                                        // $arr['All'] = 'Tất cả';
                                        // foreach($shopTypeList as $key => $value) {
                                        //     $arr[$value] = $value;
                                        // }
                                        echo form_dropdown('shopType', $shopTypeList, set_value('shopType', $shopType), 'id="selectShopType" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang('model') ?></label>
                                        <?php
                                        echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?php
                                        // $province['All'] = "Tất cả";
                                        // foreach($provinceList as $key => $value) {
                                        //     $province[$value['PROVINCE_CODE']] = $value['PROVINCE_NAME'];
                                        // }
                                        ?>
                                        <?= form_dropdown('provinceCode', $provinceList, set_value('provinceCode', $provinceCode), 'class="form-control select2 selectProvince" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 <?php echo count($shopList) == 1 ? "hidden" : "" ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?php
                                        // $district['All'] = "Tất cả";
                                        // foreach($districtList as $key => $value) {
                                        //     $district[$value['DISTRICT_CODE']] = $value['DISTRICT_NAME'];
                                        // }
                                        ?>
                                        <?= form_dropdown('districtCode', $districtList, set_value('districtCode', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <?php if ($this->session->userdata('shopCode') == 'HEAD_OFFICE'): ?>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="code">Group</label>
                                            <?php
                                            $group['All'] = "Tất cả";
                                            $group['kienlonggroup'] = 'Kiên Long Group';
                                            $group['dongtamgroup'] = 'Đồng Tâm Group';
                                            ?>
                                            <?= form_dropdown('groupCode', $group, set_value('group_code', $groupCode), 'class="form-control select2" id="selectGroup" style="width:100%;"'); ?>
                                        </div>
                                    </div>
                                <?php endif ?>

                                <div class="col-sm-12">
                                    <button id="btnSearch" type="button"
                                            class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                            <!-- <?= form_close(); ?> -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="SLRData" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <!--                                             <th class="background-color"><?= (!empty($addressShop)) ? "Địa chỉ:" : "" ?></th> -->
                                        <th class="background-color" colspan="5"></th>
                                        <!--                                        <th class="background-color"></th>-->
                                        <!--                                        <th class="background-color"></th>-->
                                        <!--                                        <th class="background-color"-->
                                        <!--                                            style="text-align: right; padding-right: 5px !important;"></th>-->
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                        <th class="background-color"
                                            style="text-align: right; padding-right: 5px !important;"></th>
                                    </tr>
                                    <tr class="active">
                                        <th></th>
                                        <th>Ngày BH</th>
                                        <th><?= lang("name_store"); ?></th>
                                        <th><?= lang("stores_code"); ?></th>
                                        <th>Mô hình</th>
                                        <th>SL Bill</th>
                                        <th>Số ly</th>
                                        <th>Tiền mặt</th>
                                        <th>CK</th>
                                        <th>Momo</th>
                                        <th>ShopeePay</th>
                                        <th>VNPay</th>
                                        <th>Beamin</th>
                                        <th>Now/Foody</th>
                                        <th>Grab</th>
                                        <th>ZaloPay</th>
                                        <th>LoShip</th>
                                        <th>GoFood</th>
                                        <th>beFood</th>
                                        <th>Tiki</th>
                                        <th>Lazada</th>
                                        <th>Shopee</th>
                                        <th>GrabMart</th>
                                        <th>TikiNgon</th>
                                        <th>SenMall</th>
                                        <th>TikTokShop</th>
                                        <th>Momo-QR</th>
                                        <th>Doanh số</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="16"
                                            class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        var reportTable = $('#SLRData').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            scroller: true,
            deferRender: true,
            search: {
                "caseInsensitive": true,
            },
            ajax: getReportData(),
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission($this->action . ':export')):?>
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27]},
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                }
                <?php endif;?>
            ],
            columnDefs: [{"targets": 0, "type": "date-eu"}],
            fixedColumns: {
                leftColumns: 4
            },
            order: [[1, "desc"]],
            columns: [
                {
                    data: null, searchable: false, orderable: false, render: function (data, type, row) {
                        return '<div class="text-center"><div class="btn-group"><a class="btn btn-warning btn-xs" href="<?php echo base_url('reports/rptItem?s=' . hash('sha384', 'obscafeongbau') . '&shopCode=') ?>' + row['shopCode'] + '&fromDate=' + row['saleDate'] + '&toDate=' + row['saleDate'] + '&shopType=' + row['shopType'] + '" ><i class="fa fa-eye"></i></a></div></div>';
                    }
                },
                {title: 'Ngày', data: "saleDate", render: formatDate},
                {title: 'Cửa hàng', data: "shopName"},
                {title: 'Mã CH', data: 'shopCode'},
                {title: 'Mô hình', data: "shopType"},
                {title: 'SL Bill', data: "totalBills", formatNumber, className: "text-right"},
                {title: 'Số ly', data: "cups", formatNumber, className: "text-right"},
                {title: 'Tiền mặt', data: "cash", render: formatNumber, className: "text-right"},
                {title: 'Chuyển khoản', data: "transfer", render: formatNumber, className: "text-right"},
                {title: 'Momo', data: "Momo", render: formatNumber, className: "text-right"},
                {title: 'ShopeePay', data: "ShopeePay", render: formatNumber, className: "text-right"},
                {title: 'VNPay', data: "VNPay", render: formatNumber, className: "text-right"},
                {title: 'Beamin', data: "Beamin", render: formatNumber, className: "text-right"},
                {title: 'Now/Foody', data: "NowFoody", render: formatNumber, className: "text-right"},
                {title: 'Grab', data: "Grab", render: formatNumber, className: "text-right"},
                {title: 'ZaloPay', data: "ZaloPay", render: formatNumber, className: "text-right"},
                {title: 'LoShip', data: "LoShip", render: formatNumber, className: "text-right"},
                {title: 'GoFood', data: "GoFood", render: formatNumber, className: "text-right"},
                {title: 'beFood', data: "beFood", render: formatNumber, className: "text-right"},
                {title: 'Tiki', data: "Tiki", render: formatNumber, className: "text-right"},
                {title: 'Lazada', data: "Lazada", render: formatNumber, className: "text-right"},
                {title: 'Shopee', data: "Shopee", render: formatNumber, className: "text-right"},
                {title: 'GrabMart', data: "GrabMart", render: formatNumber, className: "text-right"},
                {title: 'TikiNgon', data: "TikiNgon", render: formatNumber, className: "text-right"},
                {title: 'SenMall', data: "SenMall", render: formatNumber, className: "text-right"},
                {title: 'TikTokShop', data: "TikTokShop", render: formatNumber, className: "text-right"},
                {title: 'Momo-QR', data: "Momo-QR", render: formatNumber, className: "text-right"},
                {title: 'Doanh số', data: "amount", render: formatNumber, className: "dt-body-right"},

            ],
            headerCallback: function (thead, data, start, end, display) {
                let api = this.api();
                // SL Bill
                $(thead).find('th').eq(1).html(cf(api.column(5, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                // Số Ly
                $(thead).find('th').eq(2).html(cf(api.column(6, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(3).html(cf(api.column(7, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(4).html(cf(api.column(8, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(5).html(cf(api.column(9, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(6).html(cf(api.column(10, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(7).html(cf(api.column(11, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(8).html(cf(api.column(12, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(9).html(cf(api.column(13, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(10).html(cf(api.column(14, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(11).html(cf(api.column(15, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(12).html(cf(api.column(16, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(13).html(cf(api.column(17, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(14).html(cf(api.column(18, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(15).html(cf(api.column(19, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(16).html(cf(api.column(20, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(17).html(cf(api.column(21, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(18).html(cf(api.column(22, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(19).html(cf(api.column(23, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(20).html(cf(api.column(24, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(21).html(cf(api.column(25, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(22).html(cf(api.column(26, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
                $(thead).find('th').eq(23).html(cf(api.column(27, {'filter': 'applied'}).data().reduce(function (a, b) {
                    return pf(a) + pf(b);
                }, 0)));
            },
            // initComplete: function(settings, json) {
            //     $('#SLRData_filter input').unbind();
            //     $('#SLRData_filter input').bind('keyup', function(e) {
            //         if(e.keyCode == 13) {
            //             table.search( this.value ).draw();
            //         }
            //     });
            // },

        });

        $('#btnSearch').click(function (e) {
            reportTable.ajax.reload();
        });

        function getReportData() {
            return {
                url: '<?=site_url('api/report/reportDaily');?>',
                type: 'GET',
                data: {
                    fromDate: function () {
                        return $('#fromDate').val();
                    },
                    toDate: function () {
                        return $('#toDate').val();
                    },
                    shopCode: function () {
                        return $('#selectShop').val();
                    },
                    shopType: function () {
                        return $('#selectShopType').val();
                    },
                    provinceCode: function () {
                        return $('#selectProvince').val();
                    },
                    districtCode: function () {
                        return $('#selectDistrict').val();
                    },
                    shopModel: function () {
                        return $('#selectShopModel').val();
                    },
                    groupCode: function () {
                        if ($('#selectGroup').val() === undefined) {
                            return 'All';
                        } else {
                            return $('#selectGroup').val();
                        }
                    },
                },
                dataSrc: function (response) {
                    if (response.success) {
                        return response.data;
                    } else if (response.message != null) {
                        console.log("error: " + response.message);
                    }
                    return [];
                }
            }
        }

    });
</script>
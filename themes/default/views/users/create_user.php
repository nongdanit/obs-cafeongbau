<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <div class="col-lg-6">
                        <?= form_open('employer/add'); ?>
                        <div class="form-group">
                            <?= lang("shop_name", "shop_name"); ?>
                            <?php
                            echo form_dropdown('shop_code', $listShop, set_value('shop_code'), 'id="shop_code" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                            ?>
                        </div>

                        <div class="form-group">
                            <?= lang('username', 'username'); ?>
                            <?= form_input('username', set_value('username'), 'class="form-control" id="username"  required="required"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('password', 'password'); ?>
                            <?= form_password('password', '', 'class="form-control" id="password"  required="required"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('confirm_password', 'confirm_password'); ?>
                            <?= form_password('confirm_password', '', 'class="form-control" id="confirm_password"  required="required"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('start_date', 'start_date'); ?>
                            <?= form_input('start_date', set_value('start_date'), 'class="form-control datepicker" id="start_date"  required="required"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('end_date', 'end_date'); ?>
                            <?= form_input('end_date', set_value('end_date'), 'class="form-control datepicker" id="end_date"  required="required"'); ?>
                        </div>
                        <div class="form-group">
                            <?= form_submit('add_user', lang('add_user'), 'class="btn btn-warning"'); ?>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


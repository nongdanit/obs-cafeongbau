<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-warning">
            <div class="box-body box-profile">
                <h4 class="profile-username text-center"><?=$users['FULLNAME']?></h4>
                <p class="text-muted text-center"><?=$users['EMAIL']?></p>
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b><?=lang('emp_code')?></b> <a class="pull-right"><?=$users['EMP_CODE']?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?=lang('username')?></b> <a class="pull-right"><?=$users['USERNAME']?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?=lang('type')?></b> <a class="pull-right"><?=$users['TYPE_NAME']?></a>
                    </li>
                </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#grouprole" data-toggle="tab">Nhóm</a></li>
                    <li><a href="#userrole" data-toggle="tab">Các chức năng</a></li>
                    <li class="pull-right"><button class="btn btn-warning update-premission"><?=lang('update')?></button></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="grouprole">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-bordered table-permission">
                                    <tbody>
                                        <tr>
                                            <th>#</th>
                                            <th>#</th>
                                        </tr>
                                        <?php if(!empty($listgroup)):?>
                                            <?php foreach ($listgroup as $key => $valueGroup):?>
                                                <tr>
                                                    <td><?=$valueGroup['GROUP_NAME']?></td>
                                                    <td>
                                                        <input code="<?=$valueGroup['GROUP_CODE']?>" type="checkbox" class="flat-red" <?=(in_array($valueGroup['GROUP_CODE'], $userGroup))?"checked":""?> >
                                                    </td>
                                                </tr>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="userrole">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-bordered table-permission">
                                    <tbody>
                                        <tr>
                                            <?php if(!empty($listrole)):?>
                                                <th>#</th>
                                                <?php foreach ($listrole as $key => $valueRole):?>
                                                    <th><?=$valueRole['ROLE_NAME']?></th>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </tr>
                                        <?php if(!empty($listfunction)):?>
                                            <?php foreach ($listfunction as $key => $valueFunction):?>
                                                <tr>
                                                    <td><?=$valueFunction['FUNCTION_NAME']?></td>
                                                    <?php foreach ($listrole as $key2 => $valueRole):?>
                                                        <td>
                                                            <input functioncode="<?=$valueFunction['FUNCTION_CODE']?>" rolecode="<?=$valueRole['ROLE_CODE']?>" type="checkbox" <?=(check_value_in_array($valueFunction['FUNCTION_CODE'], $valueRole['ROLE_CODE'], 'FUNCTION_CODE', 'ROLE_CODE', $dataRoleUser))?"checked":""?> class="flat-red">
                                                        </td>
                                                    <?php endforeach;?>
                                                </tr>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.tab-content -->
            </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

</section>
<script type="text/javascript">
    var hashtab = window.location.hash;
    if(hashtab == '' || hashtab == 'undefined'){
        hashtab ='#grouprole';
    }
    $(document).ready(function() {
        $('.nav-tabs a[href="' + hashtab + '"]').tab('show');
        var selected = [];
        // $('input').iCheck('check');

        $('input').on('ifChecked', function(event){
          alert(event.type + ' callback');
        });

        // $('.update-premission').click(function(){
        //     console.log(selected);
        // });
        
        // $('#grouprole input:checked').each(function() {
        //     selected.push($(this).attr('code'));
        // });

        
    });

</script>

<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <div class="col-lg-6">
                        <?= form_open('employer/edit/'.$row_id); ?>
                        <div class="form-group">
                            <?= lang("shop_name", "shop_name"); ?>
                            <?php
                            $gp['HEAD_OFFICE']  = "Tất cả";
                            $gp['CTO0200001']   = "Hòa Bình - Cần Thơ";
                            $gp['HCM0100001']   = "331 Hoàng Diệu";
                            $gp['HCM0100002']   = "Đoàn Như Hài";
                            $gp['HCM0100003']   = "112 Nguyễn Văn Thương";
                            $gp['HCM0100004']   = "2 Ni Sư Huỳnh Liên";
                            $gp['HCM0100005']   = "Bình Thọ - Thủ Đức";

                            echo form_dropdown('shop_code', $listShop, set_value('shop_code', $users['SHOP_CODE']), 'id="shop_code" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                            ?>
                        </div>

                        <div class="form-group">
                            <?= lang('username', 'username'); ?>
                            <?= form_input('username', set_value('username', $users['USERNAME']), 'class="form-control" id="username" disabled '); ?>
                        </div>

                        <div class="panel panel-warning">
                            <div class="panel-heading"><?= lang('if_you_need_to_rest_password_for_user') ?></div>
                            <div class="panel-body" style="padding: 5px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label
                                            for="new_password"><?php echo sprintf(lang('new_password'), $min_password_length); ?></label>
                                        <br/>
                                        <?php echo form_password('new_password', '', 'class="form-control" id="new_password"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo lang('confirm_password', 'confirm_password'); ?> <br/>
                                        <?php echo form_password('confirm_password', '', 'class="form-control" id="confirm_password" '); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?= lang('start_date', 'start_date'); ?>
                            <?= form_input('start_date', set_value('start_date', date('d-m-Y', strtotime($users['START_DATE']))), 'class="form-control datepicker" id="start_date"  required="required"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('end_date', 'end_date'); ?>
                            <?= form_input('end_date', set_value('end_date', date('d-m-Y', strtotime($users['END_DATE']))), 'class="form-control datepicker" id="end_date"  required="required"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("status", "status"); ?>
                            <?php
                            $arrStatus['1']   = "Kích hoạt";
                            $arrStatus['2']   = "Không kích hoạt";
                            $arrStatus['3']   = "Đã xóa";

                            echo form_dropdown('status', $arrStatus, set_value('status', $users['STATUS']), 'id="status" data-placeholder="' . lang("select") . ' ' . lang("status") . '" class="form-control input-tip select2" style="width:100%;"');
                            ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_hidden('row_id', $row_id); ?>
                            <?php echo form_hidden($csrf); ?>
                            <?= form_submit('update', lang('update'), 'class="btn btn-warning"'); ?>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

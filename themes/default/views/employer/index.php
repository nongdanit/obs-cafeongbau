<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<?php
$v = "?v=1";
$v .= "&shopCode=".$shopCode;
$v .= "&option=".$option;
// echo $shopCode;die;

?>
<script type="text/javascript">
    function pstatus(x) {
        if (x == 1) {
            return '<span class="label label-success"><?= lang('active'); ?></span>';
        } else if (x == 0) {
            return '<span class="label label-warning"><?= lang('inactive'); ?></span>';
        } else if (x == 3) {
            return '<span class="label label-danger"><?= lang('deleted'); ?></span>';
        } else {
            return x;
        }
    }
    $(document).ready(function() {
        var table = $('#UTable').DataTable({
            scrollX: true,
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            scrollX: true,
            // autoWidth: true,
            scrollY: 400,
            //scroller: true,
            deferRender:    true,
            scrollCollapse: true,
            columnDefs : [{"targets": 2, "type":"date-eu"}, {"targets": 3, "type":"date-eu"}],
            ajax : {
                url: '<?=site_url('api/business/employee'. $v);?>',
                type: 'GET',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission( 'employee:Export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
                <?php endif;?>
            ],
            columns: [
                { "data": "employee_code"},
                { "data": "full_name"},
                { "data": "phone"},
                { "data": "department_name"},
                { "data": "date_in", "render": formatDate, type: "date-eu"},
                { "data": "date_in", "render": formatDate, type: "date-eu"},
                { "data": "active", "render": pstatus},
                { "data": null, "searchable": false, "orderable": false, "render" : function ( data, type, row ) {
                        if(row.active == 3){
                            return '<div class="text-center"><div class="btn-group"><a class="btn btn-warning btn-xs" href="<?php echo base_url('business/employee/edit/') ?>'+ row['id']+'" ><i class="fa fa-edit"></i></a></div></div>';
                        }else{
                            var t  = '<div class="text-center"><div class="btn-group"><a class="btn btn-warning btn-xs" href="<?php echo base_url('business/employee/edit/') ?>'+ row['id']+'" ><i class="fa fa-edit"></i></a>';
                            <?php if( check_permission( 'employee:Delete' )):?>
                                // t +='<a class="btn btn-danger btn-xs" href="<?php echo base_url('business/employee/delete/') ?>'+ row['id']+'" onclick="return confirm(\''+ '<?=lang('alert_x_user')?>' +'\')" ><i class="fa fa-trash-o"></i></a>';
                            <?php endif;?>
                            t +='</div></div>';
                            return t;
                        }
                    }
                }
            ]
        });

        $('#btnSearch').click(function(){
            let urlApi = '<?=site_url('api/business/employee?v=1');?>';
            let params = '&shopCode=' + $('#selectShop').val();
            params += '&option=' + $('#option').val();
            table.ajax.url(urlApi + params).load();
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });
    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if( check_permission( 'employee:insert' )):?>
                    <a href="<?= base_url('business/employee/create')?>" class="btn btn-warning btn-sm">Thêm mới</a>
                    <?php endif;?>
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("stores", "stores"); ?>
                                        <?php
                                        echo form_dropdown('shopCode', $shopList, set_value('shopCode', $shopCode), 'id="selectShop" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= lang("option", "option"); ?>
                                        <?php
                                        $arroption['LIST']  = "Danh sách";
                                        // $arroption['DEL']   = "Nhân viên đã xóa";
                                        echo form_dropdown('option', $arroption, set_value('option', $option), 'id="option" data-placeholder="' . lang("select").'" class="form-control input-tip select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button id="btnSearch" type="submit" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead class="cf">
                                    <tr>
                                        <th>Mã</th>
                                        <th>Họ và tên</th>
                                        <th><?=lang('phone'); ?></th>
                                        <th>Chức danh</th>
                                        <th><?=lang('start_date'); ?></th>
                                        <th>Lần đăng nhập mới nhất</th>
                                        <th style="width:100px;"><?=lang('status'); ?></th>
                                        <th style="width:80px;"><?=lang('actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <?= form_open('business/employee/create'); ?>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <?= lang('phone', 'phone'); ?><span class="required">(*)</span>
                            <samp>Sử dụng để đăng nhập</samp>
                            <?= form_input('tel', set_value('tel'), 'class="form-control" id="tel"  required="required"'); ?>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="last_name">Họ</label><span class="required">(*)</span>
                                    <?= form_input('last_name', set_value('last_name'), 'class="form-control" id="last_name" required="required"'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <label for="first_name">Tên</label><span class="required">(*)</span>
                                    <?= form_input('first_name', set_value('first_name'), 'class="form-control" id="first_name" required="required"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="emp_code">Mã nhân viên</label>
                            <?= form_input('emp_code', set_value('emp_code'), 'class="form-control" id="emp_code"'); ?>
                        </div>

                        <div class="form-group">
                            <label for="title">Chức danh công việc</label><span class="required">(*)</span>
                            <?= form_input('title', set_value('title'), 'class="form-control" id="title" required="required" '); ?>
                        </div>

                        <!-- <div class="form-group">
                            <?= lang('password', 'password'); ?><span class="required">(*)</span>
                            <?= form_password('password', '', 'class="form-control" id="password"  required="required"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('confirm_password', 'confirm_password'); ?><span class="required">(*)</span>
                            <?= form_password('confirm_password', '', 'class="form-control" id="confirm_password"  required="required"'); ?>
                        </div> -->

                        <!-- <div class="form-group">
                            <label>Phản hồi đánh giá cửa hàng</label>
                            <?= form_checkbox('parent', 1, set_checkbox('parent', 1))?>
                        </div> -->
                        <div class="form-group">
                            <label>Chức vụ</label>
                            <?php
                            echo form_dropdown('type_code', $listEmpTypeArr, set_value('type_code'), 'id="type_code" class="form-control input-tip select2" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Địa chỉ</label>
                            <?= form_input('address', set_value('address'), 'class="form-control" id="address" '); ?>
                        </div>
                        <div class="form-group">
                            <label>Chứng minh nhân dân</label>
                            <?= form_input('passport', set_value('passport'), 'class="form-control" id="passport" '); ?>
                        </div>
                        <div class="form-group">
                            <label>Ngày cấp</label>
                            <?= form_input('passport_date', set_value('passport_date'), 'class="form-control datepicker" id="passport_date" '); ?>
                        </div>
                        <div class="form-group">
                            <label>Nơi cấp</label>
                            <?= form_input('passport_place', set_value('passport_place'), 'class="form-control" id="passport_place" '); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('start_date', 'start_date'); ?><span class="required">(*)</span>
                            <?= form_input('start_date', set_value('start_date'), 'class="form-control datepicker" id="start_date" required="required" '); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('end_date', 'end_date'); ?>
                            <?= form_input('end_date', set_value('end_date'), 'class="form-control datepicker" id="end_date" '); ?>
                        </div>
                        <!-- <div class="form-group">
                            <label>Thông tin chung</label>
                            <?=form_textarea('description', set_value('description'), 'id="description" class="form-control" ')?>
                        </div> -->
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <?= form_submit('add_user', 'Lưu', 'class="btn btn-warning"'); ?>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</section>


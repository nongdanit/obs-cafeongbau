<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var username = '<?php echo $row_id;?>';
        var dataEmp = '<?php echo json_encode($dataListCalendarEmp)?>';
        var dataSet = [];
        dataEmp = JSON.parse(dataEmp);
        for (var i in dataEmp) {
            dataSet[i] = [
                dataEmp[i]['shop_code'],
                dataEmp[i]['shop_name'],
                dataEmp[i]['shop_address'],
                dataEmp[i]['phone'],
                formatDate(dataEmp[i]['start_date']),
                formatDate(dataEmp[i]['end_date']),
                '<div class="text-center"><div class="btn-group-obs"><a username="' + username + '" row_id="' + dataEmp[i]['id'] + '" shop_code="' + dataEmp[i]['shop_code'] + '" start_date="' + formatDate(dataEmp[i]['start_date']) + '" end_date="' + formatDate(dataEmp[i]['end_date']) + '" onclick="loadmodalEmployee(this);return false;" class="btn btn-warning btn-xs" href="javascript:void(0);"><i class="fa fa-edit"></i></a><a onclick="del_calendar_Employee(this);return false;" row_id="' + dataEmp[i]['id'] + '" class="btn btn-danger btn-xs" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a></div></div>'
            ];
        }
        //
        var dataListCalendarEmp = $('#dataListCalendarEmp').DataTable({
            lengthMenu: [[50, 150, -1], [50, 150, "All"]],
            data: dataSet,
            columns: [
                {title: "Mã cửa hàng"},
                {title: "<?=lang("name_store"); ?>"},
                {title: "<?=lang("address"); ?>"},
                {title: "<?=lang('phone')?>"},
                {title: "<?=lang('start_date')?>"},
                {title: "<?=lang('end_date')?>"},
                {title: "<?=lang("actions"); ?>", "searchable": false, "orderable": false}
            ],
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)",
                "sInfoFiltered": " (lọc từ _MAX_ dữ liệu)"
            },
        });

        $('#search_table').on('keyup change', function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (((code == 13 && table.search() !== this.value) || (table.search() !== '' && this.value === ''))) {
                table.search(this.value).draw();
            }
        });

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-warning">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <?= form_open('business/employee/edit/' . $row_id); ?>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <?= lang('phone', 'phone'); ?><span class="required">(*)</span>
                            <samp>Sử dụng để đăng nhập</samp>
                            <?= form_input('tel', set_value('tel', $users['phone']), 'class="form-control" id="tel" readonly '); ?>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="last_name">Họ</label><span class="required">(*)</span>
                                    <?= form_input('last_name', set_value('last_name', $users['last_name']), 'class="form-control" id="last_name" required="required"'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <label for="first_name">Tên</label><span class="required">(*)</span>
                                    <?= form_input('first_name', set_value('first_name', $users['first_name']), 'class="form-control" id="first_name" required="required"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="emp_code">Mã nhân viên</label>
                            <?= form_input('emp_code', set_value('emp_code', $users['employee_code']), 'class="form-control" id="emp_code"'); ?>
                        </div>

                        <div class="form-group">
                            <label for="title">Chức danh công việc</label><span class="required">(*)</span>
                            <?= form_input('title', set_value('title', $users['title']), 'class="form-control" id="title"'); ?>
                        </div>

                        <div class="panel panel-warning">
                            <div class="panel-heading"><?= lang('if_you_need_to_rest_password_for_user') ?></div>
                            <div class="panel-body" style="padding: 5px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label
                                                for="new_password"><?php echo sprintf(lang('new_password'), $min_password_length); ?></label>
                                        <br/>
                                        <?php echo form_password('new_password', '', 'class="form-control" id="new_password"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo lang('confirm_password', 'confirm_password'); ?> <br/>
                                        <?php echo form_password('confirm_password', '', 'class="form-control" id="confirm_password" '); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label>Phản hồi đánh giá cửa hàng</label>
                            <?php //echo form_checkbox('parent', 1, ($users['PARENT'] == 1) ? TRUE : FALSE) ?>
                        </div> -->
                        <div class="form-group">
                            <label>Chức vụ</label>
                            <?php
                            echo form_dropdown('type_code', $listEmpTypeArr, set_value('type_code', $users['department_id']), 'id="type_code" data-placeholder="' . lang("select") . ' ' . lang("status") . '" class="form-control select2" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Địa chỉ</label>
                            <?= form_input('address', set_value('address', $users['address']), 'class="form-control" id="address" '); ?>
                        </div>
                        <div class="form-group">
                            <label>Chứng minh nhân dân</label>
                            <?= form_input('passport', set_value('passport', $users['id_card']), 'class="form-control" id="passport" '); ?>
                        </div>
                        <div class="form-group">
                            <label>Ngày cấp</label>
                            <?= form_input('passport_date', set_value('passport_date', (!empty($users['doi'])) ? date('d-m-Y', strtotime($users['doi'])) : ''), 'class="form-control datepicker" id="passport_date" '); ?>
                        </div>
                        <div class="form-group">
                            <label>Nơi cấp</label>
                            <?= form_input('passport_place', set_value('passport_place', $users['poi']), 'class="form-control" id="passport_place" '); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('start_date', 'start_date'); ?>
                            <?= form_input('start_date', set_value('start_date', (!empty($users['date_in'])) ? date('d-m-Y', strtotime($users['date_in'])) : ""), 'class="form-control datepicker" id="start_date" '); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('end_date', 'end_date'); ?>
                            <?= form_input('end_date', set_value('end_date', (!empty($users['date_out'])) ? date('d-m-Y', strtotime($users['date_out'])) : ""), 'class="form-control datepicker" id="end_date" '); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("status", "status"); ?>
                            <?php
                            $arrStatus['1'] = "Kích hoạt";
                            $arrStatus['0'] = "Không kích hoạt";

                            echo form_dropdown('status', $arrStatus, set_value('status', $users['active']), 'id="status" data-placeholder="' . lang("select") . ' ' . lang("status") . '" class="form-control input-tip select2" style="width:100%;"');
                            ?>
                        </div>
                        <!-- <div class="form-group">
                            <label>Thông tin chung</label>
                            <?php //echo form_textarea('description', set_value('description', $users['DESCRIPTION']), 'id="description" class="form-control" ') ?>
                        </div> -->
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <?php echo form_hidden('row_id', $row_id); ?>
                            <?php echo form_hidden($csrf); ?>
                            <?= form_submit('update', lang('update'), 'class="btn btn-warning"'); ?>
                            <a href="<?= base_url('business/employee/create') ?>" class="btn btn-default">Thêm mới</a>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-warning">
                <div class="panel-heading">Lịch làm việc<a href="javascript:void(0);"
                                                           class="btn btn-warning btn-sm pull-right"
                                                           username="<?= $row_id ?>" id="add_Calendar">Thêm lịch
                        làm việc</a></div>
                <div class="panel-body" style="padding: 5px;">
                    <div class="table-responsive">
                        <table id="dataListCalendarEmp" class="table table-striped table-bordered table-hover">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="loadmodalEmployee" role="dialog" aria-labelledby="mediumModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Lịch làm việc</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-warning">
                            <div class="box-header">
                            </div>
                            <div class="box-body">
                                <div class="col-lg-12">
                                    <?= form_open_multipart("business/employee/submitCalendarEmployee", 'id="editCalendarEmployee"'); ?>
                                    <input type="hidden" id="row_id_employee" name="row_id" value="0">
                                    <input type="hidden" id="username" name="username" value="0">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang("stores", "stores"); ?>
                                                <?php
                                                echo form_dropdown('shop_code', $listShop, set_value('shop_code'), 'id="shop_code_employee" data-placeholder="' . lang("select") . ' ' . lang("shop_name") . '" class="form-control select2" style="width:100%;"');
                                                ?>
                                            </div>
                                            <div class="form-group">
                                                <label>Ngày bắt đầu</label>
                                                <?= form_input('start_date', set_value('start_date'), 'class="form-control datepicker" id="start_date_employee" autocomplete="off"'); ?>
                                            </div>
                                            <div class="form-group">
                                                <label>Ngày kết thúc</label>
                                                <?= form_input('end_date', set_value('end_date'), 'class="form-control datepicker" id="end_date_employee" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal"><?= lang('cancel') ?></button>
                                        <?= form_submit('calendar', 'Thêm', 'class="btn btn-warning"'); ?>
                                    </div>
                                    <?= form_close(); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

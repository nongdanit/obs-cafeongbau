<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#cpassword"><?= lang('change_password'); ?></a></li>
                </ul>
                <div class="tab-content">
                    <div id="cpassword" class="tab-pane active">
                        <div class="col-lg-6">
                            <div class="white-panel">
                                <p><?= lang('update_info'); ?></p>
                                <?php echo form_open("employer/change_password"); ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php echo lang('old_password', 'curr_password'); ?> <br/>
                                            <?php echo form_password('old_password', '', 'class="form-control" id="curr_password"'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label
                                                for="new_password"><?php echo sprintf(lang('new_password'), $min_password_length); ?></label>
                                            <br/>
                                            <?php echo form_password('new_password', '', 'class="form-control" id="new_password" '); ?>
                                        </div>

                                        <div class="form-group">
                                            <?php echo lang('confirm_password', 'new_password_confirm'); ?> <br/>
                                            <?php echo form_password('new_password_confirm', '', 'class="form-control" id="new_password_confirm" '); ?>
                                        </div>

                                        <?php echo form_input($user_id); ?>
                                        <div class="form-group">
                                            <?php echo form_submit('change_password', lang('change_password'), 'class="btn btn-primary"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>


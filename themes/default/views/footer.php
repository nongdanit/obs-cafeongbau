<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>

</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Version <strong><?= $Settings->version; ?></strong>
    </div>
    Copyright &copy; <?= date('Y') . ' ' . $Settings->site_name; ?>. All rights reserved.
</footer>
</div>

<div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticModalLabel">Thông báo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning bt-action" data-dismiss="modal">OK</button>
                <a href="#" class="btn btn-warning href">Di chuyển danh sách</a>
            </div>
        </div>
    </div>
</div>
<a id="hiddenDownload" style="display: none;"></a>
<div id="overlay">
    <div id="ajaxCall"><i class="fa fa-spinner fa-pulse fa-spin"></i></div>
</div>

<div id="backtotop"><i class="fa fa-chevron-up"></i></div>
<script type="text/javascript">
    var dateformat = '<?=$Settings->dateformat;?>', timeformat = '<?= $Settings->timeformat ?>',
        timeformatfullhours = '<?= $Settings->timeformatfullhours ?>',
        timeformatfullhoursSeconds = '<?= $Settings->timeformatfullhoursSeconds ?>';
    <?php unset($Settings->protocol, $Settings->smtp_host, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->smtp_crypto, $Settings->mailpath, $Settings->timezone, $Settings->default_email, $Settings->version, $Settings->stripe, $Settings->stripe_secret_key, $Settings->stripe_publishable_key); ?>
    var Settings = <?= json_encode($Settings); ?>;
    $(window).on('load', function () {
        $('.mm_<?=$m?>').addClass('active');
        $('#<?=$m?>_<?=$v?>').addClass('active');
    });
    var lang = new Array();
    lang['code_error'] = '<?= lang('code_error'); ?>';
    lang['r_u_sure'] = '<?= lang('r_u_sure'); ?>';
    lang['register_open_alert'] = '<?= lang('register_open_alert'); ?>';
    lang['code_error'] = '<?= lang('code_error'); ?>';
    lang['r_u_sure'] = '<?= lang('r_u_sure'); ?>';
    lang['no_match_found'] = '<?= lang('no_match_found'); ?>';
</script>

<script src="<?= $assets ?>dist/js/libraries.min.js" type="text/javascript"></script>

<script src="<?= $assets ?>dist/js/scripts.min.js?r=<?= time() ?>" type="text/javascript"></script>
<script src="<?= $assets ?>plugins/chosen/chosen.jquery.min.js"></script>
<script src="<?= $assets ?>plugins/bootstrap-datetimepicker/js/moment.min.js" type="text/javascript"></script>
<script src="<?= $assets ?>plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
        type="text/javascript"></script>

<script src="<?= $assets ?>dev/js/jquery.form.js"></script>
<script src="<?= $assets ?>dev/js/jquery.validate.min.js"></script>
<script src="<?= $assets ?>dev/js/zoom.js"></script>
<script src="<?= $assets ?>dev/js/jquery.rest.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $assets ?>datatable/datatables.min.css"/>
<script type="text/javascript" src="<?= $assets ?>datatable/datatables.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_API_KEY?>&libraries=places" async defer></script>
<script src="<?= $assets ?>dev/js/main.js?r=<?= time() ?>"></script>
<script src="<?= $assets ?>dev/js/filter.js?r=<?= time() ?>"></script>
<script src="<?= $assets ?>dev/js/autoNumeric.min.js"></script>
<script src="<?= $assets ?>dev/js/currency.min.js"></script>

</body>
</html>

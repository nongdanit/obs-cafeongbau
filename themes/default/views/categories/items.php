<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    .background-color{ background-color: #FFBC10 !important; }
    #UTable thead tr th{white-space: nowrap!important;}
    #UTable tbody tr td{white-space: normal!important;}
    .background-color{ background-color: #FFBC10 !important; }
</style>
<?php
    $v = "?v=1";
?>
<script type="text/javascript">
    function pstatus(x) {
        if (x == 0) {
            return '<span class="label label-warning"><?= lang('inactive'); ?></span>';
        } else if (x == 1) {
            return '<span class="label label-success"><?= lang('active'); ?></span>';
        } else {
            return '<span class="label label-danger"><?= lang('inactive'); ?></span>';
        }
    }
    $(document).ready(function() {
        var table = $('#UTable').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            // columnDefs : [{"targets": 1, "type":"date-eu"}, {"targets": 2, "type":"date-eu"}],
            dom: 'lBfrtip',
            ajax : {
                url: '<?=site_url('api/categories/itemList');?>',
                type: 'GET',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission( 'qc_items:Export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
                <?php endif?>
            ],
            order: [[ 1, "asc" ], [ 3, "asc" ]],
            search: {
                "caseInsensitive": true,
            },
            columns: [
                { "data": "mtlCode"},
                { "data": "mtlName"},
                { "data": "mtlGroup"},
                { "data": "mtlUnit"},
                { "data": "mtlSize"},
                { "data": "mtlPrice", className: 'text-right', render: currencyFormat},
                { "data": "shopA", searchable: false, visible: false},
                { "data": "shopB", searchable: false, visible: false},
                { "data": "shopC", searchable: false, visible: false},
                { "data": "shopD", searchable: false, visible: false},
                { "data": "shopE", searchable: false, visible: false},
                { "data": "shopA", className: 'text-center', render: function(data){
                        if(data == 1) {
                            return '<i class="fa fa-check text-success text-bold" aria-hidden="true"></i>';
                        }
                        return '';
                        
                    }
                },
                { "data": "shopB", className: 'text-center', render: function(data){
                        if(data == 1) {
                            return '<i class="fa fa-check text-success text-bold" aria-hidden="true"></i>';
                        }
                        return '';
                        
                    }
                },
                { "data": "shopC", className: 'text-center', render: function(data){
                        if(data == 1) {
                            return '<i class="fa fa-check text-success text-bold" aria-hidden="true"></i>';
                        }
                        return '';
                        
                    }
                },
                { "data": "shopD", className: 'text-center', render: function(data){
                        if(data == 1) {
                            return '<i class="fa fa-check text-success text-bold" aria-hidden="true"></i>';
                        }
                        return '';
                        
                    }
                },
                { "data": "shopE", className: 'text-center', render: function(data){
                        if(data == 1) {
                            return '<i class="fa fa-check text-success text-bold" aria-hidden="true"></i>';
                        }
                        return '';
                        
                    }
                },
                // { "data": "STATUS", "orderable": false, render: pstatus},
                
            ]
        });

        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <!-- <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a> -->
                    <?php if( check_permission( 'qc_items:insert' )):?>
                    <button type="button" class="btn btn-warning hidden" data-toggle="modal" data-target="#addTemplateQC">Thêm mẫu mới</button>
                    <?php endif?>
                </div>
                <div class="box-body">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <!-- <tr class="active">
                                            <th rowspan="2" valign="center">Mã sản phẩm</th>
                                            <th rowspan="2">Tên sản phẩm</th>
                                            <th rowspan="2">Nhóm</th>
                                            <th rowspan="2">ĐVT</th>
                                            <th rowspan="2">Size</th>
                                            <th rowspan="2">Giá</th>
                                            <th colspan="10">Mô hình</th>
                                            
                                        </tr>
                                        <tr class="active">
                                            <th>Mô hình A</th>
                                            <th>Mô hình B</th>
                                            <th>Mô hình C</th>
                                            <th>Mô hình D</th>
                                            <th>Mô hình E</th>
                                            <th>A</th>
                                            <th>B</th>
                                            <th>C</th>
                                            <th>D</th>
                                            <th>E</th>
                                        </tr> -->
                                        <tr class="active">
                                            <th class="background-color">Mã sản phẩm</th>
                                            <th class="background-color">Tên sản phẩm</th>
                                            <th class="background-color">Nhóm</th>
                                            <th class="background-color">ĐVT</th>
                                            <th class="background-color">Size</th>
                                            <th class="background-color">Giá</th>
                                            <th>Mô hình A</th>
                                            <th>Mô hình B</th>
                                            <th>Mô hình C</th>
                                            <th>Mô hình D</th>
                                            <th>Mô hình E</th>
                                            <th class="background-color">A</th>
                                            <th class="background-color">B</th>
                                            <th class="background-color">C</th>
                                            <th class="background-color">D</th>
                                            <th class="background-color">E</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade"  id="addTemplateQC" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <?= form_open("categories/updateFormulaQC", array('method'=>'post','id' => 'myform_FormulaQC'));?>
            <div class="modal-header">
                <h5 class="modal-title">Thêm mẫu đánh giá</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label" for="code">Tên mẫu</label>
                                    <?= form_input('name', set_value('name'), 'class="form-control" id="name" '); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="start_date">Ngày bắt đầu</label>
                                    <?= form_input('start_date', set_value('start_date'), 'class="form-control datepicker" id="start_date" ');?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="end_date">Ngày kết thúc</label>
                                    <?= form_input('end_date', set_value('end_date'), 'class="form-control datepicker" id="end_date" ');?>
                                </div>

                                <div class="form-group">
                                    <label>Nội dung</label>
                                    <?=form_textarea('description', set_value('description'), 'id="description" class="form-control" ')?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-warning">Lưu</button>
              </div>
        </div>
        <?= form_hidden('formtype', 'create');?>
        <?= form_close();?>
    </div>
</div>

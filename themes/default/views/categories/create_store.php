<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <?= form_open('categories/shop_info/submit', 'id="form_store"'); ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?=lang('name_store', 'name_store')?><span class="required">(*)</span>
                                <?= form_input('shop_name', set_value('shop_name'), 'class="form-control" id="shop_name"'); ?>
                            </div>
                            <div class="form-group">
                                <?=lang('stores_code', 'stores_code')?><span class="required">(*)</span>
                                <?= form_input('shop_code', set_value('shop_code'), 'class="form-control" '); ?>
                            </div>
                            <div class="form-group">
                                <?=lang('phone', 'phone')?>
                                <?= form_input('shop_tel', set_value('shop_tel'), 'class="form-control" id="shop_tel"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại chủ CH</label>
                                <?= form_input('owner_tel', set_value('owner_tel'), 'class="form-control" id="owner_tel"'); ?>
                            </div>
                            <div class="form-group">
                                <?=lang('open_date', 'open_date')?><span class="required">(*)<samp> Ngày bắt đầu gửi data bán hàng về OBS</samp></span>
                                <?= form_input('start_date', set_value('start_date'), 'class="form-control datepicker" id="start_date"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Ngày đóng cửa</label>
                                <?= form_input('end_date', set_value('end_date'), 'class="form-control datepicker" id="end_date"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Thông tin khách hàng</label><span class="required">(*)</span>
                                <?= form_textarea('shopNote', set_value('shopNote', $dataStore['NOTE'] ?? ''), 'class="form-control" maxlength="200" id="shopNote" style="max-width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6 form-group">
                                    <label>Mô hình cửa hàng</label><span class="required">(*)</span>
                                    <?php
                                    unset($shopTypeList['All']);
                                    ?>
                                    <?= form_dropdown('shop_type', $shopTypeList, '', 'class="form-control select2" data-placeholder="Chọn mô hình" id="shop_type"'); ?>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <div>
                                        <?= form_checkbox('pos_type', 1, set_checkbox('pos_type', 1))?>
                                        <label>Có dùng máy POS lớn?</label>
                                    </div>

                                    <div style="margin-top: 5px">
                                        <?= form_checkbox('sale_online', 1, set_checkbox('sale_online', 1))?>
                                        <label>Có bán online?</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <?=lang('address', 'address')?>
                                <?= form_input('address', set_value('address'), 'class="form-control" id="address"'); ?>
                            </div>
                            <div class="form-group">
                                <?=lang('province_name', 'province_name')?><span class="required">(*)</span>
                                <?php
                                $provinceList['All'] = "Chọn tỉnh thành...";
                                ?>
                                <?= form_dropdown('province_code', $provinceList, set_value('province_code'), 'class="form-control select2" id="selectProvinceEdit" style="width:100%;"'); ?>
                            </div>
                            <div class="form-group">
                                <?=lang('district_name', 'district_name')?><span class="required">(*)</span>
                                <?php
                                $arr['All'] = "Chọn quận huyện...";
                                ?>
                                <?= form_dropdown('district_code', $arr, set_value('district_code'), 'class="form-control select2" id="selectDistrictEdit" style="width:100%;"'); ?>
                            </div>
                            <div class="form-group">
                                <?=lang('ward_name', 'ward_name')?><span class="required">(*)</span>
                                <?php
                                $ward_code[''] = "Chọn phường/xã...";
                                ?>
                                <?= form_dropdown('ward_code', $ward_code, set_value('ward_code'), 'class="form-control select2" id="selectWardEdit" style="width:100%;"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Trung tâm phân phối</label>
                                <?php
                                $ttpp[''] = "Chọn trung tâm phân phối...";
                                if(!empty($dataTTPP_AM['TTPP'])){
                                    foreach($dataTTPP_AM['TTPP'] as $key => $value) {
                                        $ttpp[$value['CODE']] = $value['NAME'];
                                    }
                                }
                                ?>
                                <?= form_dropdown('ttpp', $ttpp, set_value('ttpp'), 'class="form-control select2" id="ttpp" style="width:100%;"'); ?>
                            </div>

                            <div class="form-group">
                                <label>Thuộc Cung Ứng</label>
                                <?php
                                $am[''] = "Lựa chọn Cung Ứng";
                                if(!empty($dataTTPP_AM['AM'])){
                                    foreach($dataTTPP_AM['AM'] as $key => $value) {
                                        $am[$value['CODE']] = $value['NAME'];
                                    }
                                }
                                ?>
                                <?= form_dropdown('am', $am, set_value('am'), 'class="form-control select2" id="am" style="width:100%;"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Kho</label><span class="required">(*)</span>
                                <?= form_dropdown('orgCode', $erpOrg, null, 'class="form-control select2" data-placeholder="Chọn kho xuất" id="selectOrgCode"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Nhà phân phối</label>
                                <?= form_input('npp', set_value('npp'), 'class="form-control" id="npp"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php if( check_permission( 'shop_info:insert' )):?>
                                <?= form_submit('add_store', 'Lưu', 'class="btn btn-warning"'); ?>
                                <?php endif;?>
                                <a href="<?=site_url('categories/shop_info')?>" class="btn btn-default">Danh sách</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning show-error" style="display:none;"></div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(function () {
        //console.log('<?//= $dataStore['ERP_INV']?>//');
        $('#selectOrgCode').val('');
        $('#selectOrgCode').trigger('change');
        
        $('#shop_type').val('');
        $('#shop_type').trigger('change');

    });
</script>

<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    select[readonly] {
        pointer-events: none;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <?= form_open('categories/shop_info/submit', 'id="form_store"'); ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?= lang('name_store', 'name_store') ?><span class="required">(*)</span>
                                <?= form_input('shop_name', set_value('shop_name', $dataStore['shopName']), 'class="form-control" id="shop_name"'); ?>
                            </div>
                            <div class="form-group">
                                <?= lang('stores_code', 'stores_code') ?><span class="required">(*)</span>
                                <?= form_input('shop_code', set_value('shop_code', $dataStore['shopCode']), 'class="form-control" readonly '); ?>
                            </div>
                            <div class="form-group">
                                <?= lang('phone', 'phone') ?>
                                <?= form_input('shop_tel', set_value('shop_tel', $dataStore['shopTel']), 'class="form-control" id="shop_tel"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại chủ CH</label>
                                <?= form_input('owner_tel', set_value('owner_tel', $dataStore['ownerTel']), 'class="form-control" id="owner_tel"'); ?>
                            </div>
                            <div class="form-group">
                                <?= lang('open_date', 'open_date') ?><span class="required">(*)<samp> Ngày bắt đầu gửi data bán hàng về OBS</samp></span>
                                <?= form_input('start_date', set_value('start_date', date('d-m-Y', strtotime($dataStore['startDate']))), 'class="form-control datepicker" id="start_date"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Ngày đóng cửa</label>
                                <?= form_input('end_date', set_value('end_date', (!empty($dataStore['endDate'])) ? date('d-m-Y', strtotime($dataStore['endDate'])) : ''), 'class="form-control datepicker" id="end_date"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Thông tin khách hàng</label><span class="required">(*)</span>
                                <?= form_textarea('shopNote', set_value('shopNote', $dataStore['note'] ?? ''), 'class="form-control" maxlength="200" id="shopNote" style="max-width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6 form-group">
                                    <label>Mô hình cửa hàng</label><span class="required">(*)</span>

                                    <?= form_dropdown('shop_type', $shopTypeList, set_value('shop_type', $dataStore['shopType']), 'class="form-control" id="shop_type" readonly '); ?>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <div>
                                        <?= form_checkbox('pos_type', 1, ($dataStore['posType'] == 1) ? TRUE : FALSE) ?>
                                        <label>Có dùng máy POS lớn?</label>
                                    </div>
                                    <div style="margin-top: 5px">
                                        <?= form_checkbox('sale_online', 1, ($dataStore['saleOnline'] == 1) ? TRUE : FALSE) ?>
                                        <label>Có bán online?</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= lang('address', 'address') ?>
                                <?= form_input('address', set_value('address', $dataStore['address'], FALSE), 'class="form-control" id="address"'); ?>
                            </div>
                            <div class="form-group">
                                <?= lang('province_name', 'province_name') ?><span class="required">(*)</span>

                                <?= form_dropdown('province_code', $provinceList, set_value('province_code', $dataStore['provinceCode']), 'class="form-control selectProvinceEdit" id="selectProvinceEdit" readonly'); ?>
                            </div>
                            <div class="form-group">
                                <?= lang('district_name', 'district_name') ?><span class="required">(*)</span>

                                <?= form_dropdown('district_code', $districtList, set_value('district_code', $dataStore['districtCode']), 'class="form-control selectDistrictEdit select2" id="selectDistrictEdit"'); ?>
                            </div>
                            <div class="form-group">
                                <?= lang('ward_name', 'ward_name') ?><span class="required">(*)</span>
                                <?php
                                $ward_code[''] = "Chọn phường/xã...";
                                foreach ($listWard as $key => $value) {
                                    $ward_code[$value['WARD_CODE']] = $value['WARD'];
                                }
                                ?>
                                <?= form_dropdown('ward_code', $ward_code, set_value('ward_code', $dataStore['wardCode']), 'class="form-control selectWardEdit select2" id="selectWardEdit"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Trung tâm phân phối</label>
                                <select class="form-control select2" name="ttpp">
                                    <option value="">Chọn trung tâm phân phối...</option>
                                    <?php
                                    if (!empty($dataTTPP_AM['TTPP'])) {
                                        foreach ($dataTTPP_AM['TTPP'] as $key => $value) { ?>
                                            <option <?= ($dataStore['ttpp'] === $value['NAME']) ? 'selected' : '' ?>
                                                    value="<?= $value['CODE'] ?>"><?= $value['NAME'] ?></option>
                                        <?php }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Chăm sóc khách hàng</label>
                                <select class="form-control select2" name="am">
                                    <option value="">Chọn chăm sóc khách hàng...</option>
                                    <?php
                                    if (!empty($dataTTPP_AM['AM'])) {
                                        foreach ($dataTTPP_AM['AM'] as $key => $value) { ?>
                                            <option <?= ($dataStore['am'] === $value['NAME']) ? 'selected' : '' ?>
                                                    value="<?= $value['CODE'] ?>"><?= $value['NAME'] ?></option>
                                        <?php }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Kho</label><span class="required">(*)</span>
                                <?= form_dropdown('orgCode', $erpOrg, null, 'class="form-control select2" data-placeholder="Chọn kho xuất" id="selectOrgCode"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Nhà phân phối</label>
                                <?= form_input('npp', set_value('npp', $dataStore['distributor']), 'class="form-control" id="npp"'); ?>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php if (check_permission('shop_info:Update')): ?>
                                    <?= form_submit('add_store', 'Lưu', 'class="btn btn-warning"'); ?>
                                <?php endif; ?>
                                <a href="<?= site_url('categories/shop_info') ?>" class="btn btn-default">Danh sách</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning show-error" style="display:none;"></div>
                        </div>
                    </div>
                    <?php echo form_hidden('shop_id', $dataStore['shopId']); ?>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        //console.log('<?//= $dataStore['ERP_INV']?>//');
        $('#selectOrgCode').val('<?= $dataStore['orgCode']?>');
        $('#selectOrgCode').trigger('change');

    });
</script>


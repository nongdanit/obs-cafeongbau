<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td{white-space: nowrap;}
</style>
<?php
    $v = "?v=1";
    $v .= "&shopType=".$shopType;
    $v .= "&provinceCode=".$provinceCode;
    $v .= "&districtCode=".$districtCode;
    $v .= "&shopModel=".$shopModel;
    $v .= "&status=".$status;
?>
<script type="text/javascript">
    var shopTable;

    $(document).ready(function() {
        shopTable = $('#UTable').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 400,
            //scroller: true,
            deferRender:    true,
            scrollCollapse: true,
            // select: {
            //     info: false,
            //     style: 'single'
            // },
            search: {
                "caseInsensitive": true,
            },
            ajax : {
                url: '<?=site_url('api/categories/stores'. $v);?>',
                type: 'GET',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            columnDefs : [
                {"targets": 13, "type":"date-eu"},
                {"targets": 14, "type":"date-eu"}
            ],
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            fixedColumns: {
                leftColumns: 2
            },
            buttons: [
            <?php if( check_permission( 'shop_info:Export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 , 15] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
            <?php endif;?>
            ],
            columns: [
                { "data": null, "searchable": false, "orderable": false, "render" : function ( data, type, row ) {
                        return '<div class="text-center"><div class="btn-group-obs"><a class="btn btn-warning btn-xs" title="<?=lang('update')?>" href="<?php echo base_url('categories/shop_info/edit/') ?>'+ row['shopId']+'" ><i class="fa fa-edit"></i></a></div></div>';
                    }
                },
                { "data": "shopCode"},
                { "data": "shopName"},
                { "data": "shopAddress"},
                { "data": "lat"},
                { "data": "lng"},
                { "data": "shopTel"},
                { "data": "ownerTel"},
                { "data": "shopModel"},
                { "data": "shopType"},
                { "data": "ttpp"},
                { "data": "am"},
                { "data": "distributor"},
                { "data": "orgCode"},
                { "data": "startDate", "render": formatDate, type: 'date-eu' },
                { "data": "endDate", "render": formatDate, type: 'date-eu' },
                { "data": "activeStatus", "orderable": false},

            ]
        });
        shopTable.on('user-select', function ( e, dt, type, cell, originalEvent ) {
            var row = dt.row( cell.index().row ).node();
            $(row).removeClass('hover');
            if ( $(row).hasClass('selected') ) {
                e.preventDefault();
                    // deselect
            }
        });

        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                    <?php if( check_permission('shop_info:insert' )):?>
                    <a href="<?=site_url('categories/shop_info/create')?>" class="btn btn-warning btn-sm">Thêm mới cửa hàng</a>
                    <?php endif;?>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <?= form_open("categories/shop_info", array('method'=>'get','id' => 'myform'));?>
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('type')?></label>
                                        <?php

                                        echo form_dropdown('shopType', $shopTypeList, set_value('shoptype', $shopType), 'id="shoptype" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('model')?></label>
                                        <?php
                                            echo form_dropdown('shopModel', $shopModelList, set_value('shopModel', $shopModel), 'id="selectShopModel" data-placeholder="' . lang("select") . ' ' . lang("model") . '" class="form-control select2" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?= form_dropdown('provinceCode', $provinceList, set_value('provinceCode', $provinceCode), 'class="form-control select2" id="selectProvince" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?= form_dropdown('districtCode', $districtList, set_value('districtCode', $districtCode), 'class="form-control select2" id="selectDistrict" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?=lang('status'); ?></label>
                                        <?php
                                            $statusArr['All'] = "Tất cả";
                                            $statusArr['1'] = "Đang hoạt động";
                                            $statusArr['2'] = "Sắp khai trương";
                                            $statusArr['3'] = "Đã đóng cửa";
                                            //$statusArr['4'] = "Không xác định";
                                        ?>
                                        <?= form_dropdown('status', $statusArr, set_value('status', $status), 'class="form-control" id="status" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                    <a href="<?= base_url('categories/shop_info')?>" class="btn btn btn-default"><?= lang("reset"); ?></a>
                                </div>
                            </div>
                            <?= form_close();?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped" style="wid:100%">
                                    <thead>
                                        <tr class="active">
                                            <th><?=lang('actions'); ?></th>
                                            <th><?=lang("stores_code"); ?></th>
                                            <th><?=lang("name_store"); ?></th>
                                            <th><?=lang("address"); ?></th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>SĐT</th>
                                            <th>SĐT chủ CH</th>
                                            <th>Loại hình</th>
                                            <th><?=lang('type')?></th>
                                            <th>Thuộc TTPP</th>
                                            <th>Thuộc Cung Ứng</th>
                                            <th>Nhà phân phối</th>
                                            <th>Kho</th>
                                            <th>Ngày khai trương</th>
                                            <th>Ngày đóng</th>
                                            <th><?=lang('status'); ?></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    #UTable thead tr th{white-space: nowrap!important;}
    #UTable tbody tr td{white-space: normal!important;}
</style>
<?php
    $v = "?v=1";
?>
<script type="text/javascript">
    function pstatus(x) {
        if (x == 0) {
            return '<span class="label label-warning"><?= lang('inactive'); ?></span>';
        } else if (x == 1) {
            return '<span class="label label-success"><?= lang('active'); ?></span>';
        } else {
            return '<span class="label label-danger"><?= lang('inactive'); ?></span>';
        }
    }
    $(document).ready(function() {
        var table = $('#UTable').DataTable({
            lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
            columnDefs : [{"targets": 1, "type":"date-eu"}, {"targets": 2, "type":"date-eu"}],
            dom: 'lBfrtip',
            ajax : {
                url: '<?=site_url('categories/get_qc_template/'. $v);?>',
                type: 'POST',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission( 'qc_template:Export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: { columns: [ 0, 1, 2, 3, 4, 5] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
                <?php endif?>
            ],
            columns: [
                { "data": "NAME"},
                { "data": "VERSION_ID"},
                { "data": "START_DATE", "render": formatDate},
                { "data": "END_DATE", "render": formatDate},
                { "data": "DESCRIPTION"},
                { "data": "STATUS", "orderable": false, render: pstatus},
                { "data": null, "searchable": false, "orderable": false, "render" : function ( data, type, row ) {
                        var t = '<div class="text-center"><div class="btn-group"><a class="btn btn-warning btn-xs" title="<?=lang('update')?>" href="<?php echo base_url('categories/edit_qc_template/') ?>'+ row['ROW_ID']+'" ><i class="fa fa-edit"></i></a>';

                        <?php if( check_permission( 'qc_template:Write' )):?>
                            t +='<a onclick="copyTemplate(this)" row_id="'+ row['ROW_ID'] +'" class="btn btn-primary btn-xs" title="Copy" href="javascript:void(0)" ><i class="fa fa-copy"></i></a>';
                        <?php endif;?>

                        t +='</div></div>';


                        return t;
                    }
                }
            ]
        });

        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <!-- <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a> -->
                    <?php if( check_permission( 'qc_template:Write' )):?>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#addTemplateQC">Thêm mẫu mới</button>
                    <?php endif?>
                </div>
                <div class="box-body">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th>Tên mẫu</th>
                                            <th>Version</th>
                                            <th><?=lang("start_date"); ?></th>
                                            <th><?=lang("end_date"); ?></th>
                                            <th><?=lang("description"); ?></th>
                                            <th><?=lang('status'); ?></th>
                                            <th><?=lang('actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade"  id="addTemplateQC" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <?= form_open("categories/updateFormulaQC", array('method'=>'post','id' => 'myform_FormulaQC'));?>
            <div class="modal-header">
                <h5 class="modal-title">Thêm mẫu đánh giá</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label" for="code">Tên mẫu</label>
                                    <?= form_input('name', set_value('name'), 'class="form-control" id="name" '); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="start_date">Ngày bắt đầu</label>
                                    <?= form_input('start_date', set_value('start_date'), 'class="form-control datepicker" id="start_date" ');?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="end_date">Ngày kết thúc</label>
                                    <?= form_input('end_date', set_value('end_date'), 'class="form-control datepicker" id="end_date" ');?>
                                </div>

                                <div class="form-group">
                                    <label>Nội dung</label>
                                    <?=form_textarea('description', set_value('description'), 'id="description" class="form-control" ')?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-warning">Lưu</button>
              </div>
        </div>
        <?= form_hidden('formtype', 'create');?>
        <?= form_close();?>
    </div>
</div>

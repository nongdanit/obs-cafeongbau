<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<?php
    $v = "?v=1";
    if($this->input->get('cate_code')){
        $v .= '&cate_code='.$this->input->get('cate_code');
    }
?>
<script type="text/javascript">
    var target, table;
    $(document).ready(function() {
        $.get("/categories/loadQcItem", { id: $("#id_qc_template").val()}, function(data){
            $(".tableUpdatePoint > tbody").html(data);
        });
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            target = $(e.target).attr("href");
            // localStorage.setItem('activeTab', target);
            store('activeTab', target);

            if(target == "#addItemIntoFormula"){
                //Get Category
                $.get("/categories/loadCate", null, function(data){
                    var temp = $('#cate_code, #cate_code_modal');
                    temp.empty();

                    $("#cate_code").append("<option value='All'>Tất cả</option>");
                    $("#cate_code_modal").append("<option value=''>Tất cả</option>");
                    $.each(data.dataCateQC, function (i, datav) {
                        $('<option>',
                           {
                               value: datav.CATE_CODE,
                               text: datav.CATE_NAME
                           }).html(datav.CATE_NAME).appendTo("#cate_code");

                        $('<option>',
                           {
                               value: datav.CATE_CODE,
                               text: datav.CATE_NAME
                           }).html(datav.CATE_NAME).appendTo("#cate_code_modal");
                    });
                    $('#cate_code').val('<?=$this->input->get('cate_code')?$this->input->get('cate_code'):'All';?>').trigger('change');
                }, 'json');
                //Draw datatable
                table = $('#UTable').DataTable({
                    lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ],
                    destroy: true,
                    dom: 'lBfrtip',
                    order: [[ 1, "asc" ]],
                    ajax : {
                        url: '<?=site_url('/categories/get_item_qc/'. $v);?>',
                        type: 'POST',
                        "data": function ( d ) {
                            d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                        }
                    },
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                    },
                    buttons: [],
                    columns: [
                        { "data": null, "searchable": false, "orderable": false, "render" :
                            function (data, type, row){
                                return '<input type="checkbox" name="id_item_qc[]" value="' + row['ROW_ID'] + '#' + row['POINT'] + '">';
                            }
                        },
                        { "data": "CATE_CODE", "visible": false},
                        { "data": "ITEM_NAME"},
                        { "data": "POINT"},
                        { "data": null, "searchable": false, "orderable": false, "render" : function ( data, type, row ) {
                                var t = '<div class="dropdown"><button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i> </a><span class="caret"></span></button><ul class="dropdown-menu">';
                                <?php if( check_permission( 'qc_template:Update' )):?>
                                t +='<li><a onclick="loadItemQC(this)" point="'+ row['POINT'] +'" description="'+ row['DESCRIPTION'] +'" item_name="'+ row['ITEM_NAME'] +'" cate_code="'+ row['CATE_CODE'] +'" row_id="'+ row['ROW_ID'] +'" title="<?=lang('update')?>" href="javascript:void(0)"><i class="fa fa-edit"></i>Cập nhật</a></li>';
                                <?php endif?>
                                <?php if( check_permission( 'qc_template:Delete' )):?>
                                t +='<li><a onclick="deleteItemQC(this)" row_id="'+ row['ROW_ID'] +'" title="<?=lang('delete')?>" href="javascript:void(0)"><i class="fa fa-trash-o"></i>Xóa</a></li>';
                                <?php endif?>
                                t +='</ul></div>';
                                return t;
                            }
                        }
                    ]
                });
                <?php if ($this->input->get('cate_code') && $this->input->get('cate_code') != 'All' ):?>
                    var val = '<?php echo $this->input->get('cate_code');?>'
                    table.columns( 1 ).search( val ).draw();
                <?php endif;?>
                // Handle click on "Select all" control
                $('input[type=checkbox]#select-all').on('ifChanged', function(event){
                    // Get all rows with search applied
                    $(this).parent().removeClass('background-check-all');
                    var rows = table.rows({ 'search': 'applied' }).nodes();
                    // Check/uncheck checkboxes for all rows in the table
                    $('input[type="checkbox"]', rows).prop('checked', this.checked);
                });
                // Handle click on checkbox to set state of "Select all" control
                $('#UTable tbody').on('change', 'input[type="checkbox"]', function(){
                  // If checkbox is not checked
                    if(!this.checked){
                        var el = $('input[type=checkbox]#select-all').get(0);
                        // If "Select all" control is checked and has 'indeterminate' property
                        if(el && el.checked && ('indeterminate' in el)){
                            $('input[type=checkbox]#select-all').parent().addClass('background-check-all');
                        }
                    }
                });
                // Handle form submission event
                $('#form_table').on('submit', function(event){
                    var form = this;

                    // // Iterate over all checkboxes in the table
                    // table.$('input[type="checkbox"]').each(function(){
                    // // If checkbox doesn't exist in DOM
                    //     if(!$.contains(document, this)){
                    //     // If checkbox is checked
                    //         if(this.checked){
                    //            // Create a hidden element
                    //            $(form).append(
                    //               $('<input>')
                    //                  .attr('type', 'hidden')
                    //                  .attr('name', this.name)
                    //                  .val(this.value)
                    //            );
                    //         }
                    //     }
                    // });

                    $(form).ajaxSubmit({
                        type:"POST",
                        dataType: 'json',
                        data: $(form).serializeArray(),
                        url:  $(form).attr('action'),
                        success: function(e) {
                            $('#staticModal').on('show.bs.modal', function (event) {
                                var modal       = $(this);
                                modal.find('.modal-body').text(e.dataReturn.message);
                                modal.find('.href').hide();
                            });
                            $('#staticModal').modal('show');
                            $('#UTable tbody input[type="checkbox"]').prop('checked', false);
                        },
                        error: function(jqXHR, textStatus) {
                            console.log(jqXHR);
                        }
                    })
                    event.preventDefault();
                });

            }else if(target == "#updatePoint"){
                $.get("/categories/loadQcItem", { id: $("#id_qc_template").val()}, function(data){
                    $(".tableUpdatePoint > tbody").html(data);
                });
            }
        });

        var activeTab = get('activeTab');
        if(activeTab){
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#updatePoint" data-toggle="tab" aria-expanded="false">Cập nhật điểm chuẩn</a></li>
                    <li class=""><a href="#addItemIntoFormula" data-toggle="tab" aria-expanded="true">Thêm đánh giá vào mẫu</a></li>
                    <!-- <li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li> -->
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="updatePoint">
                        <div class="row">
                            <form action="" id="qc_item" method="post">
                                <input type="hidden" id="id_qc_template" name="id_qc_template" value="<?=$id?>">
                                <div class="col-sm-12">
                                    <table class="table table-striped tableUpdatePoint">
                                        <thead>
                                            <tr style="background-color:#FFBC10">
                                                <th>Hạng mục đánh giá</th>
                                                <th class="col-sm-2">Điểm chuẩn</th>
                                                <th class="col-sm-3"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12">
                                    <a href="<?=site_url('categories/qc_template')?>" class="btn btn-warning">Danh sách</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="addItemIntoFormula">
                        <div class="row">
                            <div class="col-md-8">
                                <div id="form" class="panel panel-warning">
                                    <div class="panel-body">
                                        <?= form_open("categories/edit_qc_template/".$id, array('method'=>'get','id' => 'myform'));?>
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label class="control-label" for="code">Nhóm đánh giá</label>
                                                    <?php
                                                        $cateQC['All'] = "Tất cả";
                                                    ?>
                                                    <?= form_dropdown('cate_code', $cateQC, set_value('cate_code'), 'class="form-control select2" id="cate_code" style="width: 100%;"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn btn-warning"><?= lang("search"); ?></button>
                                                <a href="<?= base_url('categories/edit_qc_template/'.$id)?>" class="btn btn btn-default"><?= lang("reset"); ?></a>
                                                <?php if( check_permission( 'qc_template:insert' )):?>
                                                <button type="button" class="btn btn-warning pull-right" data-toggle="modal" data-target="#addGroupQC">Thêm nhóm đánh giá</button>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                        <?= form_close();?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="row">
                                    <?= form_open("categories/action_edit_qc_template", array('method'=>'post','id' => 'form_table'));?>
                                    <?= form_hidden('formtype', 'table_qc')?>
                                    <?= form_hidden('id_qc_template', $id)?>
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table id="UTable" class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr class="active">
                                                        <th><input type="checkbox" name="select_all" value="1" id="select-all"></th>
                                                        <th></th>
                                                        <th>Đánh giá</th>
                                                        <th>Điểm</th>
                                                        <th style="width: 65px"><?=lang('actions'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="margin-bottom: 20px">
                                        <?php if( check_permission( 'qc_template:insert' )):?>
                                        <button type="submit" class="btn btn btn-warning"><?= lang("add"). ' vào mẫu'; ?></button>
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#addItemQC">Thêm đánh giá</button>
                                        <?php endif?>
                                    </div>
                                    <?= form_close();?>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box box-warning">
                                    <div class="box-header">
                                        <h4>Thông tin mẫu</h4>
                                    </div>
                                    <div class="box-body">
                                        <?= form_open("categories/updateFormulaQC", array('method'=>'post','id' => 'myform_FormulaQC'));?>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="code">Tên mẫu</label>
                                                    <?= form_input('name', set_value('name', $dataFormulaQC['NAME']), 'class="form-control" id="name" '); ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="start_date">Ngày bắt đầu</label>
                                                    <?= form_input('start_date', set_value('start_date', ($dataFormulaQC['START_DATE'] != '')?date('d-m-Y', strtotime($dataFormulaQC['START_DATE'])):'' ), 'class="form-control datepicker" id="start_date" ');?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="end_date">Ngày kết thúc</label>
                                                    <?= form_input('end_date', set_value('end_date', ($dataFormulaQC['END_DATE'] != '')?date('d-m-Y', strtotime($dataFormulaQC['END_DATE'])):'' ), 'class="form-control datepicker" id="end_date" ');?>
                                                </div>

                                                <div class="form-group">
                                                    <label>Nội dung</label>
                                                    <?=form_textarea('description', set_value('description', $dataFormulaQC['DESCRIPTION']), 'id="description" class="form-control" ')?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if( check_permission( 'qc_template:Update' )):?>
                                        <input type="submit" class="btn btn-warning" value="Lưu" />
                                        <?php endif?>
                                        <?= form_hidden('formtype', 'edit');?>
                                        <?= form_hidden('id_qc_template', $id)?>
                                        <?= form_hidden('version_id', $dataFormulaQC['VERSION_ID'])?>
                                        <?= form_hidden('version_name', $dataFormulaQC['VERSION_NAME'])?>
                                        <?= form_close();?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="tab-pane" id="settings">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputName" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputExperience" class="col-sm-2 control-label">Experience</label>

                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label">Skills</label>

                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                    </div> -->
                  <!-- /.tab-pane -->
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade"  id="addGroupQC" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <?= form_open("categories/submit_GroupQC", array('method'=>'post','id' => 'myform_GroupQC'));?>
            <div class="modal-header">
                <h5 class="modal-title">Thêm nhóm đánh giá</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label" for="code">Tên nhóm</label>
                                    <?= form_input('name_group', set_value('name_group'), 'class="form-control" id="name_group" '); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-warning">Lưu</button>
              </div>
        </div>
        <?= form_hidden('formtype', 'create');?>
        <?= form_close();?>
    </div>
</div>

<div class="modal fade"  id="addItemQC" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= form_open("categories/submit_ItemQC", array('method'=>'post','id' => 'myform_ItemQC'));?>
            <div class="modal-header">
                <h5 class="modal-title">Thêm đánh giá</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="cate_code_modal">Nhóm đánh giá</label>
                            <?php
                                $cateQC['All'] = "Tất cả";
                            ?>
                            <?= form_dropdown('cate_code_modal',$cateQC, set_value('cate_code_modal'), 'class="form-control select2" id="cate_code_modal" style="width: 100%;"'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="item_name">Đánh giá</label>
                            <?=form_textarea('item_name', set_value('item_name'), 'id="item_name" class="form-control" ')?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="description">Nội dung</label>
                            <?=form_textarea('description', set_value('description'), 'id="item_description" class="form-control" ')?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="description">Điểm đánh giá</label>
                            <input class="form-control" id="item_point" type="number" name="point" min="0" max="100" style="width: 20%">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-warning">Lưu</button>
              </div>
        </div>
        <?= form_hidden('formtype', 'create');?>
        <?= form_hidden('row_id', 0);?>
        <?= form_hidden('id_qc_template', $id);?>
        <?= form_hidden('cate_code', $this->input->get('cate_code')?$this->input->get('cate_code'):'' );?>
        <?= form_close();?>
    </div>
</div>
<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?>
<style type="text/css">
    th,
    td {
        white-space: nowrap;
    }

    /*.modal-body {
            max-height: calc(100vh - 212px);
            overflow-y: auto;
        }*/
    #priceDetailModel .modal-dialog {
        height: 90% !important; /* = 90% of the .modal-backdrop block = %90 of the screen */
    }

    #priceDetailModel .modal-content {
        height: 100%; /* = 100% of the .modal-dialog block */
    }

    #priceDetailModel .modal-lg {
        width: 85%;
    }

    #priceDetailModel .modal-body {
        /* 100% = dialog height, 120px = header + footer */
        height: calc(100% - 120px);
        min-height: calc(100% - 120px);
        /*overflow-y: auto;*/
    }

    .modal-title {
        font-weight: 700;
    }

    .red {
        color: red !important;
    }
</style>
<?php
//$v = "?v=1";
//$v .= "&shoptype=" . $shop_type;
//$v .= "&province_code=" . $province_code;
//$v .= "&district_code=" . $district_code;
//$v .= "&shopmodel=" . $shopmodel;
//$v .= "&status=" . $status;
//?>
<script type="text/javascript">
    $(document).ready(function () {
        const restClient = new $.RestClient('/api/categories/');
        restClient.add('priceList');
        restClient.add('storeByPrice');
        restClient.add('itemByPrice');
        restClient.add('priceDetail');
        var priceTable, itemTable, shopTable;
        var currentPrice, currentItem;
        const autoNumericOptionsEuro = {
            // allowDecimalPadding: AutoNumeric.options.allowDecimalPadding.never,
            caretPositionOnFocus: AutoNumeric.options.caretPositionOnFocus.end,
            currencySymbol: AutoNumeric.options.currencySymbol.dong,
            currencySymbolPlacement: AutoNumeric.options.currencySymbolPlacement.suffix,
            decimalPlaces: 0,
            minimumValue: 0,
        };
        priceInit();
        new AutoNumeric('#itemDefaultPrice', 0, autoNumericOptionsEuro).update({readOnly: true});
        new AutoNumeric('#itemPrice', 0, autoNumericOptionsEuro).update({readOnly: true});
        new AutoNumeric('#itemNewPrice', 0, autoNumericOptionsEuro);

        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });

        $('#btnItemSave').click(function () {
            //var test = $('#itemNewPrice').autoNumeric('')
            //if(itemNewPriceNumeric != null) {
            //alert(AutoNumeric.getNumber('#itemNewPrice'));
            itemTable.data()[0].itemName = "test";
            console.log(itemTable.data());
            itemTable.rows().invalidate().draw();
            //}

        });

        $('#priceDetailModel').on('shown.bs.modal', function (e) {
            priceDetailInit();
            $('.dataTables_info').addClass('pull-left');
        });

        $('#priceDetailEditModel').on('shown.bs.modal', function (e) {
            var select2Data = $.map(currentPrice.items, function (obj) {
                var tmp = {};
                tmp.id = obj.itemId;
                tmp.text = obj.itemCode + ' - ' + obj.itemName;
                tmp.title = obj.index;
                return tmp;
            });
            selectItemUpdate(select2Data);
            $('#selectItem').val(currentItem.itemId); // Select the option with a value of '1'
            $('#selectItem').trigger('change.select2');
            showDetailItem();


        });

        $('#priceDetailModel a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
        });

        function priceDetailInit() {
            $('#priceDetailModel .modal-title').html(currentPrice.priceName);
            const height = $('.modal-body').height() - 120;
            //alert(height);
            //if(itemTable == null){

            if (itemTable != null) {
                itemTable.destroy();
            }
            itemTable = $('#itemTable').DataTable({
                destroy: true,
                lengthMenu: [
                    [50, 100, 150, -1],
                    [50, 100, 150, "All"]
                ],
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                },
                dom: 'lBfrtip',
                scrollX: true,
                scrollY: height,
                //scrollCollapse: true,
                paging: false,
                aaSorting: [],
                select: {
                    info: false,
                    style: 'single'
                },
                buttons: [
                    // {
                    //     text: 'Add new button',
                    //     className: 'btn btn-warning pull-left',
                    //     action: function ( e, dt, node, config ) {
                    //         dt.button().add( 1, {
                    //             text: 'Button '+(counter++),
                    //             action: function () {
                    //                 this.remove();
                    //             }
                    //         } );
                    //     }
                    // },
                    <?php if( check_permission('prices:Export')):?>
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                        className: 'btn btn-warning',
                        title: 'Bảng giá "' + currentPrice.priceName + '" (danh sách sản phẩm)',
                        footer: false,
                        exportOptions: {
                            // columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                            columns: ':visible',
                            modifier: {
                                order: 'current',
                                page: 'all',
                                selected: false,
                            },
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-default')
                        },
                    },
                    <?php endif;?>

                ],
                columnDefs : [
                    {targets: 9, type: "date-euro"},
                ],
                columns: [
                    {
                        data: '', visible: false, orderable: false, render: function (data) {
                            return '<button class="btn btn-info btn-sm"><span class="glyphicon glyphicon-cog"></span></button>';
                        }
                    },
                    {data: 'itemCode'},
                    {data: 'itemName'},
                    {data: 'groupName'},
                    {data: 'price', class: 'dt-body-right', render: formatNumber},
                    {data: 'defaultPrice', class: 'dt-body-right', render: formatNumber},
                    {
                        data: '', class: 'dt-body-right', render: function (data, type, row) {
                            if (row.priceDiff < 0) {
                                return '<div>' + formatNumber(Math.abs(row.priceDiff)) + '<span class="glyphicon glyphicon-triangle-top text-success" aria-hidden="true"></span></div>';
                            } else if (row.priceDiff > 0) {
                                return '<div>' + formatNumber(Math.abs(row.priceDiff)) + '<span class="glyphicon glyphicon-triangle-bottom text-danger" aria-hidden="true"></span></div>';
                            } else {
                                return '-';
                            }

                        }
                    },
                    {
                        data: 'status', render: function (data, type, row) {
                            if (row.status == 0) return "";
                            return "Kích hoạt";
                        }
                    },
                    {data: 'createBy'},
                    {data: 'createDate', render: formatFullDate, type: 'date-euro'},
                ],
                initComplete: function (settings, json) {
                    // if(currentPrice != null) this.rows.add( currentPrice.items ).draw();
                },
                createdRow: function (row, data, dataIndex) {
                    data.index = dataIndex;
                    if (data.rowId == 0) {
                        $(row).addClass('red');
                    }
                }
            });

            itemTable.on('destroy.dt', function (e, settings) {
                $(this).off('click', 'td');
                $(this).off('user-select');
            });

            // itemTable.on('click', 'button', function () {
            //     currentItem = itemTable.row($(this).parents('tr')).data();
            //     $('#priceDetailEditModel').modal().show();
            //     //console.log(currentItem);
            // });

            itemTable.on('user-select', function (e, dt, type, cell, originalEvent) {
                const row = dt.row(cell.index().row).node();
                $(row).removeClass('hover');
                if ($(row).hasClass('selected')) {
                    e.preventDefault();
                    // deselect
                }
                currentItem = itemTable.row(row).data();
                // $('#priceDetailEditModel .modal-title').html('Cài đặt giá: ' + currentItem.itemCode + ' - ' + currentItem.itemName);
                $('#priceDetailEditModel').modal().show();
            });
            if(shopTable != null) shopTable.destroy();
            shopTable = $('#shopTable').DataTable({
                destroy: true,
                lengthMenu: [
                    [50, 100, 150, -1],
                    [50, 100, 150, "All"]
                ],
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                },
                dom: 'lBfrtip',
                scrollX: true,
                scrollY: height,
                //scrollCollapse: true,
                paging: false,
                aaSorting: [],
                buttons: [
                    <?php if( check_permission('prices:Export')):?> {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                        className: 'btn btn-warning',
                        title: 'Bảng giá ' + currentPrice.priceName + '(danh sách cửa hàng áp dụng)',
                        footer: false,
                        exportOptions: {
                            // columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                            columns: ':visible',
                            modifier: {
                                order: 'current',
                                page: 'all',
                                selected: false,
                            },
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-default')
                        }
                    }
                    <?php endif;?>
                ],
                columnDefs : [{"targets": 4, "type":"date-eu"}],
                columns: [
                    {data: 'shopCode'},
                    {data: 'shopName'},
                    {data: 'shopType'},
                    {data: 'shopAddress'},
                    // {data: 'defaultPrice', render: currencyFormat},
                    // {data: 'createBy'},
                    {data: 'priceStartDate', render: formatDate},
                ],
                initComplete: function (settings, json) {
                    // if(currentPrice != null) this.rows.add( currentPrice.items ).draw();
                }
            });
            //}
            itemTable.clear();
            shopTable.clear();
            if (currentPrice != null && currentPrice.items != null) itemTable.rows.add(currentPrice.items).draw();
            if (currentPrice != null && currentPrice.shops != null) shopTable.rows.add(currentPrice.shops).draw();
        }

        function priceInit() {
            priceTable = $('#mainTable').DataTable({
                lengthMenu: [
                    [50, 100, 150, -1],
                    [50, 100, 150, "All"]
                ],
                dom: 'lBfrtip',
                scrollX: true,
                scrollY: 400,
                deferRender: true,
                scrollCollapse: true,
                aaSorting: [],
                select: {
                    info: false,
                    style: 'single'
                },
                search: {
                    "caseInsensitive": true,
                },
                //timeout: 60000,
                ajax: {
                    url: '<?=site_url('api/categories/priceList');?>',
                    type: 'GET',
                    data: function (d) {
                        d.<?=$this->security->get_csrf_token_name();?> =
                            "<?=$this->security->get_csrf_hash()?>";
                    },
                    dataSrc: function (dataJson) {
                        if (dataJson.success) {
                            return dataJson.data;
                        } else {
                            alert(dataJson.message);
                            return [];
                        }
                    }
                },
                oLanguage: {
                    "sEmptyTable": "Không có dữ liệu",
                    "sSearch": "Tìm kiếm nhanh:",
                    "sLengthMenu": "Hiển thị _MENU_ dòng",
                    "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                },
                buttons: [
                    <?php if( check_permission('prices:Export')):?> {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                        className: 'btn btn-warning',
                        footer: false,
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7],
                            //columns: ':visible',
                            modifier: {
                                order: 'current',
                                page: 'all',
                                selected: false,
                            },
                        },
                        init: function (api, node, config) {
                            $(node).removeClass('btn-default')
                        }
                    }
                    <?php endif;?>
                ],
                // columnDefs: [{
                //         "targets": 13,
                //         "type": "date-eu"
                //     },
                //     {
                //         "targets": 14,
                //         "type": "date-eu"
                //     }
                // ],
                // fixedColumns: {
                //     leftColumns: 2
                // },
                columns: [
                    {
                        data: '', visible: false, orderable: false, render: function (data) {
                            return '<button class="btn btn-info btn-sm"><span class="glyphicon glyphicon-th-list"></span></button>';
                        }
                    },
                    {
                        data: "priceName", class: "dt-body-left"
                    },
                    {
                        data: "note", class: "dt-body-left"
                    },
                    {
                        data: "status", class: "dt-body-left", render: function (data) {
                            if (data === 1)
                                return 'Hoạt động';
                            return 'Kết thúc';
                        }
                    },
                    {
                        data: "startDate", visible: false, class: "dt-body-left", render: formatDate
                    },
                    {
                        data: "endDate", visible: false, class: "dt-body-left", render: formatDate
                    },
                    {
                        data: "createBy", class: "dt-body-left"
                    },
                    {
                        data: "createDate", class: "dt-body-left", render: formatFullDate, type: "date-euro"
                    },
                ]
            });

            priceTable.on('user-select', function (e, dt, type, cell, originalEvent) {
                var row = dt.row(cell.index().row).node();
                $(row).removeClass('hover');
                if ($(row).hasClass('selected')) {
                    e.preventDefault();
                    // deselect
                } else {
                    currentPrice = priceTable.row(row).data();
                    restClient.priceDetail.read({priceId: currentPrice.priceId}).done(function (response, textStatus, xhrObject) {
                        //console.log(textStatus);
                        if (textStatus == 'success') {
                            if (response.success) {
                                currentPrice.items = $.merge(response.data.items, response.data.moreItems);
                                currentPrice.shops = response.data.shops;
                                $('#priceDetailModel').modal().show();
                            } else {
                                alert(response.message);
                            }

                        }

                    });
                }
            });

            priceTable.on('click', 'button', function () {
                currentPrice = priceTable.row($(this).parents('tr')).data();
                restClient.priceDetail.read({priceId: currentPrice.priceId}).done(function (response, textStatus, xhrObject) {
                    //console.log(textStatus);
                    if (textStatus == 'success') {
                        if (response.success) {
                            currentPrice.items = $.merge(response.data.items, response.data.moreItems);
                            currentPrice.shops = response.data.shops;
                            $('#priceDetailModel').modal().show();
                        } else {
                            alert(response.message);
                        }

                    }

                });
            });
        }

        function selectItemUpdate(source) {
            //$('#selectItem').select2("data", select2Data, true);
            $('#selectItem').select2('destroy');
            $('#selectItem').off('select2:select');
            $('#selectItem').select2({
                data: source,
                // templateSelection: formatState,
            });

            // $('#selectItem').on("select2:select", function (e) {
            //     currentItem = itemTable.data()[parseInt(e.params.data.title)];
            //     showDetailItem();
            //     //console.log(e.params.data.title);
            // });

        }


        function showDetailItem() {
            //console.log(currentItem);
            AutoNumeric.set('#itemDefaultPrice', currentItem.defaultPrice);
            AutoNumeric.set('#itemPrice', currentItem.price);
            AutoNumeric.set('#itemNewPrice', currentItem.price);

            if (currentItem.status == 1) {
                $('#itemActive').parent().addClass('checked');
            } else {
                $('#itemActive').parent().removeClass('checked');
            }
        }

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if (check_permission( 'prices:insert')): ?>
                        <div id="btnPriceCreate" class="btn btn-warning">Thêm mới</div>
                    <?php endif; ?>
                </div>
                <div class="box-body">
                    <div class="clearfix"></div>
                    <div class="row" id="pricesTable">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="mainTable" class="table table-bordered table-striped" style="width: 100%">
                                    <thead>
                                    <tr class="active">
                                        <th></th>
                                        <th>Bảng giá</th>
                                        <th>Chú thích</th>
                                        <th>Trạng thái</th>
                                        <th>Bắt đầu</th>
                                        <th>Kết thúc</th>
                                        <th>Cập nhật</th>
                                        <th>Ngày cập nhật</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="priceDetailModel" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Bảng giá</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 15px">
                        <li role="presentation" class="active"><a href="#itemTab" aria-controls="itemTab" role="tab"
                                                                  data-toggle="tab">Sản phẩm</a></li>
                        <li role="presentation"><a href="#shopTab" aria-controls="shopTab" role="tab" data-toggle="tab">Cửa
                                hàng</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="itemTab">
                            <!-- <div class="table-responsive"> -->
                            <table id="itemTable" class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                <tr class="active">
                                    <th></th>
                                    <th>Mã sản phẩm</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Nhóm</th>
                                    <th>Giá bán (đ)</th>
                                    <th>Giá chuẩn (đ)</th>
                                    <th>Giá chênh lệch (đ)</th>
                                    <th>Trạng thái</th>
                                    <th>Cập nhật</th>
                                    <th>Ngày cập nhật</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- </div> -->
                        </div>
                        <div role="tabpanel" class="tab-pane" id="shopTab">
                            <table id="shopTable" class="table table-bordered table-striped" style="width: 100%">
                                <thead>
                                <tr class="active">
                                    <!-- <th></th> -->
                                    <th>Mã cửa hàng</th>
                                    <th>Tên cửa hàng</th>
                                    <th>Mô hình</th>
                                    <th>Địa chỉ</th>
                                    <th>Ngày áp dụng</th>
                                    <!-- <th>Giá mặc định</th>
                                    <th>Người tạo</th>
                                    <th>Ngày tạo</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>-->
                <!--                <button id="btnSave" type="button" class="btn btn-warning">Cài đặt</button>-->
                <!--                <button id="btnDone" type="button" class="btn btn-warning" disabled="">Lưu và hoàn tất</button>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="priceDetailEditModel" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cài đặt giá</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sản phẩm</label>
                        <div class="col-sm-8">
                            <select name="itemId" class="form-control input-tip select2" id="selectItem"
                                    style="width: 100%">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Giá chuẩn</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control" id="itemDefaultPrice"
                                   placeholder="Giá chuẩn">
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-sm-3 control-label">Giá hiện tại</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control" id="itemPrice" placeholder="Giá hiện tại">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Giá bán</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="itemNewPrice" placeholder="Giá mới">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <div class="checkbox">
                                <label style="padding-left: 0px">
                                    <input type="checkbox" name="foo" id="itemActive"> Kích hoạt
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" id="btnItemSave" class="btn btn-warning">Lưu</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
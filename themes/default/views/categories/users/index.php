<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<?php
$v = "?v=1";

?>
<script type="text/javascript">

    $(document).ready(function() {
        var table = $('#UTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [ [10, 50, 100, 150, -1], [10, 50, 100, 150, "All"] ],
            // dom: 'lBfrtip',
            ajax : {
                url: '<?=site_url('admin/categories/get_users/'. $v);?>',
                type: 'POST',
                data: function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            // buttons: [
            //     { extend: 'excelHtml5',
            //         text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
            //         className: 'btn btn-warning',
            //         footer: true,
            //         exportOptions: { columns: [ 0, 1, 2, 3, 4, 5] },
            //         init: function(api, node, config) {
            //            $(node).removeClass('btn-default')
            //         }
            //     }
            // ],
            // columns: [
            //     { "data": "USERNAME"},
            //     { "data": "FULLNAME"},
            //     { "data": "EMP_CODE"},
            //     { "data": "EMAIL"},
            //     { "data": "TYPE_NAME"},
            //     { "data": "STATUS"},
            //     { "data": null, "searchable": false, "orderable": false, "render" : function ( data, type, row ) {
            //             return '<div class="text-center"><div class="btn-group-obs"><a class="btn btn-warning btn-xs" title="<?=lang('permission')?>" href="<?php echo base_url('users/permission/') ?>'+ row['USERNAME']+'" ><i class="fa fa-key"></i></a><a class="btn btn-warning btn-xs" title="<?=lang('update')?>" href="<?php echo base_url('users/edit/') ?>'+ row['ROW_ID']+'" ><i class="fa fa-edit"></i></a><a class="btn btn-danger btn-xs" title="<?=lang('delete')?>" href="<?php echo base_url('admin/users/delete/') ?>'+ row['ROW_ID']+'" onclick="return confirm(\''+ '<?=lang('alert_x_user')?>' +'\')" ><i class="fa fa-trash-o"></i></a></div></div>';
            //         }
            //     }
            // ]
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                /* Bold the grade for all 'A' grade browsers */
                // alert ('the grade of row ' + iDisplayIndex + ' is ' + aData[4]);
                console.log(aData);
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });
    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <!-- <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a> -->
                    <?php if( 'users:insert' )):?>
                        <a href="<?=site_url('categories/users/create')?>" class="btn btn-warning btn-sm">Thêm mới</a>
                    <?php endif;?>

                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr class="active">
                                        <th><?=lang('username'); ?></th>
                                        <th class="col-xs-2"><?=lang('full_name'); ?></th>
                                        <th class="col-xs-3"><?=lang('emp_code'); ?></th>
                                        <th><?=lang('email'); ?></th>
                                        <th class="col-xs-1"><?=lang('permission'); ?></th>
                                        <th class="col-xs-2"><?=lang('actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

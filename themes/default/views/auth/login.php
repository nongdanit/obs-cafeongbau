<?php (defined('BASEPATH')) or exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= $page_title . ' | ' . $Settings->site_name; ?></title>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" name="viewport">
    <link href="<?= $assets ?>dist/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="<?= $assets ?>dev/css/style.css?<?= time() ?>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="https://google.com/recaptcha/api.js"></script>
    <style type="text/css">
        .g-recaptcha {
            transform: scale(1.06);
            -webkit-transform: scale(1.06);
            transform-origin: 0 0;
            -webkit-transform-origin: 0 0;
        }
    </style>
</head>
<body class="login-page login-page-<?= $Settings->theme_style; ?> rtl rtl-inv">
<div class="login-logo">
    <a href="<?= base_url(); ?>">
        <img src="<?php echo base_url('uploads/header_login.png') ?>" alt="Logo"/>
    </a>
</div>
<div class="login-box">
    <div class="login-box-body">
        <?php if ($error) { ?>
            <div style="padding: 2px;">
                <span style="color: red;"><?= $error; ?></span>
            </div>
        <?php }
        if ($message) { ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?= $message; ?>
            </div>
        <?php } ?>
        <?= form_open("login"); ?>
        <div class="form-group has-feedback">
            <input type="text" name="identity" value="<?= set_value('identity'); ?>" class="form-control"
                   placeholder="<?= lang('username'); ?>"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" value="<?= DEMO ? '12345678' : ''; ?>" class="form-control"
                   placeholder="<?= lang('password'); ?>"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <div class="g-recaptcha form-group" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div>

        <button type="submit" class="btn btn-warning btn-block btn-flat"><i class="glyphicon glyphicon-log-in"></i>
            &nbsp;<?= lang('sign_in'); ?></button>

        <?= form_close(); ?>

    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open("admin/auth/forgot_password"); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</i></button>
                <h4 class="modal-title"><?= lang('forgot_password'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= lang('forgot_password_heading'); ?></p>
                <input type="email" name="forgot_email" placeholder="<?= lang('email'); ?>" autocomplete="off"
                       class="form-control placeholder-no-fix">
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left"
                        type="button"><?= lang('close'); ?></button>
                <button class="btn btn-primary" type="submit"><?= lang('submit'); ?></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script src="<?= $assets ?>dev/js/jquery-3.6.0.min.js"></script>
<script src="<?= $assets ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>

<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-16">
    <title><?= $page_title.' | '.$Settings->site_name; ?></title>
    <!-- Set your app to full screen mode -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Set your app name -->
    <meta name="apple-mobile-web-app-title" content="OB System">
    <!-- Set app’s status bar style -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?= $assets ?>images/app_57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= $assets ?>images/app_60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $assets ?>images/app_72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= $assets ?>images/app_76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $assets ?>images/app_114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= $assets ?>images/app_120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= $assets ?>images/app_144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= $assets ?>images/app_144x144.png">

    <link rel="apple-touch-startup-image" href="<?= $assets ?>images/obs-logo-320×480.png" media="screen resolution">
    <!-- For landscape mode -->
    <link rel="apple-touch-startup-image" href="<?= $assets ?>images/obs-logo-748×1024.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
    <!-- For portrait mode -->
    <link rel="apple-touch-startup-image" href="<?= $assets ?>images/obs-logo-768×1004.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />

    <link rel="icon" type="image/png" href="<?= $assets ?>images/app_192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="<?= $assets ?>images/favicon_48x48.png" sizes="96x96">
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <!-- Set the viewport -->
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />

   
    <link href="<?= $assets ?>dist/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="<?= $assets ?>dev/css/chosen/chosen.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $assets ?>dev/css/style.css?<?= time()?>" rel="stylesheet" type="text/css" />
    <script src="<?= $assets ?>dev/js/jquery-3.6.0.min.js"></script>

    <script type="text/javascript">
        var base_url = '<?=base_url();?>';
        var site_url = '<?=site_url();?>';
        $(document).ready(function(){
                // iOS web app full screen hacks.
                if(window.navigator.standalone == true) {
                    // make all link remain in web app mode.
                    $('a').click(function() {
                        window.location = $(this).attr('href');
                        return false;
                    });
                }
        });
    </script>
</head>
<body class="skin-<?= $Settings->theme_style; ?> fixed sidebar-mini <?=$classbody?>">
<div class="wrapper">

    <header class="main-header">
        <a href="<?= site_url($this->session->userdata('homepage')); ?>" class="logo">
            <span class="logo-mini">OBS</span>
            <span class="logo-lg"><img style="width: 156px;" src="<?php echo base_url('uploads/oblogo.png')?>"></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <?php if(ENVIRONMENT !== 'production'){?>
                <ul class="nav navbar-nav pull-left">
                    <li class="dropdown hidden-x">
                        <a class="dropdown-toggle">Environment TEST</a>
                    </li>
                    <!-- <li class="dropdown hidden-xs">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?= $assets; ?>images/<?= $Settings->selected_language; ?>.png" alt="<?= $Settings->selected_language; ?>"></a>
                        <ul class="dropdown-menu">
                            <?php $scanned_lang_dir = array_map(function ($path) {
                                return basename($path);
                            }, glob(APPPATH . 'language/*', GLOB_ONLYDIR));
                            foreach ($scanned_lang_dir as $entry) { ?>
                                <li><a href="<?= site_url('pos/language/' . $entry); ?>"><img
                                            src="<?= $assets; ?>images/<?= $entry; ?>.png"
                                            class="language-img"> &nbsp;&nbsp;<?= ucwords($entry); ?></a></li>
                            <?php } ?>
                        </ul>
                    </li> -->
                </ul>
            <?php }?>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="hidden-xs hidden-sm"><a href="#" class="clock"></a></li>
                    <!-- <li class="hidden-xs"><a href="<?= site_url('welcome'); ?>" data-toggle="tooltip" data-placement="bottom" title="<?= lang('dashboard'); ?>"><i class="fa fa-dashboard"></i></a></li> -->

                    <!-- <li class="hidden-xs"><a href="<?= site_url('settings'); ?>" data-toggle="tooltip" data-placement="bottom" title="<?= lang('settings'); ?>"><i class="fa fa-cogs"></i></a></li> -->

                    <li class="dropdown user user-menu" style="padding-right:5px;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= base_url('uploads/avatars/thumbs/male.png')?>" class="user-image" alt="Avatar" />
                            <span class="hidden-xs"><?= $this->session->userdata('userName'); ?></span>
                        </a>
                        <ul class="dropdown-menu" style="padding-right:3px;">
                            <li class="user-headers">
                                <a href="<?= site_url('employer/profile/'.$this->session->userdata('userId')); ?>" class=""><i class="fa fa-key"></i><?= lang('change_password'); ?></a>
                            </li>
                            <li class="user-footers">
                                    <a href="<?= site_url('logout'); ?>" class=""><i class="fa fa-sign-out"></i><?= lang('sign_out'); ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <?php $menu = $this->session->userdata('menus');?>
                <?php foreach ($menu as $mainMenu) :?>
                    <li class="<?= count($mainMenu['child']) ? 'treeview':''?> mm_<?=strtolower($mainMenu['formId']) ?>">
                        <?php if(count($mainMenu['child'])):
                                $url = site_url($mainMenu['formId'].'/'.$mainMenu['child'][0]['formId']);
                            else:
                                $url = site_url($mainMenu['formId']);
                            endif;
                        ?>
                        <a href="<?= $url ?>"><i class="fa <?= $mainMenu['image'] ?>"></i> <span><?= $mainMenu['menuName'] ?></span><?= count($mainMenu['child'])?'<i class="fa fa-angle-left pull-right"></i>':''?></a>
                        <?php if(count($mainMenu['child'])):?>
                            <ul class="treeview-menu">
                                <?php foreach ($mainMenu['child'] as $valueChild): ?>
                                    <?php if($valueChild['menuName'] === '-'):?>
                                        <li class="divider"></li>
                                    <?php else:?>
                                        <li id="<?=strtolower($mainMenu['formId'])?>_<?=strtolower($valueChild['formId'])?>"><a href="<?= site_url($mainMenu['formId'].'/'.strtolower($valueChild['formId'])); ?>"><i class="fa <?= $valueChild['image'] ?>"></i> <?= $valueChild['menuName'] ?></a></li>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </ul>
                        <?php endif?>
                    </li>
                <?php endforeach?>
            </ul>
        </section>
    </aside>

    <div class="content-wrapper">
        <section class="content-header">
            <h1><?= $page_title; ?></h1>
            <ol class="breadcrumb">
                <li><a href="<?= site_url('welcome'); ?>"><i class="fa fa-dashboard"></i> <?= lang('dashboard'); ?></a></li>
                <?php
                foreach ($bc as $b) {
                    if ($b['link'] === '#') {
                        echo '<li class="active">' . $b['page'] . '</li>';
                    } else {
                        echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                    }
                }
                ?>
            </ol>
        </section>

        <div class="col-lg-12 alerts">
            <div id="custom-alerts" style="display:none;">
                <div class="alert alert-dismissable">
                    <div class="custom-msg"></div>
                </div>
            </div>
            <?php if ($error)  { ?>
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-ban"></i> <?= lang('error'); ?></h4>
                <?= $error; ?>
            </div>
            <?php } if ($warning) { ?>
            <div class="alert alert-warning alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-warning"></i> <?= lang('warning'); ?></h4>
                <?= $warning; ?>
            </div>
            <?php } if ($message) { ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4>    <i class="icon fa fa-check"></i> <?= lang('Success'); ?></h4>
                <?= $message; ?>
            </div>
            <?php } ?>
        </div>
        <div class="clearfix"></div>

<style type="text/css">
    th, td {
        white-space: nowrap;
    }

    .background-color {
        background-color: #FFBC10 !important;
    }

    .dataTables_filter {
        float: left !important;
    }

    .mr-1 {
        margin-right: 5px !important;
    }

    /*.dataTables_scrollHeadInner {*/
    /*    width: 100% !important*/
    /*}*/

</style>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-warning">

                <div class="box-body">
                    <form class="form-inline">
                        <div class="form-group">
                            <label>Năm:</label>
                            <select id="selectYear" class="form-control">
                                <option value="2022" selected="selected">2022</option>
                                <option value="2021">2021</option>
                                <option value="2020">2020</option>
                            </select>
                            <!--                            <input type="" class="form-control" id="exampleInputName2" placeholder="Jane Doe">-->
                        </div>
                        <button id="btnSearch" type="button" class="btn btn btn-warning">Xem</button>
                    </form>
                    <div class="col-sm-12 col-md-12" style="margin-top: 10px; padding: 0px">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#aopTab" aria-controls="home" role="tab"
                                                                      data-toggle="tab">AOP</a></li>
                            <li role="presentation"><a href="#nsoTab" aria-controls="profile" role="tab"
                                                       data-toggle="tab">AOP NSO</a></li>
                            <li role="presentation"><a href="#realTab" aria-controls="messages" role="tab"
                                                       data-toggle="tab">AOP Thực tế</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content" style="padding-top: 5px">
                            <div role="tabpanel" class="tab-pane active" id="aopTab">
                                <div class="table-responsive">
                                    <table id="aopTable" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Mã cửa hàng</th>
                                            <th>Cửa hàng</th>
                                            <th>Năm</th>
                                            <th>Tổng</th>
                                            <th>Tháng 1</th>
                                            <th>Tháng 2</th>
                                            <th>Tháng 3</th>
                                            <th>Tháng 4</th>
                                            <th>Tháng 5</th>
                                            <th>Tháng 6</th>
                                            <th>Tháng 7</th>
                                            <th>Tháng 8</th>
                                            <th>Tháng 9</th>
                                            <th>Tháng 10</th>
                                            <th>Tháng 11</th>
                                            <th>Tháng 12</th>
                                        </tr>

                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="nsoTab">
                                <div class="table-responsive">
                                    <table id="nsoTable" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Mã</th>
                                            <th>Loại hình</th>
                                            <th>Mô hình</th>
                                            <th>Tỉnh/Thành</th>
                                            <th>Năm</th>
                                            <th>Năm</th>
                                            <th>Năm</th>
                                            <th>Tổng</th>
                                            <th>Tháng 1</th>
                                            <th>Tháng 2</th>
                                            <th>Tháng 3</th>
                                            <th>Tháng 4</th>
                                            <th>Tháng 5</th>
                                            <th>Tháng 6</th>
                                            <th>Tháng 7</th>
                                            <th>Tháng 8</th>
                                            <th>Tháng 9</th>
                                            <th>Tháng 10</th>
                                            <th>Tháng 11</th>
                                            <th>Tháng 12</th>
                                        </tr>

                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="realTab">
                                <div class="table-responsive">
                                    <table id="realTable" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Mã</th>
                                            <th>Loại hình</th>
                                            <th>Mô hình</th>
                                            <th>Tỉnh/Thành</th>
                                            <th>Năm</th>
                                            <th>Năm</th>
                                            <th>Năm</th>
                                            <th>Tổng</th>
                                            <th>Tháng 1</th>
                                            <th>Tháng 2</th>
                                            <th>Tháng 3</th>
                                            <th>Tháng 4</th>
                                            <th>Tháng 5</th>
                                            <th>Tháng 6</th>
                                            <th>Tháng 7</th>
                                            <th>Tháng 8</th>
                                            <th>Tháng 9</th>
                                            <th>Tháng 10</th>
                                            <th>Tháng 11</th>
                                            <th>Tháng 12</th>
                                        </tr>

                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="orderModal" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= form_open('', array('method' => 'post', 'id' => 'formCreateAOP')); ?>
            <div class="modal-header">
                <h3 class="modal-title" id="aopModalTitle">Thêm mới AOP</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- <form id="formCreateOrder"> -->
                                <div class="form-group">
                                    <label class="control-label" for="code"><?= lang("name_store"); ?></label>
                                    <?php
                                    unset($shopList['All']);
                                    echo form_dropdown('shopCode', $shopList, set_value('shopCode'), 'id="selectShop" data-placeholder="Chọn cửa hàng" class="form-control select2" style="width:100%"');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="address"><?= lang("address"); ?></label>
                                    <?php
                                    echo form_dropdown('shopAddress', $shopAddress, set_value('shopAddress'), 'id="selectShopAddress" class="form-control select2" disabled style="width:100%"');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="orderCode">Năm</label>
                                    <select id="aopYear" class="form-control">
                                        <option value="2022" selected="selected">2022</option>
                                        <option value="2021">2021</option>
                                        <option value="2020">2020</option>
                                    </select>
                                    <!--                                    --><? //= form_input('orderCode', set_value('orderCode', '2022'), 'class="form-control" id="aopYear" disabled '); ?>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 1</label>
                                        <input type="text" id="m1" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 2</label>
                                        <input type="text" id="m2" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 3</label>
                                        <input type="text" id="m3" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 4</label>
                                        <input type="text" id="m4" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>

                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 5</label>
                                        <input type="text" id="m5" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 6</label>
                                        <input type="text" id="m6" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 7</label>
                                        <input type="text" id="m7" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 8</label>
                                        <input type="text" id="m8" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>

                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 9</label>
                                        <input type="text" id="m9" name="salary" class="form-control aopValue" value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 10</label>
                                        <input type="text" id="m10" name="salary" class="form-control aopValue"
                                               value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 11</label>
                                        <input type="text" id="m11" name="salary" class="form-control aopValue"
                                               value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 5px">
                                        <label class="control-label" for="date">Tháng 12</label>
                                        <input type="text" id="m12" name="salary" class="form-control aopValue"
                                               value="0"
                                               autocomplete="off"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                    </div>

                                </div>
                                <div class="form-group hidden">
                                    <label class="control-label" for="status">Status</label>
                                    <?= form_input('orderStatus', set_value('status', 1), 'class="form-control" id="aopStatus" disabled '); ?>
                                </div>
                                <!-- </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-warning">Lưu</button>
                <label class="hidden" id="aopId">0</label>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
    <input id='uploadFile' type='file' name="file" alt="aop"
           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="hidden"
           onChange="return uploadFile()"/>
</div>


<script type="text/javascript">
    var aopTable, nsoTable, realTable;
    $(document).ready(function () {

        $('.toggle_form').click(function () {
            $("#form").slideToggle();
            return false;
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust().fixedColumns().relayout();
            ;
        })

        $('#btnSearch').click(function () {
            aopTable.ajax.reload();
            nsoTable.ajax.reload();
            realTable.ajax.reload();
        });

        $('#selectShop').on('change', function (e) {
            let shop = $(this).val();
            $('#selectShopAddress').val(shop);
            $('#selectShopAddress').trigger('change'); // Notify any JS components that the value changed
        });

        $("#formCreateAOP").validate({
            rules: {
                shopCode: {
                    required: true
                },
            },
            messages: {
                shopCode: {
                    required: 'Vui lòng chọn cửa hàng'
                },
            },
            submitHandler: function (form) {
                let aopId = $('#aopId').html();
                let shopCode = $('#selectShop').val();
                let aopYear = $('#aopYear').val();
                let aopStatus = $('#aopStatus').val();
                let m1 = AutoNumeric.getNumber('#m1');
                let m2 = AutoNumeric.getNumber('#m2');
                let m3 = AutoNumeric.getNumber('#m3');
                let m4 = AutoNumeric.getNumber('#m4');
                let m5 = AutoNumeric.getNumber('#m5');
                let m6 = AutoNumeric.getNumber('#m6');
                let m7 = AutoNumeric.getNumber('#m7');
                let m8 = AutoNumeric.getNumber('#m8');
                let m9 = AutoNumeric.getNumber('#m9');
                let m10 = AutoNumeric.getNumber('#m10');
                let m11 = AutoNumeric.getNumber('#m11');
                let m12 = AutoNumeric.getNumber('#m12');
                let data = {
                    id: aopId,
                    shopCode: shopCode,
                    aopYear: aopYear,
                    status: aopStatus,
                    m1: m1,
                    m2: m2,
                    m3: m3,
                    m4: m4,
                    m5: m5,
                    m6: m6,
                    m7: m7,
                    m8: m8,
                    m9: m9,
                    m10: m10,
                    m11: m11,
                    m12: m12,
                };
                // console.log(aopId);
                if (aopId == 0) {
                    $.post('<?= base_url('api/bi/aop') ?>', data, function (data) {
                        if (data.success) {
                            alert('Thêm mới AOP thành công.');
                            $('#orderModal').modal('hide');
                            // if (aopTable != null) detailTable.clear().draw();
                            aopTable.ajax.reload();
                        } else {
                            alert(data.message);
                        }
                    }, 'json');
                } else {
                    $.ajax({
                        url: '<?= base_url('api/bi/aop/') ?>' + aopId,
                        type: 'put',
                        contentType: 'application/json',
                        data: JSON.stringify(data),
                        success: function (result) {
                            // console.log(result);
                            if (result.success) {
                                alert('Cập nhật AOP thành công.');
                                $('#orderModal').modal('hide');
                                // if (aopTable != null) detailTable.clear().draw();
                                aopTable.ajax.reload();
                            } else {
                                alert(data.message);
                            }
                            // Do something with the result
                        }
                    });
                }

            }
        });

        aopTable = $('#aopTable').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 600,
            scrollCollapse: true,
            scroller: true,
            deferRender: true,
            select: {
                info: false,
                style: 'single'
            },
            search: {
                "caseInsensitive": true,
            },
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            ajax: {
                url: '<?=base_url('api/bi/aop');?>',
                type: 'GET',
                data: {
                    year: function () {
                        return $('#selectYear').val();
                    }
                }
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="glyphicon glyphicon-plus"></i> Thêm mới',
                    className: 'btn btn-warning mr-1',
                    footer: false,
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        // alert('test');
                        showAopModel('create');
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Import',
                    className: 'btn btn-warning mr-1',
                    footer: false,
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        // alert('test');
                        $('#uploadFile').attr('alt', 'aop');
                        document.getElementById('uploadFile').click();
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    // exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]},
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                },

            ],
            // fixedColumns: {
            //     leftColumns: 3
            // },
            columns: [
                {title: 'Mã cửa hàng', data: "shopCode"},
                {title: 'Cửa hàng', data: "shopName"},
                {title: 'Năm', data: "aopYear"},
                {title: 'Tổng', data: 'totalAop', className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 1', data: "M1", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 2', data: "M2", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 3', data: "M3", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 4', data: "M4", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 5', data: "M5", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 6', data: "M6", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 7', data: "M7", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 8', data: "M8", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 9', data: "M9", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 10', data: "M10", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 11', data: "M11", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 12', data: "M12", className: 'text-right', render: numberWithCommas},
            ],

        });

        aopTable.on('dblclick', 'tr', function () {
            if (!aopTable.rows().data().length) return;
            let row = aopTable.row(this);
            let rowData = row.data();
            // console.log(rowData);
            showAopModel('update', rowData);
        });

        aopTable.on('user-select', function (e, dt, type, cell, originalEvent) {
                // if (cell.index().column == 0) return false;
                let row = dt.row(cell.index().row).node();
                $(row).removeClass('hover');
                if ($(row).hasClass('selected')) {
                    e.preventDefault();
                    // deselect
                }
            }
        )

        nsoTable = $('#nsoTable').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 600,
            scrollCollapse: true,
            scroller: true,
            deferRender: true,
            select: {
                info: false,
                style: 'single'
            },
            search: {
                "caseInsensitive": true,
            },
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            ajax: {
                url: '<?=base_url('api/bi/nso');?>',
                type: 'GET',
                data: {
                    year: function () {
                        return $('#selectYear').val();
                    }
                }
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="glyphicon glyphicon-plus"></i> Thêm mới',
                    className: 'btn btn-warning mr-1 hidden',
                    footer: false,
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        // alert('test');
                        showAopModel('create');
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Import',
                    className: 'btn btn-warning mr-1',
                    footer: false,
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        // alert('test');
                        $('#uploadFile').attr('alt', 'nso');
                        document.getElementById('uploadFile').click();
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    // exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]},
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                },

            ],
            // fixedColumns: {
            //     leftColumns: 3
            // },
            columns: [
                {title: 'Mã', data: "shopCode"},
                {title: 'Loại hình', data: "shopModel"},
                {title: 'Mô hình', data: "shopType"},
                {title: 'Tỉnh/Thành', data: "provinceName"},
                {title: 'Năm', data: "aopYear"},
                {title: 'Tháng mở mới', data: "monthOpen"},
                {title: 'Số cửa hàng', data: "newShopCount"},
                {title: 'Tổng', data: 'totalAop', className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 1', data: "M1", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 2', data: "M2", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 3', data: "M3", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 4', data: "M4", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 5', data: "M5", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 6', data: "M6", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 7', data: "M7", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 8', data: "M8", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 9', data: "M9", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 10', data: "M10", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 11', data: "M11", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 12', data: "M12", className: 'text-right', render: numberWithCommas},
            ],

        });

        nsoTable.on('user-select', function (e, dt, type, cell, originalEvent) {
                // if (cell.index().column == 0) return false;
                let row = dt.row(cell.index().row).node();
                $(row).removeClass('hover');
                if ($(row).hasClass('selected')) {
                    e.preventDefault();
                    // deselect
                }
            }
        )

        realTable = $('#realTable').DataTable({
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            dom: 'lBfrtip',
            scrollX: true,
            scrollY: 600,
            scrollCollapse: true,
            scroller: true,
            deferRender: true,
            select: {
                info: false,
                style: 'single'
            },
            search: {
                "caseInsensitive": true,
            },
            oLanguage: {
                sEmptyTable: "Không có dữ liệu",
                sSearch: "Tìm kiếm nhanh:",
                sLengthMenu: "Hiển thị _MENU_ dòng",
                sInfo: "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            ajax: {
                url: '<?=base_url('api/bi/real');?>',
                type: 'GET',
                data: {
                    year: function () {
                        return $('#selectYear').val();
                    }
                }
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="glyphicon glyphicon-plus"></i> Thêm mới',
                    className: 'btn btn-warning mr-1 hidden',
                    footer: false,
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        // alert('test');
                        showAopModel('create');
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Import',
                    className: 'btn btn-warning mr-1',
                    footer: false,
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                    action: function (e, dt, node, config) {
                        // alert('test');
                        $('#uploadFile').attr('alt', 'real');
                        document.getElementById('uploadFile').click();
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: false,
                    // exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]},
                    init: function (api, node, config) {
                        $(node).removeClass('btn-default')
                    },
                },

            ],
            // fixedColumns: {
            //     leftColumns: 3
            // },
            columns: [
                {title: 'Mã', data: "shopCode"},
                {title: 'Loại hình', data: "shopModel"},
                {title: 'Mô hình', data: "shopType"},
                {title: 'Tỉnh/Thành', data: "provinceName"},
                {title: 'Năm', data: "aopYear"},
                {title: 'Tháng mở mới', data: "monthOpen"},
                {title: 'Số cửa hàng', data: "newShopCount"},
                {title: 'Tổng', data: 'totalAop', className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 1', data: "M1", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 2', data: "M2", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 3', data: "M3", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 4', data: "M4", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 5', data: "M5", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 6', data: "M6", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 7', data: "M7", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 8', data: "M8", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 9', data: "M9", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 10', data: "M10", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 11', data: "M11", className: 'text-right', render: numberWithCommas},
                {title: 'Tháng 12', data: "M12", className: 'text-right', render: numberWithCommas},
            ],

        });

        realTable.on('user-select', function (e, dt, type, cell, originalEvent) {
                // if (cell.index().column == 0) return false;
                let row = dt.row(cell.index().row).node();
                $(row).removeClass('hover');
                if ($(row).hasClass('selected')) {
                    e.preventDefault();
                    // deselect
                }
            }
        )

        initAopNumeric();

        function initAopNumeric() {
            const autoNumericOptionsEuro = {
                // allowDecimalPadding: AutoNumeric.options.allowDecimalPadding.never,
                // caretPositionOnFocus: AutoNumeric.options.caretPositionOnFocus.null,
                // currencySymbol: AutoNumeric.options.currencySymbol.none,
                // currencySymbolPlacement: AutoNumeric.options.currencySymbolPlacement.suffix,
                decimalPlaces: 0,
                minimumValue: 0,
            };
            AutoNumeric.multiple('.aopValue', 0, autoNumericOptionsEuro);
        }

        function showAopModel(type, rowData = null) {
            $('#aopId').html(0);
            if (type == 'update') {
                // console.log(rowData.id);
                $('#aopId').html(rowData.id);
                $('#aopModalTitle').html('AOP - Cập nhật');
                $('#selectShop').prop('disabled', true);
                $('#aopYear').prop('disabled', true);
                $('#selectShop').val(rowData.shopCode);
                $('#selectShop').trigger('change');
                // AutoNumeric.setNumber()
                setAopValue('#m1', rowData.M1);
                setAopValue('#m2', rowData.M2);
                setAopValue('#m3', rowData.M3);
                setAopValue('#m4', rowData.M4);
                setAopValue('#m5', rowData.M5);
                setAopValue('#m6', rowData.M6);
                setAopValue('#m7', rowData.M7);
                setAopValue('#m8', rowData.M8);
                setAopValue('#m9', rowData.M9);
                setAopValue('#m10', rowData.M10);
                setAopValue('#m11', rowData.M11);
                setAopValue('#m12', rowData.M12);
            } else if (type == 'create') {
                $('#aopModalTitle').html('AOP - Thêm mới');
                $('#selectShop').prop('disabled', false);
                $('#aopYear').prop('disabled', false);
                $('#selectShop').val(''); // Select the option with a value of '1'
                $('#selectShop').trigger('change');
            }
            $('#orderModal').modal('show');
        }

        function setAopValue(elmId, value) {
            AutoNumeric.getAutoNumericElement(elmId).set(value);
        }

    })

    function uploadFile() {
        let fileData = $('#uploadFile').prop('files');
        if (fileData.length == 0) return;
        let type = $('#uploadFile').attr('alt');
        let myFormData = new FormData();
        jQuery.each(fileData, function (i, file) {
            myFormData.append('file', file);
        });
        $.ajax({
            url: '<?= base_url('api/bi/import/')?>' + type, // Upload Script
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            async: true,
            data: myFormData, // Setting the data attribute of ajax with file_data
            success: function (response) {
                // Do something after Ajax completes
                if (response.success) {
                    if (type == 'aop') {
                        aopTable.ajax.reload();
                    } else if (type == 'nso') {
                        nsoTable.ajax.reload();
                    }else if (type == 'real') {
                        realTable.ajax.reload();
                    }
                }else{
                    alert(response.message);
                }
            }
        }).always(function () {
            // alert('')
        });
        $("#uploadFile").val(null);
    }
</script>

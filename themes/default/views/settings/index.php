<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<style type="text/css">
    th, td{white-space: nowrap;}
    .background-color{ background-color: #FFBC10 !important; }
    .DTFC_LeftBodyLiner { overflow-y: unset !important; }

    /*.dataTables_scrollBody {
        transform:rotateX(180deg);
    }
    .dataTables_scrollBody table {
        transform:rotateX(180deg);
    }*/
</style>
<?php
    $v = "?v=1";
    if ($this->input->get('shoptype')) {
        $v .= "&shoptype=".$this->input->get('shoptype');
    }
    if ($this->input->get('province_code')) {
        $v .= "&province_code=".$this->input->get('province_code');
    }
    if ($this->input->get('district_code')) {
        $v .= "&district_code=".$this->input->get('district_code');
    }
    if ($this->input->get('shopmodel')) {
        $v .= "&shopmodel=".$this->input->get('shopmodel');
    }
    if ($this->input->get('status')) {
        $v .= "&status=".$this->input->get('status');
    }
?>
<script type="text/javascript">
    function check_location(x) {
        if (x == 0) {
            return '<span class="label label-warning">Không</span>';
        } else {
            return '<span class="label label-success">Có</span>';
        }
    }
    $(document).ready(function() {
        var table = $('#UTable').DataTable({
            lengthMenu: [ [20 ,50, 100, 150, -1], [20, 50, 100, 150, "All"] ],
            dom: 'lBfrtip',
            ajax : {
                url: '<?=site_url('admin/settings/get_list_shop/'. $v);?>',
                type: 'POST',
                "data": function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            // columnDefs : [
            //     {"targets": 7, "type":"date-eu"},
            //     {"targets": 8, "type":"date-eu"}
            // ],
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
            <?php //if( check_permission( $this->session->userdata('sys_menu'), $m, 2, 'Read', 'StoreConfig', 'Export' )):?>
                // { extend: 'excelHtml5',
                //     text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                //     className: 'btn btn-warning',
                //     footer: true,
                //     exportOptions: { columns: [ 0, 1, 2, 3, 4] },
                //     init: function(api, node, config) {
                //        $(node).removeClass('btn-default')
                //     }
                // }
            <?php //endif;?>
            ],
            columns: [
                { "data": "SHOP_CODE"},
                { "data": "SHOP_NAME"},
                { "data": "MIN_DISTANCE"},
                { "data": "CHECK_LOCATION", "orderable": false, "render": check_location},
                { "data": null, "searchable": false, "orderable": false, "render" : function ( data, type, row ) {
                        <?php if( check_permission( 'StoreConfig:Update' )):?>
                        return '<div class="text-center"><div class="btn-group"><a class="btn btn-warning btn-xs" shop_id="'+row['SHOP_ID']+'" max_distance="'+row['MIN_DISTANCE']+'" check_location="'+row['CHECK_LOCATION']+'" onclick="loadEditSettingsStoreConfig(this);return false;" href="javascript:void(0);" ><i class="fa fa-edit"></i></a></div></div>';
                    <?php endif;?>
                    }
                }
            ]
        });

        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });

        $('#staticModal').on('click', '#myFormSubmit', function(e) {
            var th = $(this);
            var form = th.closest('#form-popup');
            var action          = form.attr('action'),
                max_distance    = form.find('input[type="text"]').val(),
                shop_id         = form.find('input[type="hidden"]').val(),
                check_location  = form.find('select').val();
            var arr = {};
            $.extend(arr, { shop_id: shop_id, max_distance: max_distance, check_location: check_location });
            $.post(action, {arr: arr}, function(data, status, xhr){
                console.log(data);
                if(typeof data.error && data.error === true)
                {
                    console.log(data.message);
                    return false;
                } else {
                    alert(data.message);
                    $('#staticModal').modal('hide');
                    table.ajax.reload(null, false);
                    return true;
                }
            }, 'json');
        });

    });
</script>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <!-- <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a> -->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="UTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr class="active">
                                        <th><?=lang("stores_code"); ?></th>
                                        <th><?=lang("name_store"); ?></th>
                                        <th>Khoảng cách tối đa</th>
                                        <th>Kiểm tra khoảng cách</th>
                                        <th><?=lang('actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

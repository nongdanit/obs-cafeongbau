<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var target;
    var datadetail;
    var datatableNVLlistLapRapTrangThietBi;
    var datatablelistDaoTaoVanHanh;

    $(document).ready(function() {
        /**
         * Load DOI THU CANH TRANH
         */
        $.get("/business/loadCompetitors", {id: $("#doithucanhtranh").attr('idx')}, function(data){
            $(".rowdata .table > tbody").html(data);
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            target = $(e.target).attr("href");
            if(target == "#OBSRaDonHangTTBVaNVL"){
                datadetail = $('#listRaDonHangTTBVaNVL').DataTable( {
                    lengthMenu: [ [20, 50, 150, -1], [20, 50, 150, "All"] ],
                    destroy: true,
                    dom  : 'lBfrtip',
                    ajax : { url: '/business/getOrderStoreByShopId?shop_id=<?=$stores['ROW_ID']?>', type: 'GET', "data": function ( d ) {
                        d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                    }},
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                    },
                    buttons: [],
                    columns: [
                        { "data": "ITEM_CODE" },
                        { "data": "ITEM_NAME" },
                        { "data": "BRANCH_CODE" },
                        { "data": "QTY", "render": currencyFormat },
                        { "data": null, "searchable": false, "orderable": false, "render": function ( data, type, row ) {
                                return '<div class="text-center"><div class="btn-group-obs"><a onclick="loadmodalformedit(this, \'modalformedit\', 1);return false;" class="btn btn-warning btn-xs" id='+row['ROW_ID']+' code="'+row['ITEM_CODE']+'" name="'+row['ITEM_NAME']+'" qty='+row['QTY']+' group="'+row['BRANCH_CODE']+'" href="javascript:void(0);"><i class="fa fa-edit"></i></a></div></div>';
                            }
                        }
                    ],
                    fnRowCallback: function (nRow, aData, iDisplayIndex) {
                        if( iDisplayIndex == 0 || iDisplayIndex == 1 || iDisplayIndex == 2 ){
                            $('.create-order-store').hide();
                        }
                    },
                });

                /**
                 * Create order NVL
                 */
                $('.create-order-store').click(function(event) {
                    var th = $(this);
                    var shopid      = $(this).attr('shopid');
                    var formula_id  = $(this).attr('formula-id');
                    var idfather    = $(this).attr('idfather');
                    if(formula_id === null || formula_id == '' || formula_id == 0) {
                        $('#staticModal').on('show.bs.modal', function (event) {
                            var modal       = $(this);
                            modal.find('.modal-body').text('<?=lang('alert_update_store')?>');
                            modal.find('.href').hide();
                        });
                        $('#staticModal').modal('show');
                        $('#type_id').focus();
                        return false;
                    }else{
                        $.ajax({
                            url: base_url + "/business/createOrderStore",
                            type: "POST",
                            dataType: 'json',
                            data: { shopid: shopid, formula_id: formula_id, idfather: idfather },
                            cache: false,
                            success: function(e)
                            {
                                if(typeof e.error && e.error === true){
                                    $('#staticModal').on('show.bs.modal', function (event) {
                                        var modal       = $(this);
                                        modal.find('.modal-body').html(e.message);
                                        modal.find('.href').hide();
                                    });
                                    $('#staticModal').modal('show');
                                    return false;
                                }else{
                                    datadetail.clear().draw();
                                    datadetail.rows.add( e.data ).draw();
                                }
                            }
                        });
                    }

                });
            }else if(target == "#OBSLapRapTrangThietBi"){
                datatableNVLlistLapRapTrangThietBi = $('#listLapRapTrangThietBi').DataTable( {
                    lengthMenu: [ [20, 50, 150, -1], [20, 50, 150, "All"] ],
                    dom  : 'lBfrtip',
                    destroy: true,
                    ajax : { url: '/business/getListLRTTBByShopId?shop_id=<?=$stores['ROW_ID']?>', type: 'GET', "data": function ( d ) {
                        d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                    }},
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                    },
                    buttons: [],
                    columns: [
                        { "data": "ITEM_CODE" },
                        { "data": "ITEM_NAME" },
                        { "data": "BRANCH_CODE" },
                        { "data": "QTY", "render": currencyFormat },
                        { "data": "SETUP_DAY", "render": hrsd },
                        { "data": "NOTE", "render": nl2brjs },
                        { "data": null, "searchable": false, "orderable": false, "render": function ( data, type, row ) {
                                return '<div class="text-center"><div class="btn-group-obs"><a onclick="loadmodalformedit(this, \'modalformedit\', 2);return false;" class="btn btn-warning btn-xs" id='+row['ROW_ID']+' code="'+row['ITEM_CODE']+'" name="'+row['ITEM_NAME']+'" qty='+row['QTY']+' group="'+row['BRANCH_CODE']+'" setupdate='+hrsd(row['SETUP_DAY'])+' note="'+row['NOTE']+'"  href="javascript:void(0);"><i class="fa fa-edit"></i></a></div></div>';
                            }
                        }
                    ],
                    fnRowCallback: function (nRow, aData, iDisplayIndex) {
                        if( iDisplayIndex == 0 || iDisplayIndex == 1 || iDisplayIndex == 2 ){
                            $('.create-order-store').hide();
                        }
                    },
                });
            }else if(target == "#OBSDaoTaoVanHang"){
                datatablelistDaoTaoVanHanh = $('#listDaoTaoVanHanh').DataTable( {
                    lengthMenu: [ [20, 50, 150, -1], [20, 50, 150, "All"] ],
                    dom  : 'lBfrtip',
                    destroy: true,
                    ajax : { url: '/business/getListClassTraning?shop_id=<?=$stores['ROW_ID']?>', type: 'GET', "data": function ( d ) {
                        d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                    }},
                    oLanguage: {
                        "sEmptyTable": "Không có dữ liệu",
                        "sSearch": "Tìm kiếm nhanh:",
                        "sLengthMenu": "Hiển thị _MENU_ dòng",
                        "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
                    },
                    buttons: [],
                    columns: [
                        { "data": "TITLE" },
                        { "data": "TRAINERS" },
                        { "data": "TRAINING_DAY", "render": hrsd },
                        { "data": "QTY_JOIN", "render": currencyFormat },
                        { "data": "NOTE", "render": nl2brjs },
                        { "data": null, "searchable": false, "orderable": false, "render": function ( data, type, row ) {
                                return '<div class="text-center"><div class="btn-group-obs"><a onclick="loadmodalformedit(this, \'modalformedit\', 3);return false;" class="btn btn-warning btn-xs" id='+row['ROW_ID']+' title="'+row['TITLE']+'" trainer="'+row['TRAINERS']+'" qty_join='+row['QTY_JOIN']+' trainingday='+hrsd(row['TRAINING_DAY'])+' note_training="'+row['NOTE']+'"  href="javascript:void(0);"><i class="fa fa-edit"></i></a></div></div>';
                            }
                        }
                    ],
                    fnRowCallback: function (nRow, aData, iDisplayIndex) {
                        if( iDisplayIndex == 0 || iDisplayIndex == 1 || iDisplayIndex == 2 ){
                            $('.create_class_tranning').hide();
                        }
                    },
                });
                /**
                 * create_class_tranning
                 */
                $('.create_class_tranning').click(function(event) {
                    var th = $(this);
                    var shopid      = $(this).attr('shopid');
                    var idfather    = $(this).attr('idfather');
                    $.ajax({
                        url: base_url + "/business/createClassTrainning",
                        type: "POST",
                        dataType: 'json',
                        data: { shopid: shopid, idfather: idfather },
                        cache: false,
                        success: function(e)
                        {
                            if(typeof e.error && e.error === true){
                                $('#staticModal').on('show.bs.modal', function (event) {
                                    var modal       = $(this);
                                    modal.find('.modal-body').html(e.message);
                                    modal.find('.href').hide();
                                });
                                $('#staticModal').modal('show');
                                return false;
                            }else{
                                datatablelistDaoTaoVanHanh.clear().draw();
                                datatablelistDaoTaoVanHanh.rows.add( e.data ).draw();
                            }
                        }
                    });

                });
            }
        });

        $("#view").find( 'input[type=text], select' ).prop("disabled", true);
        $("#general").find( ':input, select' ).prop("disabled", true);
        $("select").removeClass('fa');

    });
</script>

<!-- <div class="settings" data-toggle="control-sidebar">
    <i class="fa fa-gears"></i>
</div> -->

<!-- <aside class="control-sidebar control-sidebar-light">
    <div class="tab-content">
        <div class="tab-pane active" id="control-sidebar-settings-tab">
            
        </div>
    </div>
</aside> -->
<div class="control-sidebar-bg"></div>
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-warning" id="general">
                <div class="box-header with-border">
                    <h3 class="box-title">Thông tin chung</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <?= form_open_multipart("business/submit_store", 'id="stores"');?>
                    <input type="hidden" name="formtype" value="edit">
                    <input type="hidden" id="shopid"  name="id" value="<?= $stores['ROW_ID'] ?>">
                    <div class="box-body">
                        <div class="col-lg-12">
                            <div class="row form-horizontal">
                                <div class="col-lg-12">
                                    <?php if(check_permission('mbs_store:Update' )):?>
                                        <div class="form-group pull-right">
                                            <a class="btn btn-warning allowform"><?=lang('edit')?></a>
                                        </div>
                                    <?php endif?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=lang('name_store')?><span class="required">(*)</span>
                                        <?= form_input('shop_name', set_value('shop_name', $stores['SHOP_NAME']), 'class="form-control tip" id="shop_name"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('address')?><span class="required">(*)</span>
                                        <?= form_input('address', set_value('address', $stores['ADDRESS']), 'class="form-control tip" id="address"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('cus_name')?>
                                        <?= form_input('cus_name', set_value('cus_name', $stores['CUS_NAME']), 'class="form-control tip" id="cus_name"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('stores_code')?>
                                        <?= form_input('shop_code', set_value('shop_code', $stores['SHOP_CODE']), 'class="form-control tip" id="shop_code"'); ?>
                                    </div>
                                    <div class="form-group">
                                        Đổi bảng hay mở mới<span class="required">(*)</span>
                                        <?php
                                            $changemodel[0] = 'Đổi bảng';
                                            $changemodel[1] = 'Mở mới';
                                        ?>
                                        <?= form_dropdown('changemodel', $changemodel, set_value('changemodel', (!empty($stores['SHOP_NAME_OLD']))?0:1), 'class="form-control select2" id="changemodel" '); ?>
                                    </div>
                                    <?php
                                        if(!empty($stores['SHOP_NAME_OLD'])){
                                            $readonly = '';
                                            $required = 'required';

                                        }else{
                                            $readonly = 'readonly';
                                            $required = '';
                                        }
                                    ?>
                                    <div class="form-group pre_model">
                                        Mô hình kinh doanh trước đây
                                        <?= form_input('pre_model', set_value('pre_model', $stores['SHOP_NAME_OLD']), 'class="form-control" id="pre_model" '.$readonly.' '.$required.'  ' ); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('status')?>
                                        <?php
                                            $arrStatus              = [];
                                            foreach ($listStatus as $key => $value) {
                                                $arrStatus[$value['STATUS_ID']] = $value['STATUS_NAME'];
                                            }
                                        ?>
                                        <?= form_dropdown('status', $arrStatus, $stores['STATUS'], 'class="form-control" '); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('note')?>
                                        <textarea id="note_shop" name="note_shop" class="form-control" cols="5" rows="5"><?=@$stores['NOTE']?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?=lang('phone')?>
                                        <?= form_input('tel', set_value('tel', @$stores['TEL']), 'class="form-control" id="tel"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('email')?>
                                        <?= form_input('email', set_value('email', @$stores['EMAIL']), 'class="form-control" id="email"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('province_name')?><span class="required">(*)</span>
                                        <?php
                                        $province_code[''] = "Chọn tỉnh/thành phố...";
                                        foreach($listProvince as $key => $value) {
                                            $province_code[$value['PROVINCE_CODE']] = $value['PROVINCE_NAME'];
                                        }
                                        ?>
                                        <?= form_dropdown('province_code', $province_code, set_value('province_code', $stores['PROVINCE_CODE']), 'class="form-control selectProvince select2" id="province_code" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('district_name')?><span class="required">(*)</span>
                                        <?php
                                        $district_code[''] = "Chọn quận huyện...";
                                        foreach($listDistrict as $key => $value) {
                                            $district_code[$value['DISTRICT_CODE']] = $value['DISTRICT_NAME'];
                                        }
                                        ?>
                                        <?= form_dropdown('district_code', $district_code, set_value('district_code', $stores['DISTRICT_CODE']), 'class="form-control selectDistrict select2" id="district" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('ward_name')?><span class="required">(*)</span>
                                        <?php
                                        $ward_code[''] = "Chọn phường/xã...";
                                        foreach($listWard as $key => $value) {
                                            $ward_code[$value['WARD_CODE']] = $value['WARD_NAME'];
                                        }
                                        ?>
                                        <?= form_dropdown('ward_code', $ward_code, set_value('ward_code', $stores['WARD_CODE']), 'class="form-control selectWard select2" id="ward" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group">
                                        Mô hình cửa hàng<span class="required">(*)</span>
                                        <?php
                                        $type_id[''] = 'Chọn mô hình cửa hàng';
                                        foreach($listShopType as $key => $value) {
                                            $type_id[$value['ROW_ID']] = $value['TITLE'];
                                        }
                                        ?>
                                        <?= form_dropdown('type_id', $type_id, set_value('type_id', $stores['TYPE_ID']), 'class="form-control tip" id="type_id" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" id="lon" name="lon" value="<?=$stores['LNG']?>">
                                        <input type="hidden" id="lat" name="lat" value="<?=$stores['LAT']?>">
                                        Lấy vị trí:
                                        <a id="get_address" class="btn btn-warning" ><i class="fa fa-location-arrow"></i></a>
                                        <p id="error_map"></p>
                                        <div id="mapholder" style="height: 250px; width: 100%"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        Mô hình kinh doanh<span class="required">(*)</span>
                                        <?php
                                        $que[''] = 'Chọn mô hình kinh doanh';
                                        foreach($listShopModel as $key => $value) {
                                            $que[$value['ROW_ID']] = $value['TITLE'];
                                        }
                                        ?>
                                        <?= form_dropdown('model_id', $que, set_value('model_id', $stores['MODEL_ID']), 'class="form-control tip" id="model_id" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group">
                                        Ngày khảo sát<span class="required">(*)</span>
                                        <?= form_input('start_date', set_value('start_date', ( !empty($stores['START_DATE']) && $stores['START_DATE']!='0001-01-01T00:00:00')?date('d-m-Y', strtotime($stores['START_DATE'])):""), 'class="form-control datepicker" id="start_date"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?=lang('open_date')?> dự tính
                                        <?= form_input('open_date', set_value('open_date', ( !empty($stores['OPEN_DATE']) && $stores['OPEN_DATE']!='0001-01-01T00:00:00')?date('d-m-Y', strtotime($stores['OPEN_DATE'])):""), 'class="form-control datepicker" id="open_date"'); ?>
                                    </div>
                                    <div class="form-group formuploadimg_post">
                                        Hình ảnh
                                        <input shopid="<?=$stores['ROW_ID']?>" countimg='<?=@count(json_decode($stores['SHOP_IMAGEs'][0]['IMAGE']))?>' type="file" name="files" id="files" multiple class="form-control-file">
                                        <p style="color: red;">Số lượng tối đa là 12 tấm ảnh</p>
                                    </div>
                                    <div class="form-group">
                                        <div class="alert alert-danger" id="message" role="alert">
                                            This is a danger alert—check it out!
                                        </div>
                                        <div id="uploaded_images">
                                        <?php if( !empty($stores['SHOP_IMAGEs'][0]['IMAGE']) && $stores['SHOP_IMAGEs'][0]['IMAGE'] != 'null') :
                                            $image = json_decode($stores['SHOP_IMAGEs'][0]['IMAGE']);
                                            if(is_array($image)):
                                            $dataImage = array(
                                                'image_current' => $image
                                            );
                                            $this->session->set_userdata($dataImage);
                                            foreach($image as $k => $v):
                                            ?>
                                            <div class="col-sm-3">
                                                <a shopid="<?=$stores['ROW_ID']?>" onclick="del_image(this)" name_image="<?=$v?>" class="del_image" title="Xóa">x</a>
                                                <img src="<?= site_url().URL_SHOPS.$v?>" class="img-responsive img-thumbnail" data-action="zoom">
                                            </div>
                                        <?php endforeach;?>
                                        <?php endif; endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if(!empty($stores['AREA_SHOP_INFOs'])):?>
                                <div class="row">
                                <?php foreach ($stores['AREA_SHOP_INFOs'] as $keyWORKINGs => $valueWORKINGs):?>
                                    <div class="col-md-6">
                                        <div class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?=$valueWORKINGs['TITLE']?></h3>
                                                <div class="box-tools pull-right">
                                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <?php
                                                        $countWORKING_DETAILs = round( (count($valueWORKINGs['WORKING_DETAILs']) ) / 2 );
                                                        ?>
                                                        <div class="col-md-6">
                                                            <?php
                                                            foreach ($valueWORKINGs['WORKING_DETAILs'] as $key => $workingDetail) {
                                                                $lastkey =  $key;
                                                                if($key >= $countWORKING_DETAILs) break;
                                                                ?>
                                                                <div class="form-group">
                                                                    <?=$workingDetail['TITLE']?>
                                                                    <?php
                                                                    $input_type_web = json_decode($workingDetail['INPUT_TYPE_WEB'], true);
                                                                    if($input_type_web['type'] == 'text'):
                                                                        $unit = (!empty($input_type_web['unit'])) ? '('.$input_type_web['unit'].')' : '';
                                                                        echo $unit;
                                                                        if($input_type_web["attr"]['layout'] == 1):
                                                                            $arrVal = json_decode($workingDetail['CONTENT'], true);

                                                                            if($input_type_web["attr"]["class"] == 'number') $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                        ?>
                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' " '.@$input_type_web["attr"]["required"].' '); ?>
                                                                        <?php else:?>

                                                                            <?php
                                                                                $scope = $input_type_web['scope'];
                                                                            if(count($scope)>0):
                                                                            ?>
                                                                            <?php $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                if($input_type_web["attr"]["class"] == 'number'){
                                                                                    $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                                    $classSelect = "";
                                                                                }else{
                                                                                    $classSelect = $input_type_web["attr"]["class"];
                                                                                }
                                                                            ?>
                                                                                <div class="row">
                                                                                    <div class="col-lg-4">
                                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' " '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                    </div>
                                                                                    <div class="col-lg-8">
                                                                                        <?php
                                                                                            $arr = [];
                                                                                            // $arr[''] = 'Vui lòng chọn';
                                                                                            foreach($scope as $key => $value) {
                                                                                                $arr[$value] = $value;
                                                                                            }
                                                                                        ?>
                                                                                        <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                            <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['exp'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                        <?php else:?>
                                                                                            <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['exp'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                        <?php endif?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php else:?>
                                                                                <?php
                                                                                $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                if($input_type_web["attr"]["class"] == 'number'){
                                                                                    $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                                    $arrVal['value2'] = $this->data_lib->formatNumber($arrVal['value2']);
                                                                                }
                                                                                ?>
                                                                                <div class="row">
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' " '.@$input_type_web["attr"]["required"].' placeholder="Case 1" '); ?>
                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title2_'.$workingDetail['ROW_ID'], @$arrVal['value2'],'class="form-control '. $input_type_web["attr"]["class"] .' " '.@$input_type_web["attr"]["required"].' placeholder="Case 2"'); ?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php endif;?>
                                                                        <?php endif;?>
                                                                    <?php elseif($input_type_web['type'] == 'select'):?>
                                                                        <?php
                                                                            if($input_type_web["attr"]["class"] == 'number'){
                                                                                $classSelect = "";
                                                                            }else{
                                                                                $classSelect = $input_type_web["attr"]["class"];
                                                                            }
                                                                        ?>
                                                                        <?php if($input_type_web["attr"]['layout'] == 1):
                                                                            $scope = $input_type_web['scope'];
                                                                            $arr = [];
                                                                            // $arr[''] = 'Vui lòng chọn';
                                                                            foreach($scope as $key => $value) {
                                                                                $arr[$value] = $value;
                                                                            }
                                                                            $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                            ?>
                                                                            <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['value'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '.@$input_type_web["attr"]["required"].' '); ?>
                                                                            <?php else:?>
                                                                                <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['value'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["required"].' '); ?>
                                                                            <?php endif?>
                                                                        <?php elseif($input_type_web["attr"]['layout'] == 2):?>
                                                                            <div class="row">
                                                                                <div class="col-lg-6">
                                                                                    <?php $scope = $input_type_web['scope'];
                                                                                    $arr = [];
                                                                                    // $arr[''] = 'Vui lòng chọn';
                                                                                    foreach($scope as $key => $value) {
                                                                                        $arr[$value] = $value;
                                                                                    }
                                                                                    $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                    ?>
                                                                                    <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                        <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['value'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                    <?php else:?>
                                                                                        <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['value'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                    <?php endif?>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <textarea name="textarea_<?=$workingDetail['ROW_ID']?>" class="form-control <?=$input_type_web["attr"]["class"]?>" cols="5" rows="5"><?=$workingDetail['NOTE']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        <?php endif;?>
                                                                    <?php endif?>
                                                                </div>
                                                            <?php }?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php
                                                            foreach ($valueWORKINGs['WORKING_DETAILs'] as $key => $workingDetail) :
                                                                if($key < $lastkey) continue;
                                                                ?>
                                                                <div class="form-group">
                                                                    <?php
                                                                    $input_type_web = json_decode($workingDetail['INPUT_TYPE_WEB'], true);
                                                                    echo ($input_type_web['type'] != 'doithucanhtranh')?$workingDetail['TITLE']:"";

                                                                    if($input_type_web['type'] == 'text'):
                                                                        $unit = (!empty($input_type_web['unit'])) ? '('.$input_type_web['unit'].')' : '';
                                                                        echo $unit;
                                                                        if($input_type_web["attr"]['layout'] == 1):
                                                                            $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                            if($input_type_web["attr"]["class"] == 'number') $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                        ?>
                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], $arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' " '.@$input_type_web["attr"]["required"].' '); ?>
                                                                        <?php else:?>

                                                                            <?php
                                                                                $scope = $input_type_web['scope'];
                                                                            if(count($scope)>0):
                                                                            ?>
                                                                            <?php $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                if($input_type_web["attr"]["class"] == 'number'){
                                                                                    $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                                    $classSelect = "";
                                                                                }else{
                                                                                    $classSelect = $input_type_web["attr"]["class"];
                                                                                }
                                                                            ?>
                                                                                <div class="row">
                                                                                    <div class="col-lg-4">
                                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' " '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                    </div>
                                                                                    <div class="col-lg-8">
                                                                                        <?php
                                                                                            $arr = [];
                                                                                            // $arr[''] = 'Vui lòng chọn';
                                                                                            foreach($scope as $key => $value) {
                                                                                                $arr[$value] = $value;
                                                                                            }
                                                                                        ?>
                                                                                        <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                            <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['exp'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                        <?php else:?>
                                                                                            <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['exp'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                        <?php endif?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php else:?>
                                                                                <?php
                                                                                $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                if($input_type_web["attr"]["class"] == 'number'){
                                                                                    $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                                    $arrVal['value2'] = $this->data_lib->formatNumber($arrVal['value2']);
                                                                                }
                                                                                ?>
                                                                                <div class="row">
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' " '.@$input_type_web["attr"]["required"].' placeholder="Case 1" '); ?>
                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title2_'.$workingDetail['ROW_ID'], @$arrVal['value2'],'class="form-control '. $input_type_web["attr"]["class"] .' " '.@$input_type_web["attr"]["required"].' placeholder="Case 2"'); ?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php endif;?>
                                                                        <?php endif;?>
                                                                    <?php elseif($input_type_web['type'] == 'select'):?>
                                                                        <?php
                                                                            if($input_type_web["attr"]["class"] == 'number'){
                                                                                $classSelect = "";
                                                                            }else{
                                                                                $classSelect = $input_type_web["attr"]["class"];
                                                                            }
                                                                        ?>
                                                                        <?php if($input_type_web["attr"]['layout'] == 1):
                                                                            $scope = $input_type_web['scope'];
                                                                            $arr = [];
                                                                            // $arr[''] = 'Vui lòng chọn';
                                                                            foreach($scope as $key => $value) {
                                                                                $arr[$value] = $value;
                                                                            }
                                                                            $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                            ?>
                                                                            <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['value'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '.@$input_type_web["attr"]["required"].' '); ?>
                                                                            <?php else:?>
                                                                                <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['value'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["required"].' '); ?>
                                                                            <?php endif?>
                                                                        <?php elseif($input_type_web["attr"]['layout'] == 2):?>
                                                                            <div class="row">
                                                                                <div class="col-lg-6">
                                                                                    <?php $scope = $input_type_web['scope'];
                                                                                    $arr = [];
                                                                                    // $arr[''] = 'Vui lòng chọn';
                                                                                    foreach($scope as $key => $value) {
                                                                                        $arr[$value] = $value;
                                                                                    }
                                                                                    $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                    ?>
                                                                                    <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                        <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['value'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                    <?php else:?>
                                                                                        <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['value'], 'class="form-control '. $classSelect .'" style="width:100%;" '.@$input_type_web["attr"]["required"].' '); ?>
                                                                                    <?php endif?>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <textarea name="textarea_<?=$workingDetail['ROW_ID']?>" class="form-control <?=$input_type_web["attr"]["class"]?>" cols="5" rows="5"><?=$workingDetail['NOTE']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        <?php endif;?>

                                                                    <?php elseif($input_type_web['type'] == 'doithucanhtranh'):?>
                                                           </div></div>
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                Đối thủ cạnh tranh
                                                                                <button type="button" class="btn btn-warning mb-1" data-toggle="modal" id="doithucanhtranh" idx="<?=$workingDetail['ROW_ID']?>">Thêm đối thủ cạnh tranh</button>
                                                                            </div>
                                                                            <div class="row rowdata">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                          <th scope="col">Thương hiệu</th>
                                                                                          <th scope="col">Khoảng cách đến mặt bằng</th>
                                                                                          <th scope="col">Doanh thu TB/khách</th>
                                                                                          <th scope="col">SL khách TB/ngày</th>
                                                                                          <th scope="col">Doanh thu TB/ngày</th>
                                                                                          <th scope="col"></th>
                                                                                      </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>

                                                                    <?php endif?>
                                                                <?php if($input_type_web['type'] != 'doithucanhtranh'): ?></div><?php endif;?>
                                                            <?php endforeach;?>

                                                        <?php if($input_type_web['type'] != 'doithucanhtranh'): ?></div><?php endif;?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>

                                <?php endforeach;?>
                                </div>
                            <?php endif;?>

                            <div class="form-group">
                                <?= form_submit('update', lang('save'), 'class="btn btn-warning"'); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="col-lg-12"><span class="label label-danger">(*) bắt buộc, không được bỏ trống</span></p></div>
                    </div>
                <?= form_close();?>
            </div>

            <div class="nav-tabs-custom" id="view">
                <ul class="nav nav-tabs">
                    <?php if(!empty($stores['WORKINGs'])):
                        foreach ($stores['WORKINGs'] as $keyWORKINGs => $valueWORKINGs):?>
                            <?php if($keyWORKINGs==0){
                                $class = 'active';
                            }else $class = '';?>
                    <li class="<?=$class?>"><a href="#OBS<?=$valueWORKINGs['FUNCTION_CODE']?>" data-toggle="tab"><?=$valueWORKINGs['TITLE']?></a></li>
                    <?php endforeach;endif;?>
                </ul>

                <div class="tab-content">
                    <?php if(!empty($stores['WORKINGs'])): $obs = 0;
                        foreach ($stores['WORKINGs'] as $key => $value):?>
                            <?php if($key==0){
                                $class = 'active';
                            }else $class = '';?>
                        <div class="<?=$class?> tab-pane" id="OBS<?=$value['FUNCTION_CODE']?>">
                            <form action="<?= site_url('business/submitupdateworking')?>" method="post" name="form-<?=$value['FUNCTION_CODE']?>" id="<?=$value['FUNCTION_CODE']?>">
                                <input type="hidden" name="type_function" value="<?=$value['FUNCTION_CODE']?>">
                                <input type="hidden" name="working_id" value="<?=$value['ROW_ID']?>">
                                <div id="">
                                    <div class="row form-horizontal">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label"><?=lang('start_date')?></label>
                                                <div class="col-sm-8">
                                                    <?= form_input('start_date',check_date($value['START_DATE']),'class="form-control datepicker"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label"><?=lang('end_date')?></label>
                                                <div class="col-sm-8">
                                                    <?= form_input('end_date',check_date($value['END_DATE']),'class="form-control datepicker"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label"><?=lang('status')?></label>
                                                <div class="col-sm-8">
                                                    <?php
                                                        $arrStatus              = [];
                                                        $arrStatus[OPEN]        = lang('status_open');
                                                        $arrStatus[DOING]       = lang('status_doing');
                                                        $arrStatus[NOTAPPROVE]  = lang('status_notapprove');
                                                        $arrStatus[OPEN]        = lang('status_open');
                                                        $arrStatus[APPROVE]     = lang('action_approve');
                                                    ?>
                                                    <?= form_dropdown('status', $arrStatus, $value['STATUS'], 'class="form-control" '); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?php
                                            $positioncurent   = $value['POS'];
                                            $keys       = $key - 1;
                                            if ( $keys >= 0 ) {
                                                $rowIdBefore   = $stores['WORKINGs'][$keys]['ROW_ID'];
                                                $positionBefore= $stores['WORKINGs'][$keys]['POS'];
                                                if($positioncurent == $positionBefore){
                                                    $obs ++;
                                                    $keys = $key - 1 - $obs;
                                                    $rowIdBefore   = $stores['WORKINGs'][$keys]['ROW_ID'];
                                                }else{
                                                    $obs = 0;
                                                }

                                            }else{
                                                $rowIdBefore = 0;
                                                $positionBefore = 0;
                                            }
                                        ?>
                                        <?php if( check_permission( 'mbs_store:Update' )):?>
                                        <a parentid=0 shopid="<?=$stores['ROW_ID']?>" position="<?=$positioncurent?>" rowidbefore="<?=$rowIdBefore?>" class="btn btn-warning editform"><?=lang('edit').' '.$value['TITLE']?></a>
                                        <?php endif;?>
                                        <?= form_submit('Update', lang('update'), 'class="btn btn-warning submitform" style="display:none;" idx="'. $value['FUNCTION_CODE'] .'" '); ?>
                                    </div>
                                </div>
                            </form>
                            <?php if(!empty($value['WORKING_CHILDRENs'])):
                                foreach ($value['WORKING_CHILDRENs'] as $keyWORKINGs => $valueWORKINGs):?>
                                    <form action="<?= site_url('business/submitupdateworking')?>" method="post" name="form-<?=$valueWORKINGs['FUNCTION_CODE']?>" id="<?=$valueWORKINGs['FUNCTION_CODE']?>">
                                        <input type="hidden" name="type_function" value="<?=$valueWORKINGs['FUNCTION_CODE']?>">
                                        <input type="hidden" name="working_id" value="<?=$valueWORKINGs['ROW_ID']?>">
                                        <div id="" class="box box-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?=$valueWORKINGs['TITLE']?></h3>
                                                <div class="box-tools pull-right">
                                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-lg-12">
                                                    <div class="row form-horizontal">
                                                        <div class="col-lg-12">
                                                            <div class="form-group pull-right">
                                                                <?php if( check_permission( 'mbs_store:Update' )):?>
                                                                <a idfather="<?=$valueWORKINGs['PARENT_ID']?>" class="btn btn-warning editform"><?=lang('edit').' '.$valueWORKINGs['TITLE']?></a>
                                                                <?php endif;?>
                                                                <?= form_submit('Update', lang('update'), 'class="btn btn-warning submitform" style="display:none;" idx="'. $valueWORKINGs['FUNCTION_CODE'] .'" '); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-horizontal">
                                                        <div class="col-lg-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-4 control-label"><?=lang('start_date')?></label>
                                                                <div class="col-sm-8">
                                                                    <?= form_input('start_date',check_date($valueWORKINGs['START_DATE']),'class="form-control datepicker"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-4 control-label"><?=lang('end_date')?></label>
                                                                <div class="col-sm-8">
                                                                    <?= form_input('end_date',check_date($valueWORKINGs['END_DATE']),'class="form-control datepicker"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-4 control-label"><?=lang('status')?></label>
                                                                <div class="col-sm-8">
                                                                    <?php
                                                                        $arrStatus              = [];
                                                                        $arrStatus[OPEN]        = lang('status_open');
                                                                        $arrStatus[DOING]       = lang('status_doing');
                                                                        $arrStatus[NOTAPPROVE]  = lang('status_notapprove');
                                                                        $arrStatus[OPEN]        = lang('status_open');
                                                                        $arrStatus[APPROVE]     = lang('action_approve');
                                                                    ?>
                                                                    <?= form_dropdown('status', $arrStatus, $valueWORKINGs['STATUS'], 'class="form-control" '); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <?php
                                                        $countWORKING_DETAILs = round( (count($valueWORKINGs['WORKING_DETAILs']) ) / 2 );
                                                        ?>
                                                        <div class="col-md-6">
                                                            <?php
                                                            foreach ($valueWORKINGs['WORKING_DETAILs'] as $key => $workingDetail) {
                                                                $lastkey =  $key;
                                                                if($key >= $countWORKING_DETAILs) break;
                                                                ?>
                                                                <div class="form-group">
                                                                    <?=$workingDetail['TITLE']?>
                                                                    <?php
                                                                    $input_type_web = json_decode($workingDetail['INPUT_TYPE_WEB'], true);
                                                                    if($input_type_web['type'] == 'text'):
                                                                        $unit = (!empty($input_type_web['unit'])) ? '('.$input_type_web['unit'].')' : '';
                                                                        echo $unit;
                                                                        if($input_type_web["attr"]['layout'] == 1):
                                                                            $arrVal = json_decode($workingDetail['CONTENT'], true);

                                                                            if($input_type_web["attr"]["class"] == 'number') $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                        ?>
                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' "'); ?>
                                                                        <?php else:?>

                                                                            <?php
                                                                                $scope = $input_type_web['scope'];
                                                                            if(count($scope)>0):
                                                                            ?>
                                                                            <?php $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                if($input_type_web["attr"]["class"] == 'number') $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                            ?>
                                                                                <div class="row">
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' "'); ?>
                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <?php
                                                                                            $arr[''] = 'Vui lòng chọn';
                                                                                            foreach($scope as $key => $value) {
                                                                                                $arr[$value] = $value;
                                                                                            }
                                                                                        ?>
                                                                                        <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                            <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['exp'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '); ?>
                                                                                        <?php else:?>
                                                                                            <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['exp'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '); ?>
                                                                                        <?php endif?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php else:?>
                                                                                <?php
                                                                                $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                if($input_type_web["attr"]["class"] == 'number'){
                                                                                    $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                                    $arrVal['value2'] = $this->data_lib->formatNumber($arrVal['value2']);
                                                                                }
                                                                                ?>
                                                                                <div class="row">
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' " placeholder="Case 1" '); ?>
                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title2_'.$workingDetail['ROW_ID'], @$arrVal['value2'],'class="form-control '. $input_type_web["attr"]["class"] .' " placeholder="Case 2"'); ?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php endif;?>
                                                                        <?php endif;?>
                                                                    <?php elseif($input_type_web['type'] == 'select'):?>
                                                                        <?php if($input_type_web["attr"]['layout'] == 1):
                                                                            $scope = $input_type_web['scope'];
                                                                            $arr = [];
                                                                            $arr[''] = 'Vui lòng chọn';
                                                                            foreach($scope as $key => $value) {
                                                                                $arr[$value] = $value;
                                                                            }
                                                                            $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                            ?>
                                                                            <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['value'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '); ?>
                                                                            <?php else:?>
                                                                                <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['value'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '); ?>
                                                                            <?php endif?>
                                                                        <?php elseif($input_type_web["attr"]['layout'] == 2):?>
                                                                            <div class="row">
                                                                                <div class="col-lg-6">
                                                                                    <?php $scope = $input_type_web['scope'];
                                                                                    $arr = [];
                                                                                    $arr[''] = 'Vui lòng chọn';
                                                                                    foreach($scope as $key => $value) {
                                                                                        $arr[$value] = $value;
                                                                                    }
                                                                                    $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                    ?>
                                                                                    <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                        <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['value'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '); ?>
                                                                                    <?php else:?>
                                                                                        <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['value'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '); ?>
                                                                                    <?php endif?>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <textarea name="textarea_<?=$workingDetail['ROW_ID']?>" class="form-control <?=$input_type_web["attr"]["class"]?>" cols="5" rows="5"><?=$workingDetail['NOTE']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        <?php endif;?>
                                                                    <?php endif?>
                                                                </div>
                                                            <?php }?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php
                                                            foreach ($valueWORKINGs['WORKING_DETAILs'] as $key => $workingDetail) :
                                                                if($key < $lastkey) continue;
                                                                ?>
                                                                <div class="form-group">
                                                                    <?php
                                                                    $input_type_web = json_decode($workingDetail['INPUT_TYPE_WEB'], true);
                                                                    echo ($input_type_web['type'] != 'doithucanhtranh')?$workingDetail['TITLE']:"";

                                                                    if($input_type_web['type'] == 'text'):
                                                                        $unit = (!empty($input_type_web['unit'])) ? '('.$input_type_web['unit'].')' : '';
                                                                        echo $unit;
                                                                        if($input_type_web["attr"]['layout'] == 1):
                                                                            $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                            if($input_type_web["attr"]["class"] == 'number') $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                        ?>
                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], $arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' "'); ?>
                                                                        <?php else:?>

                                                                            <?php
                                                                                $scope = $input_type_web['scope'];
                                                                            if(count($scope)>0):
                                                                            ?>
                                                                            <?php $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                if($input_type_web["attr"]["class"] == 'number') $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                            ?>
                                                                                <div class="row">
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' "'); ?>
                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <?php
                                                                                            $arr[''] = 'Vui lòng chọn';
                                                                                            foreach($scope as $key => $value) {
                                                                                                $arr[$value] = $value;
                                                                                            }
                                                                                        ?>
                                                                                        <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                            <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['exp'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '); ?>
                                                                                        <?php else:?>
                                                                                            <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['exp'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '); ?>
                                                                                        <?php endif?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php else:?>
                                                                                <?php
                                                                                $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                if($input_type_web["attr"]["class"] == 'number'){
                                                                                    $arrVal['value'] = $this->data_lib->formatNumber($arrVal['value']);
                                                                                    $arrVal['value2'] = $this->data_lib->formatNumber($arrVal['value2']);
                                                                                }
                                                                                ?>
                                                                                <div class="row">
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title_'.$workingDetail['ROW_ID'], @$arrVal['value'],'class="form-control '. $input_type_web["attr"]["class"] .' " placeholder="Case 1" '); ?>
                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <?= form_input('title2_'.$workingDetail['ROW_ID'], @$arrVal['value2'],'class="form-control '. $input_type_web["attr"]["class"] .' " placeholder="Case 2"'); ?>
                                                                                    </div>
                                                                                </div>
                                                                            <?php endif;?>
                                                                        <?php endif;?>
                                                                    <?php elseif($input_type_web['type'] == 'select'):?>
                                                                        <?php if($input_type_web["attr"]['layout'] == 1):
                                                                            $scope = $input_type_web['scope'];
                                                                            $arr = [];
                                                                            $arr[''] = 'Vui lòng chọn';
                                                                            foreach($scope as $key => $value) {
                                                                                $arr[$value] = $value;
                                                                            }
                                                                            $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                            ?>
                                                                            <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['value'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '); ?>
                                                                            <?php else:?>
                                                                                <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['value'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '); ?>
                                                                            <?php endif?>
                                                                        <?php elseif($input_type_web["attr"]['layout'] == 2):?>
                                                                            <div class="row">
                                                                                <div class="col-lg-6">
                                                                                    <?php $scope = $input_type_web['scope'];
                                                                                    $arr = [];
                                                                                    $arr[''] = 'Vui lòng chọn';
                                                                                    foreach($scope as $key => $value) {
                                                                                        $arr[$value] = $value;
                                                                                    }
                                                                                    $arrVal = json_decode($workingDetail['CONTENT'], true);
                                                                                    ?>
                                                                                    <?php if(!empty($input_type_web["attr"]["multiple"])):?>
                                                                                        <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'].'[]', $arr, @$arrVal['value'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '.@$input_type_web["attr"]["multiple"].' '); ?>
                                                                                    <?php else:?>
                                                                                        <?= form_dropdown('input_exp_'.$workingDetail['ROW_ID'], $arr, @$arrVal['value'], 'class="form-control '. $input_type_web["attr"]["class"] .'" style="width:100%;" '); ?>
                                                                                    <?php endif?>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <textarea name="textarea_<?=$workingDetail['ROW_ID']?>" class="form-control <?=$input_type_web["attr"]["class"]?>" cols="5" rows="5"><?=$workingDetail['NOTE']?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        <?php endif;?>

                                                                    <?php elseif($input_type_web['type'] == 'doithucanhtranh'):?>
                                                           </div></div>
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">
                                                                                Đối thủ cạnh tranh
                                                                                <button type="button" class="btn btn-warning mb-1" data-toggle="modal" id="doithucanhtranh" idx="<?=$workingDetail['ROW_ID']?>">Thêm đối thủ cạnh tranh</button>
                                                                            </div>
                                                                            <div class="row rowdata">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                          <th scope="col">Thương hiệu</th>
                                                                                          <th scope="col">Khoảng cách đến mặt bằng</th>
                                                                                          <th scope="col">Doanh thu TB/khách</th>
                                                                                          <th scope="col">SL khách TB/ngày</th>
                                                                                          <th scope="col">Doanh thu TB/ngày</th>
                                                                                          <th scope="col"></th>
                                                                                      </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>

                                                                    <?php endif?>
                                                                <?php if($input_type_web['type'] != 'doithucanhtranh'): ?></div><?php endif;?>
                                                            <?php endforeach;?>

                                                        <?php if($input_type_web['type'] != 'doithucanhtranh'): ?></div><?php endif;?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </form>
                            <?php endforeach;endif;?>

                            <?php if(@$value['FUNCTION_CODE'] == 'RaDonHangTTBVaNVL'):?>
                                <div id="" class="box box-warning">
                                    <div class="box-header with-border">
                                        <?php if( check_permission( 'mbs_store:Update' )):?>
                                        <h3 class="box-title"><a idfather="<?=$value['ROW_ID']?>" shopid="<?=$stores['ROW_ID']?>" formula-id="<?=$stores['FORMULA_ID']?>" class="btn btn-warning create-order-store"><?=lang('create_order')?></a></h3>
                                        <?php endif;?>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-lg-12">
                                            <div class="row listRaDonHangTTBVaNVL">
                                                <div class="col-sm-12">
                                                    <table id="listRaDonHangTTBVaNVL" class="table table-striped table-bordered table-condensed table-hover">
                                                         <thead>
                                                            <tr>
                                                                <th>Mã</th>
                                                                <th>Tên</th>
                                                                <th>Nhóm</th>
                                                                <th>Số lượng</th>
                                                                <th>Thao tác</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            <?php endif;?>

                            <?php if(@$value['FUNCTION_CODE'] == 'LapRapTrangThietBi'):?>
                                <div id="" class="box box-warning">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Danh sách trang thiết bị</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-lg-12">
                                            <div class="row LapRapTrangThietBi">
                                                <div class="col-sm-12">
                                                    <table id="listLapRapTrangThietBi" class="table table-striped table-bordered table-condensed table-hover">
                                                         <thead>
                                                            <tr>
                                                                <th>Mã</th>
                                                                <th>Tên</th>
                                                                <th>Nhóm</th>
                                                                <th>Số lượng</th>
                                                                <th>Ngày lắp đặt</th>
                                                                <th>Ghi chú</th>
                                                                <th>Thao tác</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            <?php endif;?>

                            <?php if(@$value['FUNCTION_CODE'] == 'DaoTaoVanHang' || @$value['FUNCTION_CODE'] == 'DaoTaoVanHanh' ):?>
                                <div id="" class="box box-warning">
                                    <div class="box-header with-border">
                                        <?php if( check_permission( 'mbs_store:Update' )):?>
                                        <h3 class="box-title"><a idfather="<?=$value['ROW_ID']?>" shopid="<?=$stores['ROW_ID']?>" class="btn btn-warning create_class_tranning"><?=lang('create_class_tranning')?></a></h3>
                                        <?php endif;?>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-lg-12">
                                            <div class="row DaoTaoVanHanh">
                                                <div class="col-sm-12">
                                                    <table id="listDaoTaoVanHanh" class="table table-striped table-bordered table-condensed table-hover">
                                                         <thead>
                                                            <tr>
                                                                <th>Tên lớp</th>
                                                                <th>Người đào tạo</th>
                                                                <th>Ngày đào tạo</th>
                                                                <th>SL người học</th>
                                                                <th>Ghi chú</th>
                                                                <th>Thao tác</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            <?php endif;?>

                        </div>
                    <?php endforeach;endif;?>
                </div>
            </div>


        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        /**
         * Thêm và Update Formula
         */
        $("#actionShopFormula").validate({
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    dataType: 'json',
                    data: $(form).serializeArray(),
                    url:  $('#actionShopFormula').attr('action'),
                    success: function(e) {
                        if(typeof e.dataReturn && e.dataReturn.error === true)
                        {
                            $('.show-error').html(e.dataReturn.message).show();
                            return false;
                        } else {
                            $('#actionShopFormula').trigger("reset");
                            $('#modalformedit').modal('hide');
                            if(e.dataReturn.message == 1){
                                datadetail.ajax.reload();
                            }else if(e.dataReturn.message == 2){
                                datatableNVLlistLapRapTrangThietBi.ajax.reload();

                                // datatableNVLlistLapRapTrangThietBi.row(this).data( [] ).draw();
                            }else if(e.dataReturn.message == 3){
                                datatablelistDaoTaoVanHanh.ajax.reload();
                            }
                        }
                    },
                    error: function(jqXHR, textStatus) {
                    }
                })
            }
        });
    });
</script>

<div class="modal fade"  id="modalformedit" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Cập nhật</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-warning">
                            <div class="box-header">
                            </div>
                            <div class="box-body">
                                <div class="col-lg-12">
                                    <?= form_open_multipart("business/actionShopFormula", 'id="actionShopFormula"');?>
                                    <input type="hidden" name="row_id" id="row_id" value="">
                                    <input type="hidden" name="type" id="type" value="">

                                    <div class="row type1and2">
                                        <p class="text-red show-error"></p>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Mã
                                                <?= form_input('code', set_value('code'), 'class="form-control" disabled id="code"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Tên
                                                <?= form_input('name', set_value('name'), 'class="form-control" disabled id="name" '); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Nhóm
                                                <?= form_input('group', set_value('group'), 'class="form-control" disabled id="group" '); ?>
                                            </div>
                                            <div class="form-group">
                                                Số lượng
                                                <?= form_input('qty', set_value('qty'), 'class="form-control" id="qty"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 expect" style="display: none;">
                                            <div class="form-group">
                                                Ngày
                                                <?= form_input('setupdate', set_value('setupdate'), 'class="form-control datepicker" id="setupdate" '); ?>
                                            </div>
                                            <div class="form-group">
                                                Ghi chú
                                                <textarea id="note" name="note" class="form-control" cols="5" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row type3">
                                        <p class="text-red show-error"></p>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Tên lớp
                                                <?= form_input('class_name', set_value('class_name'), 'class="form-control" disabled id="class_name"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Người đào tạo
                                                <?= form_input('trainer', set_value('trainer'), 'class="form-control" id="trainer" '); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Số người tham gia
                                                <?= form_input('qty_join', set_value('qty_join'), 'class="form-control" id="qty_join"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Ngày đào tạo
                                                <?= form_input('trainingday', set_value('trainingday'), 'class="form-control datepicker" id="trainingday" '); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Ghi chú
                                                <textarea id="note_training" name="note_training" class="form-control" cols="5" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=lang('cancel')?></button>
                                        <?= form_submit('add', lang('update'), 'class="btn btn-warning"'); ?>
                                    </div>
                                    <?= form_close();?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade"  id="doithucanhtranhModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Thêm đối thủ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-warning">
                            <div class="box-header">
                            </div>
                            <div class="box-body">
                                <div class="col-lg-12">
                                    <?= form_open_multipart("business/submitCompetitors", 'id="formCompetitors"');?>
                                    <input type="hidden" class="working_detail_id" name="working_detail_id" value="">
                                    <input type="hidden" class="row_id" name="row_id" value="0">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                Tên thương hiệu
                                                <?= form_input('title', set_value('title'), 'class="form-control tip" id="title"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Sản phẩm đặc trưng
                                                <?= form_input('content', set_value('content'), 'class="form-control" id="content"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Khoảng cách đến mặt bằng(m)
                                                <?= form_input('range', set_value('range'), 'class="form-control number" id="range" '); ?>
                                            </div>
                                            <div class="form-group">
                                                Mô hình cửa hàng
                                                <?= form_input('shop_model', set_value('shop_model'), 'class="form-control" id="shop_model" '); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                Doanh thu trung bình/khách
                                                <?= form_input('revenue_of_guest', set_value('revenue_of_guest'), 'class="form-control number revenue" id="revenue_of_guest"'); ?>
                                            </div>
                                            <div class="form-group">
                                                Số lượng khách trung bình/ngày
                                                <?= form_input('customer_of_day', set_value('customer_of_day'), 'class="form-control number revenue" id="customer_of_day" '); ?>
                                            </div>
                                            <div class="form-group">
                                                Doanh thu trung bình/ngày
                                                <?= form_input('revenue_of_day', set_value('revenue_of_day'), 'class="form-control number" id="revenue_of_day" readonly'); ?>
                                            </div>
                                            <div class="form-group formuploadimg_post">
                                                Hình ảnh
                                                <input competitorsid='' countimg='' type="file" name="image_competitors" id="image_competitors" multiple class="form-control-file">
                                                <p style="color: red;">Số lượng tối đa là 4 tấm ảnh</p>
                                            </div>
                                            <div class="form-group">
                                                <div style="display: none;" class="alert alert-danger" id="message_competitors" role="alert">
                                                    This is a danger alert—check it out!
                                                </div>
                                                <div id="uploaded_images_competitors">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=lang('cancel')?></button>
                                        <?= form_submit('add_doithucanhtranh', 'Thêm đối thủ cạnh tranh', 'class="btn btn-warning"'); ?>
                                    </div>
                                    <?= form_close();?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_API_KEY?>&libraries=places&callback=initMap" async defer></script>
<script src="<?= $assets ?>dev/js/map_google.js?r=<?=time()?>"></script>
<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="alert alert-warning show-error" style="display:none;">

                    </div>
                    <div class="col-lg-12">
                        <?= form_open_multipart("business/mbs_store/submit", 'id="stores"');?>
                        <input type="hidden" name="formtype" value="create">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?=lang('name_store')?><span class="required">(*)</span>
                                    <?= form_input('shop_name', set_value('shop_name'), 'class="form-control tip" id="shop_name"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('address')?><span class="required">(*)</span>
                                    <?= form_input('address', set_value('address'), 'class="form-control tip" id="address"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('cus_name')?>
                                    <?= form_input('cus_name', set_value('cus_name'), 'class="form-control tip" id="cus_name"'); ?>
                                </div>
                                <div class="form-group">
                                    Đổi bảng hay mở mới<span class="required">(*)</span>
                                    <?php
                                        $changemodel[0] = 'Đổi bảng';
                                        $changemodel[1] = 'Mở mới';
                                    ?>
                                    <?= form_dropdown('changemodel', $changemodel, set_value('changemodel'), 'class="form-control select2" id="changemodel" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group pre_model">
                                    Mô hình kinh doanh trước đây
                                    <?= form_input('pre_model', set_value('pre_model'), 'class="form-control" id="pre_model" required'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('note')?>
                                    <textarea id="note_shop" name="note_shop" class="form-control" cols="5" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?=lang('phone')?>
                                    <?= form_input('tel', set_value('tel'), 'class="form-control tip" id="tel"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('email')?>
                                    <?= form_input('email', set_value('email'), 'class="form-control" id="email"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('province_name')?><span class="required">(*)</span>
                                    <?php
                                    $provinceList['All'] = "Chọn tỉnh/thành phố...";
                                    ?>
                                    <?= form_dropdown('province_code', $provinceList, set_value('province_code'), 'class="form-control selectProvinceEdit select2" id="selectProvinceEdit" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('district_name')?><span class="required">(*)</span>
                                    <?php
                                    $arr['All'] = "Chọn quận huyện...";
                                    ?>
                                    <?= form_dropdown('district_code', $arr, set_value('district_code'), 'class="form-control select2" id="selectDistrictEdit" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('ward_name')?><span class="required">(*)</span>
                                    <?php
                                    $ward_code[''] = "Chọn phường/xã...";
                                    ?>
                                    <?= form_dropdown('ward_code', $ward_code, set_value('ward_code'), 'class="form-control select2" id="selectWardEdit" style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    Mô hình cửa hàng<span class="required">(*)</span>
                                    <?php
                                    $que[''] = 'Chọn mô hình cửa hàng';
                                    foreach($listShopType as $key => $value) {
                                        $que[$value['ROW_ID']] = $value['TITLE'];
                                    }
                                    ?>
                                    <?= form_dropdown('type_id', $que, set_value('type_id'), 'class="form-control tip" id="type_id" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    Mô hình kinh doanh<span class="required">(*)</span>
                                    <?php
                                    $model[''] = 'Chọn mô hình kinh doanh';
                                    foreach($listShopModel as $key => $value) {
                                        $model[$value['ROW_ID']] = $value['TITLE'];
                                    }
                                    ?>
                                    <?= form_dropdown('model_id', $model, set_value('model_id'), 'class="form-control tip" id="model_id" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    Ngày khảo sát<span class="required">(*)</span>
                                    <?= form_input('start_date', set_value('start_date'), 'class="form-control datepicker" id="start_date"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('open_date')?> dự tính
                                    <?= form_input('open_date', set_value('open_date'), 'class="form-control datepicker" id="open_date"'); ?>
                                </div>
                                <div class="form-group formuploadimg_post">
                                    Chọn ảnh
                                    <input shopid="0" countimg='0' type="file" name="files" id="files" multiple class="form-control-file">
                                    <p style="color: red;">Số lượng tối đa là 12 tấm ảnh</p>
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-danger" id="message" role="alert">
                                        This is a danger alert—check it out!
                                    </div>
                                    <div id="uploaded_images"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?= form_submit('add_shop', lang('add_shop'), 'class="btn btn-warning"'); ?>
                        </div>
                        <?= form_close();?>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="col-lg-12"><span class="label label-danger">(*) bắt buộc, không được bỏ trống</span></p></div>
                </div>
            </div>
        </div>
    </div>
</section>

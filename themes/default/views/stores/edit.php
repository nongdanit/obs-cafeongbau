<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <!-- <h3 class="box-title"><?= lang('enter_info'); ?></h3> -->
                </div>
                <div class="box-body">
                    <div class="col-lg-12">
                        <?= form_open_multipart("stores/submit", 'id="stores"');?>
                        <input type="hidden" name="formtype" value="edit">
                        <input type="hidden" name="id" value="<?= $stores['ROW_ID'] ?>">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?=lang('name_store')?>
                                    <?= form_input('shop_name', set_value('shop_name', $stores['SHOP_NAME']), 'class="form-control tip" id="shop_name"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('address')?>
                                    <?= form_input('address', set_value('address', $stores['ADDRESS']), 'class="form-control tip" id="address"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('cus_name')?>
                                    <?= form_input('cus_name', set_value('cus_name', $stores['CUS_NAME']), 'class="form-control tip" id="cus_name"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('phone')?>
                                    <?= form_input('tel', set_value('tel', $stores['TEL']), 'class="form-control" id="phone"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('stores_code')?>
                                    <?= form_input('shop_code', set_value('shop_code', $stores['SHOP_CODE']), 'class="form-control tip" id="shop_code"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?=lang('province_name')?>
                                    <?php
                                    $province_code[''] = "Chọn tỉnh/thành phố...";
                                    foreach($listProvince as $key => $value) {
                                        $province_code[$value['PROVINCE_CODE']] = $value['PROVINCE_NAME'];
                                    }
                                    ?>
                                    <?= form_dropdown('province_code', $province_code, set_value('province_code', $stores['PROVINCE_CODE']), 'class="form-control standardSelectProvince" id="province_code" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('district_name')?>
                                    <?php
                                    $district_code[''] = "Chọn quận huyện...";
                                    foreach($listDistrict as $key => $value) {
                                        $district_code[$value['DISTRICT_CODE']] = $value['DISTRICT_NAME'];
                                    }
                                    ?>
                                    <?= form_dropdown('district_code', $district_code, set_value('district_code', $stores['DISTRICT_CODE']), 'class="form-control standardSelectDistrict" id="district" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('ward_name')?>
                                    <?php
                                    $ward_code[''] = "Chọn phường/xã...";
                                    foreach($listWard as $key => $value) {
                                        $ward_code[$value['WARD_CODE']] = $value['WARD_NAME'];
                                    }
                                    ?>
                                    <?= form_dropdown('ward_code', $ward_code, set_value('ward_code', $stores['WARD_CODE']), 'class="form-control standardSelectWard" id="ward" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    Loại
                                    <?php
                                    $type_id[''] = 'Chọn loại mặt bằng';
                                    foreach($listShopType as $key => $value) {
                                        $type_id[$value['ROW_ID']] = $value['TITLE'];
                                    }
                                    ?>
                                    <?= form_dropdown('type_id', $type_id, set_value('type_id', $stores['TYPE_ID']), 'class="form-control tip" id="type_id" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('status')?>
                                    <?php
                                        $arrStatus              = [];
                                        $arrStatus[OPEN]        = lang('status_open');
                                        $arrStatus[DOING]       = lang('status_doing');
                                        $arrStatus[NOTAPPROVE]  = lang('status_notapprove');
                                        $arrStatus[OPEN]        = lang('status_open');
                                        $arrStatus[APPROVE]     = lang('action_approve');
                                        $arrStatus[DONE]        = lang('status_done');
                                    ?>
                                    <?= form_dropdown('status', $arrStatus, $stores['STATUS'], 'class="form-control" '); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    Mô hình kinh doanh
                                    <?php
                                    $que[''] = 'Chọn mô hình kinh doanh';
                                    foreach($listShopModel as $key => $value) {
                                        $que[$value['ROW_ID']] = $value['TITLE'];
                                    }
                                    ?>
                                    <?= form_dropdown('model_id', $que, set_value('model_id', $stores['MODEL_ID']), 'class="form-control tip" id="model_id" style="width:100%;"'); ?>
                                </div>
                                <div class="form-group">
                                    <?=lang('open_date')?>
                                    <?= form_input('open_date', set_value('open_date', ( !empty($stores['OPEN_DATE']) && $stores['OPEN_DATE']!='0001-01-01T00:00:00')?date('d-m-Y', strtotime($stores['OPEN_DATE'])):""), 'class="form-control datepicker" id="open_date"'); ?>
                                </div>
                                <div class="form-group formuploadimg_post">
                                    Chọn ảnh
                                    <input type="file" id="file-multiple-images" name="images" class="form-control-file">
                                    <input type="hidden" name="image" id="image">
                                    <?php if( count($stores['SHOP_IMAGEs']) > 0 ) :?>
                                    <img class="demo_image" style="width: 200px" src="<?= site_url().URL_SHOPS.$stores['SHOP_IMAGEs'][0]['IMAGE']?>">
                                    <?php else:?>
                                    <img class="demo_image" style="width: 200px" src="">
                                    <?php endif;?>
                                    <div id="image_preview" style="display: none">
                                        <img style="width: 200px" src="" id="previewing" />
                                    </div>
                                    <div id="loading"></div>
                                    <div class="alert alert-danger" id="message" role="alert">
                                        This is a danger alert—check it out!
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?= form_submit('update', lang('update'), 'class="btn btn-warning"'); ?>
                            <!-- <?php if(in_array($stores['STATUS'], [CANCEL, OPEN, DOING, NOTAPPROVE])):?>
                                <?= form_submit('approve', lang('action_approve'), 'class="btn btn-warning"'); ?>
                            <?php else:?>
                                <?= form_submit('unapprove', lang('status_unapprove'), 'class="btn btn-warning"'); ?>
                            <?php endif;?> -->
                        </div>
                        <?= form_close();?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

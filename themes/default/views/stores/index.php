<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<?php
    $v = "?v=1";
    if ($this->input->get('status')){
        $v .= "&status=".$this->input->get('status');
    }
    if ($this->input->get('province_code')) {
        $v .= "&province_code=".$this->input->get('province_code');
    }
    if ($this->input->get('district_code')) {
        $v .= "&district_code=".$this->input->get('district_code');
    }
    if ($this->input->get('model')) {
        $v .= "&model=".$this->input->get('model');
        $model = $this->input->get('model');
    }else $model='';

?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.toggle_form').click(function(){
            $("#form").slideToggle();
            return false;
        });
        var table = $('#listShop').DataTable({
            lengthMenu: [ [20, 50, 150, -1], [20, 50, 150, "All"] ],
            dom: 'lBfrtip',
            scrollX: true,
            // autoWidth: true,
            scrollY: 400,
            //scroller: true,
            deferRender:    true,
            scrollCollapse: true,
            "order": [[ 3, "asc" ]],
            columnDefs : [{"targets": 7, "type":"date-eu"}],
            ajax: { 
                url: '<?=site_url('api/business/stores'. $v);?>', 
                type: 'GET', 
                data: function ( d ) {
                    d.<?=$this->security->get_csrf_token_name();?> = "<?=$this->security->get_csrf_hash()?>";
                }
            },
            oLanguage: {
                "sEmptyTable": "Không có dữ liệu",
                "sSearch": "Tìm kiếm nhanh:",
                "sLengthMenu": "Hiển thị _MENU_ dòng",
                "sInfo": "Hiển thị từ _START_ đến _END_ trong tổng (_TOTAL_)"
            },
            buttons: [
                <?php if( check_permission( 'mbs_store:Export' )):?>
                { extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i> Xuất Excel',
                    className: 'btn btn-warning',
                    footer: true,
                    exportOptions: { columns: [ 2, 3, 4, 5, 6, 7, 8 ] },
                    init: function(api, node, config) {
                       $(node).removeClass('btn-default')
                    }
                }
                <?php endif;?>
            ],
            columns: [
                { "data": "PROVINCE_CODE", "visible": false },
                { "data": "DISTRICT_CODE", "visible": false },
                { "data": "SHOP_CODE" },
                { "data": "SHOP_NAME" },
                { "data": null, "render": function ( data, type, row ) {
                        var t = row['ADDRESS'];
                            t += (row['WARD_NAME'] != '')?', ' +row['WARD_NAME']:'';
                            t += (row['DISTRICT_NAME'] != '')?', ' +row['DISTRICT_NAME']:'';
                            t += (row['PROVINCE_NAME'] != '')?', ' +row['PROVINCE_NAME']:'';
                        return t;

                    }
                },
                { "data": "SHOP_TYPE" },
                { "data": "SHOP_MODEL" },
                { "data": "OPEN_DATE", "render": formatDate },
                { "data": "STATUS_NAME" },
                { "data": null, "searchable": false, "orderable": false, "render": function ( data, type, row ) {
                        <?php if( check_permission( 'mbs_store:Read' )):?>
                        return '<div class="text-center"><div class="btn-group-obs"><a class="tip btn btn-warning btn-xs" href="<?=base_url('business/mbs_store/view/')?>'+row['ROW_ID']+'?s=<?=hash('sha384', 'obscafeongbau')?>"><i class="fa fa-eye"></i></a></div></div>';
                        <?php endif;?>
                    }
                }
            ],
            infoCallback: function( settings, start, end, max, total, pre ) {
                $('#listShop thead').find('th').eq(2).html( total +" cửa hàng");
                return "Hiển thị từ "+start+" đến "+end+" trong tổng ("+total+")"
            }
        });

        $('#search_table').on( 'keyup change', function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (((code == 13 && table.search() !== this.value) || (table.search() !== '' && this.value === ''))) {
                table.search( this.value ).draw();
            }
        });

        <?php
            if ($this->input->get('status') && $this->input->get('status') != 'All' ) {
        ?>
            var val = '<?php echo $this->input->get('status');?>'
            table.columns( 8 ).search( val ).draw();
        <?php }
        ?>

        <?php
            if ($this->input->get('model') && $this->input->get('model') != 'All' ) {
        ?>
            var val = '<?php echo $this->input->get('model');?>'
            table.columns( 6 ).search( val ).draw();
        <?php }
        ?>

        <?php
            if ($this->input->get('province_code') && $this->input->get('province_code') != 'All' ) {
        ?>
            var val = '<?php echo $this->input->get('province_code');?>'
            table.columns( 0 ).search( val ).draw();
        <?php }
        ?>

        <?php
            if ($this->input->get('district_code') && $this->input->get('district_code') != 'All' ) {
        ?>
            var val = '<?php echo $this->input->get('district_code');?>'
            table.columns( 1 ).search( val ).draw();
        <?php }
        ?>

    });
</script>
<style type="text/css">
    .table td:first-child { padding: 1px; }
    .table td:nth-child(6), .table td:nth-child(7), .table td:nth-child(8) { text-align: center; }
    .table td:nth-child(9) { text-align: right; }
</style>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning">
                <div class="box-header">
                    <?php if( check_permission( 'mbs_store:insert' )):?>
                    <a href="<?= base_url('business/mbs_store/create')?>" class="btn btn-warning btn-sm">Thêm cửa hàng khảo sát</a>
                    <?php endif;?>
                    <a href="#" class="btn btn-warning btn-sm toggle_form pull-right"><?= lang("show_hide"); ?></a>
                </div>
                <div class="box-body">
                    <div id="form" class="panel panel-warning">
                        <div class="panel-body">
                            <?= form_open("business/mbs_store", array('method'=>'get'));?>
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="start_date"><?= lang("status"); ?></label>
                                        <?php
                                            $arrStatus        = [];
                                            $arrStatus['All']     = 'Tất cả';
                                            foreach ($listStatus as $key => $value) {
                                                $arrStatus[$value['STATUS_NAME']] = $value['STATUS_NAME'];
                                            }
                                        ?>

                                        <?= form_dropdown('status', $arrStatus, $status, 'class="form-control" id="status" onchange="if(this.value != null ) { this.form.submit(); }" ');?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("province_name"); ?></label>
                                        <?php
                                            $province['All'] = "Tất cả";
                                            foreach($listProvince as $key => $value) {
                                                $province[$value['PROVINCE_CODE']] = $value['PROVINCE_NAME'];
                                            }
                                        ?>
                                        <?= form_dropdown('province_code', $province, set_value('province_code', $province_code), 'class="form-control select2 selectProvince" id="province_code" onchange="if(this.value != null ) { this.form.submit(); }"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code"><?= lang("district_name"); ?></label>
                                        <?php
                                            $district['All'] = "Tất cả";
                                            foreach($listDistrict as $key => $value) {
                                                $district[$value['DISTRICT_CODE']] = $value['DISTRICT_NAME'];
                                            }
                                        ?>
                                        <?= form_dropdown('district_code', $district, set_value('district_code', $district_code), 'class="form-control select2" id="district" onchange="if(this.value != null ) { this.form.submit(); }"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="code">Loại hình</label>
                                        <?php
                                        $que['All'] = 'Tất cả';
                                        foreach($listShopModel as $key => $value) {
                                            $que[$value['TITLE']] = $value['TITLE'];
                                        }
                                        ?>
                                        <?= form_dropdown('model', $que, set_value('model', $model), 'class="form-control" id="model" onchange="if(this.value != null ) { this.form.submit(); }" '); ?>
                                    </div>
                                </div>
                            </div>
                            <?= form_close();?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="listShop" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr style="background-color: #FFBC10;">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>Tổng:</th>
                                            <th colspan="6">6 cửa hàng</th>
                                        </tr>
                                        <tr class="active">
                                            <th></th>
                                            <th></th>
                                            <th class="col-sm-1">Mã</th>
                                            <th class="col-sm-2"><?= lang("name_store"); ?></th>
                                            <th class="col-sm-3"><?= lang("address"); ?></th>
                                            <th><?=lang('type')?></th>
                                            <th><?=lang('model')?></th>
                                            <th><?=lang("open_date_list"); ?></th>
                                            <th><?=lang("status"); ?></th>
                                            <th><?=lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="12" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

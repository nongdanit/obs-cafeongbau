$(function(){
	var districtStorage = {};
    var shopStorage = {};
	$('#selectProvince').on('change', function(e) {
        e.preventDefault();
        var myValProvince = this.value;
        var temp = $('#selectDistrict');
        if(districtStorage[myValProvince] == undefined){
            $.ajax({
                type: "GET",
                url: "/api/utils/districts?v=1",
                dataType: "json",
                data: { provinceCode: myValProvince, t: getTimeString() },
                success: function(data) {
                    temp.empty();
                    temp.val();
                    let options = [];
                    options.push({id: 'All', text: 'Tất cả'});
                    $.each(data, function(i, datav) {
                        options.push({id: datav.districtCode, text: datav.districtName});
                    });
                    districtStorage[myValProvince] = options;
                    temp.select2({
                      data: options
                    });
                    temp.select2().val('All').trigger('change');
                }
            });
        }else{
            temp.empty();
            temp.val();
            temp.select2({
                data: districtStorage[myValProvince]
            });
            temp.select2().val('All').trigger('change');
        }
    });

    $('#selectDistrict, #selectShopModel, #selectShopType, #selectGroup').on('change', function(e) {
        e.preventDefault();
        var temp = $('#selectShop');
        if (temp.length <= 0) return;
        var shopCode = 'All';
        var shopModel = $('#selectShopModel').val();
        var shopType = $('#selectShopType').val();
        var provinceCode = $('#selectProvince').val();
        var districtCode = $('#selectDistrict').val();
        if ($('#selectGroup').length) {
            shopCode = $('#selectGroup').val();
        }
        getShops(shopModel, shopType, provinceCode, districtCode, shopCode, temp);
    });

    function getShops(shopModel = 'All', shopType  = 'All', provinceCode  = 'All', districtCode  = 'All', shopCode  = 'All', elm) {
        var temp = elm;
        var key = shopModel + shopType + provinceCode + districtCode + shopCode;
        if(shopStorage[key] == undefined){
            $.ajax({
                type: "GET",
                url: "/api/utils/shopList?v=1",
                dataType: "json",
                data: {
                    shopModel: shopModel,
                    shopType: shopType,
                    provinceCode: provinceCode,
                    districtCode: districtCode,
                    shopCode: shopCode,
                    t: getTimeString()
                },
                success: function(data) {
                    //console.log(data);
                    let options = [];
                    options.push({id: 'All', text: 'Tất cả'});
                    $.each(data, function(i, datav) {
                        //console.log(datav);
                        options.push({id: datav.shopCode, text: datav.shopName});
                    });

                    temp.empty();
                    temp.val();
                    shopStorage[key] = options;
                    temp.select2({
                      data: options
                    });
                    temp.select2().val('All').trigger('change');
                }
            });
        }else{
            temp.empty();
            temp.val();
            temp.select2({
                data: shopStorage[key]
            });
            temp.select2().val('All').trigger('change');
        }
        
    };
})


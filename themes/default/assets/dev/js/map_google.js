var map, geocoder;
var marker = [];
function initMap() {
    const latitude  = parseFloat(document.getElementById("lat").value);
    const longitude = parseFloat(document.getElementById("lon").value);
    const shopid    = document.getElementById("shopid").value;

    if(latitude == 0 && longitude == 0 || latitude == 0.0 && longitude == 0.0 || latitude == '0.0' && longitude == '0.0'){
        map = new google.maps.Map(document.getElementById('mapholder'), {
            center: {lat: 10.760189, lng: 106.698978},
            zoom: 16,
            mapTypeControl:false,
        });
        $('#error_map').html('<span class="label label-danger">Địa chỉ chưa xác định tọa độ, vui lòng nhấn lấy vị trí! </span>');
    }else{
        map = new google.maps.Map(document.getElementById('mapholder'), {
            center: {lat: latitude, lng: longitude},
            zoom: 16,
            mapTypeControl:false,
        });
        loadPosition(longitude, latitude, shopid, true);
    }
    geocoder = new google.maps.Geocoder();
    document.getElementById('get_address').addEventListener('click', function() {
        geocodeAddress(geocoder, map, shopid);
    });
}
function geocodeAddress(geocoder, resultsMap, shopid) {
    var iconBase = base_url+'uploads/map-marker-location.svg';
    var address = document.getElementById('address').value;
    var sel = document.getElementById("ward");
    var ward= sel.options[sel.selectedIndex].text;
    ward = ward.replace("P. ", "P");
    var sel = document.getElementById("district");
    var district= sel.options[sel.selectedIndex].text;
    district = district.replace("Q. ", "Q");
    var sel = document.getElementById("province_code");
    var province= sel.options[sel.selectedIndex].text;
    console.log(address + ' ' + ward + ' ' + district + ' ' + province);
    geocoder.geocode( { 'address': address + ' ' + ward + ' ' + district + ' ' + province}, function(results, status) {
        if (status == 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location,
                animation:google.maps.Animation.BOUNCE,
                icon: iconBase,
                title:"Bạn đang ở đây!"
            });
            document.getElementById('lon').value = results[0].geometry.location.lng();
            document.getElementById('lat').value = results[0].geometry.location.lat();
            $('#error_map').html('');
            //GET SHOPS
            loadPosition(results[0].geometry.location.lng(), results[0].geometry.location.lat(), shopid, false);
        } else {
            $('#error_map').html('<span class="label label-danger">Địa chỉ không tồn tại! </span>');
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function loadPosition(lng, lat, shopid, flag= true){
    var iconBase = base_url+'uploads/map-marker-location.svg';
    $.post("/business/getLocation", {shopid: shopid, lng: lng, lat: lat}, function(data){
        try{
            if(data.dataReturn.status == 'OK' || data.dataReturn.status == 'ERROR') {
                var result = data.dataReturn.result;
                if( result ) {
                    var warning = result.LIST_WARNING;
                    var shop    = result.LIST_SHOP;
                    var infowindow = new google.maps.InfoWindow();
                    var i;
                    if( warning.length > 0){
                        $("#error_map").html('<span class="label label-danger">Vi phạm khoảng cách giữa các cửa hàng! </span>');
                        warning.forEach(function(item){
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(item.LAT, item.LNG),
                                icon: iconBase,
                                map: map
                            });
                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                return function() {
                                    infowindow.setContent(item.SHOP_NAME);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        });
                    }
                    if( shop.length > 0 ){
                        shop.forEach(function(item){
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(item.LAT, item.LNG),
                                icon: iconBase,
                                map: map
                            });
                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                return function() {
                                    infowindow.setContent(item.SHOP_NAME);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        });
                    }

                    if( flag ){
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(lat, lng),
                            icon: iconBase,
                            animation:google.maps.Animation.BOUNCE,
                            map: map,
                            title:"Bạn đang ở đây!"
                        });
                    }

                    return true;
                }
            }else{
                $('#mapholder').html('Lỗi API Google');
                return;
            }
        }catch(err){
            $("#error_map").html(err);
        }

    }, 'json');
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
    if( marker && marker.length > 0){
        for (var i = 0; i < marker.length; i++) {
            marker[i].setMap(map);
        }
    }
}
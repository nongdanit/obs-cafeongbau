// Format DD-MM-YYYY
Date.prototype.yyyymmdd = function () {
    let mm = this.getMonth() + 1; // getMonth() is zero-based
    let dd = this.getDate();
    let h = this.getHours();
    let m = this.getMinutes();
    let s = this.getSeconds();
    return ['BK', this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd,
        '_',
        (h > 9 ? '' : '0') + h,
        (m > 9 ? '' : '0') + m,
        (s > 9 ? '' : '0') + s,
        '_OBS'
    ].join('');
};
$.extend($.fn.dataTableExt.oSort, {
    "date-eu-pre": function (date) {
        if (!date) {
            return 0;
        }
        date = date.replace(" ", "");
        if (!date) {
            return 0;
        }

        let year;
        let eu_date = date.split(/[\.\-\/]/); //. or -

        /*year (optional)*/
        if (eu_date[2]) {
            year = eu_date[2];
        } else {
            year = 0;
        }

        /*month*/
        let month = eu_date[1];
        if (month.length == 1) {
            month = 0 + month;
        }

        /*day*/
        let day = eu_date[0];
        if (day.length == 1) {
            day = 0 + day;
        }

        return (year + month + day) * 1;
    },

    "date-eu-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "date-eu-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});
// Format DD-MM-YYYY HH:mm:ss
$.extend($.fn.dataTableExt.oSort, {
    "date-euro-pre": function (a) {
        var x;
        if ($.trim(a) !== '') {
            var frDatea = $.trim(a).split(' ');
            var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [00, 00, 00];
            var frDatea2 = frDatea[0].split(/[\.\-\/]/); //. or -
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + ((undefined != frTimea[2]) ? frTimea[2] : 0)) * 1;
        } else {
            x = Infinity;
        }
        return x;
    },
    "date-euro-asc": function (a, b) {
        return a - b;
    },
    "date-euro-desc": function (a, b) {
        return b - a;
    }
});

$.fn.dataTable.ext.order.intl = function (locales, options) {
    if (window.Intl) {
        var collator = new window.Intl.Collator(locales, options);
        var types = $.fn.dataTable.ext.type;

        delete types.order['string-pre'];
        types.order['string-asc'] = collator.compare;
        types.order['string-desc'] = function (a, b) {
            return collator.compare(a, b) * -1;
        };
    }
};

$.fn.dataTable.render.moment = function (from, to, locale) {
    // Argument shifting
    if (arguments.length === 1) {
        locale = 'en';
        to = from;
        from = 'YYYY-MM-DD';
    } else if (arguments.length === 2) {
        locale = 'en';
    }

    return function (d, type, row) {
        if (!d) {
            return type === 'sort' || type === 'type' ? 0 : d;
        }

        var m = window.moment(d, from, locale, true);

        // Order and type get a number value from Moment, everything else
        // sees the rendered value
        return m.format(type === 'sort' || type === 'type' ? 'x' : to);
    };
};
//
// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
//
$.fn.dataTable.pipeline = function (opts, dataSrc) {
    // Configuration options
    var conf = $.extend({
        pages: 5,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
        //               // matching how `ajax.data` works in DataTables
        method: 'GET' // Ajax HTTP method
    }, opts);

    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;

    return function (request, drawCallback, settings) {
        var ajax = false;
        var requestStart = request.start;
        var drawStart = request.start;
        var requestLength = request.length;
        var requestEnd = requestStart + requestLength;

        if (settings.clearCache) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        } else if (cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper) {
            // outside cached data - need to make a request
            ajax = true;
        } else if (JSON.stringify(request.order) !== JSON.stringify(cacheLastRequest.order) ||
            JSON.stringify(request.columns) !== JSON.stringify(cacheLastRequest.columns) ||
            JSON.stringify(request.search) !== JSON.stringify(cacheLastRequest.search)
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }

        // Store the request for checking next time around
        cacheLastRequest = $.extend(true, {}, request);

        if (ajax) {
            // Need data from the server
            if (requestStart < cacheLower) {
                requestStart = requestStart - (requestLength * (conf.pages - 1));

                if (requestStart < 0) {
                    requestStart = 0;
                }
            }

            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);

            request.start = requestStart;
            request.length = requestLength * conf.pages;

            // Provide the same `data` options as DataTables.
            if (typeof conf.data === 'function') {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data(request);
                if (d) {
                    $.extend(request, d);
                }
            } else if ($.isPlainObject(conf.data)) {
                // As an object, the data given extends the default
                $.extend(request, conf.data);
            }

            return $.ajax({
                "type": conf.method,
                "url": conf.url + '?t=' + getTimeString(),
                "data": request,
                "dataType": "json",
                "cache": false,
                "success": function (json) {
                    cacheLastJson = $.extend(true, {}, json);

                    if (cacheLower != drawStart) {
                        json.data.splice(0, drawStart - cacheLower);
                    }
                    if (requestLength >= -1) {
                        json.data.splice(requestLength, json.data.length);
                    }
                    if (dataSrc != null) {
                        dataSrc(json);
                    }
                    drawCallback(json);
                }
            });
        } else {
            json = $.extend(true, {}, cacheLastJson);
            json.draw = request.draw; // Update the echo for each response
            json.data.splice(0, requestStart - cacheLower);
            json.data.splice(requestLength, json.data.length);

            drawCallback(json);
        }
    }
};

// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register('clearPipeline()', function () {
    return this.iterator('table', function (settings) {
        settings.clearCache = true;
    });
});

/* Custom filtering function which will search data in column four between two values */
$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
        if (settings.sTableId == 'orderDetail') {
            // console.log('here');
            if ($('#allItem').is(":checked")) {
                return true;
            }
            if (settings.nTable.classList.value.includes('processingOrder')) {
                let qty = parseInt(data[3]) || 0;
                return qty > 0;
            } else if (settings.nTable.classList.value.includes('pendingOrder')) {
                let qty = parseInt(data[3]) || 0;
                let qtyApprove = parseInt(data[4]) || 0;
                return (qty > 0 || qtyApprove > 0);
            }
        } else {
            return true;
        }
    }
);
jQuery.event.special.wheel = {
    setup: function (_, ns, handle) {
        this.addEventListener("wheel", handle, {passive: true});
    }
};
jQuery.event.special.mousewheel = {
    setup: function (_, ns, handle) {
        this.addEventListener("mousewheel", handle, {passive: true});
    }
};
jQuery.event.special.touchstart = {
    setup: function (_, ns, handle) {
        if (ns.includes("noPreventDefault")) {
            this.addEventListener("touchstart", handle, {passive: false});
        } else {
            this.addEventListener("touchstart", handle, {passive: true});
        }
    }
};
jQuery.event.special.touchmove = {
    setup: function (_, ns, handle) {
        if (ns.includes("noPreventDefault")) {
            this.addEventListener("touchmove", handle, {passive: false});
        } else {
            this.addEventListener("touchmove", handle, {passive: true});
        }
    }
};
$(document).ready(function ($) {
    "use strict";
    var english = /^[A-Za-z0-9\w\s\.\-\*\[\]]*$/;

    $.fn.dataTable.ext.order.intl('vn');
    $('.standardSelectProvince, .standardSelectDistrict, .standardSelectWard, .standardSelect').chosen({
        disable_search_threshold: 10,
        no_results_text: "Oops, không có dữ liệu!",
        width: "100%"
    });

    $('.modal').on('shown.bs.modal', function () {
        var now = moment(),
            begin = moment().subtract(1, 'days').startOf('day'),
            end = moment().add(0, 'days').endOf('day')

        $('.datetimepicker').datetimepicker({
            format: 'HH:mm:ss',
            showClear: true,
            showClose: true,
            useCurrent: true,
            minDate: begin,
            maxDate: end
        });
    });


    /**
     * From create grounds
     */
    $('.datepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        showClear: true,
        showClose: true,
        useCurrent: false,
        widgetPositioning: {horizontal: 'auto', vertical: 'auto'}
    });

    var minMonth = moment('2020-04-01');
    var minMonth = moment(minMonth).add(1, 'M');
    var maxMonth = moment().add(1, 'months');
    $('.monthpicker').datetimepicker({
        format: "MM-YYYY",
        showClear: true,
        showClose: true,
        viewMode: 'months',
        useCurrent: true,
        widgetPositioning: {horizontal: 'auto', vertical: 'auto'},
        minDate: minMonth,
        maxDate: maxMonth
    });

    var minDate = moment('2020-05-22');
    var maxDate = moment().add(36500, 'days');
    $('.date_max_current').datetimepicker({
        format: "DD-MM-YYYY",
        showClear: true,
        showClose: true,
        useCurrent: true,
        widgetPositioning: {horizontal: 'auto', vertical: 'auto'},
        minDate: minDate,
        maxDate: maxDate
    }).on('dp.show', function () {
        //$('.date_max_current').data("DateTimePicker").maxDate(moment());
    });

    $('.standardSelectProvince').chosen().change(function (e) {
        e.preventDefault();
        var myValProvince = $(this).chosen().val();
        $.ajax({
            type: "GET",
            url: "/settings/getDistrict",
            dataType: "json",
            data: {province: myValProvince},
            success: function (data) {
                var temp = $('#district'); // cache it
                temp.empty();
                $("#district").append("<option value=''>Vui lòng chọn</option>");
                $.each(data.resultDistrictAPI, function (i, datav) { // bind the dropdown list using json result
                    $('<option>', {
                        value: datav.DISTRICT_CODE,
                        text: datav.DISTRICT_NAME
                    }).html(datav.DISTRICT_NAME).appendTo("#district");
                });
                $('#district').trigger("chosen:updated");
            }
        })
        $(this).val($(this).chosen().val());
    });


    $('.standardSelectDistrict').chosen().change(function (e) {
        e.preventDefault();
        var myValDistrict = $(this).chosen().val();
        $.ajax({
            type: "GET",
            url: "/settings/getWard",
            dataType: "json",
            data: {district: myValDistrict},
            success: function (data) {
                var temp = $('#ward');
                temp.empty();
                $("#ward").append("<option value=''>Vui lòng chọn</option>");
                $.each(data.resultWardAPI, function (i, datav) { // bind the dropdown list using json result
                    $('<option>', {
                        value: datav.WARD_CODE,
                        text: datav.WARD_FULL
                    }).html(datav.WARD_FULL).appendTo("#ward");
                });
                $('#ward').trigger("chosen:updated");
            }
        });
        $(this).val($(this).chosen().val());
    });


    $('#selectProvinceEdit').on('change', function (e) {
        e.preventDefault();
        var myValProvince = this.value;
        var temp = $('#selectDistrictEdit');
        if (myValProvince == 'All') {
            temp.empty();
            temp.val();
            temp.append("<option value='All'>Chọn quận huyện...</option>");
            temp.select2().val('All').trigger('change');
        } else {
            $.ajax({
                type: "GET",
                url: "/api/utils/districts?provinceCode=" + myValProvince,
                success: function (data) {
                    //console.log(data);
                    temp.empty();
                    temp.val();
                    temp.append("<option value='All'>Chọn quận huyện...</option>");
                    $.each(data, function (i, datav) {

                        $('<option>', {
                            value: datav.districtCode,
                            text: datav.districtName
                        }).html(datav.districtName).appendTo(temp);
                    });
                    temp.select2().val('All').trigger('change');
                }
            });
        }
    });


    // $('#shopmodel').val(shopModel).trigger('change');


    $('#selectDistrictEdit').on('change', function (e) {
        e.preventDefault();
        var myValDistrict = this.value;
        var temp = $('#selectWardEdit');
        if (myValDistrict == 'All') {
            temp.empty();
            temp.val();
            temp.append("<option value='All'>Chọn phường/xã...</option>");
            temp.select2().val('All').trigger('change');
        } else {
            $.ajax({
                type: "GET",
                url: "/api/utils/wards?districtCode=" + myValDistrict,
                success: function (data) {
                    //console.log(data);
                    temp.empty();
                    temp.val();
                    temp.append("<option value='All'>Chọn phường/xã...</option>");
                    $.each(data, function (i, datav) {
                        //console.log(datav);
                        $('<option>', {
                            value: datav.WARD_CODE,
                            text: datav.WARD
                        }).html(datav.WARD).appendTo(temp);
                    });
                    temp.select2().val('All').trigger('change');
                }
            });
        }

    });


    $("#files").change(function () {
        var countimgcurrent = $(this).attr('countimg');
        var shopid = $(this).attr('shopid');
        var files = $('#files')[0].files;
        var countimgnew = files.length;
        var error = '';
        var form_data = new FormData();
        form_data.append("shopid", shopid);
        if ((Number(countimgnew) + Number(countimgcurrent)) > 12) {
            error += "<p id='error'>Vượt số lượng hình ảnh cho phép</p>" + "<h4>Note</h4>" + "<span id='error_message'>Số lượng tối đa là 12</span>";
        } else {
            for (var count = 0; count < files.length; count++) {
                var name = files[count].name;
                var extension = name.split('.').pop().toLowerCase();
                if (jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1) {
                    error += "<p id='error'>Vui lòng chọn đúng định dạng hình ảnh</p>" + "<h4>Note</h4>" + "<span id='error_message'>Chỉ có thể: jpeg, jpg and png được sử dụng</span>"
                } else {
                    form_data.append("files[]", files[count]);
                }
            }
        }
        if (error == '') {
            $.ajax({
                url: base_url + "/business/upload_store",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#uploaded_images').html('').append(data);
                    $('#files').val('');
                    $("#message").hide().html('');
                    $('#files').attr('countimg', Number(countimgnew) + Number(countimgcurrent));
                }
            })
        } else {
            $("#message").show().html(error);
            return false;
        }
    });

    $("#image_competitors").change(function (event) {
        var countimgcurrent = $(this).attr('countimg');
        var competitorsid = $(this).attr('competitorsid');
        var image_competitors = $('#image_competitors')[0].files;
        var countimgnew = image_competitors.length;
        var error = '';
        var form_data = new FormData();
        form_data.append("competitorsid", competitorsid);
        if ((Number(countimgnew) + Number(countimgcurrent)) > 4) {
            error += "<p id='error'>Vượt số lượng hình ảnh cho phép</p>" + "<h4>Note</h4>" + "<span id='error_message'>Số lượng tối đa là 4</span>";
        } else {
            for (var count = 0; count < image_competitors.length; count++) {
                var name = image_competitors[count].name;
                var extension = name.split('.').pop().toLowerCase();
                if (jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1) {
                    error += "<p id='error'>Vui lòng chọn đúng định dạng hình ảnh</p>" + "<h4>Note</h4>" + "<span id='error_message'>Chỉ có thể: jpeg, jpg and png được sử dụng</span>"
                } else {
                    form_data.append("image_competitors[]", image_competitors[count]);
                }
            }
        }
        if (error == '') {
            $.ajax({
                url: base_url + "/business/upload_competitors",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                },
                success: function (data) {
                    $('#uploaded_images_competitors').html('').append(data);
                    $('#image_competitors').val('');
                    $("#message_competitors").hide().html('');
                    $('#image_competitors').attr('countimg', Number(countimgnew) + Number(countimgcurrent));
                }
            })
        } else {
            $("#message_competitors").show().html(error);
            return false;
        }

    });

    jQuery.extend(jQuery.validator.messages, {
        required: "Trường này không được rỗng.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });

    /*
     ** Tạo mặt bằng
     */
    $("#stores").validate({
        rules: {
            shop_name: {
                required: true,
                minlength: 5
            },
            address: 'required',
            email: {
                required: false,
                email: true
            },
            tel: {
                required: false,
                number: true
            },
            type_id: 'required',
            model_id: 'required',
            province_code: 'required',
            district_code: 'required',
            ward_code: 'required',
            start_date: 'required'
        },
        messages: {
            shop_name: {
                required: 'Vui lòng nhập tên mặt bằng',
                minlength: 'Vui lòng nhập ít nhất 5 kí tự'
            },
            address: 'Vui lòng nhập địa chỉ',
            email: 'Email phải đúng định dạng',
            tel: {number: 'Số điện thoại phải là số'},
            type_id: 'Vui lòng chọn mô hình cửa hàng',
            model_id: 'Vui lòng chọn mô hình kinh doanh',
            province_code: 'Vui lòng chọn tỉnh thành',
            district_code: 'Vui lòng chọn quận huyện',
            ward_code: 'Vui lòng chọn phường xã',
            start_date: 'Vui lòng chọn ngày khảo sát'
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                type: "POST",
                dataType: 'json',
                data: $(form).serializeArray(),
                url: $('#stores').attr('action'),
                success: function (e) {
                    if (typeof e.dataReturn && e.dataReturn.error === true) {
                        // $('.show-error').html(e.dataReturn.message).show();
                        // return false;
                        $('#staticModal').on('show.bs.modal', function (event) {
                            var modal = $(this);
                            modal.find('.modal-body').text(e.dataReturn.message);
                            modal.find('.href').hide();
                        });
                        $('#staticModal').modal('show');
                        return false;
                    } else {
                        // $('#stores :input').attr('disabled', 'disabled');
                        if (e.dataReturn.formtype === 'edit') {
                            $('#staticModal').on('show.bs.modal', function (event) {
                                var modal = $(this);
                                modal.find('.modal-body').text(e.dataReturn.message);
                                modal.find('.href').hide();
                            });
                            $('#staticModal').modal('show');
                            location.reload();
                        } else {
                            $('#staticModal').on('show.bs.modal', function (event) {
                                var modal = $(this);
                                modal.find('.modal-body').text(e.dataReturn.message);
                                modal.find('.bt-action').hide();
                                modal.find('.href').text('OK');
                                modal.find('.href').attr('href', e.dataReturn.url);
                            });
                            $('#staticModal').modal('show');
                        }
                    }
                },
                error: function (jqXHR, textStatus) {
                }
            })
        }
    });

    /**
     * Thêm và Update đối thủ cạnh tranh
     */
    $("#formCompetitors").validate({
        rules: {
            title: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            title: {
                required: 'Vui lòng nhập tên thương hiệu',
                minlength: 'Vui lòng nhập ít nhất 5 kí tự'
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                type: "POST",
                dataType: 'json',
                data: $(form).serializeArray(),
                url: $('#formCompetitors').attr('action'),
                success: function (e) {
                    //console.log(e);
                    if (typeof e.dataReturn && e.dataReturn.error === true) {
                        $('.show-error').html(e.dataReturn.message).show();
                        return false;
                    } else {
                        alert(e.dataReturn.message);
                        $('#formCompetitors').trigger("reset");
                        $('#doithucanhtranhModal').modal('hide');
                        $.get("/business/loadCompetitors", {id: $('#formCompetitors').find('.working_detail_id').val()}, function (data) {
                            $(".rowdata .table > tbody").html(data);
                        });
                    }
                },
                error: function (jqXHR, textStatus) {
                }
            })
        }
    });

    /**
     * Update Thông số mặt bằng
     */
    $(".editform").click(function (event) {
        var th = $(this);
        var rowId = $(this).attr('rowidbefore');
        var shopid = $(this).attr('shopid');
        var position = $(this).attr('position');
        var parentid = $(this).attr('parentid');
        var idfather = $(this).attr('idfather');
        if (idfather == '' || idfather == undefined) {
            if (rowId == 0) {
                $(this).parentsUntil("form").find('input, select').prop('disabled', false);
                $(this).parentsUntil("form").find(".standardSelect").attr('disabled', false).trigger("chosen:updated");
                $(this).next('.submitform').show();
                $(this).hide();
            } else {
                $.ajax({
                    url: base_url + "/business/checkrowID",
                    type: "POST",
                    dataType: 'json',
                    data: {rowid: rowId, shopid: shopid, position: position, parentid: parentid},
                    cache: false,
                    success: function (e) {
                        if (typeof e.dataReturn && e.dataReturn.error === true) {
                            $('#staticModal').on('show.bs.modal', function (event) {
                                var modal = $(this);
                                modal.find('.modal-body').html(e.dataReturn.message);
                                modal.find('.href').hide();
                            });
                            $('#staticModal').modal('show');
                            return false;
                        } else {
                            $(th).parentsUntil("form").find('input, select').prop('disabled', false);
                            $(th).parentsUntil("form").find(".standardSelect").attr('disabled', false).trigger("chosen:updated");
                            $(th).next('.submitform').show();
                            $(th).hide();
                        }
                    }
                });
            }
        } else {
            $.ajax({
                url: base_url + "/business/checkeditworkingdetail",
                type: "POST",
                dataType: 'json',
                data: {idfather: idfather},
                cache: false,
                success: function (e) {
                    if (typeof e.dataReturn && e.dataReturn.error === true) {
                        $('#staticModal').on('show.bs.modal', function (event) {
                            var modal = $(this);
                            modal.find('.modal-body').html(e.dataReturn.message);
                            modal.find('.href').hide();
                        });
                        $('#staticModal').modal('show');
                        return false;
                    } else {
                        $(th).parentsUntil("form").find('input, select').prop('disabled', false);
                        $(th).parentsUntil("form").find(".standardSelect").attr('disabled', false).trigger("chosen:updated");
                        $(th).next('.submitform').show();
                        $(th).hide();
                    }
                }
            });
        }

    });

    $('.allowform').click(function (event) {
        $("#general").find(':input, select').prop("disabled", false);
        $("#general").find('.standardSelect').attr('disabled', false).trigger("chosen:updated");
    });

    $('.submitform').click(function (event) {
        var th = $(this);
        var frm = $(this).attr('idx');
        var form = $('#' + frm);
        form.ajaxForm({
            type: 'POST',
            dataType: 'json',
            data: form.serializeArray(),
            url: form.attr('action'),
            success: function (e) {
                if (typeof e.dataReturn && e.dataReturn.error === true) {
                    $('#staticModal').on('show.bs.modal', function (event) {
                        var modal = $(this);
                        modal.find('.modal-body').text(e.dataReturn.message);
                        modal.find('.href').hide();
                    });
                    $('#staticModal').modal('show');
                } else {
                    $(th).prev('.editform').show();
                    $(th).hide();
                    $("#" + frm).fadeTo("slow", 1, function () {
                        $(this).find('input[type=text], select').prop('disabled', true);
                        // $(this).find(':input').attr('disabled', 'disabled');
                        $(this).find('label').css('cursor', 'default');
                    });
                    // $('#staticModal').on('show.bs.modal', function (event) {
                    //     var modal       = $(this);
                    //     modal.find('.modal-body').text(e.dataReturn.message);
                    //     modal.find('.href').hide();
                    // });
                    // $('#staticModal').modal('show');
                }
            },
            error: function (jqXHR, textStatus) {
                console.log(jqXHR);
            }
        });
    });

    /**
     * DOI THU CANH TRANH
     */
    $("#doithucanhtranh").click(function (event) {
        var working_detail_id = $(this).attr('idx');
        $('#doithucanhtranhModal').on('show.bs.modal', function (event) {
            var modal = $(this);
            modal.find('.working_detail_id').val(working_detail_id);

            modal.find('#mediumModalLabel').text('Thêm mới');
            modal.find('input[type=submit]').val('Lưu');

            modal.find('#image_competitors').attr('competitorsid', 0);
            modal.find('#image_competitors').attr('countimg', 0);
            modal.find('#uploaded_images_competitors').html("");
            modal.find('.row_id').val(0);
            $('#formCompetitors').trigger("reset");
            $.get("/business/reset", {id: working_detail_id}, function (data) {
            });
        });
        $('#doithucanhtranhModal').modal('show');
    });


    $(".number").keyup(function (event) {
        /* Act on the event */
        var c = FormatNumber(this.value);
        $(this).val(c);
    });

    $('input.revenue').keyup(function () {
        var revenue_of_day = 1;
        $('input.revenue').each(function () {
            var value = $(this).val();
            value = intVal(value)
            if (!isNaN(value)) {
                revenue_of_day *= value;
            }
        });
        $('#revenue_of_day').val(formatNumberChange(revenue_of_day));
    });

    $('#changemodel').change(function (event) {
        var val = $(this).val();
        if (val == 0) {
            $(this).parentsUntil("form").find(".pre_model").html('Mô hình kinh doanh trước đây <input type="text" name="pre_model" value="" class="form-control" id="pre_model" required >');
        } else {
            $(this).parentsUntil("form").find(".pre_model").html('Mô hình kinh doanh trước đây <input type="text" name="pre_model" value="" class="form-control" id="pre_model" readonly>');
        }
    });

    /**
     * Thêm và Update lịch làm việc nhân viên
     */
    $("#editCalendarEmployee").validate({
        rules: {
            start_date: {
                required: true
            }
        },
        messages: {
            start_date: {
                required: 'Vui lòng chọn ngày bắt đầu'
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                type: "POST",
                dataType: 'json',
                data: $(form).serializeArray(),
                url: $('#editCalendarEmployee').attr('action'),
                success: function (e) {
                    if (typeof e.dataReturn && e.dataReturn.error === true) {
                        //console.log(e.dataReturn.message);
                        return false;
                    } else {
                        alert(e.dataReturn.message);
                        $('#editCalendarEmployee').trigger("reset");
                        $('#loadmodalEmployee').modal('hide');
                        window.location.reload();
                    }
                },
                error: function (jqXHR, textStatus) {
                    console.log(jqXHR);
                }
            })
        }
    });

    $("#add_Calendar").click(function (event) {
        var username = $(this).attr('username');
        $('#loadmodalEmployee').on('show.bs.modal', function (event) {
            var modal = $(this);
            modal.find('#row_id_employee').val(0);
            modal.find('#username').val(username);

            modal.find('#mediumModalLabel').text('Thêm mới');
            modal.find('input[type=submit]').val('Lưu');

            $('#editCalendarEmployee').trigger("reset");
        });
        $('#loadmodalEmployee').modal('show');
    });

    /**
     * Thêm và Update Store(Shop bên A Cường)
     */

    jQuery.validator.addMethod("shop_code", function (value, element) {
        return this.optional(element) || /^[A-Z]{3,3}0(1|2)[0-9]{5,5}$/.test(value);
    }, 'Vui lòng nhập đúng định dạng');

    jQuery.validator.addMethod("customphone", function (value, element) {
        return this.optional(element) || /((09|03|07|08|05)+([0-9]{8})\b)/.test(value);
    }, 'Vui lòng nhập đúng định dạng số điện thoại');

    $("#form_store").validate({
        rules: {
            shop_name: {
                required: true,
                minlength: 5
            },
            shop_code: {
                required: true,
                // minlength: 10,
                // maxlength: 10,
                // shop_code: true
            },
            shop_tel: {
                number: true,
                customphone: true
            },
            owner_tel: {
                number: true,
                customphone: true
            },
            shop_type: {
                required: true
            },
            province_code: {
                required: true
            },
            district_code: {
                required: true
            },
            ward_code: {
                required: true
            },
            start_date: {
                required: true
            },
            npp: {
                minlength: 4,
                maxlength: 50,
            },
            shopNote: {
                required: true,
                maxlength:200
            },
            orgCode: {
                required: true
            },
        },
        messages: {
            shop_name: {
                required: 'Vui lòng nhập tên cửa hàng',
                minlength: 'Tên cửa hàng ít nhất 5 kí tự'
            },
            shop_code: {
                required: 'Vui lòng nhập mã cửa hàng',
                minlength: 'Mã cửa hàng có 10 kí tự',
                maxlength: 'Mã cửa hàng có 10 kí tự'
            },
            shop_tel: {
                number: 'Số điện thoại phải là số'
            },
            owner_tel: {
                number: 'Số điện thoại phải là số'
            },
            shop_type: {
                required: 'Vui lòng chọn mô hình cửa hàng',
            },
            province_code: {
                required: 'Vui lòng chọn Tỉnh/thành'
            },
            district_code: {
                required: 'Vui lòng chọn Quận/Huyện'
            },
            ward_code: {
                required: 'Vui lòng chọn Phường/Xã'
            },
            start_date: {
                required: 'Vui lòng chọn ngày khai trương'
            },
            npp: {
                minlength: 'Nhà phân phối có ít nhất 4 kí tự',
                maxlength: 'Nhà phân phối có tối đa 50 kí tự'
            },
            shopNote: {
                required: 'Vui lòng nhập Thông tin khách hàng.',
                // minlength: 'Nhà phân phối có ít nhất 4 kí tự',
                maxlength: 'Thông tin khách hàng có tối đa 200 kí tự.'
            },
            orgCode:{
                required: 'Vui lòng chọn Kho'
            }
        },
        submitHandler: function (form) {
            var address = document.getElementById('address').value;
            var sel = document.getElementById("selectWardEdit");
            var ward = sel.options[sel.selectedIndex].text;
            ward = ward.replace("P. ", "P");
            var sel = document.getElementById("selectDistrictEdit");
            var district = sel.options[sel.selectedIndex].text;
            district = district.replace("Q. ", "Q");
            var sel = document.getElementById("selectProvinceEdit");
            var province = sel.options[sel.selectedIndex].text;
            var lng, lat = 0;
            var geocoder = new google.maps.Geocoder();
            var paramArr = $(form).serializeArray();
            // console.log(paramArr);return;
            geocoder.geocode({'address': address + ' ' + ward + ' ' + district + ' ' + province}, function (results, status) {
                if (status == 'OK') {
                    lng = results[0].geometry.location.lng();
                    lat = results[0].geometry.location.lat();
                    paramArr.push({name: 'lng', value: lng});
                    paramArr.push({name: 'lat', value: lat});
                    // var paramArr = $(form).serializeArray();
                    // paramArr.push({ name: 'lng', value: lng });
                    // paramArr.push({ name: 'lat', value: lat });
                    // $.post($('#form_store').attr('action'), $.param(paramArr), [], 'json')
                    //     .done(function(e) {
                    //         if (typeof e.dataReturn && e.dataReturn.error === true) {
                    //             $('.show-error').html(e.dataReturn.message).show();
                    //             return false;
                    //         }
                    //         $('#staticModal').on('show.bs.modal', function(event) {
                    //             var modal = $(this);
                    //             modal.find('.modal-body').text(e.dataReturn.message);
                    //             modal.find('.bt-action').hide();
                    //             modal.find('.href').text('OK');
                    //             modal.find('.href').attr('href', e.dataReturn.url);
                    //         });
                    //         $('#staticModal').modal('show');
                    //     })
                    //     .fail(function(xhr, textStatus, errorThrown) {
                    //         console.log(xhr.responseText);
                    //     });
                } else {

                    //$('.show-error').html('Địa chỉ không tồn tại').show();
                    //return false;
                }


                $.post($('#form_store').attr('action'), $.param(paramArr), [], 'json')
                    .done(function (e) {
                        if (typeof e.dataReturn && e.dataReturn.error === true) {
                            $('.show-error').html(e.dataReturn.message).show();
                            return false;
                        }
                        $('#staticModal').on('show.bs.modal', function (event) {
                            var modal = $(this);
                            modal.find('.modal-body').text(e.dataReturn.message);
                            modal.find('.bt-action').hide();
                            modal.find('.href').text('OK');
                            modal.find('.href').attr('href', e.dataReturn.url);
                        });
                        $('#staticModal').modal('show');
                    })
                    .fail(function (xhr, textStatus, errorThrown) {
                        console.log(xhr.responseText);
                    });
            });
        }
    });

    /**
     * QC
     */
    $("#qc").validate({
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                type: "POST",
                dataType: 'json',
                data: $(form).serializeArray(),
                url: $('#qc').attr('action'),
                success: function (e) {
                    if (typeof e.dataReturn && e.dataReturn.error === true) {
                        $('.show-error').html(e.dataReturn.message).show();
                        return false;
                    }
                    $('#staticModal').on('show.bs.modal', function (event) {
                        var modal = $(this);
                        modal.find('.modal-body').text(e.dataReturn.message);
                        modal.find('.bt-action').hide();
                        modal.find('.href').text('OK');
                        modal.find('.href').attr('href', e.dataReturn.url);
                    });
                    $('#staticModal').modal('show');
                },
                error: function (jqXHR, textStatus) {
                    console.log(jqXHR);
                }
            })
        }
    });
    // Change mode edit and view
    $(".mode_change").click(function (event) {
        var val = $(this).attr('val');
        if (val == 1) {
            $(this).text('Chuyển sang chế độ xem')
            $('.editPoint, .note_child, .feed').hide();
            $('.input_point, .input_note, .btn_edit, .image_qc').show();
            $('.head_feed').text('Thao tác')
            $(".image_qc").filestyle('disabled', false);
        } else {
            $(this).text('Chuyển sang chế độ chỉnh sửa')
            $('.editPoint, .note_child, .feed').show();
            $('.input_point, .input_note, .btn_edit, .image_qc').hide();
            $('.head_feed').text('SM/SA Phản hồi')
            $(".image_qc").filestyle('disabled', true);
        }
        $(this).attr('val', 1 - val);
    });
    // Check point and max point
    $('.input_point').keyup(function (event) {
        var val = $(this).val();
        var max = $(this).attr('max');
        if (val > max) {
            alert('Điểm đánh giá không thể lớn hơn điểm chuẩn');
            $(this).val(max);
            return false;
        } else {
            var class_position = $(this).attr('position');
            var valold = $(this).attr('valold');
            var point_cate = $("." + class_position + " .point_cate").attr('valpoint_cate');
            var point_cate_change = parseInt(point_cate) - parseInt(valold) + parseInt(val);
            $("." + class_position + " .point_cate").text(point_cate_change);
        }
    });


    // Update QC
    $('.btn_edit').click(function (event) {
        var row_id = $(this).attr('row_id');
        var shop_formula_qc_id = $(this).attr('shop_formula_qc_id');
        var item_qc_id = $(this).attr('item_qc_id');
        var class_position = $(this).attr('position');
        var input_point = $('.input_point' + row_id).val();
        var input_note = $('.input_note' + row_id).val();
        var non_score = $('.non_score' + row_id).iCheck('update')[0].checked;
        var sourceImg = $('.demo_image_qc' + row_id).attr('src');
        var formtype = $('#formtype').val();
        var nameimage_qc = $('.nameimage_qc' + row_id).val();
        $.ajax({
            url: base_url + "/business/submit_qc",
            method: "POST",
            dataType: 'json',
            data: {
                row_id: row_id,
                formtype: formtype,
                shop_formula_qc_id: shop_formula_qc_id,
                item_qc_id: item_qc_id,
                input_point: input_point,
                input_note: input_note,
                sourceImg: sourceImg,
                nameimage_qc: nameimage_qc,
                non_score: non_score
            },
            success: function (e) {
                //console.log(e);
                if (typeof e.dataReturn && e.dataReturn.error === true) {
                    // console.log(e.dataReturn.message);
                    return false;
                } else {
                    alert(e.dataReturn.message);
                    $('.editPoint' + row_id).text(input_point);
                    $('.note_child' + row_id).text(input_note);
                    $('.nameimage_qc' + row_id).val(e.dataReturn.image);
                    $("." + class_position + " .point_cate").attr('valpoint_cate', $("." + class_position + " .point_cate").text());
                }
            },
            error: function (jqXHR, textStatus) {
                console.log(jqXHR);
            }
        })

    });

    // Upload image QC
    // $(".image_qc").change(function() {
    //     $("#message").hide();
    //     var th = this;
    //     var file = this.files[0];
    //     if (file != undefined && (file.type != "undefined" || file.type != undefined)) {
    //         var imagefile = file.type;
    //         var match = ["image/jpeg", "image/png", "image/jpg", "image/gif"];
    //         if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
    //             $("#message").show().html("<label class='error'>Chỉ cho phép các file: jpeg, jpg, gif and png.</label>");
    //             return false;
    //         } else {
    //             var str = file.name.replace(/\s+/g, '');
    //             if (!english.test(str)) {
    //                 $("#message").show().html("<label class='error'>Vui lòng kiểm tra tên file</label>");
    //                 return false;
    //             }
    //             var reader = new FileReader();
    //             reader.onload = function(e) {
    //                 $(th).parent().next().find('img').attr('src', e.target.result);
    //             }
    //             reader.readAsDataURL(this.files[0]);
    //         }
    //     }
    // });


    $(".image_qc").change(function () {
        var countimgcurrent = $(this).attr('countimg');
        var qcid = $(this).attr('qcid');
        var files = $('#files' + qcid)[0].files;
        var countimgnew = files.length;
        var error = '';
        var form_data = new FormData();
        form_data.append("qcid", qcid);
        if ((Number(countimgnew) + Number(countimgcurrent)) > 5) {
            error += "<p id='error'>Vượt số lượng hình ảnh cho phép</p>" + "<h5>Note</h5>" + "<span id='error_message'>Số lượng tối đa là 5</span>";
        } else {
            for (var count = 0; count < files.length; count++) {
                var name = files[count].name;
                var extension = name.split('.').pop().toLowerCase();
                if (jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1) {
                    error += "<p id='error'>Vui lòng chọn đúng định dạng hình ảnh</p>" + "<h5>Note</h5>" + "<span id='error_message'>Chỉ có thể: jpeg, jpg and png được sử dụng</span>"
                } else {
                    form_data.append("files[]", files[count]);
                }
            }
        }
        if (error == '') {
            //console.log(form_data)
            $.ajax({
                url: base_url + "/business/upload_image_qc",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#uploaded_images' + qcid).html('').append(data);
                    $('#files' + qcid).val('');
                    $("#message" + qcid).hide().html('');
                    $('#files' + qcid).attr('countimg', Number(countimgnew) + Number(countimgcurrent));
                }
            })
        } else {
            $("#message" + qcid).show().html(error);
            return false;
        }
    });

    // Submit Template QC
    $("#myform_FormulaQC").validate({
        rules: {
            name: {
                required: true,
                minlength: 5
            },
            start_date: {
                required: true
            },
            end_date: {
                required: true
            }
        },
        messages: {
            name: {
                required: 'Vui lòng nhập tên mẫu',
                minlength: 'Tên mẫu ít nhất 5 kí tự'
            },
            start_date: {
                required: 'Vui lòng chọn ngày bắt đầu'
            },
            end_date: {
                required: 'Vui lòng chọn ngày kết thúc'
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                type: "POST",
                dataType: 'json',
                data: $(form).serializeArray(),
                url: $('#myform_FormulaQC').attr('action'),
                success: function (e) {
                    if (e.dataReturn.formtype == 'create') {
                        $('#addTemplateQC').modal('hide');
                        $('#staticModal').on('show.bs.modal', function (event) {
                            var modal = $(this);
                            modal.find('.modal-body').text(e.dataReturn.message);
                            modal.find('.bt-action').hide();
                            modal.find('.href').text('OK');
                            modal.find('.href').attr('href', e.dataReturn.url);
                        });
                    } else {
                        $('#staticModal').on('show.bs.modal', function (event) {
                            var modal = $(this);
                            modal.find('.modal-body').text(e.dataReturn.message);
                            modal.find('.href').hide();
                        });
                    }
                    $('#staticModal').modal('show');
                    return true;
                },
                error: function (jqXHR, textStatus) {
                    console.log(jqXHR);
                }
            })
        }
    });

    // Submit Group QC
    $("#myform_GroupQC").validate({
        rules: {
            name_group: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            name_group: {
                required: 'Vui lòng nhập tên nhóm đánh giá',
                minlength: 'Tên nhóm đánh giá ít nhất 5 kí tự'
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                type: "POST",
                dataType: 'json',
                data: $(form).serializeArray(),
                url: $('#myform_GroupQC').attr('action'),
                success: function (e) {
                    if (e.dataReturn.formtype == 'create') {
                        $('#addGroupQC').modal('hide');
                        $('#staticModal').on('show.bs.modal', function (event) {
                            var modal = $(this);
                            modal.find('.modal-body').text(e.dataReturn.message);
                            modal.find('.href').hide();
                            $('#cate_code, #cate_code_modal').append($('<option>', {
                                value: e.dataReturn.code,
                                text: e.dataReturn.name
                            }));
                        });
                    } else {
                        // $('#staticModal').on('show.bs.modal', function (event) {
                        //     var modal       = $(this);
                        //     modal.find('.modal-body').text(e.dataReturn.message);
                        //     modal.find('.href').hide();
                        // });
                    }
                    $('#staticModal').modal('show');
                    return true;
                },
                error: function (jqXHR, textStatus) {
                    console.log(jqXHR);
                }
            })
        }
    });

    // Submit Item QC
    $("#myform_ItemQC").validate({
        rules: {
            cate_code_modal: {
                required: true
            },
            item_name: {
                required: true,
                minlength: 5
            },
            point: {
                required: true
            }
        },
        messages: {
            cate_code_modal: {
                required: 'Vui lòng chọn nhóm đánh giá'
            },
            item_name: {
                required: 'Vui lòng nhập đánh giá',
                minlength: 'Đánh giá có độ dài lớn hơn 5'
            },
            point: {
                required: 'Vui lòng nhập số điểm đánh giá'
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                type: "POST",
                dataType: 'json',
                data: $(form).serializeArray(),
                url: $('#myform_ItemQC').attr('action'),
                success: function (e) {
                    $('#addItemQC').modal('hide');
                    $('#staticModal').on('show.bs.modal', function (event) {
                        var modal = $(this);
                        modal.find('.modal-body').text(e.dataReturn.message);
                        modal.find('.bt-action').hide();
                        modal.find('.href').text('OK');
                        modal.find('.href').attr('href', e.dataReturn.url);
                    });
                    $('#staticModal').modal('show');
                    return true;
                },
                error: function (jqXHR, textStatus) {
                    console.log(jqXHR);
                }
            })
        }
    });

    $('#shopIDOrder').on('change', function (e) {
        e.preventDefault();
        var shopid = this.value;
        if (shopid == '') {
            return;
        }
        $.ajax({
            type: "GET",
            url: "/business/getAddressStore",
            dataType: "json",
            data: {shopid: shopid},
            success: function (data) {
                $('#address').val(data.fulladdress);
            }
        });
    });

    $('body').on('hidden.bs.modal', '.modal', function (event) {
        $(this).find('form').trigger('reset');
    });

    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "inherit");
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "auto");
    });

    // /**
    //  * Edit table
    //  * 
    //  */
    // $('.edittable').on('click', '.table tbody tr td .row_data', function(e) {
    //     e.preventDefault();
    //     $(this).addClass('bg-warning');
    //     var col_name    = $(this).attr('col_name');
    //     var col_val     = $(this).text();
    //     var max_val = $(this).attr("original_entry");
    //     $(this).removeClass('row_data');
    //     var html = '<input class="form-control input-text" onkeypress="validateNumber(event)" type="text">';
    //     $(this).html('').append(html);
    //     // $(this).closest('div').attr('contenteditable', 'true');
    //     $(this).find('input').focus().val(col_val);
    //     $(this).attr('original_entry', max_val);
    //     $(this).attr("name", col_val);
    // });


    // $('.edittable').on('focusout', '.table tbody tr td div .input-text', function(event) {
    //     event.preventDefault();

    //     var th = $(this);
    //     var tr = $(this).closest('tr');
    //     var idl_id = $(this).closest('tr').attr('idl_id');
    //     var inv_id = $(this).closest('tr').attr('inv_id');
    //     var code = $(this).closest('tr').attr('code');
    //     var name = $(this).closest('tr').attr('name');
    //     var unit = $(this).closest('tr').attr('unit');
    //     var type = $(this).closest('tr').attr('type');
    //     var qty_approve = $(this).closest('tr').attr('qty_approve');
    //     var qty_delivery = $(this).closest('tr').attr('qty_delivery');
    //     var price = $(this).closest('tr').attr('price');
    //     var row_div = $(this).parent().removeClass('bg-warning').addClass('row_data');
    //     var original_entry = $(this).parent().attr('original_entry');
    //     var col_name    = $(this).parent().attr('col_name');
    //     var col_val     = Number($(this).val()).toString();
    //     var old_value = $(this).parent().attr('name');
    //     if(parseInt(col_val) > parseInt(original_entry)){
    //         col_val = original_entry;
    //     }
    //     var arr = {};
    //     arr[col_name] = col_val;
    //     // //use the "arr" object for your ajax call
    //     if(parseInt(old_value) != parseInt(col_val)){
    //         $.extend(arr, {idl_id: idl_id, inv_id: inv_id, code: code, name: name, unit: unit, qty_approve: qty_approve, price: price, type: type});
    //         saveGridTable(arr, tr);
    //     } else{
    //         col_val = old_value;
    //     }    
    //     row_div.text(col_val);  
    //     //out put to show
    //     // $('.post_msg').html('').html( '<pre class="bg-success">'+JSON.stringify(arr, null, 2) +'</pre>');
    // });


    $('.input-text').on('keypress', function (e) {
        if (e.which === 13) {
            //Disable textbox to prevent multiple submit
            $(this).attr("disabled", "disabled");

            //Do Stuff, submit, etc..

            //Enable the textbox again if needed.
            // $(this).removeAttr("disabled");
        }
    });

});

var intVal = function (i) {
    return typeof i === 'string' ?
        i.replace(/[\$,]/g, '') * 1 :
        typeof i === 'number' ?
            i : 0;
};
var changeUrl = function (key, value) {
    if (typeof (history.pushState) != "undefined") {
        key = encodeURI(key);
        value = encodeURI(value);

        var kvp = document.location.search.substr(1).split('&');

        var i = kvp.length;
        var x;
        while (i--) {
            x = kvp[i].split('=');

            if (x[0] == key) {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }
        if (i < 0) {
            kvp[kvp.length] = [key, value].join('=');
        }

        var obj = {Page: "OBS", Url: "?" + kvp.join('&')};
        history.pushState(obj, obj.Page, obj.Url);
    }
}

function formatNumberChange(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function nl2brjs(str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function FormatNumber(str) {
    var strTemp = GetNumber(str);
    if (strTemp.length <= 3)
        return strTemp;
    var strResult = "";
    for (var i = 0; i < strTemp.length; i++)
        strTemp = strTemp.replace(",", "");
    var m = strTemp.lastIndexOf(".");
    if (m == -1) {
        for (var i = strTemp.length; i >= 0; i--) {
            if (strResult.length > 0 && (strTemp.length - i - 1) % 3 == 0)
                strResult = "," + strResult;
            strResult = strTemp.substring(i, i + 1) + strResult;
        }
    } else {
        var strphannguyen = strTemp.substring(0, strTemp.lastIndexOf("."));
        var strphanthapphan = strTemp.substring(strTemp.lastIndexOf("."),
            strTemp.length);
        var tam = 0;
        for (var i = strphannguyen.length; i >= 0; i--) {

            if (strResult.length > 0 && tam == 4) {
                strResult = "," + strResult;
                tam = 1;
            }

            strResult = strphannguyen.substring(i, i + 1) + strResult;
            tam = tam + 1;
        }
        strResult = strResult + strphanthapphan;
    }
    return strResult;
}

function GetNumber(str) {
    var count = 0;
    for (var i = 0; i < str.length; i++) {
        var temp = str.substring(i, i + 1);
        if (!(temp == "," || temp == "." || (temp >= 0 && temp <= 9))) {
            alert('Giá trị nhập vào không phải là số');
            return str.substring(0, i);
        }
        if (temp == " ")
            return str.substring(0, i);
    }
    return str;
}

function IsNumberInt(str) {
    for (var i = 0; i < str.length; i++) {
        var temp = str.substring(i, i + 1);
        if (!(temp == "." || (temp >= 0 && temp <= 9))) {
            alert('Giá trị nhập vào không phải là số');
            return str.substring(0, i);
        }
        if (temp == ",") {
            return str.substring(0, i);
        }
    }
    return str;
}

function subtring2dot(str) {
    if (is_float(str)) {
        return +(Math.round(str + "e+2") + "e-2");
        // return Math.round( str * 100 + Number.EPSILON ) / 100;
    }
    return str;
}

function imageIsLoaded(e) {
    $('#image_preview').css("display", "block");
    $('#previewing').attr('src', e.target.result);
    $('#previewing').attr('width', '200px');
    $('#previewing').attr('height', '180px');
}

function deleteCompetitors(th) {
    if (confirm('Bạn có chắc chắn muốn xóa đối thủ cạnh tranh này?')) {
        var id = $(th).attr('id');
        var idx = $(th).attr('idx');

        $.post("/business/deleteCompetitors", {row_id: id, working_detail_id: idx}, function (data) {
            $(".rowdata .table > tbody").html(data);
        });
    }
    return false;
}

function loadCompetitor(th) {
    var id = $(th).attr('id');
    var html_image = '';
    $.get("/business/getCompetitors", {id: id}, function (data) {
        var jsp = JSON.parse(data);
        // console.log(jsp);
        $('#doithucanhtranhModal').on('show.bs.modal', function (event) {
            var modal = $(this);
            modal.find('.row_id').val(jsp.ROW_ID);
            modal.find('#mediumModalLabel').text('Cập nhật');
            modal.find('input[type=submit]').val('Cập nhật');
            modal.find('.working_detail_id').val(jsp.WORKING_DETAIL_ID);
            modal.find('#title').val(jsp.TITLE);
            modal.find('#shop_model').val(jsp.SHOP_MODEL);
            modal.find('#content').val(jsp.CONTENT);
            modal.find('#range').val(formatNumberChange(jsp.RANGE));
            modal.find('#revenue_of_guest').val(formatNumberChange(jsp.REVENUE_OF_GUEST));
            modal.find('#image_competitors').attr('competitorsid', id);
            modal.find('#customer_of_day').val(formatNumberChange(jsp.CUSTOMER_OF_DAY));
            modal.find('#revenue_of_day').val(formatNumberChange(jsp.REVENUE_OF_DAY)).prop('readonly', true);
            //console.log(jsp.IMAGE);
            if (typeof jsp.IMAGE && jsp.IMAGE != null) {
                jsp.IMAGE.forEach(function (item) {
                    var d = new Date();
                    var n = d.getTime();
                    if (item != '') html_image += "<div class='col-sm-3'><a onclick='del_image_competitors(this)' competitorsid='" + id + "' name_img='" + item + "' class='del_image'>x</a><img src='" + base_url + "uploads/competitors/" + item + "?" + n + "' class='img-responsive img-thumbnail' data-action='zoom'></div>"
                });
                var countimg = jsp.IMAGE.length;
            } else var countimg = 0;
            modal.find('#uploaded_images_competitors').html(html_image);
            modal.find('#image_competitors').attr('countimg', countimg)
        });
        $('#doithucanhtranhModal').modal('show');
    });
}

function loadmodalformedit(th, element, type) {
    if (type == 1) {
        var id = $(th).attr('id');
        var code = $(th).attr('code');
        var name = $(th).attr('name');
        var qty = $(th).attr('qty');
        var group = $(th).attr('group');
        $('#modalformedit').on('show.bs.modal', function (event) {
            var modal = $(this);
            $('.type1and2 .show-error').hide();
            $('.type1and2 .expect').hide();
            $('.type3').hide();
            modal.find('#row_id').val(id);
            modal.find('#type').val(type);
            modal.find('#code').val(code);
            modal.find('#qty').val(qty);
            modal.find('#name').val(name);
            modal.find('#group').val(group);
        });
        $('#modalformedit').modal('show');
    } else if (type == 2) {
        var id = $(th).attr('id');
        var code = $(th).attr('code');
        var name = $(th).attr('name');
        var qty = $(th).attr('qty');
        var group = $(th).attr('group');
        var setupdate = $(th).attr('setupdate');
        setupdate = (setupdate == 'null') ? "" : setupdate;
        var note = $(th).attr('note');
        $('#modalformedit').on('show.bs.modal', function (event) {
            var modal = $(this);
            $('.type1and2 .show-error').hide();
            $('.type1and2 .expect').show();
            $('.type3').hide();
            modal.find('#row_id').val(id);
            modal.find('#type').val(type);
            modal.find('#code').val(code);
            modal.find('#qty').val(qty).prop('readonly', true);
            modal.find('#name').val(name);
            modal.find('#group').val(group);
            modal.find('#setupdate').val(setupdate);
            modal.find('#note').val(note);
        });
        $('#modalformedit').modal('show');
    } else if (type == 3) {
        var id = $(th).attr('id');
        var title = $(th).attr('title');
        var trainer = $(th).attr('trainer');
        var qty_join = $(th).attr('qty_join');
        var trainingday = $(th).attr('trainingday');
        trainingday = (trainingday == 'null') ? "" : trainingday;
        var note_training = $(th).attr('note_training');
        $('#modalformedit').on('show.bs.modal', function (event) {
            var modal = $(this);
            $('.type3').show();
            $('.type1and2').hide();
            $('.type3 .show-error').hide();
            modal.find('#row_id').val(id);
            modal.find('#type').val(type);
            modal.find('#class_name').val(title);
            modal.find('#trainer').val(trainer);
            modal.find('#qty_join').val(qty_join);
            modal.find('#note_training').val(note_training);
            modal.find('#trainingday').val(trainingday);
        });
        $('#modalformedit').modal('show');
    }
}

function loadmodalDetailProduct(th) {
    var idl_id = $(th).attr('idl_id');
    var inv_id = $(th).attr('inv_id');
    var code = $(th).attr('code');
    var name = $(th).attr('name');
    var unit = $(th).attr('unit');
    var qty_approve = $(th).attr('qty_approve');
    var price = $(th).attr('price');
    var type = $(th).attr('type');
    $('#modalformeditOrderDetail').on('show.bs.modal', function (event) {
        var modal = $(this);
        $('.show-error').hide();
        if (idl_id == 0) {
            modal.find('.modal-title').text('Thêm sản phẩm');
            modal.find('input[type=submit]').val('Thêm');
        } else {
            modal.find('.modal-title').text('Cập nhật sản phẩm');
            modal.find('input[type=submit]').val('Cập nhật');
        }
        if (type == 2) {
            var qty_delivery = $(th).attr('qty_delivery');
            modal.find('#qty_approve').prop('readonly', true);
            modal.find('#qty_delivery').val(qty_delivery == 'undefined' ? 0 : qty_delivery);
        } else if (type == 1) {
            var qty_order = $(th).attr('qty_order');
            modal.find('#qty_order').val(qty_order == 'null' ? 0 : qty_order).prop('readonly', true);
        }
        modal.find('#type').val(type);
        modal.find('#idl_id').val(idl_id);
        modal.find('#inv_id').val(inv_id);
        modal.find('#code').val(code).prop('readonly', true);
        modal.find('#name').val(name).prop('readonly', true);
        modal.find('#unit').val(unit).prop('readonly', true);
        modal.find('#qty_approve').val(qty_approve == 'null' ? 0 : qty_approve);
        modal.find('#price').val(price == 'null' ? 0 : formatNumberChange(price)).prop('readonly', true);
    });
    $('#modalformeditOrderDetail').modal('show');
}

function loadmodalEmployee(th) {
    var row_id = $(th).attr('row_id');
    var shop_code = $(th).attr('shop_code');
    var start_date = $(th).attr('start_date');
    var end_date = $(th).attr('end_date');
    //console.log(end_date);
    var username = $(th).attr('username');
    $('#loadmodalEmployee').on('show.bs.modal', function (event) {
        var modal = $(this);
        $('.show-error').hide();
        if (row_id == 0) {
            modal.find('.modal-title').text('Thêm lịch');
            modal.find('input[type=submit]').val('Thêm');
        } else {
            modal.find('.modal-title').text('Cập nhật lịch');
            modal.find('input[type=submit]').val('Cập nhật');
        }
        modal.find('#shop_code_employee').val(shop_code).change();
        modal.find('#start_date_employee').val((start_date));
        modal.find('#end_date_employee').val((end_date != 'null') ? end_date : '');
        modal.find('#row_id_employee').val(row_id);
        modal.find('#username').val(username);
    });
    $('#loadmodalEmployee').modal('show');
}

function del_calendar_Employee(th) {
    if (confirm('Bạn có chắc chắn muốn xóa dữ liệu này?')) {
        var row_id = $(th).attr('row_id');
        $.post("/api/business/deleteCalendarEmployee", {
            row_id: row_id
        }, function (e) {
            if (typeof e.dataReturn && e.dataReturn.error === true) {
                console.log(e.dataReturn.message);
                return false;
            } else {
                alert(e.dataReturn.message);
                $(th).parent().parent().parent().parent().remove();
                // window.location.reload();
            }
        }, 'json');
    }
    return false;
}

function del_image(th) {
    var shopid = $(th).attr('shopid');
    var name_img = $(th).attr('name_image');
    var countimg = $('#files').attr('countimg');
    if (confirm('Bạn có chắc chắn muốn xóa tấm ảnh này?')) {
        $.ajax({
            url: base_url + "/business/delImageSession",
            type: "POST",
            data: {shopid: shopid, name_img: name_img},
            beforeSend: function () {
                // $('#uploaded_images').html("<label class='text-success'>Uploading...</label>");
            },
            success: function (data) {
                $('#files').attr('countimg', Number(countimg) - 1);
                $('#uploaded_images').html(data);
                $("#message").hide().html('');
            }
        })
    }
    return false;
}

function del_image_qc(th) {
    var qcid = $(th).attr('qcid');
    var name_img = $(th).attr('name_image');
    var countimg = $('#files' + qcid).attr('countimg');
    if (confirm('Bạn có chắc chắn muốn xóa tấm ảnh này?')) {
        $.ajax({
            url: base_url + "/business/delImageQCSession",
            type: "POST",
            data: {qcid: qcid, name_img: name_img},
            beforeSend: function () {
                // $('#uploaded_images').html("<label class='text-success'>Uploading...</label>");
            },
            success: function (data) {
                $('#files' + qcid).attr('countimg', Number(countimg) - 1);
                $('#uploaded_images' + qcid).html(data);
                $("#message" + qcid).hide().html('');
            }
        })
    }
    return false;
}

function del_image_competitors(th) {
    var competitorsid = $(th).attr('competitorsid');
    var name_img = $(th).attr('name_img');
    var countimg = $('#image_competitors').attr('countimg');
    if (confirm('Bạn có chắc chắn muốn xóa tấm ảnh này?')) {
        $.ajax({
            url: base_url + "/business/delImageCompetitors",
            type: "POST",
            data: {competitorsid: competitorsid, name_img: name_img},
            beforeSend: function () {
            },
            success: function (data) {
                // console.log(data);
                $('#image_competitors').attr('countimg', Number(countimg) - 1);
                $('#uploaded_images_competitors').html(data);
                $("#message_competitors").hide().html('');
            }
        })
    }
    return false;
}

function updateFormulaQCDetail(th) {
    var row_id = $(th).attr('row_id');
    var formula_id = $(th).attr('formula_id');
    var cate_code = $(th).attr('cate_code');
    var tr = $(th).parent().parent().parent();
    var point = tr.find('td').eq(1).find('input').val();
    var val_old = tr.find('td').eq(1).find('input').attr('val_old');
    var max_point = tr.find('td').eq(1).find('input').attr('max_point');
    var item_qc_id = $(th).attr('item_qc_id');
    // var name        = tr.find('td').eq(0).text();
    $.post(base_url + "/categories/updateFormulaQCDetail", {
        row_id: row_id,
        formula_id: formula_id,
        point: point,
        item_qc_id: item_qc_id
    }, function (e) {
        if (typeof e.dataReturn && e.dataReturn.error === true) {
            //console.log(e.dataReturn.message);
            return false;
        } else {
            $('#staticModal').on('show.bs.modal', function (event) {
                var modal = $(this);
                modal.find('.modal-body').text(e.dataReturn.message);
                modal.find('.href').hide();
                var new_max_point = Number(max_point) - Number(val_old) + Number(point);
                // tr.find('td').eq(1).find('input').attr('max_point', new_max_point);
                tr.find('td').eq(1).find('input').attr('val_old', point);
                $('.max_point_cate' + cate_code).text(new_max_point);
                $('.max_point' + cate_code).attr('max_point', new_max_point);
            });
            $('#staticModal').modal('show');
            return true;
        }
    }, 'json');
}

function deleteFormulaQCDetail(th) {
    if (confirm('Bạn có chắc chắn xóa Đánh giá này?')) {
        var row_id = $(th).attr('row_id');
        var cate_code = $(th).attr('cate_code');
        var tr = $(th).parent().parent().parent();
        var val_old = tr.find('td').eq(1).find('input').attr('val_old');
        var max_point = tr.find('td').eq(1).find('input').attr('max_point');
        $.post(base_url + "/categories/deleteFormulaQCDetail", {row_id: row_id}, function (e) {
            if (typeof e.dataReturn && e.dataReturn.error === true) {
                //console.log(e.dataReturn.message);
                return false;
            } else {
                $('#staticModal').on('show.bs.modal', function (event) {
                    var modal = $(this);
                    modal.find('.modal-body').text(e.dataReturn.message);
                    modal.find('.href').hide();
                    var new_max_point = Number(max_point) - Number(val_old);
                    $(tr).remove();
                    $('.max_point_cate' + cate_code).text(new_max_point);
                    $('.max_point' + cate_code).attr('max_point', new_max_point);

                });
                $('#staticModal').modal('show');
                return true;
            }
        }, 'json');
    }
    return false;
}

function loadItemQC(th) {
    var row_id = $(th).attr('row_id');
    var point = $(th).attr('point');
    var description = $(th).attr('description');
    var item_name = $(th).attr('item_name');
    var cate_code = $(th).attr('cate_code');
    $('#addItemQC').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-title').text('Cập nhật đánh giá');

        modal.find('#item_name').val(item_name);
        modal.find('#item_description').val(description);
        modal.find('#item_point').val(point);
        modal.find('input[name="formtype"]').val('edit');
        modal.find('input[name="row_id"]').val(row_id);
        modal.find('#cate_code_modal').val(cate_code).trigger('change');
    });
    $('#addItemQC').modal('show');
}

function deleteItemQC(th) {
    var row_id = $(th).attr('row_id');
    if (confirm('Bạn có chắc chắn muốn xóa?')) {
        $.post(base_url + "/categories/deleteItemQC", {row_id: row_id}, function (e) {
            $('#staticModal').on('show.bs.modal', function (event) {
                var modal = $(this);
                modal.find('.modal-body').text(e.dataReturn.message);
                modal.find('.href').hide();
                $(th).parent().parent().parent().parent().parent().remove();
            });
            $('#staticModal').modal('show');
            return true;
        }, 'json');
    }
    return false;
}

function copyTemplate(th) {
    var row_id = $(th).attr('row_id');
    if (confirm('Bạn có muốn copy mẫu này?')) {
        $.post(base_url + "/categories/copyTemplate", {row_id: row_id}, function (e) {
            alert(e.dataReturn.message);
            window.location.reload();
        }, 'json');
    }
    return false;
}

function formatTimeExtra(time) {
    if (time !== null) {
        return format2(time)
    }
    return '';
}

let format2 = (n) => `0${n / 60 ^ 0}`.slice(-2) + ':' + ('0' + n % 60).slice(-2);

function formatDate(date) {
    if (date !== null) {
        return moment(date).format('DD-MM-YYYY');
    }
    return (date === null) ? '' : date;
}

function formatFullDate(date) {
    if (date !== null) {
        return moment(date).format('DD-MM-YYYY HH:mm:ss');
    }
    return (date === null) ? '' : date;
}

function formattime(ldate) {
    if (ldate === '00:00:00') return '';
    if (ldate !== null) {
        return date('H:i', strtotime(ldate));
    }
    return (ldate === null) ? '' : ldate;
}

function isInteger(x) {
    return typeof x === "number" && isFinite(x) && Math.floor(x) === x;
}

function isFloat(x) {
    return !!(x % 1);
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function validateNumber(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}


function loadEditSettingsStoreConfig(th) {
    var shop_id = $(th).attr('shop_id');
    var max_distance = $(th).attr('max_distance');
    var check_location = $(th).attr('check_location');
    $('#staticModal').on('show.bs.modal', function (event) {
        var modal = $(this);
        var url = base_url + 'settings/updatestoreconfig';
        modal.find('.modal-title').text('Cập nhật');
        var html = '<form action="' + url + '" id="form-popup">';
        html += '<div class="form-group has-feedback">';
        html += '<label>Khoảng cách tối đa(m)</label>';
        html += '<input maxlength="10" type="text" onkeypress="validateNumber(event)" name="max_distance" value="' + max_distance + '" class="form-control">';
        html += '</div>';
        html += '<div class="form-group has-feedback">';
        html += '<label>Kiểm tra khoảng cách</label>';
        html += '<select class="form-control" name="check_location">';
        html += '<option ';
        if (check_location == 1 || check_location == 'true') html += 'selected';
        html += ' value="1">Có</option>';
        html += '<option ';
        if (check_location == 0 || check_location == 'false') html += 'selected';
        html += ' value="0">Không</option>';
        html += '</select>';
        html += '</div>';
        html += '<input type="hidden" name="shop_id" value="' + shop_id + '">';
        html += '<button id="myFormSubmit" type="button" class="btn btn-warning">&nbsp;Lưu</button>&nbsp; <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>';
        html += '</form>';
        modal.find('.modal-body').html(html);
        modal.find('.modal-footer').remove();
    });
    $('#staticModal').modal('show');
}

function saveGridTable(arr, th) {
    $.post(base_url + "/business/saveGridTableDelivery", {arr: arr}, function (e) {
        if (e.dataReturn.error == false) {
            var qty_delivery = e.dataReturn.data.orderdetail_upd.qty_delivery;
            var price = arr.price;
            //console.log(arr)
            $(th).attr('qty_delivery', qty_delivery)
            $(th).find('td').eq(6).html(currencyFormat(price * qty_delivery));
            //console.log(qty_delivery)
            // $(th).closest('tr').attr('qty_delivery', qty_delivery);
        } else {
            alert(e.dataReturn.message);
        }
    }, 'json');
}

var defaultTop = $(window).height() - 10 - 60 //10 vị trí canh bottom ban đầu, 249 là chiều cao
var ranPo = ['-298px 0', '-447px 0', '-596px 0', '-745px 0']
$(window).resize(function () {
    defaultTop = $(window).height() - 10 - 60 //phòng trường hợp resize trình duyệt
})
$(window).scroll(function () { //Khi cuộn chuột xuống 50
    var $top = $(window).scrollTop()
    if ($top > 50) {
        $('#backtotop').fadeIn(100)
        $('#backtotop').css('top', defaultTop)
    } else {
        $('#backtotop').fadeOut(100)
    }
})
$('#backtotop').click(function () {
    timeranPo = window.setInterval(function () {
        $('#backtotop').css({
            'background-position': ranPo[Math.floor(Math.random() * ranPo.length)],
            'display': 'block'
        })
    }, 200)

    setTimeout(function () {
        $('html,body').stop().animate({scrollTop: '50px'}, 300, function () {
            $('#backtotop').stop().animate({'top': '-250px'}, 200, function () {
                clearInterval(timeranPo)
                $('#backtotop').css('background-position', '-149px 0').hide()
            })
        })
    }, 200)
})

$(document).on({
    mouseenter: function (event) {
        var table = $(event.target).closest('table');
        if (table.hasClass('DTFC_Cloned')) {
            var targetTable = $(event.target).parents('.dataTables_wrapper').find('table:not(.DTFC_Cloned)');
        } else {
            var targetTable = $(event.target).parents('.dataTables_wrapper').find('table.DTFC_Cloned');
        }
        var row = $(event.target).closest('tr');
        if (!row.hasClass('selected')) {
            trIndex = $(this).index();
            //console.log(targetTable.find("tr:eq(" + trIndex + ")"));
            targetTable.find("tbody>tr:eq(" + trIndex + ")").addClass("hover");
            row.addClass("hover");
        }

    },
    mouseleave: function (event) {
        var table = $(event.target).closest('table');
        if (table.hasClass('DTFC_Cloned')) {
            var targetTable = $(event.target).parents('.dataTables_wrapper').find('table:not(.DTFC_Cloned)');
        } else {
            var targetTable = $(event.target).parents('.dataTables_wrapper').find('table.DTFC_Cloned');
        }
        var row = $(event.target).closest('tr');
        // var table = $(event.target).parents('.dataTables_wrapper').find('table#UTable');
        trIndex = $(this).index();
        targetTable.find("tbody>tr:eq(" + trIndex + ")").removeClass("hover");
        row.removeClass("hover");
    }
}, "table.dataTable tbody tr");

function getTimeString() {
    return new Date().getTime();
}


//format number
function formatIntegerSeparator(value) {
    if (isNaN(value)) return 'isNaN';
    return currency(value, {symbol: '', separator: ',', precision: 0}).format();
}

function sumByProperty(items, prop) {
    return items.reduce((prev, cur) => prev + cur[prop], 0);
}

function downloadFile(url, fileName) {
    let request = new XMLHttpRequest();
    let _OBJECT_URL;
    $('#overlay, #ajaxCall').show();
    request.addEventListener('readystatechange', function (e) {
        if (request.readyState == 2 && request.status == 200) {
            // console.log('start download');
            // Download is being started
        } else if (request.readyState == 3) {
            // Download is under progress
        } else if (request.readyState == 4) {
            // Downloaing has finished
            //console.log('done');
            $('#overlay, #ajaxCall').hide();
            _OBJECT_URL = URL.createObjectURL(request.response);
            // Set href as a local object URL
            $('#hiddenDownload').attr('href', _OBJECT_URL);

            // Set name of download
            $('#hiddenDownload').attr('download', fileName);
            $('#hiddenDownload')[0].click();
            setTimeout(function () {
                window.URL.revokeObjectURL(_OBJECT_URL);
            }, 60 * 1000);
        }
    });
    request.addEventListener('progress', function (e) {
        if (e.lengthComputable) {
            var percent_complete = (e.loaded / e.total) * 100;
            // console.log(percent_complete);
        }

    });
    request.responseType = 'blob';
    // Downloading excel file
    request.open('get', url);
    request.send();
}

function getDataSelectedRowDatatable(table) {
    return table.rows({selected: true}).data();
}

function getSelectedRowId(table, index = 0) {
    return table.rows({selected: true})[0][index];
}

function getRowDataById(table, rowId) {
    return table.row(rowId).data();
    // return table.rows({selected: true}).data()[index];
}

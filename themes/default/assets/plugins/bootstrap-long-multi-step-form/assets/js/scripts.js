
function scroll_to_class(chosen_class) {
	var nav_height = $('nav').outerHeight();
	var scroll_to = $(chosen_class).offset().top - nav_height;

	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 1000);
	}
}


jQuery(document).ready(function() {

	/*
	    Fullscreen background
	*/
	$.backstretch(base_url + "default/assets/plugins/bootstrap-long-multi-step-form/assets/img/backgrounds/1.jpg");

	/*
	    Multi Step Form
	*/
	$('.msf-form form fieldset:first-child').fadeIn('slow');

	// next step
	$('.msf-form form .btn-next').on('click', function() {
		var class_position = $(this).attr('position');
		$(this).parents('fieldset').fadeOut(400, function() {
	    	$(this).next().fadeIn();
	    	// console.log(class_position);
	    	// console.log($("."+class_position + " .point_cate").attr('valpoint_cate'))
			$("."+class_position + " .point_cate").text( $("."+class_position + " .point_cate").attr('valpoint_cate') );
			// scroll_to_class('.msf-form');
	    });
	});
	
	// previous step
	$('.msf-form form .btn-previous').on('click', function() {
		var class_position = $(this).attr('position');
		$(this).parents('fieldset').fadeOut(400, function() {
			$(this).prev().fadeIn();
			// console.log($("."+class_position + " .point_cate").attr('valpoint_cate'))
			$("."+class_position + " .point_cate").text( $("."+class_position + " .point_cate").attr('valpoint_cate') );
			// scroll_to_class('.msf-form');
		});
	});
	

	$('.btn-step_by_step').on('click', function(e) {
		var idx = $(this).attr('idx');
		var class_position = $(this).attr('position');
		$("."+class_position + " .point_cate").text( $("."+class_position + " .point_cate").attr('valpoint_cate') );
		$(this).parents('ul').next('form.qc').find('fieldset').css('display', 'none')
		$('#idx'+ idx).fadeIn();
		// $('#qc').find('fieldset').fadeOut('slow', function() {
		// 	$('#'+ idx).fadeIn();
		// });
	});
});

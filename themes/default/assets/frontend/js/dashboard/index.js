var checkfull = 0;
var restClient;

$(document).ready(function() {
    restClient = new $.RestClient('/api/dashboard/');
    restClient.add('summaryShop', { isSingle: true });
    restClient.add('summaryChart', { isSingle: true });

    loadChart($("#fromDate").val(), $("#toDate").val(), $("#shop_code").val(), $("#shoptype").val(), $("#shopmodel").val(), $("#province_code").val(), $("#district").val(), checkfull);
    loadSummary($("#fromDate").val(), $("#toDate").val(), $("#shop_code").val(), $("#shoptype").val(), $("#shopmodel").val(), $("#province_code").val(), $("#district").val(), checkfull);
    var time = setInterval(function() {
        loadSummary($("#fromDate").val(), $("#toDate").val(), $("#shop_code").val(), $("#shoptype").val(), $("#shopmodel").val(), $("#province_code").val(), $("#district").val(), checkfull);
        // clearInterval(time);
    }, 1000 * 60 * 5);

    $('input[type=checkbox].check24h').on('ifChecked', function(event) {
        console.log('asdasd');
        checkfull = 1;
    });

    $('input[type=checkbox].check24h').on('ifUnchecked', function(event) {
        checkfull = 0;
    });

    $("#updateDataChart").click(function() {
        loadChart($("#fromDate").val(), $("#toDate").val(), $("#shop_code").val(), $("#shoptype").val(), $("#shopmodel").val(), $("#province_code").val(), $("#district").val(), checkfull);
        loadSummary($("#fromDate").val(), $("#toDate").val(), $("#shop_code").val(), $("#shoptype").val(), $("#shopmodel").val(), $("#province_code").val(), $("#district").val(), checkfull);
    });


});

function buildChart(title, data, categories) {
    $('#chart').highcharts({
        chart: {
            type: 'column',
            style: {
                fontFamily: 'Tahoma'
            }
        },
        // title: { text: title +' ('+ $("#shop_code option:selected").text().replace('|---' ,'') +')' },
        title: false,
        credits: { enabled: false },
        exporting: { enabled: false },

        subtitle: false, //{ text: 'Source: WorldClimate.com'},
        xAxis: {
            categories: categories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: false,
            // tickInterval: 20
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        colors: ['#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce',
            '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a',
            '#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE',
            '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'
        ],
        series: data
    });
}

function loadChart(fromDate = null, toDate = null, shopCode = null, shopType = null, shopModel = null, provinceCode = null, districtCode = null, checkFull = null) {
    restClient.summaryChart.read({ fromDate: fromDate, toDate: toDate, shopCode: shopCode, shopType: shopType, shopModel: shopModel, provinceCode: provinceCode, districtCode: districtCode, checkFull: checkFull })
        .done(function(data, textStatus, xhrObject) {
            if (textStatus == 'success') {
                buildChart(data.title, data.data, data.categories);
            }
        });
}

function loadSummary(fromDate = null, toDate = null, shopCode = null, shopType = null, shopModel = null, provinceCode = null, districtCode = null, checkFull = null) {
    restClient.summaryShop.read({ fromDate: fromDate, toDate: toDate, shopCode: shopCode, shopType: shopType, shopModel: shopModel, provinceCode: provinceCode, districtCode: districtCode, checkFull: checkFull })
        .done(function(data, textStatus, xhrObject) {
            //console.log(textStatus);
            if (textStatus == 'success') {
                $('.sales').text(formatNumber(data.totalAmounts));
                $('.bill').text(formatNumber(data.totalBills));
                $('.cup').text(formatNumber(data.totalCups));
                $('.countActive').text(formatNumber(data.totalActive));
                $('.countPending').text(formatNumber(data.totalPending));
                $('.countDeactive').text(formatNumber(data.totalStop));
            }
        });
}
$(document).on("keydown",function(ev){
	if(ev.keyCode===27||ev.keyCode===122) return false
});

$(document).on('click', '.view', function (e) {
    $('.aQuestion').each(function(){
	   	$(this).toggleClass("nochoice",$(this).find('input[type="radio"]:checked').length == 0);

		if($(this).find('input[type="radio"]').is(':checked')) {
			$(this).find('input[type="hidden"].textchoice').val($(this).find('input[type="radio"]:checked').attr('id'));
		}
	   
	});
	
	var check = $( ".aQuestion" ).hasClass( "nochoice" );
	if(!check) {
		e.preventDefault();
		var elems = $('form[name=frmQuestion]');
		var OS = getOS();
        var info = elems.serialize() + '&os=' + OS;
		$.ajax({
			url: elems.attr('action'),
			type: 'POST',
			data: info,
			success: function (re) {
				var json = JSON.parse(re);
				if(typeof json.error && json.error === true){
					alert(json.message);
					return ;
                }else{
	                $('.aQuestion').each(function(){
						if($(this).find('input[type="radio"]').is(':checked')) {
							valRadio = $(this).find('input[type="radio"]:checked').attr('value');
							if(valRadio === 'yes'){
								// $(this).addClass('right');
								$(this).find('input[type="radio"]:checked').parent().css({"color":"#155724", "font-weight":"bold", "background-image":"url(/default/assets/frontend/images/tick.png)", "background-repeat":"no-repeat"});
							}else{
								$(this).addClass('wrong');
								$(this).find('input[type="radio"]:checked').parent().css({"color":"red","background-image":"url(/default/assets/frontend/images/edit_delete.png)", "background-repeat":"no-repeat"});
							}
						}
					});

	                $('#frmQuestion :input').attr('disabled', 'disabled');
					$('.button-fixed .view').hide();
					if(checkbrowser()) $('.button-fixed .closeW').show();
                }
			}
		});
	}
});


function getOS() {
    var userAgent = window.navigator.userAgent,
        platform = window.navigator.platform,
        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
        os = null;

        if (macosPlatforms.indexOf(platform) !== -1) {
            os = 'Mac OS';
        } else if (iosPlatforms.indexOf(platform) !== -1) {
            os = 'iOS';
        } else if (windowsPlatforms.indexOf(platform) !== -1) {
            os = 'Windows';
        } else if (/Android/.test(userAgent)) {
            os = 'Android';
        } else if (!os && /Linux/.test(platform)) {
            os = 'Android';
        }
        return os;
}

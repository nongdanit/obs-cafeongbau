<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/**
 * Defined
 */
define('URL_API_DOWNLOAD_FILE_', "https://obs.cafeongbau.com:5003");

if(ENVIRONMENT == 'production') {
	define('URL_API', "http://10.5.26.3:3034");
	define('URL_API2', "https://mbs.nutifood.net:8443");
	define('URL_API3', "https://obs.cafeongbau.com:5003/v1/api/");
}elseif (ENVIRONMENT == 'testing') {
	//define('URL_API', "https://pos.cafeongbau.com:3099");
	define('URL_API', "http://10.5.26.3:3099");
	define('URL_API2', "https://mbsdev.nutifood.net:9443");
}elseif (ENVIRONMENT == 'development') {
	define('URL_API', "https://obs.cafeongbau.com:3034");
	define('URL_API2', "https://mbs.nutifood.net:8443");
	// define('URL_API', "http://pos.cafeongbau.com:3034");
	define('URL_API3', "https://obs.cafeongbau.com:5003/v1/api/");
	// const ACCESS_TOKEN = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJ1c2VyX25hbWUiOiJuYW10biIsImVtcGxveWVlX2lkIjoxLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE3MTQwMjg0MTF9.GZNMNOMynabpds1h5EOi1cwneUAtF8XjuJdNkBXe7iO50chREFdR8XGbstiRroPKXgHpkK2zlnqFeLWpbN0mQGb9vk3c_v5i2TIOKoYibg1WL3ffrPJpeVuuAK8H9DMpfTOBZF8iwKz4BAyHh4s1FTgmh1Qntsftv9C0XjOg1rtZ0Ty6Cl5S0gZNUcyC8iUAmPzD3VjmseLrhdZPqoN9uPbFNtUObg6xhHdzuikY4pwz_aXlT4wp2BOAVrzFjZwFvD4w5JO79zZWBrF8vAPwHo9KhoVTzrxschQ_aNEKzuRFKs7HWQ39uZrqawmS3EmW0flcnoR0zCXhG3AFDmHRrg';
}else{
	define('URL_API', "http://localhost:3099");
	define('URL_API2', "https://mbsdev.nutifood.net:9443");
}

define('UPLOADS', FCPATH."uploads/");
//SHOPS
define('SHOPS', UPLOADS."shops/");
define('URL_SHOPS','uploads/shops/');
//COMPETITORS
define('COMPETITORS', UPLOADS."competitors/");
define('URL_COMPETITORS','uploads/competitors/');
//QC
define('QC', UPLOADS."qc/");
define('URL_QC','uploads/qc/');
// Status Store
define('CANCEL', 0);
define('OPEN', 1);
define('DOING', 5);
define('NOTAPPROVE', 8);
define('UNAPPROVE', 9);
define('APPROVE', 10);
define('DONE', 100);
// Status account
define('NOTACTIVE', 0);
define('ACTIVE', 1);
define('LOCKED', 2);
define('DELETE', 3);

//Group user OBS
define('ADMIN', 1);
define('ACCOUNTANT', 2);
define('ONGBAU', 3);
define('DICHVUKHACHHANG', 13);
define('TTPP', 7);

//Status Order OBS
define('ORDER_APPROVE', 5); //Đã duyệt
define('ORDER_WAITING_ACCOUNTANT', 4); //Chờ kế toán duyệt
define('ORDER_DELIVERY', 6); //Giao hàng


const HTTP_POST = 'POST';
const HTTP_GET = 'GET';
const HTTP_PUT = 'PUT';
const HTTP_DELETE = 'DELETE';

const GOOGLE_API_KEY = 'AIzaSyCVFu8l3KXNPQQfroMz9ObbXdn4AZLOy7o';

define('URL_IMG_CHECK_IN_OUT', 'https://obs.cafeongbau.com:5003');

# Account Login Chấm công
const ACCOUNT_USER = 'namtn';
const ACCOUNT_PASS = '123456789';

const X_api_key = 'i71n9ZbAYSAF5ZRD0eKHgsRB2UvA+AQSikqZpMPEq+fFw3XoEGw5onm5mnTiHWZaYyehRS7bOkSS6bW0//OpbO9HDh9XJNPLDqRPAzeVvQqB3Pk23nDWO5ouM3EHm4w5Oh1p3LkWx47m8o8rinlnxcbtDghUkvF7/c0XypYW3K/53KjJf6Ojod80gOpckPsBcXdoO4qPqLZIIEUqhrLrJ5kNqrK69tPRbchyLbZoMoniW4M1btI0YsLINoGpOO4U0R/fWZDSmp2m4X2P0epdSDm9V3/t80mPq2fIgdGHogKJJEX/X8CvSPokInAnqRxxueoxulSMQc3PG48wjTbcKQ==';
const ACCESS_TOKEN = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJ1c2VyX25hbWUiOiJhZG1pbiIsImVtcGxveWVlX2lkIjoxLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE3MjE2OTY4MTAsImV4cCI6ODY0MTcyMTYxMDQxMH0.YJhu0v69WRJTHyS4zz417oXLcuU91UwsTtxmkRrooo_FzR2NU_IPmvp_ePLUHDVcdL2wp5t4O_0p_2nNIt9zV3aMBzsJXQ2xFsUAMVUR8tdSOgsnxDX-8yHBmOrll-conj_yJRAVpRMeWhGKNzyP-FMdRUSghJnUe836ID3FvyVEcCN6xvjJpGPxi4WwSwZ_0leG5HMt41bz2-2HMF2IRZoSVoPUsoZBfxHgRqf4EldK-bHA7nKMaZNDCGfpfuhA0ER_e7-LwUhPRzDClgpWgXxfCvRjcrZAi-3Ys2CUBEvsdtJ9dnWS1jsoh_jkS-yEmidKwHIGOD9MV8mABET3Sg';
<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin/auth/login';//'home/home/openwindow';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;


$route['login'] = 'admin/auth/login';
$route['logout'] = 'admin/auth/logout';
$route['welcome'] = 'admin/dashboard';
$route['logout/(:any)'] = 'admin/auth/logout/$1';

$route['employer'] = 'admin/employer/index';
$route['employer/add'] = 'admin/employer/add';
$route['employer/(:any)'] = 'admin/employer/$1';
$route['employer/profile/(:any)'] = 'admin/employer/profile/$1';
$route['employer/edit/(:num)'] = 'admin/employer/edit/$1';

$route['users'] = 'admin/users/index';
$route['users/add'] = 'admin/users/add';
$route['users/(:any)'] = 'admin/users/$1';
$route['users/profile/(:any)'] = 'admin/users/profile/$1';
$route['users/edit/(:num)'] = 'admin/users/edit/$1';
$route['users/permission/(:any)'] = 'admin/users/permission/$1';


$route['settings/(:any)'] = 'admin/settings/$1';

$route['reports'] = 'admin/reports/index';
$route['reports/rptshop'] = 'admin/reports/rptshop_info';
$route['reports/(:any)'] = 'admin/reports/$1';
$route['reports/(:any)/(:any)'] = 'admin/reports/$1/$2';


$route['dashboard'] = 'admin/dashboard/index';
$route['dashboard/(:any)'] = 'admin/dashboard/$1';
$route['dashboard/(:any)/(:any)'] = 'admin/dashboard/$1/$2';

$route['business'] = 'admin/business/index';
$route['business/approveOrder'] = 'admin/business/approve_order';

$route['business/(:any)'] = 'admin/business/$1';
$route['business/(:any)'] = 'admin/business/$1';
$route['business/(:any)/(:any)'] = 'admin/business/$1/$2';
$route['business/(:any)/(:any)/(:num)'] = 'admin/business/$1/$2/$3';

//categories
$route['categories'] = 'admin/categories/shop';
$route['categories/shop'] = 'admin/categories/shop_info';
$route['categories/(:any)'] = 'admin/categories/$1';
$route['categories/(:any)/(:any)'] = 'admin/categories/$1/$2';
$route['categories/(:any)/(:any)/(:num)'] = 'admin/categories/$1/$2/$3';


//Api
$route['api'] = 'admin/api';
$route['api/(:any)'] = 'admin/api/$1';

// $route['api/dashboard/(:any)']			= 'api/dashboard/$1';

// $route['api/report/(:any)']			= 'api/report/$1';


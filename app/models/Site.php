<?php defined('BASEPATH') or exit('No direct script access allowed');

class Site extends CI_Model
{
    protected $errors;

    public function __construct()
    {
        parent::__construct();
        $this->errors = array();
    }

    public function getSettings()
    {
        $arrSetting = array(
            'logo' => 'logo1.png',
            'site_name' => 'OBS',
            'tel' => '0105292122',
            'dateformat' => 'd-m-Y',
            'timeformat' => 'h:i A',
            'timeformatfullhours' => 'H:i',
            'timeformatfullhoursSeconds' => 'H:i:s',
            //'default_email' => 'nutifood@nutifood.com.vn',
            'language' => 'vietnamese',
            'version' => '1.0.1',
            'theme' => 'default',
            'timezone' => 'Asia/Ho_Chi_Minh',
            // 'protocol' => 'smtp',
            // 'smtp_host' => 'mail.nutifood.com.vn',
            // 'smtp_user' => 'nutifood@nutifood.com.vn',
            // 'smtp_pass' => 'Nuti@384',
            // 'smtp_port' => '587',
            // 'smtp_crypto' =>'',
            //'mmode' => 0,
            //'captcha' => 0,
            //'mailpath' =>'',
            //'currency_prefix' => 'USD',
            'default_customer' => 3,
            'default_tax_rate' => '5%',
            'rows_per_page' => 10,
            'total_rows' => 30,
            'header' => '<h2><strong>OBS</strong></h2>Ông Bầu System<br>',
            'footer' => 'Ông Bầu System!<br>',
            'bsty' => 3,
            'display_kb' => 0,
            'default_category' => 1,
            'default_discount' => 0,
            'item_addition' => 1,
            'barcode_symbology' => '',
            'pro_limit' => 10,
            'receipt_printer' => '',
            'pos_printers' => '',
            'cash_drawer_codes' => '',
            'char_per_line' => 42,
            'rounding' => 1,
            'pin_code' => 2122,
            //'purchase_code' => '7a5123a0-49b3-446b-a331-87b43d092e06',
            //'envato_username' => 'doanthanhit',
            'theme_style' => 'green',
            'after_sale_page' => '',
            'overselling' => 1,
            'printer' => 1,
            'order_printers' => '',
            'auto_print' => 0,
            'local_printers' => '',
            //'google_API_key' =>'AIzaSyBgfUaw4y0EhqUJszq0lo_V4Wu4cWZVV2w',
            'lng' => 150.644,
            'lat' => -34.397,
            'qty_decimals' => 2,
            'symbol' => null,
            'display_symbol' => null,
            'decimals' => 0,
            'thousands_sep' => ',',
            'decimals_sep' => '.',
            'sac' => null
        );
        return (object)($arrSetting);
    }
    /**
     * API @Thành change
     * API @Nam
     */
    # Employer
    function getEMPLOYERByShopCode($data)
    {
        $url = 'employee';
        $shops = '';
        if(is_array($data['shop_code']))
        {
            $shops = implode(",", $data['shop_code']);
        }
        $dataPost = array(
            'shop_codes' => $shops
        );
        // return executeApi(HTTP_POST, $url, $dataPost, false);
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "getEMPLOYERByShopCode resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getListEmpDelByShopCode($data)
    {
        $url = '/api/ob/web/Emp/getListEmpDelByShopCode';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'SHOP_CODE' => $data['shop_code']
        );
        return executeApi(HTTP_POST, $url, $dataPost, false);
       // $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
       // return $resultAPI;
    }

    function getEmployerByRowID($row_id)
    {
        $url = 'employee/'.$row_id;
        $resultAPI = $this->data_lib->callAPI($url, 'GET', [], $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "getEmployerByRowID resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getListEmpType()
    {
        $url = 'department';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', [], $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "getListEmpType resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }


    function updateEmp($data = NULL)
    {
        $url = 'employee/'.$data['row_id'];
        $dataPost = array(
            'title'         => $data['title'],
            'employee_code' => $data['employee_code'],
            'department_id' => $data['department_id'],
            'first_name'    => $data['first_name'],
            'last_name'     => $data['last_name'],
            // 'phone'         => $data['phone'],
            'address'       => $data['address'],
            'id_card'       => $data['id_card'],
            'poi'           => $data['poi'],
            'doi'           => (!empty($data['doi']))?date('Y-m-d', strtotime($data['doi'])): NULL,
            'date_in'       => (!empty($data['date_in']))?date('Y-m-d', strtotime($data['date_in'])): NULL,
            'date_out'      => (!empty($data['date_out']))?date('Y-m-d', strtotime($data['date_out'])): NULL,
            'active'        => $data['active']
        );

        if (!empty($data['password'])) {
            $dataPass = array(
                'new_password' => $data['password'],
                'confirm_password'  => $data['password'],
            );
            $resultAPIPass = $this->data_lib->callAPI('access/reset-password/'.$data['phone'], 'PUT', $dataPass, $this->session->userdata('access_token'), FALSE);
            if (!empty($resultAPIPass)) {
                if ($resultAPIPass['status'] == 200) {
                    log_message("debug", $resultAPIPass['message']." reset-password-success: ". $data['phone']);
                }else{
                    log_message("debug", "reset-password-failed: ". $data['phone']);
                }
            }
        }
        // log_message("debug", "updateEmp dataPost: " . print_r($dataPost, true));
        $resultAPI = $this->data_lib->callAPI($url, 'PUT', $dataPost, $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "updateEmp resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function deleteEmp($data = NULL)
    {
        $url = '/api/ob/web/Emp/deleteEmp';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'UPDATE_BY' => $data['update_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "deleteEmp resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getListOBSShop()
    {
        $url = '/api/ob/obs/Shop/getListOBSShop';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getListOBSShop resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateStoreConfigModel($data)
    {
        $url = '/api/ob/obs/Shop/updateOBSShopByShopId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "SHOP_ID" => (int)$data['shop_id'],
            "MIN_DISTANCE" => (int)$data['max_distance'],
            "CHECK_LOCATION" => (int)$data['check_location'],
            "TARGET" => 0,
            "MODIFY_BY" => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "updateStoreConfigModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getListCalendarEmp($data = NULL)
    {
        $url = 'calendar-work';
        $dataPost = array(
            'employee_id' => $data['employee_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('access_token'), FALSE);
        return $resultAPI;
    }

    function getCheckIncheckOut($data = NULL)
    {
        $url = '/api/ob/web/report/openTimeCloseTimeShop';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'DATETIME' => $data['date'],
            'SHOP_CODE' => $data['shop_code']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "getCheckIncheckOut resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getCheckIncheckOutDetail($data = NULL)
    {
        $url    = 'attendance/reports';
        $shops  = $data['shop_code'];
        if(is_array($shops))
        {
            $shops = implode(",", $data['shop_code']);
        }
        $dataPost = array(
            'shop_codes' => $shops,
            'start_date' => $data['from_date'],
            'end_date'   => $data['to_date'],
            'offset'     => 0,
            'limit'      => 999
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "getCheckIncheckOutDetail resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function checkInCheckOutByEmpCodeAndShopCodeAndDay($data = NULL)
    {
        $url = 'attendance/'.$data['userId'].'/'.$data['datetime'].'/'.$data['shopcode'];
        $resultAPI = $this->data_lib->callAPI($url, 'GET', [], $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "checkInCheckOutByEmpCodeAndShopCodeAndDay resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function leaveBalance($data = NULL)
    {
        $url = 'leave-balance/'.$data['userId'].'/'.$data['datetime'].'/'.$data['shopcode'];
        $resultAPI = $this->data_lib->callAPI($url, 'GET', [], $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "leaveBalance resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getReason()
    {
        $url = 'reason';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', ['reason_type' => 'leave_balance'], $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "getReason resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateEmpCheckOut($data = NULL)
    {
        $url = 'attendance/'.$data['row_id'];
        $dataPost = array(
            'lat'       => 100.6000,
            'lng'       => 10.3333,
            'check_out' => $data['timecheckout'],
            'description' => $data['note'],
            'modify_by' => $data['update_by']
        );
        log_message("debug", "updateEmpCheckOut dataPost: " . print_r($dataPost, true));
        $resultAPI = $this->data_lib->callAPI($url, 'PATCH', $dataPost, $this->session->userdata('access_token'), FALSE);
        log_message("debug", "updateEmpCheckOut resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }


    function addAnnualLeave($data = NULL): array
    {
        if (!empty($data['rowId'])) {
            $url = 'leave-balance/'.$data['rowId'];
            $dataPost = array(
                'title'     =>  $data['reason_title'],
                'reason_id' =>  $data['reason'],
                'description' => $data['note'],
                'modify_by'   => $this->session->userdata('userId')
            );
            $resultAPI = $this->data_lib->callAPI($url, 'PUT', $dataPost, $this->session->userdata('access_token'), FALSE);
        } else {
            $url = 'leave-balance';
            $dataPost = array(
                'title'     =>  $data['reason_title'],
                'user_id'   =>  $data['user_id'],
                'shop_code' =>  $data['shopCode'],
                'start_date'=>  date("Y-m-d", strtotime($data['date'])),
                'end_date'  =>  date("Y-m-d", strtotime($data['date'])),
                'from_hour' =>  "07:00:00",
                'to_hour'   =>  "21:00:00",
                'reason_id' =>  $data['reason'],
                'description' => $data['note'],
                'create_by'   => $this->session->userdata('userId')
            );
            $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, $this->session->userdata('access_token'), FALSE);
        }
        // log_message("debug", "addAnnualLeave dataPost: " . print_r($dataPost, true));
        // log_message("debug", "addAnnualLeave resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function addCalendarEmpWork($data = NULL)
    {
        $url = 'calendar-work';
        $dataPost = array(
            'employee_id' => $data['username'],
            'shop_code' => $data['shop_code'],
            'start_date' => (!empty($data['start_date'])) ? date('Y-m-d', strtotime($data['start_date'])) : NULL,
            'end_date' => (!empty($data['end_date'])) ? date('Y-m-d', strtotime($data['end_date'])) : NULL,
            'create_by' => $this->session->userdata('userId')
        );
        // log_message("debug", "addCalendarEmpWork dataPost - POST: " . print_r($dataPost, true));
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "addCalendarEmpWork resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateCalendarEmpWork($data = NULL)
    {
        $url = 'calendar-work/'.$data['row_id'];
        $dataPost = array(
            'employee_id'   => $data['username'],
            'shop_code'     => $data['shop_code'],
            'start_date'    => (!empty($data['start_date'])) ? date('Y-m-d', strtotime($data['start_date'])) : NULL,
            'end_date'      => (!empty($data['end_date'])) ? date('Y-m-d', strtotime($data['end_date'])) : NULL,
            'modify_by'     => $this->session->userdata('userId')
        );
        $resultAPI          = $this->data_lib->callAPI($url, 'PUT', $dataPost, $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "updateCalendarEmpWork resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function deleteCalendarEmpWork($data = NULL)
    {
        $url = 'calendar-work/'.$data['row_id'];
        $dataPost = array(
            'modify_by' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'DELETE', $dataPost, $this->session->userdata('access_token'), FALSE);
        // log_message("debug", "deleteCalendarEmpWork resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    # Shop
    function getShopType($data = NULL)
    {
        $url = '/api/ob/web/shop/getListShopType';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getShopType resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getShopModel($data = NULL)
    {
        $url = '/api/ob/web/shop/getListShopModel';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getShopById($id)
    {
        // $url = '/api/ob/web/shop/getShopInfoByRowId';
        $url = '/api/ob/web/shop/getShopInfoByRowIdV2';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $id
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getShopById resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getListShop()
    {
        $url = '/api/ob/web/shop/getListShopInfo';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function insertShop($data)
    {
        // $url = '/api/ob/web/shop/themMoiMatBang';
        $url = '/api/ob/web/shop/themMoiMatBangV2';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "SHOP_NAME" => $data['shop_name'],
            "ADDRESS" => $data['address'],
            "WARD_CODE" => $data['ward_code'],
            "DISTRICT_CODE" => $data['district_code'],
            "PROVINCE_CODE" => $data['province_code'],
            "LNG" => $data['lng'],
            "LAT" => $data['lat'],
            "TYPE_ID" => $data['type_id'],
            "MODEL_ID" => $data['model_id'],
            "CUS_NAME" => $data['cus_name'],
            "TEL" => $data['tel'],
            "EMAIL" => $data['email'],
            "NOTE" => $data['note_shop'],
            "CREATE_BY" => $data['create_by'],
            "IMAGE" => $data['image'],
            "WAREHOUSE_ID" => $data['warehouse_id'],
            "DELIVERY_DAY_FIRST" => $data['delivery_day_first'],
            "DELIVERY_DAY" => $data['delivery_day'],
            "INVOICE_INFO" => $data['invoice_info'],
            "SHOP_NAME_OLD" => $data['shop_name_old'],
            "START_DATE" => $data['start_date']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "insertShop resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateShop($data, $dataUpdateWorkingDetail = NULL)
    {
        $url = '/api/ob/web/shop/updateShopInfoV3';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            "SHOP_NAME" => $data['shop_name'],
            "SHOP_CODE" => $data['shop_code'],
            "TEL" => $data['tel'],
            "EMAIL" => $data['email'],
            "NOTE" => $data['note_shop'],
            "STATUS" => $data['status'],
            "CUS_NAME" => $data['cus_name'],
            "ADDRESS" => $data['address'],
            "WARD_CODE" => $data['ward_code'],
            "DISTRICT_CODE" => $data['district_code'],
            "PROVINCE_CODE" => $data['province_code'],
            "LNG" => $data['lng'],
            "LAT" => $data['lat'],
            "TYPE_ID" => $data['type_id'],
            "MODEL_ID" => $data['model_id'],
            "UPDATE_BY" => $data['update_by'],
            "ROW_ID_IMAGE" => $data['row_id_image'],
            "IMAGE" => $data['image'],
            "WAREHOUSE_ID" => $data['warehouse_id'],
            "DELIVERY_DAY_FIRST" => $data['delivery_day_first'],
            "DELIVERY_DAY" => $data['delivery_day'],
            "INVOICE_INFO" => $data['invoice_info'],
            "SHOP_NAME_OLD" => $data['shop_name_old'],
            "START_DATE" => $data['start_date']
        );
        if (!empty($data['open_date'])) $dataPost['OPEN_DATE'] = $data['open_date'];
        if (!empty($dataUpdateWorkingDetail)) $dataPost['WORKING_DETAILs'] = array_values($dataUpdateWorkingDetail);

        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "updateShopInfoV3 resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function insertCompetitors($data)
    {
        $url = '/api/ob/web/shop/insertCompetitors';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "WORKING_DETAIL_ID" => $data['working_detail_id'],
            "TITLE" => $data['title'],
            "CONTENT" => $data['content'],
            "SHOP_MODEL" => $data['shop_model'],
            "RANGE" => $data['range'],
            "REVENUE_OF_GUEST" => $data['revenue_of_guest'],
            "CUSTOMER_OF_DAY" => $data['customer_of_day'],
            "REVENUE_OF_DAY" => $data['revenue_of_day'],
            "IMAGE" => $data['image'],
            "CREATE_BY" => $data['create_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "insertCompetitors resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateCompetitors($data)
    {
        $url = '/api/ob/web/shop/updateCompetitors';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "ROW_ID" => $data['row_id'],
            "TITLE" => $data['title'],
            "CONTENT" => $data['content'],
            "SHOP_MODEL" => $data['shop_model'],
            "RANGE" => $data['range'],
            "REVENUE_OF_GUEST" => $data['revenue_of_guest'],
            "CUSTOMER_OF_DAY" => $data['customer_of_day'],
            "REVENUE_OF_DAY" => $data['revenue_of_day'],
            "IMAGE" => $data['image'],
            "UPDATE_BY" => $data['update_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "updateCompetitors resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function deleteCompetitors($data)
    {
        $url = '/api/ob/web/shop/deleteCompetitors';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "ROW_ID" => $data['row_id'],
            "WORKING_DETAIL_ID" => $data['working_detail_id'],
            "DELETE_BY" => $data['delete_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "deleteCompetitors resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getlistCompetitors($data)
    {
        $url = '/api/ob/web/shop/getlistCompetitorsByWorkingDetailId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'WORKING_DETAIL_ID' => $data['working_detail_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getlistFORMULA($data)
    {
        $url = '/api/ob/web/manager/getListFormulaByRowId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getCompetitorsByRowID($data)
    {
        $url = '/api/ob/web/shop/getlistCompetitorsByRowId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function updateWorkingDetail($data)
    {
        $url = '/api/ob/web/shop/updateWorkingDetail';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'NOTE' => (!empty($data['note'])) ? $data['note'] : "",
            'ROW_ID' => $data['row_id'],
            'CONTENT' => $data['content'],
            'STATUS' => $data['status'],
            'UPDATE_BY' => $data['update_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "updateWorkingDetail resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateWorking($data, $dataUpdateWorkingDetail)
    {
        // $url = '/api/ob/web/shop/updateWorking';
        $url = '/api/ob/web/shop/updateWorkingv2';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'NOTE' => (!empty($data['note'])) ? $data['note'] : "",
            'ASSIGN' => $data['assign'],
            'START_DATE' => $data['start_date'],
            'END_DATE' => $data['end_date'],
            'STATUS' => $data['status'],
            'UPDATE_BY' => $data['update_by']
        );
        if (!empty($dataUpdateWorkingDetail)) $dataPost['WORKING_DETAILs'] = array_values($dataUpdateWorkingDetail);
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "updateWorking resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getWorking($data)
    {
        $url = '/api/ob/web/shop/getWorkingByRowId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function approveWorking($data)
    {
        $url = '/api/ob/web/shop/approveWorking';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'STATUS' => $data['status'],
            'UPDATE_BY' => $data['update_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "approveWorking resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getListWorkingDetailByWorkingId($data = NULL)
    {
        $url = '/api/ob/web/shop/getListWorkingDetailByWorkingId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'WORKING_ID' => $data['working_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    # City, District
    function getListProvince()
    {
        $url = '/api/ob/web/manager/getProvinceList';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getDistrict($province)
    {
        $url = '/api/ob/web/manager/getDistrictListByProvinceCode';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'PROVINCE_CODE' => $province
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getWard($district)
    {
        $url = '/api/ob/web/manager/getWardListByDistrictCode';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'DISTRICT_CODE' => $district
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getListFunction()
    {
        $url = '/api/ob/web/manager/getListFuntion';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    # Role
    function getListRole()
    {
        $url = '/api/ob/web/manager/getListRole';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getListGroup()
    {
        $url = '/api/ob/web/user/getListGroup';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getRoleUserByUsername($data = NULL)
    {
        $url = '/api/ob/web/manager/getRoleUserByUsername';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'USERNAME' => $data['username']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getRoleGroupByUsername($data)
    {
        $url = '/api/ob/web/manager/getRoleGroupByUsername';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'USERNAME' => $data['username']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getRoleGroupByUsernamev2($data)
    {
        $url = '/api/ob/web/manager/getRoleGroupByUsernamev2';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'USERNAME' => $data['username'],
            'CREATE_BY' => $data['username']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    # Account
    function getListUserAccount()
    {
        $url = '/api/ob/web/user/getListUserAccount';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function getUserAccountByUsername($data)
    {
        $url = '/api/ob/web/user/getUserAccountByUsername';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'USERNAME' => $data['username']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    #Report Stores
    function reportStores()
    {
        $url = '/api/ob/web/report/shop';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    #Tạo phiếu yêu cầu NVL
    function requestTTBAndCSVC($data)
    {
        $url = '/api/ob/web/manager/requestTTBAndCSVC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "SHOP_ID" => $data['shop_id'],
            "FORMULA_ID" => $data['formula_id'],
            "CREATE_BY" => $data['create_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    # Lấy lắp ráp trang thiết bị
    function getListTTBXayDungByShopId($data)
    {
        $url = '/api/ob/web/manager/getListTTBXayDungByShopId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "SHOP_ID" => $data['shop_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    # Lấy đơn hàng TTB và nguyên vật liệu
    function getListTTBAndCSVCByShopId($data)
    {
        $url = '/api/ob/web/manager/getListTTBAndCSVCByShopId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "SHOP_ID" => $data['shop_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    function updateShopFormula($data)
    {
        $url = '/api/ob/web/manager/updateShopFormula';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'UPDATE_BY' => $data['update_by']
        );
        if (!empty($data['qty'])) $dataPost['QTY'] = $data['qty'];
        if (!empty($data['setupdate'])) $dataPost['SETUP_DAY'] = $data['setupdate'];
        if (!empty($data['note'])) $dataPost['NOTE'] = $data['note'];

        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "updateShopFormula resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    #Update training
    function updateTrainingForShop($data)
    {
        $url = '/api/ob/web/manager/updateTrainingForShop';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'TRAINERS' => $data['trainer'],
            'TRAINING_DAY' => $data['trainingday'],
            'QTY_JOIN' => $data['qty_join'],
            'NOTE' => $data['note_training'],
            'UPDATE_BY' => $data['update_by']
        );

        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        //log_message("debug", "updateTrainingForShop resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    #Check status working
    function checkSatatusWorking($data)
    {
        $url = '/api/ob/web/shop/checkWorkingApprovedByShopIdAndPos';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'SHOP_ID' => $data['shop_id'],
            'POS' => $data['position'],
            'PARENT_ID' => $data['parent_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    #Danh sách class traning theo shop
    function getListClassTraning($data)
    {
        $url = '/api/ob/web/manager/getListTrainingForShop';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "SHOP_ID" => $data['shop_id']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    #Tạo class đào tạo
    function createClassTrainning($data)
    {
        $url = '/api/ob/web/manager/createListTrainingForShop';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "SHOP_ID" => $data['shop_id'],
            "CREATE_BY" => $data['create_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "createClassTrainning resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    #Get trạng thái
    function getListStatus()
    {
        $url = '/api/ob/web/manager/getListStatus';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        return $resultAPI;
    }

    // #Check Location
    function getCheckLocationShop($data)
    {
        // $url = '/api/ob/web/shop/getCheckLocationShop';
        $url = '/api/ob/web/shop/getCheckLocationShopWithGoogleApi';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            "ROW_ID" => $data['row_id'],
            "LNG" => $data['lng'],
            "LAT" => $data['lat'],
            "RADIUS_TO_SHOP" => 5000
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getCheckLocationShop resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    /**
     * QC
     */
    function getListStore()
    {
        $url = '/api/ob/web/qc/getListOBSShopInfo';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getListStore resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getListQC()
    {
        $url = '/api/ob/web/qc/getListShopFormulaQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getListQC resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getFormulaQC()
    {
        $url = '/api/ob/web/qc/getFormulaQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getFormulaQC resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getFormulaQCNotNullItemModel()
    {
        $url = '/api/ob/web/qc/getFormulaQCNotNullItem';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getFormulaQCNotNullItemModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function insertShopFormulaQC($data)
    {
        $url = '/api/ob/web/qc/addShopFormulaQCV2';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'SHOP_ID' => $data['shop_id'],
            'FORMULA_ID' => $data['formula_id'],
            'START_TIME' => !empty($data['start_date']) ? date('Y-m-d', strtotime($data['start_date'])) : '',
            'END_TIME' => !empty($data['end_date']) ? date('Y-m-d', strtotime($data['end_date'])) : '',
            'NOTE' => $data['note'],
            'POINT' => 0,
            'REVIEW_NAME' => $data['review_name'],
            'REVIEW_DATE' => !empty($data['review_date']) ? date('Y-m-d', strtotime($data['review_date'])) : '',
            'CREATE_BY' => $data['create_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "insertShopFormulaQC resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateShopFormulaQC($data)
    {
        $url = '/api/ob/web/qc/updateShopFormulaQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'SHOP_ID' => $data['shop_id'],
            'FORMULA_ID' => $data['formula_id'],
            'START_TIME' => !empty($data['start_date']) ? date('Y-m-d', strtotime($data['start_date'])) : '',
            'END_TIME' => !empty($data['end_date']) ? date('Y-m-d', strtotime($data['end_date'])) : '',
            'NOTE' => $data['note'],
            'POINT' => 0,
            'REVIEW_NAME' => $data['review_name'],
            'REVIEW_DATE' => !empty($data['review_date']) ? date('Y-m-d', strtotime($data['review_date'])) : '',
            'UPDATE_BY' => $data['update_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "updateShopFormulaQC resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getShopFormulaQCByRowID($row_id)
    {
        $url = '/api/ob/web/qc/getShopFormulaQCByRowID';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $row_id
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getShopFormulaQCByRowID resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateShopFormulaQCDetail($data)
    {
        $url = '/api/ob/web/qc/updateShopFormulaQCDetail';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'SHOP_FORMULA_QC_ID' => $data['shop_formula_qc_id'],
            'ITEM_QC_ID' => $data['item_qc_id'],
            'POINT' => $data['input_point'],
            'NOTE' => $data['input_note'],
            'IMAGES' => $data['image'],
            'NON_SCORE' => $data['non_score'],
            'UPDATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "updateShopFormulaQCDetail resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getFormulaQCDetailByFormulaID($formula_id)
    {
        $url = '/api/ob/web/qc/getFormulaQCDetailByFormulaID';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'FORMULA_ID' => $formula_id
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getFormulaQCDetailByFormulaID resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getItemQC()
    {
        $url = '/api/ob/web/qc/getItemQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getItemQC resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getCateQC()
    {
        $url = '/api/ob/web/qc/getCateQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH"))
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getCateQC resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateFormulaQCDetailModel($data)
    {
        $url = '/api/ob/web/qc/updateFormulaQCDetail';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'FORMULA_ID' => $data['formula_id'],
            'ITEM_QC_ID' => $data['item_qc_id'],
            'POINT' => $data['point'],
            'UPDATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "updateFormulaQCDetail resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function deleteFormulaQCDetailModel($data)
    {
        $url = '/api/ob/web/qc/deleteFormulaQCDetail';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'UPDATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "deleteFormulaQCDetailModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateFormulaQCModel($data)
    {
        $url = '/api/ob/web/qc/updateFormulaQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'NAME' => $data['name'],
            'VERSION_ID' => $data['version_id'],
            'VERSION_NAME' => $data['version_name'],
            'START_DATE' => !empty($data['start_date']) ? date('Y-m-d', strtotime($data['start_date'])) : '',
            'END_DATE' => !empty($data['end_date']) ? date('Y-m-d', strtotime($data['end_date'])) : '',
            'DESCRIPTION' => $data['description'],
            'UPDATE_BY' => $this->session->userdata('userId')
        );
        // log_message("debug", "updateFormulaQCModel dataPost: " . print_r($dataPost, true));
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "updateFormulaQCModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function addFormulaQCModel($data)
    {
        $url = '/api/ob/web/qc/addFormulaQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'NAME' => $data['name'],
            'START_DATE' => !empty($data['start_date']) ? date('Y-m-d', strtotime($data['start_date'])) : '',
            'END_DATE' => !empty($data['end_date']) ? date('Y-m-d', strtotime($data['end_date'])) : '',
            'DESCRIPTION' => $data['description'],
            'CREATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "addFormulaQCModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function getFormulaQCByRowIdModel($id)
    {
        $url = '/api/ob/web/qc/getFormulaQCByRowId';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $id
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "getFormulaQCByRowId resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function addCateQCModel($data)
    {
        $url = '/api/ob/web/qc/addCateQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'CATE_CODE' => $data['code'],
            'CATE_NAME' => $data['name'],
            'TOTAL_POINT' => $data['point'],
            'CREATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "addCateQCModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function addItemQCModel($data)
    {
        $url = '/api/ob/web/qc/addItemQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'CATE_CODE' => $data['cate_code_modal'],
            'ITEM_NAME' => $data['item_name'],
            'DESCRIPTION' => $data['description'],
            'POINT' => $data['point'],
            'CREATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "addItemQCModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function updateItemQCModel($data)
    {
        $url = '/api/ob/web/qc/updateItemQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $data['row_id'],
            'CATE_CODE' => $data['cate_code_modal'],
            'ITEM_NAME' => $data['item_name'],
            'DESCRIPTION' => $data['description'],
            'POINT' => $data['point'],
            'UPDATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "updateItemQCModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function addFormulaQCDetailModel($data)
    {
        $url = '/api/ob/web/qc/addFormulaQCDetail';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'FORMULA_ID' => $data['formula_id'],
            'ITEM_QC_ID' => $data['item_qc_id'],
            'POINT' => $data['point'],
            'CREATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "addFormulaQCDetailModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function deleteItemQCModel($row_id)
    {
        $url = '/api/ob/web/qc/deleteItemQC';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $row_id,
            'UPDATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "deleteItemQCModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    function copyTemplateModel($row_id)
    {
        $url = '/api/ob/web/qc/addFormulaQCWithTemplate';
        $dataPost = array(
            'TOKEN' => md5("Nutifood" . date("YmdH")),
            'ROW_ID' => $row_id,
            'CREATE_BY' => $this->session->userdata('userId')
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL, FALSE);
        // log_message("debug", "copyTemplateModel resultAPI: " . print_r($resultAPI, true));
        return $resultAPI;
    }

    /**
     * API @A. Cường
     */
    /**
     * general
     */
    function getListTTPPAM()
    {
        $url = '/api/utilities/general';
        $dataPost = array();
        return executeApi(HTTP_GET, $url, $dataPost);
    }

    /**
     * [Stores]
     */
    public function getShopList($code)
    {
        $url = '/api/shoplists';
        $dataPost = array(
            'shop_code' => (!empty($code)) ? $code : $this->session->userdata('shopCode')
        );

        $shopList = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'));
        return $shopList;
    }

    function getStoreList($data)
    {
        $url = '/api/shops';
        $dataPost = array(
            'shop_code' => $this->session->userdata('shopCode'),
            'ProvinceCode' => $data['ProvinceCode'],
            'DistrictCode' => $data['DistrictCode'],
            'ShopType' => $data['ShopType'],
            'ShopModel' => $data['ShopModel']
        );
        if (isset($data['Status']) && null != $data['Status']) {
            $dataPost['Status'] = $data['Status'];
        }
        //log_message("debug", "aaaa resultAPI: " . print_r($dataPost, true));
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'));

        return $resultAPI;
    }

    function storesModel($data)
    {
        $url = '/api/categories/shop/create';
        $dataPost = array(
            "shop_id" => !empty($data['shop_id']) ? $data['shop_id'] : 0,
            "shop_code" => $data['shop_code'],
            "shop_name" => $data['shop_name'],
            "address" => $data['address'],
            "province_code" => $data['province_code'],
            "district_code" => $data['district_code'],
            "ward_code" => $data['ward_code'],
            "shop_tel" => $data['shop_tel'],
            "owner_tel" => $data['owner_tel'],
            "shop_type" => $data['shop_type'],
            "shop_logo" => "",
            "ob" => "1",
            "parent_id" => !empty($data['shop_id']) ? $data['parent_id'] : $data['shop_code'],
            "status" => 1,
            "sync" => !empty($data['shop_id']) ? $data['sync'] : 0,
            "lng" => $data['lng'],
            "lat" => $data['lat'],
            "create_by" => !empty($data['shop_id']) ? $data['create_by'] : $this->session->userdata('userId'),
            "modify_by" => $this->session->userdata('userId'),
            "show_sponsor" => 1,
            "start_date" => date('Ymd', strtotime($data['start_date'])),
            "end_date" => (!empty($data['end_date'])) ? date('Ymd', strtotime($data['end_date'])) : null,
            "dev" => 0,
            "mm_id" => $data['mm_id'],
            "sale_online" => ($data['sale_online']) ? 1 : 0,
            "time_open" => null,
            "pos_type" => ($data['pos_type']) ? 1 : 0,
            "ttpp" => $data['ttpp'],
            "am" => $data['am'],
            "npp" => $data['npp'],
            "note" => $data['note'] ?? null,
            "orgCode" => $data['orgCode'] ?? null,
        );
        $method = !empty($data['shop_id']) ? 'PUT' : 'POST';
        if ($method == 'POST') {
            $resultAPI = executeApi(HTTP_POST, '/api/categories/shop/create', $dataPost);
        } else {
            $resultAPI = executeApi(HTTP_PUT, '/api/categories/shop/update', $dataPost);
        }

        // log_message("debug", "storesModel: ".$method."  resultAPI: " . print_r($dataPost, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function getStore($shop_id)
    {
        $url = '/api/shops';
        $dataPost = array(
            'shop_id' => $shop_id
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'));
        // log_message("debug", "getStore resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI[0] : [];
    }

    /**
     * [Order]
     */
    public function getOrderList($data)
    {
        $url = '/api/orders';
        $dataPost = array(
            'shop_code' => (!empty($data['shop_code'])) ? $data['shop_code'] : $this->session->userdata('shopCode'),
            'fromDate' => $data['fromDate'],
            'toDate' => $data['toDate'],
            'status' => $data['status'],
            'model' => $data['model'],
            'type' => $data['type']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'));
        //log_message("debug", "getOrderList resultAPI: " . print_r($dataPost, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    public function updateOrderDetailModel($data)
    {
        $url = '/api/orderdetails';
        $dataPost = array(
            'idl_id' => $data['idl_id'],
            'qty_approve' => $data['qty_approve'],
            'modify_by' => $this->session->userdata("userId"),
            'isApprove' => $data['isApprove']
        );
        //log_message("debug", "insertOrderDetail: " . print_r($dataPost, true));
        $resultAPI = $this->data_lib->callAPI($url, 'PUT', $dataPost, $this->session->userdata('token'));

        //log_message("debug", "updateOrderDetail resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    public function insertOrderDetail($data)
    {
        $url = '/api/orderdetails';
        $dataPost = array(
            'idl_id' => $data['idl_id'],
            'inv_id' => $data['inv_id'],
            'mtl_code' => $data['mtl_code'],
            'mtl_name' => $data['mtl_name'],
            'unit_code' => $data['unit_code'],
            'price' => $data['price'],
            'qty_order' => $data['qty_order'],
            'qty_approve' => $data['qty_approve'],
            'create_by' => $this->session->userdata("userId")
        );

        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, $this->session->userdata('token'));
        //log_message("debug", "insertOrderDetail resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    public function approveOrder($data)
    {
        $url = '/api/orders';
        $dataPost = array(
            'id' => $data['id'],
            'status' => $data['status'],
            'approve_by' => $this->session->userdata("userId")
        );

        $resultAPI = $this->data_lib->callAPI($url, 'PUT', $dataPost, $this->session->userdata('token'));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    public function insertOrderModel($data)
    {
        $url = '/api/orders';
        $dataPost = array(
            'id' => $data['id'],
            'shop_code' => $data['shop_code'],
            'inv_date' => date('Ymd', strtotime($data['date'])),
            'inv_code' => $data['order_code'],
            'inv_type' => 'BK',
            'status' => $data['status'],
            'note' => $data['note'],
            'create_by' => $data['create_by']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, $this->session->userdata('token'));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function getOrderDetailModel($inv_id)
    {
        $url = '/api/orderdetails';
        $dataPost = array(
            'inv_id' => $inv_id
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function getOrderDeliveryModel($data)
    {
        $url = '/api/deliverys';
        $dataPost = array(
            'shop_code' => (!empty($data['shop_code'])) ? $data['shop_code'] : $this->session->userdata('shopCode'),
            'fromDate' => $data['fromDate'],
            'toDate' => $data['toDate'],
            'status' => $data['status'],
            'model' => $data['model']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function getDeliveryDetailsModel($inv_id)
    {
        $url = '/api/deliveryDetails';
        $dataPost = array(
            'inv_id' => $inv_id
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function updateDeliveryDetailModel($data)
    {
        $url = '/api/deliveryDetails';
        $dataPost = array(
            'idl_id' => $data['idl_id'],
            'qty_delivery' => $data['qty_delivery'],
            'modify_by' => $this->session->userdata("userId")
        );
        $resultAPI = $this->data_lib->callAPI($url, 'PUT', $dataPost, $this->session->userdata('token'));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    /**
     * Report
     */
    function rptTransDetails($data)
    {
        $url = '/api/reports/rptTransDetails';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        // log_message("debug", "rptTransDetails resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function rptHours($data)
    {
        $url = '/api/rptHours';
        $dataPost = array(
            'shop_code' => $data['shop_code'],
            'fromDate' => $data['fromDate'],
            'toDate' => $data['toDate'],
            'ProvinceCode' => $data['ProvinceCode'],
            'DistrictCode' => $data['DistrictCode'],
            'ShopType' => $data['ShopType'],
            'ShopModel' => $data['ShopModel']
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'), TRUE);
        // log_message("debug", "rptHours resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function rptDaily($data)
    {
        $url = '/api/reports/rptDaily';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        //log_message("debug", "rptIncomes resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }


    function rptIncomes($data)
    {
        $url = '/api/rptIncomes';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        //log_message("debug", "rptIncomes resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function rptItems($data)
    {
        $url = '/api/reports/rptItems';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        // log_message("debug", "rptItems resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    //nghiahv
    function rptItemByPayments($data)
    {
        $url = '/api/reports/rptItemByPayment';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        //log_message("debug", "rptItemByPayments resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function active_order($data)
    {
        $url = '/api/orders/activeOrder';
        $resultAPI = $this->data_lib->callAPI($url, 'PUT', $data, $this->session->userdata('token'), TRUE);
        // log_message("debug", "rptIncomes resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    /**
     * User
     */
    function getUser($data)
    {
        $url = '/api/users/list';
        $dataPost = array(
            'limit' => isset($data['limit']) ? $data['limit'] : 'All',
            'page' => isset($data['page']) ? $data['page'] : 0,
            'dir' => isset($data['dir']) ? $data['dir'] : 'asc',
            'orderby' => isset($data['col_name']) ? $data['col_name'] : 'USER_ID',
            'value' => isset($data['value']) ? $data['value'] : '',
        );
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $dataPost, $this->session->userdata('token'), TRUE);
        // log_message("debug", "getUser resultAPI: " . print_r($dataPost, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    //nghiahv
    function rptIncomeInfo($data)
    {
        $url = '/api/reports/rptIncomeInfo';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        return $resultAPI;
    }

    /**
     * [getUser login to OBS]
     * @param  [string] $userName [user_id]
     * @param  [string] $passWord [user_pass]
     * @return [type]           [description]
     */
    function getLogin($userName, $passWord)
    {
        $url = '/api/logins';
        $dataPost = array(
            'user_id' => $userName,
            'user_pass' => $passWord
        );
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $dataPost, NULL);
        return $resultAPI;
    }

    function getPromotions()
    {
        $url = '/api/business/promotion/list';
        $resultAPI = $this->data_lib->execute($url, 'GET');
        return $resultAPI;
    }

    function approvePromotion($data)
    {
        $url = '/api/promotionshop/approve';
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $data, $this->session->userdata('token'), TRUE);
        return $resultAPI;
    }

    function createPromotionShop($data)
    {
        $url = '/api/promotionshop/create';
        $resultAPI = $this->data_lib->callAPI($url, 'POST', $data, $this->session->userdata('token'), TRUE);
        return $resultAPI;
    }

    function getShops($data)
    {
        $url = '/api/shops/list';
        $resultAPI = $this->data_lib->execute($url, 'GET', $data);
        usort($resultAPI['data'], function ($a, $b) {
            $coll = collator_create('utf8mb4_vietnamese_ci');
            return collator_compare($coll, strtolower($a['SHOP_NAME']), strtolower($b['SHOP_NAME']));
        });
        return $resultAPI;
    }

    function getProvinces($data)
    {
        $url = '/api/users/provinceList';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        // $coll = collator_create('utf8mb4_vietnamese_ci');
        // collator_asort($coll, $resultAPI['provinces'], Collator::SORT_STRING );
        return $resultAPI;
    }

    function getDistricts($data)
    {
        $url = '/api/users/districtList';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);

        // usort($resultAPI, function($a,$b){
        //     $coll = collator_create('utf8mb4_vietnamese_ci');
        //     return collator_compare( $coll,strtolower($a['DISTRICT']), strtolower($b['DISTRICT']));
        //     //return strtolower($a['DISTRICT']) > strtolower($b['DISTRICT']);
        // });
        return $resultAPI;
    }

    function getWards($data)
    {
        $url = '/api/users/wardList';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        return $resultAPI;
    }

    function rptItemByPaymentExport($data)
    {
        $url = '/api/exports/rptItemByPayment';
        $response = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        return $response;
    }

    function rptTransDetailsExport($data)
    {
        $url = '/api/exports/rptTransDetails';
        $response = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        return $response;
    }

    function rptItemsExport($data)
    {
        $url = '/api/exports/rptItems';
        $response = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        return $response;
    }


    function rptInventory($data)
    {
        $url = '/api/reports/rptInventory';
        $response = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        return $response;
    }

    function rptInventoryExport($data)
    {
        $url = '/api/exports/rptInventory';
        $response = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        return $response;
    }

    // PROMOTION BY VIP CARD
    function rptPromotionByVipCards($data)
    {
        $url = '/api/reports/rptVipCard';
        $resultAPI = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        // log_message("debug", "rptItems resultAPI: " . print_r($resultAPI, true));
        return (isset($resultAPI)) ? $resultAPI : [];
    }

    function rptPromotionByVipCardExport($data)
    {
        $url = '/api/exports/rptVipCard';
        $response = $this->data_lib->callAPI($url, 'GET', $data, $this->session->userdata('token'), TRUE);
        return $response;
    }
}

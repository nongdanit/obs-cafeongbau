<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Model{
	public function __construct() {
        parent::__construct();
    }

    /**
     * OBS 
     */
    function authenticate($userId, $password){
    	$url = '/api/users/authenticate';
    	$data = array(
    		'userId' => $userId,
    		'password' => $password
    	);
    	$result = $this->execute($url, 'POST', $data);
    	return $result;
    }

    function execute($url = null, $method = null, $data = array(), $HO_APP = true){
        if($method == null){
            return array('sucess' => false, 'message' => 'method require');
        }
        $BASEURL    = ($HO_APP) ? URL_API : URL_API2;
        $TOKEN      = ($HO_APP) ? 'OB ' . $this->session->userdata('token') : md5("Nutifood" . date("YmdH"));
        $client = new \GuzzleHttp\Client([
            'base_uri'  => $BASEURL,
            'headers'   => [
                'Content-Type' => 'application/json', 
                'Accept' => 'application/json', 
                'charset' => 'utf-8',
            ],
            'defaults'  => [
                'exceptions' => false,
            ],
            'http_errors' => false,//Disable exceptions for Guzzle
        ]);       
        $options = [
            'headers'   => ['Authorization' => $TOKEN],
        ];

        try{
            if($method === 'POST'){
                $options['json'] = $data;
                $response = $client->post($url, $options);
            }elseif($method === 'GET'){
                $options['query'] = $data;
                $response = $client->get($url, $options);
            }elseif($method === 'PUT'){
                $options['json'] = $data;
                $response = $client->put($url, $options);
            }
            // log_message("debug", "getOrderList resultAPI: " . print_r($options, true));
            $statusCode = $response->getStatusCode();
            if($statusCode === 200){
                return json_decode($response->getBody()->getContents(), true);
            }{
                return array('success' => false, 'message' =>  $response->getStatusCode());
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            log_message("error", $BASEURL . $url . ' ' . print_r($message, true));
            return array('success' => false, 'message' => $message);
        }
          
    }
}
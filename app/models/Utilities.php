<?php

class Utilities extends CI_Model
{
    const ROOT = '/api/utilities/';

    function helloWorld()
    {
        echo 'test';
    }

    function getProvinces()
    {
        $url = self::ROOT . '';
    }

    function getErpPrice(): array
    {
        $url = self::ROOT . 'erpPrice';
        $data = [];
        return executeApi(HTTP_GET, $url, $data);
    }

    function getErpOrg(): array
    {
        $url = self::ROOT . 'erpOrg';
        $data = [];
        return executeApi(HTTP_GET, $url, $data);
    }
}
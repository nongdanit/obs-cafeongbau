<?php


class User_model extends CI_Model
{
    /**
     * @param $userId
     * @param $password
     * @return array
     */
    function authenticate($userId, $password): array
    {
        $data = [
            'userId' => $userId,
            'password' => $password
        ];
        $url = '/api/users/authenticate';
        return executeApi(HTTP_POST, $url, $data);
    }
}
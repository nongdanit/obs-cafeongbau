<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7;
defined('BASEPATH') or exit('No direct script access allowed');
if (!function_exists('slug')) {

    function slug($string, $replacement = ' ', $slug_replace = null)
    {
        $transliteration = array(
            '/À|Á|Â|Ẩ|Ậ|Ấ|Ầ|Ắ|Ằ|Ẳ|Ẵ|Ặ|Ã|Å|Ǻ|Ā|Ă|Ą|Ả|Ạ|Ã|Ǎ/' => 'A',
            '/Æ|Ǽ/' => 'AE',
            '/Ä/' => 'Ae',
            '/Ç|Ć|Ĉ|Ċ|Č/' => 'C',
            '/Ð|Ď|Đ/' => 'D',
            '/È|É|Ẹ|Ẽ|Ẻ|Ê|Ế|Ề|Ệ|Ë|Ē|Ĕ|Ė|Ể|Ễ|Ę|Ě/' => 'E',
            '/Ĝ|Ğ|Ġ|Ģ|Ґ/' => 'G',
            '/Ĥ|Ħ/' => 'H',
            '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Ĩ|Ị|Ỉ|Į|İ|І/' => 'I',
            '/Ĳ/' => 'IJ',
            '/Ĵ/' => 'J',
            '/Ķ/' => 'K',
            '/Ĺ|Ļ|Ľ|Ŀ|Ł/' => 'L',
            '/Ñ|Ń|Ņ|Ň/' => 'N',
            '/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ộ|Ồ|Ó|Ổ|Ỗ|Õ|Ồ|Ō|Ŏ|Ǒ|Ő|Ợ|Ờ|Ớ|Ở|Ỡ|Ơ|Ờ|Ố|Ồ|Ø|Ǿ/' => 'O',
            '/Œ/' => 'OE',
            '/Ö/' => 'Oe',
            '/Ŕ|Ŗ|Ř/' => 'R',
            '/Ś|Ŝ|Ş|Ș|Š/' => 'S',
            '/ẞ/' => 'SS',
            '/Ţ|Ț|Ť|Ŧ/' => 'T',
            '/Þ/' => 'TH',
            '/Ù|Ú|Û|Ũ|Ủ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ụ|Ử|Ữ|Ự|Ừ|Ứ|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ/' => 'U',
            '/Ü/' => 'Ue',
            '/Ŵ/' => 'W',
            '/Ý|Ỳ|Ỷ|Ỹ|Ÿ|Ŷ/' => 'Y',
            '/Є/' => 'Ye',
            '/Ї/' => 'Yi',
            '/Ź|Ż|Ž/' => 'Z',
            '/à|á|ạ|ả|ã|â|ã|å|ǻ|ā|ă|ą|ầ|ấ|ậ|ẩ|ẫ|ằ|ắ|ẳ|ẵ|ặ|ǎ|ª/' => 'a',
            '/ä|æ|ǽ/' => 'ae',
            '/ç|ć|ĉ|ċ|č/' => 'c',
            '/ð|ď|đ/' => 'd',
            '/è|é|ê|ë|ē|ĕ|ė|ę|ễ|ể|ế|ề|ệ|ẹ|ě/' => 'e',
            '/ƒ/' => 'f',
            '/ĝ|ğ|ġ|ģ|ґ/' => 'g',
            '/ĥ|ħ/' => 'h',
            '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı|ị|ỉ|ĩ|і/' => 'i',
            '/ĳ/' => 'ij',
            '/ĵ/' => 'j',
            '/ķ/' => 'k',
            '/ĺ|ļ|ľ|ŀ|ł/' => 'l',
            '/ñ|ń|ņ|ň|ŉ/' => 'n',
            '/ò|ọ|ỏ|õ|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ở|ỡ|ố|ồ|ổ|ỗ|ộ|ợ|ớ|ờ|ø|ǿ|º/' => 'o',
            '/ö|œ/' => 'oe',
            '/ŕ|ŗ|ř/' => 'r',
            '/ś|ŝ|ş|ș|š|ſ/' => 's',
            '/ß/' => 'ss',
            '/ţ|ț|ť|ŧ/' => 't',
            '/þ/' => 'th',
            '/ù|ú|ủ|ũ|ụ|û|ũ|ū|ŭ|ů|ű|ų|ư|ữ|ử|ự|ừ|ứ|ǔ|ǖ|ǘ|ǚ|ǜ/' => 'u',
            '/ü/' => 'ue',
            '/ŵ/' => 'w',
            '/ý|ỳ|ÿ|ŷ/' => 'y',
            '/є/' => 'ye',
            '/ї/' => 'yi',
            '/ź|ż|ž/' => 'z',
        );
        $quotedReplacement = preg_quote($replacement, '/');
        $merge = array(
            '/[^\s\p{Zs}\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
            '/[\s\p{Zs}]+/mu' => $replacement,
            sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',
        );
        $map = $transliteration + $merge;
        if (!empty($slug_replace)) {
            $str = preg_replace(array_keys($map), array_values($map), $string);
            $str = strtolower($str);
            return str_replace(" ", "-", $str);
        }
        return preg_replace(array_keys($map), array_values($map), $string);
    }

}

if (!function_exists('array_to_csv')) {
    function array_to_csv($array, $download = "", $forMSExcel = false)
    {
        if ($download != "") {
            header('Content-Type: application/csv');
            header('Content-Disposition: attachement; filename="' . $download . '"');
            header("Content-Transfer-Encoding: UTF-8");
        }

        ob_start();
        $f = fopen('php://output', 'a') or show_error("Can't open php://output");
        $n = 0;
        fprintf($f, chr(0xEF) . chr(0xBB) . chr(0xBF));
        foreach ($array as $line) {
            $n++;
            if ($forMSExcel) {
                $line = mb_convert_encoding($line, 'SJIS', 'auto');
                if (!fputcsv($f, $line)) {
                    show_error("Can't write line $n: $line");
                }
            } else {
                if (!fputcsv($f, $line)) {
                    show_error("Can't write line $n: $line");
                }
            }
        }

        fclose($f) or show_error("Can't close php://output");
        $str = ob_get_contents();
        ob_end_clean();

        if ($download == "") {
            return $str;
        } else {
            echo $str;
        }
    }
}

if (!function_exists('_json')) {
    function _json($data)
    {
        echo json_encode($data);
        exit();
    }
}

if (!function_exists('_remove_array_elements')) {
    function _remove_array_elements($array = array(), $elements = array())
    {
        if (count($array) === 0) {
            return $array;
        }
        foreach ($elements as $key => $value) {
            unset($array[$value]);
        }
        return $array;
    }
}

if (!function_exists('array_find')) {
    function array_find($needle, array $haystack)
    {
        foreach ($haystack as $key => $value) {
            if (false !== stripos($value, $needle)) {
                return $key;
            }
        }
        return false;
    }
}

if (!function_exists('check_value_in_array')) {
    function check_value_in_array($val1, $val2, $key1, $key2, array $data)
    {
        if (empty($data)) return false;
        if (array_search($val1, array_column($data, $key1)) !== false) {
            $arrColumn = array_column($data, $key2, 'FUNCTION_CODE');
            return (stripos($arrColumn[$val1], $val2) !== false ? true : false);
        } else {
            return false;
        }
    }
}

if (!function_exists('check_date')) {
    function check_date($date)
    {
        if ($date === '0001-01-01T00:00:00' || is_null($date)) {
            return '';
        }
        return date('d-m-Y', strtotime($date));
    }
}

if (!function_exists('genhash')) {
    function genhash($strlen)
    {
        $h_len = $strlen;
        $cstrong = TRUE;
        $sslkey = openssl_random_pseudo_bytes($h_len, $cstrong);
        return bin2hex($sslkey);
    }
}

if (!function_exists('shopFilter')) {
    function shopFilter($shopCode = 'All', $shopModel = 'All', $shopType = 'All', $provinceCode = 'All', $districtCode = 'All', $groupCode = 'All')
    {
        $ci = &get_instance();
        $shops = $ci->session->userdata('shops');
        $shopList = array_filter($shops, function ($shop) use ($provinceCode, $districtCode, $shopType, $shopModel, $shopCode, $groupCode) {
            return (
                ($shop['provinceCode'] == $provinceCode || 'All' == $provinceCode)
                && ($shop['districtCode'] == $districtCode || 'All' == $districtCode)
                && ($shop['shopType'] == $shopType || 'All' == $shopType)
                && ($shop['shopModel'] == $shopModel || 'All' == $shopModel)
                && ($shop['shopCode'] == $shopCode || $shop['parentCode'] == $shopCode || 'All' == $shopCode || 'HEAD_OFFICE' == $shopCode)
                && ($shop['parentCode'] == $groupCode || 'All' == $groupCode)
            );
        });
        return $shopList;
    }
}

if (!function_exists('checkPermission')) {
    function check_permission($permission)
    {
        $ci = &get_instance();
//        $ci->load->library('session');
        $permissions = $ci->session->userdata('permissions');
        return in_array(strtolower($permission), $permissions);
    }
}

/**
 * @param null $method
 * @param null $url
 * @param array $data
 * @param bool $OBS
 * @return array
 */
function executeApi($method = null, $url = null, $data = [], $OBS = true): array
{
    $result = array(
        'success' => false,
        'code' => 500,
        'message' => '',
        'data' => [],
    );
    try {
        $ci = &get_instance();
        $BASEURL = $OBS ? URL_API : URL_API2;
        $TOKEN = ($OBS) ? 'OB ' . $ci->session->userdata('token') : md5("Nutifood" . date("YmdH"));
        $client = new Client([
            'base_uri' => $BASEURL,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'charset' => 'utf-8',
                'Authorization' => $TOKEN
            ],
            'defaults' => [
                'exceptions' => false,
            ],
            'http_errors' => false,//Disable exceptions for Guzzle
        ]);
        $options = [];
        if ($OBS) {
            $options = ['json' => $data];
        } else {
            switch ($method) {
                case HTTP_GET:
                    $options = ['query' => $data];
                    break;
//            case HTTP_DELETE:
//                $options = ['query' => $data];
//                break;
                case HTTP_PUT:
                case HTTP_POST:
                    $options = ['json' => $data];
                    break;
            }
        }

        $response = $client->request($method, $url, $options);
        $httpCode = $response->getStatusCode();
        $response = json_decode($response->getBody(), true);
        $result['code'] = $httpCode;
        if ($OBS) {
            $result['success'] = $response['success'];
            $result['message'] = $response['message'] ?? '';
            $result['data'] = $response['data'] ?? [];
        } else {
            return $response;
//            $result['data'] = $response ?? [];
        }

    } catch (Exception $e) {
        $result['message'] = $e->getMessage();
        log_message('debug', print_r($e->getMessage(), true));
    }
    return $result;
}

function sendFile($filePath, $url, $OBS = true): array
{
    $result = array(
        'success' => false,
        'code' => 500,
        'message' => '',
        'data' => [],
    );
    try {
        $ci = &get_instance();
        $BASEURL = $OBS ? URL_API : URL_API2;
        $TOKEN = ($OBS) ? 'OB ' . $ci->session->userdata('token') : md5("Nutifood" . date("YmdH"));
        $client = new Client([
            'base_uri' => $BASEURL,
            'headers' => [
                'charset' => 'utf-8',
                'Authorization' => $TOKEN
            ],
            'defaults' => [
                'exceptions' => false,
            ],
            'http_errors' => false,//Disable exceptions for Guzzle
        ]);
        $options = [
            'multipart' => [
                [
                    'name' => 'uploadFile',
                    'contents' => Psr7\Utils::tryFopen($filePath, 'r')
                ],
            ]
        ];

        $response = $client->request(HTTP_POST, $url, $options);
//        return $response;
        $httpCode = $response->getStatusCode();
        $response = json_decode($response->getBody(), true);
        $result['code'] = $httpCode;
        if ($OBS) {
            $result['success'] = $response['success'];
            $result['message'] = $response['message'] ?? '';
            $result['data'] = $response['data'] ?? [];
        } else {
            return $response;
//            $result['data'] = $response ?? [];
        }

    } catch (Exception $e) {
        $result['message'] = $e->getMessage();
        log_message('debug', print_r($e->getMessage(), true));
    }
    return $result;
}

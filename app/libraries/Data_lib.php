<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Date : 02-09-2019
 * author : Quoc Su
 * Libraries
 */

class Data_lib
{

    public function __construct(){
        
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    public function clear_tags($str) {
        return htmlentities(strip_tags($str, '<span><div><a><br><p><b><i><u><img><blockquote><small><ul><ol><li><hr><big><pre><code><strong><em><table><tr><td><th><tbody><thead><tfoot><h3><h4><h5><h6>'), ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
    }

    public function decode_html($str) {
        return html_entity_decode($str, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
    }

    public function formatMoney($number, $decimal = false) {
        if ($this->Settings->sac) {
            return ($this->Settings->display_symbol == 1 ? $this->Settings->symbol : '') .
            $this->formatSAC($this->formatDecimal($number)) .
            ($this->Settings->display_symbol == 2 ? $this->Settings->symbol : '');
        }
        $decimals = $decimal !== false ? $decimal : $this->Settings->decimals;
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return ($this->Settings->display_symbol == 1 ? $this->Settings->symbol : '') .
        number_format($number, $decimals, $ds, $ts) .
        ($this->Settings->display_symbol == 2 ? $this->Settings->symbol : '');
    }

    public function formatQuantity($number, $decimals = null) {
        if (!$decimals) {
            $decimals = $this->Settings->qty_decimals;
        }
        if ($this->Settings->sac) {
            return $this->formatSAC($this->formatDecimal($number, $decimals));
        }
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return number_format($number, $decimals, $ds, $ts);
    }

    public function formatNumber($number, $decimals = null) {
        if(empty($number)) return '';
        if (!$decimals) {
            $decimals = $this->Settings->decimals;
        }
        if ($this->Settings->sac) {
            return $this->formatSAC($this->formatDecimal($number, $decimals));
        }
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return number_format($number, $decimals, $ds, $ts);
    }

    public function formatDecimal($number, $decimals = null) {
        if (!is_numeric($number)) {
            return null;
        }
        if (!$decimals) {
            $decimals = $this->Settings->decimals;
        }
        return number_format($number, $decimals, '.', '');
    }

    public function roundNumber($number, $toref = NULL) {
        switch($toref) {
            case 1:
                $rn = round($number * 20)/20;
                break;
            case 2:
                $rn = round($number * 2)/2;
                break;
            case 3:
                $rn = round($number);
                break;
            case 4:
                $rn = ceil($number);
                break;
            default:
                $rn = $number;
        }
        return $rn;
    }

    public function logged_in() {
        return (bool) $this->session->userdata('userId');
    }

    public function hrld($ldate) {
        if ($ldate) {
            return date($this->Settings->dateformat.' '.$this->Settings->timeformat, strtotime($ldate));
        }
        return FALSE;
    }

    public function hrsd($sdate) {
        if ($sdate) {
            return date($this->Settings->dateformat, strtotime($sdate));
        }
        return FASLE;
    }

    public function unset_data($ud) {
        if($this->session->userdata($ud)) {
            $this->session->unset_userdata($ud);
            return true;
        }
        return FALSE;
    }
    public static function inject($string){
        //[ \ ^ $ . | ? * + ( )
        $check= array("`","!","^","+","{","[","]","}","|",";"," ");
        return str_replace($check,"",$string);
    }

    public function send_email($to, $subject, $message, $from = NULL, $from_name = NULL, $attachment = NULL, $cc = NULL, $bcc = NULL) {
        list($user, $domain) = explode('@', $to);
        if ($domain != 'tecdiary.com' || DEMO) {
            $this->load->library('tec_mail');
            return $this->tec_mail->send_mail($to, $subject, $message, $from, $from_name, $attachment, $cc, $bcc);
        }
    }

    public function print_arrays() {
        $args = func_get_args();
        echo "<pre>";
        foreach($args as $arg){
            print_r($arg);
        }
        echo "</pre>";
        die();
    }


    private function _rglobRead($source, &$array = array()) {
        if (!$source || trim($source) == "") {
            $source = ".";
        }
        foreach ((array)glob($source . "/*/") as $key => $value) {
            $this->_rglobRead(str_replace("//", "/", $value), $array);
        }
        $hidden_files = glob($source . ".*") AND $htaccess = preg_grep('/\.htaccess$/', $hidden_files);
        $files = array_merge(glob($source . "*.*"), $htaccess);
        foreach ($files as $key => $value) {
            $array[] = str_replace("//", "/", $value);
        }
    }

    private function _zip($array, $part, $destination, $output_name = 'sma') {
        $zip = new ZipArchive;
        @mkdir($destination, 0777, true);

        if ($zip->open(str_replace("//", "/", "{$destination}/{$output_name}" . ($part ? '_p' . $part : '') . ".zip"), ZipArchive::CREATE)) {
            foreach ((array)$array as $key => $value) {
                $zip->addFile($value, str_replace(array("../", "./"), NULL, $value));
            }
            $zip->close();
        }
    }

    public function zip($source = NULL, $destination = "./", $output_name = 'sma', $limit = 5000) {
        if (!$destination || trim($destination) == "") {
            $destination = "./";
        }

        $this->_rglobRead($source, $input);
        $maxinput = count($input);
        $splitinto = (($maxinput / $limit) > round($maxinput / $limit, 0)) ? round($maxinput / $limit, 0) + 1 : round($maxinput / $limit, 0);

        for ($i = 0; $i < $splitinto; $i++) {
            $this->_zip(array_slice($input, ($i * $limit), $limit, true), $i, $destination, $output_name);
        }

        unset($input);
        return;
    }

    public function unzip($source, $destination = './') {

        // @chmod($destination, 0777);
        $zip = new ZipArchive;
        if ($zip->open(str_replace("//", "/", $source)) === true) {
            $zip->extractTo($destination);
            $zip->close();
        }
        // @chmod($destination,0755);

        return TRUE;
    }

    public function view_rights($check_id, $js = NULL, $page = NULL) {
        if (!$this->Admin) {
            if ($check_id != $this->session->userdata('userId')) {
                $this->session->set_flashdata('error', $this->data['access_denied']);
                if ($js) {
                    die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . ($page ? $page : (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome'))) . "'; }, 10);</script>");
                } else {
                    redirect($page ? $page : (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome'));
                }
            }
        }
        return TRUE;
    }

    public function dd() {
        die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('pos')) . "'; }, 10);</script>");
    }

    public function get_base64($file_name) {
        $type = pathinfo($file_name, PATHINFO_EXTENSION);
        $data = file_get_contents($file_name);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }

    public function barcode($text = null, $bcs = 'code128', $height = 74, $stext = 1, $get_be = false) {
        $drawText = ($stext != 1) ? false : true;
        $this->load->library('tec_barcode', '', 'bc');
        return $this->bc->generate($text, $bcs, $height, $drawText, $get_be);
    }

    public function send_json($data) {
        header('Content-Type: application/json');
        die(json_encode($data));
        exit;
    }


    public function formatSAC($num)
    {
        $pos = strpos((string) $num, ".");
        if ($pos === false) {
            $decimalpart = "00";
        } else {
            $decimalpart = substr($num, $pos + 1, 2);
            $num = substr($num, 0, $pos);
        }

        if (strlen($num) > 3 & strlen($num) <= 12) {
            $last3digits = substr($num, -3);
            $numexceptlastdigits = substr($num, 0, -3);
            $formatted = $this->makecomma($numexceptlastdigits);
            $stringtoreturn = $formatted . "," . $last3digits . "." . $decimalpart;
        } elseif (strlen($num) <= 3) {
            $stringtoreturn = $num . "." . $decimalpart;
        } elseif (strlen($num) > 12) {
            $stringtoreturn = number_format($num, 2);
        }

        if (substr($stringtoreturn, 0, 2) == "-,") {
            $stringtoreturn = "-" . substr($stringtoreturn, 2);
        }

        return $stringtoreturn;
    }


    /**
     * [callAPI call API Server]
     * @param  string $url      [url server call API] /api/user/login
     * @param  string $method   [method: GET, POST, ....]
     * @param  array  $data     [array data post]
     * @return [json]           [description]
     */
    public function callAPI($url = null, $method = 'POST', $postData = array(), $authorization = NULL, $domain = TRUE){
        $client = new \GuzzleHttp\Client([
            'base_uri'  => ($domain)?URL_API:URL_API3,
            'headers'   => ['Content-Type' => 'application/json', 'Accept' => 'application/json', 'charset' => 'utf-8'],
            'defaults'  => [
                'exceptions' => false,
            ],
            'http_errors' => false,//Disable exceptions for Guzzle
        ]);

        // log_message("debug", "url response: " . print_r($postData, true));
        // log_message("debug", "method response: " . print_r($method, true));
        

        // try {
            if($method == 'POST'){
                $options = [
                    'json' => $postData
                ];
                if (!$domain) {
                    $options['headers']['x-api-key'] = X_api_key;
                }
                if(!empty($authorization)) $options['headers']['Authorization'] = 'OB ' . $authorization;
                $response = $client->post($url, $options);
            } elseif($method == 'GET'){
                $options = [
                    'query' => $postData
                ];
                if (!$domain) {
                    $options['headers']['x-api-key'] = X_api_key;
                }
                if(!empty($authorization)) $options['headers']['Authorization'] = 'OB ' . $authorization;
                $response = $client->get($url, $options);
            } elseif($method == 'PUT'){
                $options = [
                    'json' => $postData
                ];
                if (!$domain) {
                    $options['headers']['x-api-key'] = X_api_key;
                }
                if(!empty($authorization)) $options['headers']['Authorization'] = 'OB ' . $authorization;
                $response = $client->put($url, $options);
            }elseif($method == 'PATCH'){
                $options = [
                    'json' => $postData
                ];
                if (!$domain) {
                    $options['headers']['x-api-key'] = X_api_key;
                }
                if(!empty($authorization)) $options['headers']['Authorization'] = 'OB ' . $authorization;
                $response = $client->patch($url, $options);
            } elseif($method == 'DELETE'){
                $options = [
                    'json' => $postData
                ];
                if (!$domain) {
                    $options['headers']['x-api-key'] = X_api_key;
                }
                if(!empty($authorization)) $options['headers']['Authorization'] = 'OB ' . $authorization;
                $response = $client->delete($url, $options);
            }
            // log_message("debug", "response response: " . print_r($response, true));

            if( in_array($response->getStatusCode(), [200, 201]) ){
                if($response->getHeader('content-type')[0] == 'text/html; charset=utf-8'){
                    $jsonBody = (string) $response->getBody();
                    log_message('error', "type text/html: ". $jsonBody );
                    return $jsonBody;
                }
                elseif($response->getHeader('content-type')[0] == 'application/octet-stream'
                    || $response->getHeader('content-type')[0] == 'application/vnd.openxmlformats'){
                    return $response;
                }else{
                    return json_decode($response->getBody(true), true);
                }
            }elseif($response->getStatusCode() === 409){
                // if($response->getHeader('content-type')[0] == 'text/html; charset=utf-8'){
                //     $jsonBody = (string) $response->getBody();
                //     log_message('error', "type text/html: ". $jsonBody );
                //     return $jsonBody;
                // }
                // elseif($response->getHeader('content-type')[0] == 'application/octet-stream'
                //     || $response->getHeader('content-type')[0] == 'application/vnd.openxmlformats'){
                    // return $response;
                // }else{
                    return json_decode($response->getBody(true), true);
                // }
            }

            else{
                log_message('error', "URL not found StatusCode ". $response->getStatusCode());
                return [];
            }
        // } catch (ClientException $e) {
        //     return [];
        // }
    }

    /**
     * [execute call api from OBS service]
     * @param  [type]  $url    [api url]
     * @param  [type]  $method [GET, POST, PUT, DELETE]
     * @param  array   $data   [data send server]
     * @param  boolean $HO_APP [false if call Thành api]
     * @return [type]          [json result]
     */
    public function execute($url = null, $method = null, $data = array(), $HO_APP = true){
        if($method == null){
            return array('sucess' => false, 'message' => 'method require');
        }
        // if(isset($data['shopCode'])){
        //     $this->checkShopAccess($data['shopCode']);
        // }
        $BASEURL    = ($HO_APP) ? URL_API : URL_API2;
        $TOKEN      = ($HO_APP) ? 'OB ' . $this->session->userdata('token') : md5("Nutifood" . date("YmdH"));
        $client = new \GuzzleHttp\Client([
            'base_uri'  => $BASEURL,
            'headers'   => [
                'Content-Type' => 'application/json', 
                'Accept' => 'application/json', 
                'charset' => 'utf-8',
            ],
            'defaults'  => [
                'exceptions' => false,
            ],
            'http_errors' => false,//Disable exceptions for Guzzle
        ]);       
        $options = [
            'headers'   => ['Authorization' => $TOKEN],
        ];

        try{
            if($method === 'POST'){
                $options['json'] = $data;
                $response = $client->post($url, $options);
            }elseif($method === 'GET'){
                $options['json'] = $data;
                $response = $client->get($url, $options);
            }elseif($method === 'PUT'){
                $options['json'] = $data;
                $response = $client->put($url, $options);
            }
            // log_message("debug", "getOrderList resultAPI: " . print_r($options, true));
            $statusCode = $response->getStatusCode();
            if($statusCode === 200){
                return json_decode($response->getBody()->getContents(), true);
            }{
                return array('success' => false, 'message' =>  $response->getStatusCode());
            }
        // } catch (GuzzleHttp\Exception\ConnectException $e) {
        //     $message = $e->getMessage();
        //     log_message("error", $BASEURL . $url . ' ' . print_r($message, true));
        //     return array('success' => false, 'message' => $message);
        } catch (Exception $e) {
            $message = $e->getMessage();
            log_message("error", $BASEURL . $url . ' ' . print_r($message, true));
            return array('success' => false, 'message' => $message);
        }
          
    }

    public function executeRestApi($url = null, $method = null, $data = array(), $authorization = NULL, $HO = TRUE){
        if($method == null){
            return array('sucess' => false, 'message' => 'method require');
        }
        $client = new \GuzzleHttp\Client([
            'base_uri'  => ($HO) ? URL_API : URL_API2,
            'headers'   => ['Content-Type' => 'application/json', 'Accept' => 'application/json', 'charset' => 'utf-8'],
            'defaults'  => [
                'exceptions' => false,
            ],
            'http_errors' => false,//Disable exceptions for Guzzle
        ]);       
        if($method == 'POST'){
            $options = [
                'json' => $data
            ];
            if(!empty($authorization)) $options['headers']['Authorization'] = 'OB ' . $authorization;
            $response = $client->post($url, $options);
        }elseif($method == 'GET'){
            $options = [
                'query' => $data
            ];
            if(!empty($authorization)) $options['headers']['Authorization'] = 'OB ' . $authorization;
            $response = $client->get($url, $options);
        }elseif($method == 'PUT'){
            $options = [
                'json' => $data
            ];
            if(!empty($authorization)) $options['headers']['Authorization'] = 'OB ' . $authorization;
            $response = $client->put($url, $options);
        }
        if($response->getStatusCode() === 200){
            return json_decode($response->getBody(true), true);
        }else{
            return array('success' => false, 'message' =>  $response->getStatusCode());
        }
    }

    public function shopListSelect(){
        $myShop = $this->session->userdata('shops');
        $shopList = array();
        if(in_array($this->session->userdata('shopCode'), ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])){
            $shopList[$this->session->userdata('shopCode')]  = 'Tất cả';
        }
        foreach ($myShop as $key => $value) {
            $shopList[$value['shopCode']] = $value['shopName'];
        }
        $this->session->set_userdata('shopList', $shopList);
        return $shopList;
    }

    public function getToken(){
        return $this->session->userdata('token');
    }


}
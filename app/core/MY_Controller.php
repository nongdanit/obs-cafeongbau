<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $data = array();

    function __construct()
    {
        parent::__construct();

        $this->Settings = $this->site->getSettings();
        $this->Settings->selected_language = $this->Settings->language;
        $this->config->set_item('language', $this->Settings->language);
        $this->data['Settings'] = $this->Settings;
        // $dataListStatus = $this->site->getListStatus();
        // if($dataListStatus['status'] == 'OK' && $dataListStatus['errorCode'] == 200){
        //     $this->data['listStatus'] = $dataListStatus['result'];
        // }else $this->data['listStatus'] = [];
        $this->data['m'] = strtolower($this->router->fetch_class());
        $this->data['v'] = strtolower($this->router->fetch_method());
        $this->controller = strtolower($this->router->fetch_class());
        $this->action = strtolower($this->router->fetch_method());
    }
}

/**
 *
 */
class Backend_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->loggedIn = $this->data_lib->logged_in();
        $this->data['loggedIn'] = $this->loggedIn;
        $this->lang->load('app', $this->Settings->language);
        $this->Settings->pin_code = $this->Settings->pin_code ? md5($this->Settings->pin_code) : NULL;

        $this->theme = $this->Settings->theme . '/views/';
        $this->data['assets'] = base_url() . $this->Settings->theme . '/assets/';
        $nonSecureController = ['employer'];
        $nonSecureAction = [
            'view_store', 'getlocation', 'loadcompetitors', 'profile', 'change_password', 'upload_image_qc',
            'get_qc', 'edit_qc', 'review_qc', 'create_qc', 'submit_qc', 'edit_stores', 'get_qc_template', 'edit_qc_template',
            'loadqcitem', 'get_item_qc', 'qc_template', 'loadcate', 'get_list_shop', 'submit_store', 'create_stores', 'updatestoreconfig',
            'rpttranscustomer',
        ];
//        print_r($this->session->userdata());die;
        if ($this->controller != 'auth') {
//             echo $this->action;die;
            if (!$this->loggedIn) redirect('login');
            // echo $this->action;die;
            // echo json_encode($this->session->userdata('permissions'));die;
            if ($this->action == 'index' && !check_permission($this->controller . ':read') && !in_array($this->controller, $nonSecureController)) {
                redirect('login');
            } elseif ($this->action != 'index' && !check_permission($this->action . ':read') && !in_array($this->action, $nonSecureAction)) {
                redirect( $this->session->userdata('homepage'));
            }

            if( in_array($this->controller, ['business', 'reports']) && in_array($this->action, ['employee', 'checkincheckout']) && empty($this->session->has_userdata('access_token')) ) {
                $session_data = array(
                    'access_token' => ACCESS_TOKEN
                );
                $this->session->set_userdata($session_data);

                // $dataPost = array(
                //     'user_name' => ACCOUNT_USER,
                //     'password' => ACCOUNT_PASS
                // );
                // $resultAPI = $this->data_lib->callAPI('access/signin', 'POST', $dataPost, NULL, FALSE);
                // if ( !empty($resultAPI) && $resultAPI['status'] == 200) {
                //     $dataResult = $resultAPI['metadata'];
                //     $session_data = array(
                //         'access_token' => $dataResult['tokens']['access_token'],
                //         'refresh_token' => $dataResult['tokens']['refresh_token']
                //     );
                //     $this->session->set_userdata($session_data);
                // } else {
                //     $this->session->set_flashdata('warning', 'Permission denied');
                //     redirect('dashboard');
                // }
            }

            $this->filterRender();
        }


    }

    function page_construct($page, $data = array(), $meta = array())
    {
        if (empty($meta)) {
            $meta['page_title'] = $data['page_title'];
        }
        $meta['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
        $meta['error'] = isset($data['error']) ? $data['error'] : $this->session->flashdata('error');
        $meta['warning'] = isset($data['warning']) ? $data['warning'] : $this->session->flashdata('warning');
        $meta['ip_address'] = $this->input->ip_address();
        $meta['loggedIn'] = $data['loggedIn'];
        $meta['Settings'] = $data['Settings'];
        $meta['assets'] = $data['assets'];
        $meta['classbody'] = @$data['classbody'];
        $this->load->view($this->theme . 'header', $meta);
        $this->load->view($this->theme . $page, $data);
        $this->load->view($this->theme . 'footer');
    }

    function filterRender($shop = true, $shopType = true, $province = true, $district = true, $shopModel = true)
    {
        $shops = $this->session->userdata('shops');
        if ($shop) {
            $shopList = array();
            $shopAddress = array();
            if (in_array($this->session->userdata('shopCode'), ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])) {
                $shopList['All'] = 'Tất cả';
                $shopAddress['All'] = '';
            }
            foreach ($shops as $key => $value) {
                $shopList[$value['shopCode']] = $value['shopCode'] . ' - ' . $value['shopName'];
                $shopAddress[$value['shopCode']] = $value['shopAddress'];
            }
            $this->data['shopList'] = $shopList;
            $this->data['shopAddress'] = $shopAddress;
        }
        if ($shopType) {
            $shopTypeList = array(
                'All' => 'Tất cả'
            );
            $shopTypes = array_values(array_unique(array_column($shops, 'shopType')));
            sort($shopTypes);
            foreach ($shopTypes as $value) {
                $shopTypeList[$value] = $value;
            }
            $this->data['shopTypeList'] = $shopTypeList;
        }
        if ($province) {
            $provinces = $this->session->userdata('provinces');
            $provinceList = array();
            $provinceList['All'] = "Tất cả";
            foreach ($provinces as $key => $value) {
                $provinceList[$value['provinceCode']] = $value['provinceName'];
            }
            $this->data['provinceList'] = $provinceList;
        }
        if ($district) {
            $districts = $this->session->userdata('districts');
            $districtList = array();
            $districtList['All'] = "Tất cả";
            foreach ($districts as $key => $value) {
                $districtList[$value['districtCode']] = $value['districtName'];
            }
            $this->data['districtList'] = $districtList;
        }
        if ($shopModel) {
            switch ($this->session->userdata('shopModel')) {
                case 'CSH':
                    $this->data['shopModelList'] = array('CSH' => 'Chủ sở hữu');
                    break;
                case 'NQ':
                    $this->data['shopModelList'] = array('NQ' => 'Nhượng quyền');
                    break;
                default:
                    $this->data['shopModelList'] = array('All' => 'Tất cả', 'CSH' => 'Chủ sở hữu', 'NQ' => 'Nhượng quyền');
                    break;
            }
        }
    }
}

/**
 *
 */
class Frontend_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

    }

    function view($page, $data = array(), $meta = array())
    {
        if (empty($meta)) {
            $meta['page_title'] = $data['page_title'];
        }
        $meta['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
        $meta['error'] = isset($data['error']) ? $data['error'] : $this->session->flashdata('error');
        $meta['warning'] = isset($data['warning']) ? $data['warning'] : $this->session->flashdata('warning');
        $meta['ip_address'] = $this->input->ip_address();
        $meta['Settings'] = $data['Settings'];
        $meta['assets'] = $data['assets'];

        $this->load->view($this->theme . 'header', $meta);
        $this->load->view($this->theme . $page, $data);
        $this->load->view($this->theme . 'footer');
    }
}
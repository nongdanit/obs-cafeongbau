<?php
defined('BASEPATH') or exit('No direct script access allowed');
# ########################Check full:
# 0: Lọc từ 06-07 --> 21-22
# 1: Lọc từ 00-01 --> 23-24
class Dashboard extends Backend_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        // setup data variables        
        $fromDate = date("Ymd");
        $toDate = date("Ymd");
        $shopType = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $allShop = $this->session->userdata('shops');

        $countActive = $countDeactive = $countPending = 0;
        foreach ($allShop as $k => &$val) {
            if ( $val['activeStatus'] == 'Đang hoạt động' && $val['status'] == 1 ) $countActive++;
            if ( $val['activeStatus'] == 'Sắp khai trương' && $val['status'] == 1 ) $countPending++;
            if ( $val['activeStatus'] == 'Đã đóng cửa' && $val['status'] == 1 ) $countDeactive++;
        }

        $this->data['countActive'] = $countActive;
        $this->data['countPending'] = $countPending;
        $this->data['countDeactive'] = $countDeactive;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['page_title'] = lang('dashboard');

        $bc = array(array('link' => '#', 'page' => lang('dashboard')));
        $meta = array('page_title' => lang('dashboard'), 'bc' => $bc);
        $this->page_construct('dashboard/dashboard', $this->data, $meta);
    }

    function about()
    {
        $bc = array(array('link' => '#', 'page' => ''));
        $meta = array('page_title' => '', 'bc' => $bc);
        $this->page_construct('blank', $this->data, $meta);
    }

}

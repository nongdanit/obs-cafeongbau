<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends Backend_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->lang->load('auth', $this->Settings->language);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->model('User_model', 'Users');
        $this->load->library('ion_auth');
    }

    function login($m = NULL)
    {
        if (!$m) {
            $m = $this->input->get('m');
        }

        if ($this->form_validation->run() == true) {
            $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));

            $userIp = $this->input->ip_address();

            $secret = $this->config->item('google_secret');

            $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $recaptchaResponse . "&remoteip=" . $userIp;
            $response = executeApi(HTTP_GET, $url, null); //$this->data_lib->callAPI($url, 'GET', NULL, NULL, FALSE);

            if ($response && !$response['success']) {
                $this->session->set_flashdata('error', 'Có lỗi xảy ra, vui lòng thử lại');
                sleep(1);
                redirect('login');
            }
            $userName = $this->input->post('identity');
            $passWord = trim($this->input->post('password'));
            $remember = (bool)$this->input->post('remember');

            $response = $this->Users->authenticate($userName, $passWord);// $this->api->authenticate($userName, $passWord);
            // echo json_encode($result);die;
            if ($response){
                if ($response['success']) {
                    $dataResult = $response['data'];
                    usort($dataResult['shops'], function ($a, $b) {
                        $coll = collator_create('utf8mb4_vietnamese_ci');
                        return collator_compare($coll, strtolower($a['shopName']), strtolower($b['shopName']));
                    });
                    $session_data = array(
                        'userId' => $dataResult['user']['userId'],
                        'userName' => $dataResult['user']['userName'],
                        'shopCode' => $dataResult['user']['shopCode'],
                        'shopModel' => $dataResult['user']['shopModel'],
                        'role' => $dataResult['user']['role'],
                        'groupId' => $dataResult['user']['groupId'],
                        'status' => $dataResult['user']['status'],
                        'sync' => $dataResult['user']['sync'],
                        'obs' => $dataResult['user']['obs'],
                        'mbs' => $dataResult['user']['mbs'],
                        'menus' => $dataResult['menus'],
                        'permissions' => array_map('strtolower', $dataResult['permissions']),
                        'token' => $dataResult['token'],
                        'shops' => $dataResult['shops'],
                        'provinces' => $dataResult['provinces'],
                        'districts' => $dataResult['districts'],
                        'order_type_inv' => $dataResult['order_type_inv']
                    );
                    $this->session->set_userdata($session_data);
                    // log_message('debug', "User data: ". print_r($session_data, true));
                    $firstMenu = $dataResult['menus'][0];
                    $controller = $firstMenu['formId'];
                    $action = '';
                    if (count($firstMenu['child']) > 0) {
                        $childMenu = $firstMenu['child'][0];
                        if ($childMenu['menuName'] == '-') {
                            $childMenu = $firstMenu['child'][1];
                        }
                        $action = '/' . $childMenu['formId'];
                    }
                    $this->session->set_userdata('homepage', $controller . $action);
                    redirect($controller . $action);
                } else {
                    $this->session->set_flashdata('error', $response['message']);
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('error', 'Có lỗi xảy ra, vui lòng thử lại');
                sleep(2);
                redirect('login');
            }
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['message'] = $this->session->flashdata('message') ? $this->session->flashdata('message') : ($m ? lang($m) : '');
            $this->data['page_title'] = lang('login');
            $this->load->view($this->theme . 'auth/login', $this->data);
        }
    }

    function logout($m = NULL)
    {
        $logout = $this->ion_auth->logout();
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('login');
    }
}

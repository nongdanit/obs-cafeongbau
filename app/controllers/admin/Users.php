<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Backend_Controller {

    function __construct() {
        parent::__construct();
        // if (!$this->loggedIn) {
        //     redirect('login');
        // }
        $this->lang->load('auth', $this->Settings->language);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->model('auth_model');
        $this->load->library('ion_auth');
    }

    function list() {
        // if (!$this->loggedIn) {
        //     redirect('login');
        // }
        $bc = array(array('link' => '#', 'page' => lang('users')));
        $meta = array('page_title' => lang('users'), 'bc' => $bc);
        $this->data['page_title'] = lang('users');
        $this->page_construct('users/index', $this->data, $meta);
    }

    function get_users(){
        $data   = [];
        $resultAPI = $this->site->getListUserAccount();
        if(!empty($resultAPI)){
            if($resultAPI['status'] == 'OK' && $resultAPI['errorCode'] == 200){
                $data = $resultAPI['result'];
            }
        }
        $sOutput = array
        (
          'data'                => $data,
        );
        echo json_encode($sOutput);
    }

    function permission($username = NULL){
        if (!$username || empty($username)) {
            redirect('users');
        }
        //Info user
        $getUserAccountByUsername = $this->site->getUserAccountByUsername(['username' => $username]);
        if(!empty($getUserAccountByUsername)){
            if($getUserAccountByUsername['status'] == 'OK' && $getUserAccountByUsername['errorCode'] == 200){
                $dataReturn = $getUserAccountByUsername['result'];
            }
        }
        if(empty($dataReturn)){
            redirect('users');
        }
        $this->data['users'] = $dataReturn;

        //get List Role
        $getListRole = $this->site->getListRole();
        if(!empty($getListRole)){
            if($getListRole['status'] == 'OK' && $getListRole['errorCode'] == 200){
                $dataListRole = $getListRole['result'];
            }
        }
        $this->data['listrole'] = isset($dataListRole)?$dataListRole:[];

        //get list group
        $getListGroup = $this->site->getListGroup();
        if(!empty($getListGroup)){
            if($getListGroup['status'] == 'OK' && $getListGroup['errorCode'] == 200){
                $dataListGroup = $getListGroup['result'];
            }
        }
        $this->data['listgroup'] = isset($dataListGroup)?$dataListGroup:[];

        //get list function
        $getListFunction = $this->site->getListFunction();
        if(!empty($getListFunction)){
            if($getListFunction['status'] == 'OK' && $getListFunction['errorCode'] == 200){
                $dataListFunction = $getListFunction['result'];
            }
        }
        $this->data['listfunction'] = isset($dataListFunction)?$dataListFunction:[];

        //Get role group by username
        $getRoleGroupByUsername = $this->site->getRoleGroupByUsername(['username' => $username]);
        if(!empty($getRoleGroupByUsername)){
            if($getRoleGroupByUsername['status'] == 'OK' && $getRoleGroupByUsername['errorCode'] == 200){
                $dataGetRole = $getRoleGroupByUsername['result'];
            }
        }
        $userGroup = array_unique(array_values(array_column($dataGetRole, 'GROUP_CODE')));
        $this->data['userGroup'] = isset($userGroup)?$userGroup:[];

        //Get role user by username
        $getRoleUserByUsername = $this->site->getRoleUserByUsername(['username' => $username]);
        if(!empty($getRoleUserByUsername)){
            if($getRoleUserByUsername['status'] == 'OK' && $getRoleUserByUsername['errorCode'] == 200){
                $dataRoleUser = $getRoleUserByUsername['result'];
            }
        }
        $this->data['dataRoleUser'] = isset($dataRoleUser)?$dataRoleUser:[];
        // $userRoleFUNCTION_CODE = array_unique(array_values(array_column($dataRoleUser, 'FUNCTION_CODE')));
        // $this->data['userRoleFUNCTION_CODE'] = isset($userRoleFUNCTION_CODE)?$userRoleFUNCTION_CODE:NULL;

        // $userRoleROLE_CODE = (array_values(array_column($dataRoleUser, 'ROLE_CODE')));
        // $this->data['userRoleROLE_CODE'] = isset($userRoleROLE_CODE)?$userRoleROLE_CODE:NULL;

        // echo '<pre>';
        // print_r($dataRoleUser);

        // $a = check_value_in_array('THONGSOMATBANG', 'TIMELINE', 'FUNCTION_CODE', 'ROLE_CODE', $dataRoleUser);
        // var_dump($a);
        // exit;


        


        $this->data['classbody']  = 'sidebar-collapse';
        $bc = array(array('link' => '#', 'page' => lang('permission_list')));
        $meta = array('page_title' => lang('permission_list'). ': ' . $username, 'bc' => $bc);
        // $this->data['page_title'] = lang('permission_list'). $username;
        $this->page_construct('users/permission', $this->data, $meta);
    }

    function create() {
        // if (!$this->loggedIn) {
        //     redirect('login');
        // }
        $listShop =  $this->session->userdata('shops');
        foreach ($listShop as $key => $value) {
            $gp[$value['shopCode']] = $value['shopName'];
        }
        $this->data['title'] = lang('add_user');
        $this->form_validation->set_rules('shop_code', lang("shop_code"), 'required');
        $this->form_validation->set_rules('username', lang("username"), 'required|min_length[4]|max_length[20]|trim');

        $this->form_validation->set_rules('password', lang('password'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');

        $this->form_validation->set_rules('confirm_password', lang('confirm_password'), 'required');

        if ($this->form_validation->run() == true) {
            $additional_data = array(
                'username'   => $this->input->post('username'),
                'password'   => $this->input->post('password'),
                'avatar'     => '',
                'shop_code'  => $this->input->post('shop_code'),
                'parent'     => '',
                'start_date' => $this->input->post('start_date'),
                'end_date'   => $this->input->post('end_date'),
                'status'     => 1,
                'create_by'  => $this->session->userdata('userId')
                );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register_ntfpg($additional_data) ) {
            $this->session->set_flashdata('message', lang('new_user_created'));
            redirect("users/list");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $this->data['listShop']     = $gp;
            $this->data['page_title'] = lang('add_user');
            $bc = array(array('link' => site_url('users/list'), 'page' => lang('people')), array('link' => '#', 'page' => lang('add_user')));
            $meta = array('page_title' => lang('add_user'), 'bc' => $bc);
            $this->page_construct('users/create_user', $this->data, $meta);
        }
    }











    function profile($id = NULL) {
        if (!$id || empty($id)) {
            redirect('welcome');
        }

        $this->data['title'] = lang('profile');
        $this->data['csrf'] = $this->_get_csrf_nonce();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'class' => 'form-control',
            'type' => 'password',
            'value' => ''
            );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'class' => 'form-control',
            'type' => 'password',
            'value' => ''
            );
        $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
        $this->data['old_password'] = array(
            'name' => 'old',
            'id' => 'old',
            'class' => 'form-control',
            'type' => 'password',
            );
        $this->data['new_password'] = array(
            'name' => 'new',
            'id' => 'new',
            'type' => 'password',
            'class' => 'form-control',
            'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
        $this->data['new_password_confirm'] = array(
            'name' => 'new_confirm',
            'id' => 'new_confirm',
            'type' => 'password',
            'class' => 'form-control',
            'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
        $this->data['user_id'] = array(
            'name' => 'user_id',
            'id' => 'user_id',
            'type' => 'hidden',
            'value' => $this->session->userdata('userId'),
            );

        $this->data['page_title'] = lang('profile');
        $bc = array(array('link' => '#', 'page' => lang('users')), array('link' => '#', 'page' => lang('profile')));
        $meta = array('page_title' => lang('profile'), 'bc' => $bc);
        $this->page_construct('auth/profile', $this->data, $meta);
    }

    function change_password() {
        // if ( ! $this->loggedIn) {
        //     redirect('login');
        // }
        $this->form_validation->set_rules('old_password', lang('old_password'), 'required');
        $this->form_validation->set_rules('new_password', lang('new_password'), 'required');//|max_length[20]
        $this->form_validation->set_rules('new_password_confirm', lang('confirm_password'), 'required|matches[new_password]');


        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('users/profile/'.$this->session->userdata('userId').'/#cpassword');
        } else {
            $url = '/api/users';
            $dataPost = array(
                'user_id'   => $this->session->userdata('userId'),
                'old_pass'  => $this->input->post('old_password'),
                'new_pass'  => $this->input->post('new_password')
            );

            $resultAPI = $this->data_lib->callAPI($url, 'PUT', $dataPost, $this->session->userdata('token'));

            if(!empty($resultAPI['message']) && $resultAPI['message'] == 'OK'){
                $this->session->set_flashdata('message', 'Đổi pass thành công, vui lòng đăng nhập lại');
                redirect('users/profile/'.$this->session->userdata('userId').'/#cpassword');
            } else {
                $this->session->set_flashdata('error', 'Đã có lỗi xảy ra, vui lòng thử lại');
                redirect('users/profile/'.$this->session->userdata('userId').'/#cpassword');
            }
        }
    }

    function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    

    

    function edit($row_id = NULL){
        if (!$row_id || empty($row_id)) {
            redirect('welcome');
        }
        $listShop =  $this->session->userdata('shops');
        foreach ($listShop as $key => $value) {
            $gp[$value['shopCode']] = $value['shopName'];
        }
        $this->data['csrf'] = $this->_get_csrf_nonce();
        $this->form_validation->set_rules('shop_code', lang("shop_code"), 'required');
        $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');

        $user = $this->site->getEmployerByRowID($row_id);
        if(!empty($user)){
            if($user['status'] == 'OK' && $user['errorCode'] == 200){
                $dataReturn = $user['result'];
                $this->data['users'] = (isset($dataReturn)) ? $dataReturn : FALSE;
            }
        }

        if(!empty($this->input->post('row_id'))){
            $data = array(
                'row_id'     => $this->input->post('row_id'),
                'username'   => $dataReturn['USERNAME'],
                'shop_code'  => $this->input->post('shop_code'),
                'start_date' => $this->input->post('start_date'),
                'end_date'   => $this->input->post('end_date'),
                'status'     => $this->input->post('status'),
                'update_by'  => $this->session->userdata('userId')
                );


            if ($this->input->post('new_password')) {
                $this->form_validation->set_rules('new_password', lang('new_password'), 'min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
                $this->form_validation->set_rules('confirm_password', lang('confirm_password'), 'required');
                $data['password'] = $this->input->post('new_password');
            }
            if ($this->form_validation->run() === TRUE && $resultAPI = $this->site->updateEmp($data) ) {
                if(!empty($resultAPI)){
                    if($resultAPI['status'] == 'OK' && $resultAPI['errorCode'] == 200){
                        $this->session->set_flashdata('message', lang('user_updated'));
                        redirect("users/list");
                    }else if($resultAPI['status'] == 'ERROR'){
                        $this->session->set_flashdata('error', lang('1106'.'nongdanit'));
                    }else{
                        $this->session->set_flashdata('error', 'Hệ thống đang bận, vui lòng quay lại sau.');
                    }
                }
            }
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['listShop']     = $gp;
        $this->data['row_id'] = $row_id;
        $bc = array(array('link' => site_url('users/list'), 'page' => lang('users')), array('link' => '#', 'page' => lang('employer_profile')));
        $meta = array('page_title' => lang('employer_profile'), 'bc' => $bc);
        $this->page_construct('auth/edit', $this->data, $meta);
    }

    function delete($id = NULL) {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($this->site->deleteEmp(['row_id' => $id, 'update_by' => $this->session->userdata('userId')])) {
            $this->session->set_flashdata('message', lang('user_deleted'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}

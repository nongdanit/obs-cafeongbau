<?php defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends Backend_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * New
     */
    function rptDaily()
    {
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopType = 'All';
        $groupCode = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['page_title'] = $this->lang->line("sales_report");
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['groupCode'] = $groupCode;

        $bc = array(array('link' => site_url('reports/rptDaily'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('sales_report')));
        $meta = array('page_title' => lang('sales_report'), 'bc' => $bc);
        $this->page_construct('reports/rptDaily', $this->data, $meta);
    }

    function rptHour()
    {
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopType = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';

        $this->data['checkFull'] = 0;
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['page_title'] = $this->lang->line("sales_report_hours");
        // $this->data['listShop']     = $gp;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;

        $bc = array(array('link' => site_url('reports/rptHour'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('sales_report_hours')));
        $meta = array('page_title' => lang('sales_report_hours'), 'bc' => $bc);
        $this->page_construct('reports/rptHour', $this->data, $meta);
    }

    function rptItem()
    {
        $fromDate = $this->input->get('fromDate') ? date('d-m-Y', strtotime($this->input->get('toDate'))) : date("d-m-Y");
        $toDate = $this->input->get('toDate') ? date('d-m-Y', strtotime($this->input->get('toDate'))) : date("d-m-Y");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $shopType = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';
        // $shopList       =  $this->session->userdata('shops');

        // if(!in_array($shopCode, ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])){
        //     $key = array_search($shopCode, array_column($shopList, 'SHOP_CODE'));
        //     $this->data['addressShop'] = $shopList[$key]['FULL_ADDRESS'];
        // }else{
        //     $this->data['addressShop'] = '';
        // }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['page_title'] = $this->lang->line("sales_report");
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;

        $bc = array(array('link' => site_url('reports/rptItem'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Doanh số theo sản phẩm'));
        $meta = array('page_title' => 'Doanh số theo sản phẩm', 'bc' => $bc);
        $this->page_construct('reports/rptItem', $this->data, $meta);
    }

    function rptTransDetails()
    {
        $permissions = [
            'read' => check_permission('rptTransDetails:read'),
            'insert' => check_permission('rptTransDetails:insert'),
            'update' => check_permission('rptTransDetails:update'),
            'delete' => check_permission('rptTransDetails:delete'),
            'export' => check_permission('rptTransDetails:export'),
        ];
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopType = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';
        $shopList = $this->session->userdata('shops');
        if (!in_array($shopCode, ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])) {
            $key = array_search($shopCode, array_column($shopList, 'shopCode'));
            $this->data['nameShop'] = $shopList[$key]['shopName'];
        } else {
            $this->data['nameShop'] = $shopCode;
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['shopModel'] = $shopModel;
        $this->data['permissions'] = $permissions;

        $bc = array(array('link' => site_url('reports/rptDaily'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Doanh số theo bill'));
        $meta = array('page_title' => 'Doanh số theo bill', 'bc' => $bc);
        $this->page_construct('reports/rptTransDetails', $this->data, $meta);
    }

    function rptCustomer()
    {
        $permissions = [
            'read' => check_permission('rptTransCustomer:read'),
            'insert' => check_permission('rptTransCustomer:insert'),
            'update' => check_permission('rptTransCustomer:update'),
            'delete' => check_permission('rptTransCustomer:delete'),
            'export' => check_permission('rptTransCustomer:export'),
        ];
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopType = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';
        $shopList = $this->session->userdata('shops');
        if (!in_array($shopCode, ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])) {
            $key = array_search($shopCode, array_column($shopList, 'shopCode'));
            $this->data['nameShop'] = $shopList[$key]['shopName'];
        } else {
            $this->data['nameShop'] = $shopCode;
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['shopModel'] = $shopModel;
        $this->data['permissions'] = $permissions;

        $bc = array(array('link' => site_url('reports/rptDaily'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Hóa đơn điện tử'));
        $meta = array('page_title' => 'Hóa đơn điện tử', 'bc' => $bc);
        $this->page_construct('reports/rptTransCustomer', $this->data, $meta);
    }

    // Nghiahv
    function rptItemByPayment()
    {
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopType = 'All';
        $groupCode = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';

        // $start = $this->input->get('start') ? $this->input->get('start'): 0;
        $limit = $this->input->get('limit') ? $this->input->get('limit') : 50;
        $shopList = $this->session->userdata('listShop');
        if (!in_array($shopCode, ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])) {
            $key = array_search($shopCode, array_column($shopList, 'SHOP_CODE'));
            $this->data['addressShop'] = $shopList[$key]['FULL_ADDRESS'];
        } else {
            $this->data['addressShop'] = '';
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['page_title'] = $this->lang->line("sales_report");
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        // $this->data['start']= $start;
        $this->data['limit'] = $limit;
        //$this->data['group_code']   = $group_code;
        $bc = array(array('link' => site_url('reports/rptItemByPayment'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Chi tiết SP theo HT thanh toán'));
        $meta = array('page_title' => 'Chi tiết SP theo HT thanh toán', 'bc' => $bc);
        $this->page_construct('reports/rptItemByPayment', $this->data, $meta);
    }

    function stores()
    {
        $qtyStore = $this->site->reportStores();

        if (isset($qtyStore)) {
            if ($qtyStore['status'] == 'OK' && $qtyStore['errorCode'] == 200) {
                $data = $qtyStore['result'];
            } else $data = [];
        } else {
            $data = [];
        }

        $this->data['qtyStore'] = $data;
        $bc = array(array('link' => site_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('stores')));
        $meta = array('page_title' => lang('stores'), 'bc' => $bc);
        $this->page_construct('reports/stores', $this->data, $meta);
    }

    function checkincheckout()
    {
        $date = $this->input->get('date') ? $this->input->get('date') : date("d-m-Y");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : '00';

        $this->data['date'] = $date;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopModel'] = $shopModel;


        $bc = array(array('link' => site_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Chấm công theo cửa hàng'));
        $meta = array('page_title' => 'Chấm công theo cửa hàng', 'bc' => $bc);
        $this->page_construct('reports/checkincheckout', $this->data, $meta);
    }


    function checkincheckout_detail()
    {
        $month = $this->input->get('month') ? $this->input->get('month') : date("d-m-Y");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');

        // $shopList =  $this->session->userdata('listShop');
        // if($this->session->userdata('shop_code') == 'HEAD_OFFICE') $gp['ALL']  = 'Tất cả';
        // foreach ($listShop as $key => $value) {
        //     $gp[$value['SHOP_CODE']] = $value['SHOP_NAME'];
        // }

        $lenmonth = strlen($month);
        if ($lenmonth <= 7) {
            $month = '01-' . $month;
        }
        // $monthCurrent =  date("Y-m", strtotime($month));
        // $toDate =  $monthCurrent . '-25';

        // $monthPre = date('Y-m', strtotime('-1 month', strtotime($month)));
        // $fromDate = $monthPre . '-26';

        // echo $fromDate;
        // echo "<br>";
        // echo $toDate;
        // exit;
        $this->data['month'] = $month;
        $this->data['page_title'] = $this->lang->line("sales_report_hours");
        // $this->data['listShop']     = $gp;
        $this->data['shopCode'] = $shopCode;
        $this->data['currentDate'] = date("d-m-Y");
        $dataReturnReason = $this->site->getReason();
        $this->data['reason'] = json_encode(array_column($dataReturnReason['metadata'], 'code'));

        $bc = array(array('link' => site_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Chấm công chi tiết'));
        $meta = array('page_title' => 'Báo cáo chấm công chi tiết', 'bc' => $bc);
        $this->page_construct('reports/checkincheckout_detail', $this->data, $meta);
    }

    function rptShop_info()
    {
        $this->data['classbody'] = 'sidebar-collapse';
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $status = $this->input->get('status') ? $this->input->get('status') : 'All';
        $this->data['shopType'] = $shopType;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['shopModel'] = $shopModel;
        $this->data['status'] = $status;

        // $this->data_render($flag_listShop=false, $flag_getShopType=true, $flag_listProvince=true, $province_code, $flag_listDistrict=true, $addAllShop=true);

        $bc = array(array('link' => '#', 'page' => 'Báo cáo ' . lang('stores')));
        $meta = array('page_title' => 'Báo cáo ' . lang('stores'), 'bc' => $bc);
        $this->page_construct('reports/rptShopinfo', $this->data, $meta);
    }

    function materials()
    {
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopModel = $this->session->userdata('shopModel');
        $shopCode = $this->session->userdata('shopCode');
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopModel'] = $shopModel;
        $this->data['shopCode'] = $shopCode;
        //$this->data['data'] = $data;
        $bc = array(array('link' => site_url('reports/materials'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Tiêu hao NVL'));
        $meta = array('page_title' => 'Tiêu hao nguyên vật liệu', 'bc' => $bc);
        $this->page_construct('reports/rptMaterial', $this->data, $meta);
    }

    function rptInventory()
    {
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');

        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopModel'] = $shopModel;
        $this->data['stockType'] = 'All';
        $this->data['shopCode'] = $shopCode;
        //$this->data['data'] = $data;
        $bc = array(array('link' => site_url('reports/rptInventory'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Kiểm kê'));
        $meta = array('page_title' => 'Kiểm kê', 'bc' => $bc);
        $this->page_construct('reports/rptInventory', $this->data, $meta);
    }

    function rptLocalTransfer()
    {
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopModel = $this->session->userdata('shopModel');
        $shopCode = $this->session->userdata('shopCode');
        $shopType = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';

        // $listShop =  $this->session->userdata('listShop');
        // if(!in_array($shopCode, ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])){
        //     $index = array_search($shopCode, array_column($listShop, 'SHOP_CODE'));
        //     $this->data['addressShop'] = $listShop[$index]['FULL_ADDRESS'];
        // }else{
        //     $this->data['addressShop'] = '';
        // }

        //$this->data_render($flag_listShop=true, $flag_getShopType=true, $flag_listProvince=true, $provinceCode, $flag_listDistrict=true, $addAllShop=false);
        //$this->data['listShop']     = $listShop;
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopModel'] = $shopModel;
        $this->data['shopType'] = $shopType;
        //$this->data['stockType']    = '-1';
        $this->data['shopCode'] = $shopCode;
        $bc = array(
            array('link' => site_url('business/localtransfer'), 'page' => lang('business')),
            array('link' => '#', 'page' => 'Xuất nhập tồn')
        );
        $meta = array('page_title' => 'Báo cáo xuất nhập tồn', 'bc' => $bc);
        $this->page_construct('reports/rptLocalTransfer', $this->data, $meta);
    }

    function lookupStore()
    {
        $this->data['classbody'] = 'sidebar-collapse';
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $status = $this->input->get('status') ? $this->input->get('status') : 'All';
        $this->data['shopType'] = $shopType;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['shopModel'] = $shopModel;
        $this->data['status'] = $status;

        //$this->data_render($flag_listShop=false, $flag_getShopType=true, $flag_listProvince=true, $province_code, $flag_listDistrict=true, $addAllShop=true);
        $bc = array(
            array(
                'link' => site_url('reports/rptDaily'),
                'page' => lang('reports')
            ),
            array(
                'link' => '#', 'page' => 'Tra cứu vị trí'
            )
        );
        //$bc = array(array('link' => '#', 'page' => 'Tra cứu vị trí'));
        $meta = array('page_title' => 'Tra cứu vị trí', 'bc' => $bc);
        $this->page_construct('reports/lookupStore', $this->data, $meta);
    }

    function rptWareHouse()
    {
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $fromDate = date("01-m-Y");
        $toDate = date("d-m-Y");
        $shopType = 'All';
        $groupCode = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';

        $shops = $this->session->userdata('shops');
        if (!in_array($shopCode, ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])) {
            $index = array_search($shopCode, array_column($shops, 'shopCode'));
            $this->data['addressShop'] = $shops[$index]['shopAddress'];
        } else {
            $this->data['addressShop'] = '';
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['page_title'] = $this->lang->line("sales_report");
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['groupCode'] = $groupCode;

        $bc = array(array('link' => site_url('reports/rptWareHouse'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Nhập kho'));
        $meta = array('page_title' => 'Nhập kho', 'bc' => $bc);
        $this->page_construct('reports/rptWareHouse', $this->data, $meta);
    }

    // PROMOTION BY VIP CARD
    function rptPromotionByVipCard()
    {
        $fromDate = $this->input->get('fromDate') ? date('d-m-Y', strtotime($this->input->get('toDate'))) : date("d-m-Y");
        $toDate = $this->input->get('toDate') ? date('d-m-Y', strtotime($this->input->get('toDate'))) : date("d-m-Y");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $shopType = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';
        // $shopList       =  $this->session->userdata('shops');

        // if(!in_array($shopCode, ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])){
        //     $key = array_search($shopCode, array_column($shopList, 'SHOP_CODE'));
        //     $this->data['addressShop'] = $shopList[$key]['FULL_ADDRESS'];
        // }else{
        //     $this->data['addressShop'] = '';
        // }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['page_title'] = $this->lang->line("sales_report");
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;

        $bc = array(array('link' => site_url('reports/rptPromotionByVipCard'), 'page' => lang('reports')), array('link' => '#', 'page' => 'Doanh số theo thẻ Vip'));
        $meta = array('page_title' => 'Doanh số theo thẻ Vip', 'bc' => $bc);
        $this->page_construct('reports/rptPromotionByVipCard', $this->data, $meta);
    }

}

<?php defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends Backend_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Utilities');
    }

    function shop_info($action = '', $id = NULL)
    {
        if ($action == 'create') {
            $this->create_store();
        } elseif ($action == 'edit') {
            $this->edit_stores($id);
        } elseif ($action == 'submit') {
            $this->submit_store();
        } else {
            $this->data['classbody'] = 'sidebar-collapse';
            $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
            $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
            $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
            $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
            $status = $this->input->get('status') ? $this->input->get('status') : 'All';
            $this->data['shopType'] = $shopType;
            $this->data['provinceCode'] = $provinceCode;
            $this->data['districtCode'] = $districtCode;
            $this->data['shopModel'] = $shopModel;
            $this->data['status'] = $status;

            $bc = array(array('link' => '#', 'page' => lang('stores')));
            $meta = array('page_title' => lang('stores'), 'bc' => $bc);
            $this->page_construct('categories/stores', $this->data, $meta);
        }

    }

    private function create_store()
    {

        // $this->data_render($flag_listShop=false, $flag_getShopType=true, $flag_listProvince=true, 0, $flag_listDistrict=false, $addAllShop=true);

        // TTPP and AM
        $dataTTPP_AM = $this->site->getListTTPPAM();
        $this->data['dataTTPP_AM'] = $dataTTPP_AM['data'];
        $erpOrg = [];
        $response = $this->Utilities->getErpOrg();
        foreach ($response['data'] as $org) {
            $erpOrg[$org['orgCode']] = $org['orgName'];
        }
        $this->data['erpOrg'] = $erpOrg;

        $bc = array(array('link' => base_url('categories/shop_info'), 'page' => lang('stores')), array('link' => '#', 'page' => lang('add') . ' ' . lang('stores')));
        $meta = array('page_title' => lang('add') . ' ' . lang('stores'), 'bc' => $bc);
        $this->page_construct('categories/create_store', $this->data, $meta);
    }

    private function edit_stores($id = 0)
    {
        if (!$id) return redirect('categories/shop_info');
        $response = executeApi(HTTP_GET, '/api/categories/shop', ['shopId' => $id]);
//        echo json_encode($response);
//        die;
        if (!$response || !count($response['data'])) return redirect('categories/shop_info');
        $dataStore = $response['data'][0];

//        echo json_encode($dataStore);die;
        // TTPP and AM
        $dataTTPP_AM = $this->site->getListTTPPAM();
        $this->data['dataTTPP_AM'] = $dataTTPP_AM['data'];

        //get erp org
        $erpOrg = [];
        $response = $this->Utilities->getErpOrg();
        foreach ($response['data'] as $org) {
            $erpOrg[$org['orgCode']] = $org['orgName'];
        }
        $this->data['erpOrg'] = $erpOrg;
        $resultAPI = $this->data_lib->execute('/api/utilities/wardList', 'GET', array('districtCode' => $dataStore['districtCode']));
        $this->data['listWard'] = $resultAPI['data'];
        $this->data['dataStore'] = $dataStore;
        $bc = array(array('link' => base_url('categories/shop_info'), 'page' => lang('stores')), array('link' => '#', 'page' => lang('edit') . ' ' . lang('stores')));
        $meta = array('page_title' => lang('edit') . ' ' . lang('stores'), 'bc' => $bc);
        $this->page_construct('categories/edit_store', $this->data, $meta);
    }

    private function submit_store()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Đã có lỗi xảy ra, vui lòng quay lại sau',
            'url' => base_url('categories/shop_info')
        );
        if ($this->input->post()) {
            $shop_id = $this->input->post("shop_id");
            $shop_name = $this->input->post("shop_name");
            $shop_code = $this->input->post("shop_code");
            $shop_tel = $this->input->post("shop_tel");
            $owner_tel = $this->input->post("owner_tel");
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $sale_online = $this->input->post("sale_online");
            $pos_type = $this->input->post("pos_type");
            $shop_type = $this->input->post("shop_type");
            $address = $this->input->post("address");
            $province_code = $this->input->post("province_code");
            $district_code = $this->input->post("district_code");
            $ward_code = $this->input->post("ward_code");
            $lng = $this->input->post("lng");
            $lat = $this->input->post("lat");
            $ttpp = $this->input->post("ttpp");
            $am = $this->input->post("am");
            $npp = $this->input->post("npp");
            $note = $this->input->post('shopNote', true);
            $orgCode = $this->input->post('orgCode', true);
            $data = array(
                'shop_name' => $shop_name,
                'shop_code' => trim($shop_code),
                'shop_tel' => $shop_tel,
                'owner_tel' => $owner_tel,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_online' => $sale_online,
                'pos_type' => $pos_type,
                'shop_type' => $shop_type,
                'address' => $address,
                'province_code' => $province_code,
                'district_code' => $district_code,
                'ward_code' => $ward_code,
                'lng' => $lng,
                'lat' => $lat,
                'ttpp' => $ttpp,
                'am' => $am,
                'npp' => $npp,
                'note' => $note,
                'orgCode' => $orgCode,
            );
//            echo json_encode($data);die;
            //var_dump($data);die;

            if (!empty($shop_id)) {
                $data['shop_id'] = $shop_id;
                $dataStore = executeApi(HTTP_GET, '/api/categories/shop', ['shopId' => $shop_id]);
                $dataStore = $dataStore['data'][0];
                $data['create_by'] = $dataStore['createBy'];
                $data['mm_id'] = $dataStore['mmId'];
                // $data['end_date']   = $dataStore['END_DATE'];
                if ($dataStore['UPDATE_GPS'] == 0) {
                    $data['lng'] = $dataStore['lng'];
                    $data['lat'] = $dataStore['lat'];
                }
                $data['parent_id'] = $dataStore['parentCode'];
                $data['sync'] = $dataStore['sync'];
            } else {
                $data['mm_id'] = null;
            }
            $dataStoresModel = $this->site->storesModel($data);
            if (!empty($dataStoresModel) && !empty($dataStoresModel['data'])) {
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Cửa hàng đã được thêm thành công',
                    'url' => base_url('categories/shop_info')
                );
                $data = $dataStoresModel['data'];
//                redirect('categories/shop_info');
            }
            if ($shop_id != 0 && !empty($dataStoresModel) && $dataStoresModel['data']['shop_id'] === $shop_id) {
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Cửa hàng đã được cập nhật thành công',
                    'url' => base_url('categories/shop_info')
                );
//                redirect('categories/shop_info/edit/' . $shop_id);
            }
            $this->session->set_userdata('shops', $this->data_lib->execute('/api/users/shop', 'GET', [])['data']);
//            redirect('categories/shop_info');
            $dataReturn = array(
                'error' => false,
                'message' => 'Cửa hàng đã được cập nhật thành công',
                'url' => base_url('categories/shop_info')
            );
        }

        $this->data["dataReturn"] = $dataReturn;
        echo json_encode($this->data);
    }

    function qc_template()
    {

        // $shop_code  = $this->input->get('shop_code') ? $this->input->get('shop_code') : $this->session->userdata('shop_code');
        // $shop_type  = $this->input->get('shoptype') ? $this->input->get('shoptype') : 'all';
        // $province_code = $this->input->get('province_code') ? $this->input->get('province_code'): '';
        // $district_code = $this->input->get('district_code') ? $this->input->get('district_code'): '';

        // $this->data['shop_code']    = $shop_code;
        // $this->data['shop_type']    = $shop_type;
        // $this->data['province_code']= $province_code;
        // $this->data['district_code']= $district_code;

        // $this->data_render($flag_listShop=true, $flag_getShopType=true, $flag_listProvince=true, $province_code, $flag_listDistrict=true, $addAllShop=true);
        $bc = array(array('link' => '#', 'page' => 'Mẫu đánh giá'));
        $meta = array('page_title' => 'Mẫu đánh giá', 'bc' => $bc);
        $this->page_construct('categories/qc/qc_template', $this->data, $meta);
    }

    function get_qc_template()
    {

        $dataFormulaQC = $this->site->getFormulaQC();
        if ($dataFormulaQC['status'] == 'OK' && $dataFormulaQC['errorCode'] == 200) {
            $data = $dataFormulaQC['result'];
        } else $data = [];

        $sOutput = array
        (
            'data' => $data,
        );
        echo json_encode($sOutput);
    }

    function updateFormulaQC()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $formtype = $this->input->post("formtype");
            $name = $this->input->post("name");
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $description = $this->input->post("description");

            $data = array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'name' => $name,
                'description' => $description
            );

            if ($formtype == 'edit') {
                $data['row_id'] = $this->input->post('id_qc_template');
                $data['version_id'] = $this->input->post('version_id');
                $data['version_name'] = $this->input->post('version_name');
                $dataReturn = $this->site->updateFormulaQCModel($data);
                if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật mẫu đánh giá thành công',
                        'formtype' => $formtype
                    );
                }
            } else if ($formtype == 'create') {
                // $data['version_id'] = '';
                // $data['version_name']= '';
                $dataReturn = $this->site->addFormulaQCModel($data);
                if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Thêm mẫu đánh giá thành công',
                        'formtype' => $formtype,
                        'url' => base_url('categories/qc_template')
                    );
                }
            }
        }
        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function copyTemplate()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $row_id = $this->input->post("row_id");
            $dataQC = $this->site->copyTemplateModel($row_id);
            if ($dataQC['status'] == 'OK' && $dataQC['errorCode'] == 200) {
                $dataResult = $dataQC['result'];
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Copy dữ liệu thành công',
                    'dataResult' => $dataResult
                );
            }
        }

        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function edit_qc_template($id = NULL)
    {

        $cate_code = $this->input->get('cate_code') ? $this->input->get('cate_code') : 'All';
        $this->data['cate_code'] = $cate_code;

        $dataFormulaQCDetail = $this->site->getFormulaQCByRowIdModel($id);
        $dataFormulaQC = [];
        if ($dataFormulaQCDetail['status'] == 'OK' && $dataFormulaQCDetail['errorCode'] == 200) {
            $dataFormulaQC = $dataFormulaQCDetail['result'];
        }
        $this->data['dataFormulaQC'] = $dataFormulaQC;

        $this->data['id'] = $id;
        $bc = array(array('link' => site_url('categories/qc_template'), 'page' => 'Mẫu đánh giá'), array('link' => '#', 'page' => 'Cập nhật mẫu đánh giá'));
        $meta = array('page_title' => 'Cập nhật mẫu đánh giá', 'bc' => $bc);
        $this->page_construct('categories/qc/edit_qc_template', $this->data, $meta);
    }

    function updateFormulaQCDetail()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $row_id = $this->input->post("row_id");
            $formula_id = $this->input->post("formula_id");
            $point = $this->input->post("point");
            $item_qc_id = $this->input->post("item_qc_id");
            $data = array(
                'row_id' => $row_id,
                'formula_id' => $formula_id,
                'point' => $point,
                'item_qc_id' => $item_qc_id
            );
            $dataQC = $this->site->updateFormulaQCDetailModel($data);
            if ($dataQC['status'] == 'OK' && $dataQC['errorCode'] == 200) {
                $dataResult = $dataQC['result'];
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Cập nhật thành công',
                    'dataResult' => $dataResult
                );
            }
        }

        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function loadQcItem()
    {
        $id = $this->input->get('id');
        $dataList = $this->site->getFormulaQCDetailByFormulaID($id);
        $html = '';
        if (!empty($dataList)) {
            if ($dataList['status'] == 'OK' && $dataList['errorCode'] == 200) {
                $dataShopFormulaQC = $dataList['result'];
                if (!empty($dataShopFormulaQC['CATEs'])):
                    foreach ($dataShopFormulaQC['CATEs'] as $key => $dataCate):
                        $html .= '<tr style="background-color: rgb(204, 229, 255)">
                            <td>' . $dataCate['CATE_NAME'] . '</td>
                            <td class="text-center"><span class="max_point_cate' . $dataCate['CATE_CODE'] . '">' . $dataCate['MAX_POINT'] . '</span></td>
                            <td class="text-center"></td>
                        </tr>';
                        if (!empty($dataCate['DETAILs'])):
                            foreach ($dataCate['DETAILs'] as $key => $dataQC):
                                $html .= '<tr>
                                <td>' . $dataQC['ITEM_NAME'] . '</td>
                                <td class="text-center"><input type="number" min="0" class="form-control max_point' . $dataCate['CATE_CODE'] . '" max_point="' . $dataCate['MAX_POINT'] . '" val_old="' . $dataQC['POINT'] . '" value="' . $dataQC['POINT'] . '" style="min-width:100px;"></td>
                                <td>
                                    <div class="text-center">';
                                if (check_permission('qc_template:Update')):
                                    $html .= '<a cate_code="' . $dataCate['CATE_CODE'] . '" item_qc_id="' . $dataQC['ITEM_QC_ID'] . '" formula_id="' . $dataQC['FORMULA_ID'] . '" type="button" row_id="' . $dataQC['ROW_ID'] . '" class="btn btn-warning" onclick="updateFormulaQCDetail(this)"><i class="fa fa-edit"></i> Cập nhật</a> ';
                                endif;
                                if (check_permission('qc_template:Delete')):
                                    $html .= '<a cate_code="' . $dataCate['CATE_CODE'] . '" type="button" row_id="' . $dataQC['ROW_ID'] . '" class="btn btn-warning" onclick="deleteFormulaQCDetail(this)"><i class="fa fa-trash-o"></i> Xóa</a>';
                                endif;
                                $html .= '</div>
                                </td>
                            </tr>';
                            endforeach;
                        endif;
                    endforeach;
                endif;
            }
        } else {
            $html .= '<tr>
                <td colspan="6">Không có dữ liệu</td>
            </tr>';
        }
        echo $html;
    }

    function loadCate()
    {
        $datagetCateQC = $this->site->getCateQC();
        $dataCateQC = [];
        if ($datagetCateQC['status'] == 'OK' && $datagetCateQC['errorCode'] == 200) {
            $dataCateQC = $datagetCateQC['result'];
        }
        $this->data['dataCateQC'] = $dataCateQC;
        echo json_encode($this->data);
    }

    function get_item_qc()
    {
        $dataItemQC = $this->site->getItemQC();
        if ($dataItemQC['status'] == 'OK' && $dataItemQC['errorCode'] == 200) {
            $data = $dataItemQC['result'];
        } else $data = [];

        $sOutput = array
        (
            'data' => $data,
        );
        echo json_encode($sOutput);
    }

    function deleteItemQC()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $row_id = $this->input->post("row_id");
            $dataQC = $this->site->deleteItemQCModel($row_id);
            if ($dataQC['status'] == 'OK' && $dataQC['errorCode'] == 200) {
                $dataResult = $dataQC['result'];
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Xóa dữ liệu thành công',
                    'dataResult' => $dataResult
                );
            }
        }

        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function deleteFormulaQCDetail()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $row_id = $this->input->post("row_id");
            $data = array(
                'row_id' => $row_id,
            );
            $dataQC = $this->site->deleteFormulaQCDetailModel($data);
            if ($dataQC['status'] == 'OK' && $dataQC['errorCode'] == 200) {
                $dataResult = $dataQC['result'];
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Xóa dữ liệu thành công',
                    'dataResult' => $dataResult
                );
            }
        }

        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function action_edit_qc_template()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $formtype = $this->input->post("formtype");
            $id = $this->input->post("id_item_qc");
            $id_qc_template = $this->input->post("id_qc_template");

            if (is_array($id)) {
                $idArray = array_unique($id);
                foreach ($idArray as $key => $strArray) {
                    $array = explode('#', $strArray);
                    $data = array(
                        'item_qc_id' => $array[0],
                        'formula_id' => $id_qc_template,
                        'point' => $array[1]
                    );
                    $this->site->addFormulaQCDetailModel($data);
                }
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Thành công',
                    'formtype' => $formtype
                );
            }
        }

        $this->data["dataReturn"] = $dataReturn;
        echo json_encode($this->data);
    }

    function submit_GroupQC()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $formtype = $this->input->post("formtype");
            $name_group = $this->input->post("name_group");

            $data = array(
                'name' => $name_group,
                'code' => slug($name_group, ''),
                'point' => 0
            );

            if ($formtype == 'edit') {
                $data['row_id'] = $this->input->post('id_qc_template');
                $dataReturn = $this->site->updateFormulaQCModel($data);
                if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật nhóm đánh giá thành công',
                        'formtype' => $formtype
                    );
                }
            } else if ($formtype == 'create') {
                $dataReturn = $this->site->addCateQCModel($data);
                if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Thêm nhóm đánh giá thành công',
                        'formtype' => $formtype
                    );
                }
            }
        }
        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function submit_ItemQC()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $formtype = $this->input->post("formtype");
            $cate_code_modal = $this->input->post("cate_code_modal");
            $item_name = $this->input->post("item_name");
            $description = $this->input->post("description");
            $point = $this->input->post("point");
            $id_qc_template = $this->input->post("id_qc_template");
            $cate_code = $this->input->post("cate_code");

            $data = array(
                'cate_code_modal' => $cate_code_modal,
                'item_name' => $item_name,
                'description' => $description,
                'point' => $point
            );

            if ($formtype == 'edit') {
                $data['row_id'] = $this->input->post('row_id');
                $dataReturn = $this->site->updateItemQCModel($data);
                if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật đánh giá thành công',
                        'formtype' => $formtype,
                        'url' => base_url('categories/edit_qc_template/' . $id_qc_template . '?cate_code=' . $cate_code)
                    );
                }
            } else if ($formtype == 'create') {
                $dataReturn = $this->site->addItemQCModel($data);
                if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Thêm đánh giá thành công',
                        'formtype' => $formtype,
                        'url' => base_url('categories/edit_qc_template/' . $id_qc_template . '?cate_code=' . $cate_code)
                    );
                }
            }
        }
        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    // function users(){
    //     // $shop_type  = $this->input->get('shoptype') ? $this->input->get('shoptype') : 'All';
    //     // $province_code = $this->input->get('province_code') ? $this->input->get('province_code'): 'All';
    //     // $district_code = $this->input->get('district_code') ? $this->input->get('district_code'): 'All';
    //     // $shopmodel = $this->input->get('shopmodel') ? $this->input->get('shopmodel'): '00';
    //     // $this->data['shop_type']    = $shop_type;
    //     // $this->data['province_code']= $province_code;
    //     // $this->data['district_code']= $district_code;
    //     // $this->data['shopmodel']    = $shopmodel;

    //     // $this->data_render($flag_listShop=false, $flag_getShopType=true, $flag_listProvince=true, $province_code, $flag_listDistrict=true, $addAllShop=true);
    //     // echo $this->data['m']; exit;

    //     $bc = array(array('link' => '#', 'page' => lang('list')));
    //     $meta = array('page_title' => lang('list'), 'bc' => $bc);
    //     $this->page_construct('categories/users/index', $this->data, $meta);
    // }

    function get_users()
    {
        // log_message("debug", "get_users start: " . print_r($this->input->post('start'), true));

        // log_message("debug", "get_users length: " . print_r($this->input->post('length'), true));

        $col = array(
            0 => 'USER_ID',
            1 => 'SHOP_CODE',
            2 => 'USER_NAME',
            3 => 'USER_TEL',
            4 => 'USER_EMAIL',
            5 => 'GROUP_ID'
        );
        $dataPost = array(
            'value' => '',
            'limit' => 'All',
            'page' => 0
        );
        $resultAPI = $this->site->getUser($dataPost);
        $recordsTotal = $resultAPI['total'];
        $recordsFiltered = $resultAPI['totalSearch'];

        if (!empty($this->input->post('search')['value'])) {
            $dataPost['value'] = $this->input->post('search')['value'];
        }
        $resultAPI = $this->site->getUser($dataPost);
        $recordsTotal = $resultAPI['total'];

        $dataPost['limit'] = $this->input->post('length');
        $dataPost['col_name'] = $col[$this->input->post('order')[0]['column']];
        $dataPost['dir'] = $col[$this->input->post('order')[0]['dir']];


        $dataPost['page'] = $this->input->post('start');
        $resultAPI = $this->site->getUser($dataPost);

        $data = [];
        if (!empty($resultAPI['data'])) {
            foreach ($resultAPI['data'] as $key => $value) {
                $subdata = array();
                $subdata[] = $value['USER_ID'];
                $subdata[] = $value['SHOP_CODE'];
                $subdata[] = $value['USER_NAME'];
                $subdata[] = $value['USER_TEL'];
                $subdata[] = $value['USER_EMAIL'];
                $subdata[] = $value['GROUP_ID'];

                $data[] = $subdata;
            }

            $recordsTotal = $resultAPI['total'];
            $recordsFiltered = $resultAPI['totalSearch'];
        }

        // $columns = array(
        //     array( 'db' => 'first_name', 'dt' => 0 ),
        //     array( 'db' => 'last_name',  'dt' => 1 ),
        //     array( 'db' => 'position',   'dt' => 2 ),
        //     array( 'db' => 'office',     'dt' => 3 ),
        //     array(
        //         'db'        => 'start_date',
        //         'dt'        => 4,
        //         'formatter' => function( $d, $row ) {
        //             return date( 'jS M y', strtotime($d));
        //         }
        //     ),
        //     array(
        //         'db'        => 'salary',
        //         'dt'        => 5,
        //         'formatter' => function( $d, $row ) {
        //             return '$'.number_format($d);
        //         }
        //     )
        // );

        $sOutput = array
        (
            'draw' => intval($this->input->post('draw')),
            'recordsTotal' => intval($recordsTotal),
            'recordsFiltered' => intval($recordsFiltered),
            'data' => $data,
            // 'columns' => $columns
        );
        echo json_encode($sOutput);
    }

    function qc_items()
    {
        // $check = check_permission( $this->session->userdata('sys_menu'), $this->data['m'], 2, 'Read', $this->data['v'], 'Read' );

        $bc = array(array('link' => '#', 'page' => 'Sản phẩm'));
        $meta = array('page_title' => 'Sản phẩm', 'bc' => $bc);
        $this->page_construct('categories/items', $this->data, $meta);
    }

    function recipe()
    {
        // $check = check_permission( $this->session->userdata('sys_menu'), $this->data['m'], 2, 'Read', $this->data['v'], 'Read' );

        $bc = array(array('link' => '#', 'page' => 'Công thức tiêu hao'));
        $meta = array('page_title' => 'Công thức tiêu hao', 'bc' => $bc);
        $this->page_construct('categories/recipe', $this->data, $meta);
    }

    function prices()
    {
        $shop_type = $this->input->get('shoptype') ? $this->input->get('shoptype') : 'All';
        $province_code = $this->input->get('province_code') ? $this->input->get('province_code') : 'All';
        $district_code = $this->input->get('district_code') ? $this->input->get('district_code') : 'All';
        $shopmodel = $this->input->get('shopmodel') ? $this->input->get('shopmodel') : $this->session->userdata('shopModel');
        $status = $this->input->get('status') ? $this->input->get('status') : 'All';
        $this->data['shop_type'] = $shop_type;
        $this->data['province_code'] = $province_code;
        $this->data['district_code'] = $district_code;
        $this->data['shopmodel'] = $shopmodel;
        $this->data['status'] = $status;
        $bc = array(
            array('link' => '#', 'page' => 'Danh mục'),
            array('link' => '#', 'page' => 'Bảng giá')
        );
        $meta = array('page_title' => 'Bảng giá', 'bc' => $bc);
        $this->page_construct('categories/prices', $this->data, $meta);
    }

}
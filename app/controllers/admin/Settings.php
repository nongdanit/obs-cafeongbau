<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Backend_Controller
{

    function __construct() {
        parent::__construct();

        // if (!$this->loggedIn) {
        //     redirect('login');
        // }
        $this->load->model('site');

    }

    function getDistrict(){
        $province        = $this->input->get("province");
        if(array_key_exists($province, $this->session->userdata('districts'))){
            echo json_encode($this->session->userdata('districts')[$province]);
        }else{
            $getDistrict     = $this->site->getDistrict($province);
            $listDistrict    = [];
            if($getDistrict['status'] == 'OK' && $getDistrict['errorCode'] == 200){
                $listDistrict= $getDistrict['result'];
            }
            $this->data['resultDistrictAPI'] = $listDistrict;
            echo json_encode($this->data);
        }       
    }

    function getWard(){
        $district       = $this->input->get("district");
        $getWard        = $this->site->getWard($district);
        $listWard       = [];
        if($getWard['status'] == 'OK' && $getWard['errorCode'] == 200){
            $listWard   = $getWard['result'];
        }
        $this->data['resultWardAPI'] = $listWard;
        echo json_encode($this->data);
    }

    function StoreConfig(){
        $bc = array(array('link' => '#', 'page' => 'Cài đặt chấm công'));
        $meta = array('page_title' => 'Cài đặt chấm công', 'bc' => $bc);
        $this->page_construct('settings/index', $this->data, $meta);
    }

    function get_list_shop()
    {
        $data   = [];
        $resultAPI = $this->site->getListOBSShop();

        if(!empty($resultAPI)){
            if($resultAPI['status'] == 'OK' && $resultAPI['errorCode'] == 200){
                $data = $resultAPI['result'];
            }
        }
        $sOutput = array
        (
          'data'                => $data,
        );
        echo json_encode($sOutput);
    }

    function updatestoreconfig(){
        $dataReturn = array(
            'error'     => true,
            'message'   => 'Không có dữ liệu',
        );
        $dataPost           = $this->input->post('arr');
        if(!empty($dataPost)){
            // $shop_id            = $this->input->post('shop_id');
            // $check_location     = $this->input->post('check_location');
            // $max_distance       = $this->input->post('max_distance');
            $resultAPI         = $this->site->updateStoreConfigModel($dataPost);
            if($resultAPI['status'] == 'OK' && $resultAPI['errorCode'] == 200){
                $sOutput = array
                (
                    'error'     => false,
                    'message'   => 'Cập nhật thành công',
                    'dataReturn'=> $dataPost,
                );
                echo json_encode($sOutput);
            }
        }
    }

    function powerbi(){
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopType = 'All';
        $groupCode = 'All';
        $provinceCode = 'All';
        $districtCode = 'All';

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['page_title'] = $this->lang->line("sales_report");
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['groupCode'] = $groupCode;
        $bc = array(array('link' => '#', 'page' => 'Thiết lập'), array('link' => '#', 'page' => 'Power Bi'));
        $meta = array('page_title' => 'Power Bi', 'bc' => $bc);
        $this->page_construct('settings/powerbi', $this->data, $meta);
    }


}

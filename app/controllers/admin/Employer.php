<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Employer extends Backend_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->loggedIn) {
            redirect('login');
        }
        $this->lang->load('auth', $this->Settings->language);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->model('auth_model');
        $this->load->library('ion_auth');
    }


    function profile($id = NULL) {
        if (!$id || empty($id)) {
            redirect('welcome');
        }

        $this->data['title'] = lang('profile');
        $this->data['csrf'] = $this->_get_csrf_nonce();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'class' => 'form-control',
            'type' => 'password',
            'value' => ''
            );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'class' => 'form-control',
            'type' => 'password',
            'value' => ''
            );
        $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
        $this->data['old_password'] = array(
            'name' => 'old',
            'id' => 'old',
            'class' => 'form-control',
            'type' => 'password',
            );
        $this->data['new_password'] = array(
            'name' => 'new',
            'id' => 'new',
            'type' => 'password',
            'class' => 'form-control',
            'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
        $this->data['new_password_confirm'] = array(
            'name' => 'new_confirm',
            'id' => 'new_confirm',
            'type' => 'password',
            'class' => 'form-control',
            'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
        $this->data['user_id'] = array(
            'name' => 'user_id',
            'id' => 'user_id',
            'type' => 'hidden',
            'value' => $this->session->userdata('userId'),
            );

        $this->data['page_title'] = lang('profile');
        $bc = array(array('link' => '#', 'page' => lang('users')), array('link' => '#', 'page' => lang('profile')));
        $meta = array('page_title' => lang('profile'), 'bc' => $bc);
        $this->page_construct('employer/profile', $this->data, $meta);
    }

    function change_password() {
        // if ( ! $this->loggedIn) {
        //     redirect('login');
        // }
        $this->form_validation->set_rules('old_password', lang('old_password'), 'required');
        $this->form_validation->set_rules('new_password', lang('new_password'), 'required');//|max_length[20]
        $this->form_validation->set_rules('new_password_confirm', lang('confirm_password'), 'required|matches[new_password]');


        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('employer/profile/'.$this->session->userdata('userId').'/#cpassword');
        } else {
            $url = '/api/users/updatePassword';
            $dataPost = array(
                'userId'   => $this->session->userdata('userId'),
                'password'  => $this->input->post('old_password'),
                'newPassword'  => $this->input->post('new_password')
            );

            $resultAPI = $this->data_lib->execute($url, 'PUT', $dataPost);

            if( !empty($resultAPI['success']) && $resultAPI['success'] ){
                $this->session->set_flashdata('message', 'Đổi mật khẩu thành công, vui lòng đăng nhập lại');
                redirect('employer/profile/'.$this->session->userdata('userId').'/#cpassword');
            } else {
                $this->session->set_flashdata('error', 'Đã có lỗi xảy ra, vui lòng thử lại');
                redirect('employer/profile/'.$this->session->userdata('userId').'/#cpassword');
            }
        }
    }

    function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

}

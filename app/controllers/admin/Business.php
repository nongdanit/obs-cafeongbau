<?php defined('BASEPATH') or exit('No direct script access allowed');

class Business extends Backend_Controller
{
    function __construct()
    {
        parent::__construct();
        // if (!$this->loggedIn) {
        //     redirect('login');
        // }

        $this->lang->load('auth', $this->Settings->language);
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->model('auth_model');
        $this->load->library('ion_auth');
        $this->load->model('Utilities');
    }

    private function resetSession()
    {
        if ($this->session->has_userdata('image_competitors_new')) {
            $arrImage = $this->session->userdata('image_competitors_new');
            foreach ($arrImage as $key => $value) {
                @unlink(COMPETITORS . $value);
            }
            $this->session->unset_userdata('image_competitors_new');
        }
        @$this->session->unset_userdata('image_competitors_del');
        @$this->session->unset_userdata('image_competitors');
        @$this->session->unset_userdata('image_competitors_current');
    }

    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    /**
     * [Order]
     */
    function approve_order()
    {
        $fromDate = '01-' . date("m-Y");
        $toDate = date("d-m-Y");
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $state = 'pending';
        $type = 'approve';
        $group_id = $this->session->userdata('groupId');
        $role = $this->session->userdata('role');
        $erpPrices = [];
        $erpOrg = $orderTypeInv = [];

        $response = $this->Utilities->getErpPrice();

        foreach ($response['data'] as $price) {
            // $erpPrices[$price['listHeaderId']] = $price['name'];
            $erpPrices[$price['rowid']] = $price['name'];
        }
        $response = $this->Utilities->getErpOrg();
        foreach ($response['data'] as $org) {
            $erpOrg[$org['orgCode']] = $org['orgName'];
        }
        foreach ($this->session->userdata('order_type_inv') as $value) {
            $orderTypeInv[$value['gc_code']] = $value['gen_code'];
        }

        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['state'] = $state;
        $this->data['type'] = $type;
        $this->data['shopModel'] = $shopModel;
        $this->data['group_id'] = $group_id;
        $this->data['erpPrices'] = $erpPrices;
        $this->data['erpOrg'] = $erpOrg;
        $this->data['typeFilter']     = 0;
        $this->data['orderType'] = 'All';
        $this->data['orderTypeInv'] = $orderTypeInv;
        // $this->data['shopModelArray']       = $shopModelArray;

        //$bc = array(array('link' => '#', 'page' => lang('approve_order')));
        $bc = array(
            array('link' => site_url('business/approve_order'), 'page' => lang('business')),
            array('link' => '#', 'page' => lang('approve_order'))
        );
        $meta = array('page_title' => lang('approve_order'), 'bc' => $bc);
        $this->page_construct('business/approveOrder', $this->data, $meta);
    }

    function get_order()
    {

        $fromDate = $this->input->get('fromDate') ? $this->input->get('fromDate') : date("Ym") . '01';
        $toDate = $this->input->get('toDate') ? $this->input->get('toDate') : date("Ymd");
        $shop_code = $this->input->get('shop_code') ? $this->input->get('shop_code') : NULL;
        $status = NULL !== $this->input->get('status') ? $this->input->get('status') : 1;
        $model = $this->input->get('model') ? $this->input->get('model') : NULL;
        $type = $this->input->get('type') ? $this->input->get('type') : 1;
        //echo $status;die;
        $resultAPI = $this->site->getOrderList(['shop_code' => $shop_code, 'fromDate' => $fromDate, 'toDate' => $toDate, 'status' => $status, 'model' => $model, 'type' => $type]);

        if (isset($resultAPI)) {
            $data = $resultAPI;
        } else {
            $data = [];
        }

        $sOutput = array
        (
            'data' => $data,
        );

        echo json_encode($sOutput);
    }

    function updateOrderDetail()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
        );
        if ($this->input->post()) {
            $idl_id = @$this->input->post("idl_id");
            $inv_id = $this->input->post("inv_id");
            $type = $this->input->post("type");
            $code = $this->input->post("code");
            $name = $this->input->post("name");
            $unit = $this->input->post("unit");
            $qty_order = $this->input->post("qty_order");
            $price = $this->input->post("price");
            if ($type == 1) {

                $qty_approve = $this->input->post("qty_approve");
                if (!empty($idl_id)) {
                    if ($this->input->post("qty_approve") != NULL) {
                        $data = array(
                            'idl_id' => $idl_id,
                            'qty_approve' => $qty_approve,
                            'isApprove' => 1
                        );
                        $dataOrderDetail = $this->site->updateOrderDetailModel($data);
                    } else {
                        $data = array(
                            'idl_id' => $idl_id,
                            'qty_approve' => $qty_order,
                            'isApprove' => 2
                        );
                        $dataOrderDetail = $this->site->updateOrderDetailModel($data);
                    }

                } else {
                    if ($this->input->post("qty_approve") != NULL) {
                        $data = array(
                            'idl_id' => 0,
                            'inv_id' => $inv_id,
                            'mtl_code' => $code,
                            'mtl_name' => $name,
                            'unit_code' => $unit,
                            'price' => str_replace(',', '', $price),
                            'qty_order' => 0,
                            'qty_approve' => $qty_approve
                        );
                    } else {
                        $data = array(
                            'idl_id' => 0,
                            'inv_id' => $inv_id,
                            'mtl_code' => $code,
                            'mtl_name' => $name,
                            'unit_code' => $unit,
                            'price' => str_replace(',', '', $price),
                            'qty_order' => $qty_order,
                            'qty_approve' => $qty_order
                        );
                    }

                    //log_message("debug", "insertOrderDetail: " . print_r($data, true));
                    // die;
                    $dataOrderDetail = $this->site->insertOrderDetail($data);
                }
            } else {
                $qty_delivery = $this->input->post("qty_delivery");
                if (!empty($idl_id)) {
                    $data = array(
                        'idl_id' => $idl_id,
                        'qty_delivery' => $qty_delivery,
                    );
                    $dataOrderDetail = $this->site->updateDeliveryDetailModel($data);
                }
            }


            if (!empty($dataOrderDetail)) {
                if (!empty($dataOrderDetail['message']) && $dataOrderDetail['message'] == 'OK') {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật thành công'
                    );
                }
            }

            $this->data["dataReturn"] = $dataReturn;
            echo json_encode($this->data);
        }
    }

    function saveGridTableDelivery()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
            'data' => null
        );
        if ($this->input->post()) {
            $post = $this->input->post('arr');
            if (!empty($post)) {
                $idl_id = @$post["idl_id"];
                $qty_delivery = $post["qty_delivery"];
                $type = $post["type"];
                if ($type == 2 && !empty($idl_id)) {
                    $data = array(
                        'idl_id' => $idl_id,
                        'qty_delivery' => $qty_delivery
                    );
                    $dataOrderDetail = $this->site->updateDeliveryDetailModel($data);
                }
            } else {
                $idl_id = @$this->input->post("idl_id");
                $inv_id = $this->input->post("inv_id");
                $type = $this->input->post("type");
                $code = $this->input->post("code");
                $name = $this->input->post("name");
                $unit = $this->input->post("unit");
                $qty_order = $this->input->post("qty_order");
                $price = $this->input->post("price");
                if ($type == 1) {
                    $qty_approve = $this->input->post("qty_approve");
                    if (!empty($idl_id)) {
                        $data = array(
                            'idl_id' => $idl_id,
                            'qty_approve' => $qty_approve
                        );
                        $dataOrderDetail = $this->site->updateOrderDetailModel($data);
                    } else {
                        $data = array(
                            'idl_id' => 0,
                            'inv_id' => $inv_id,
                            'mtl_code' => $code,
                            'mtl_name' => $name,
                            'unit_code' => $unit,
                            'price' => str_replace(',', '', $price),
                            'qty_approve' => $qty_approve
                        );
                        $dataOrderDetail = $this->site->insertOrderDetail($data);
                    }
                } else {
                    $qty_delivery = $this->input->post("qty_delivery");
                    if (!empty($idl_id)) {
                        $data = array(
                            'idl_id' => $idl_id,
                            'qty_delivery' => $qty_delivery
                        );
                        $dataOrderDetail = $this->site->updateDeliveryDetailModel($data);
                    }
                }
            }
            if (!empty($dataOrderDetail)) {
                if (!empty($dataOrderDetail['message']) && $dataOrderDetail['message'] == 'OK') {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật thành công',
                        'data' => $dataOrderDetail
                    );
                }
            }

            $this->data["dataReturn"] = $dataReturn;
            echo json_encode($this->data);
        }
    }

    function approveOrder()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
        );
        $id = $this->input->get('id');
        $shop_model = $this->input->get('shop_model');
        $status = $this->input->get('status');
        //log_message("debug", "pass to: " . print_r( $this->input->get('status'), true));
        // if($status != 0 && $status != 3){
        //     if($status  == 1) {
        //         $status = 5;
        //     }else if($status  == 5){
        //         $status  = 6;
        //     }
        //     // else{
        //     //     if($shop_model === 'CSH'){
        //     //         $status = ($status<ORDER_APPROVE)?ORDER_APPROVE:$status;
        //     //     }else{
        //     //         switch ($this->session->userdata('group_id')) {
        //     //             case ADMIN:
        //     //                 $status = ($status<ORDER_WAITING_ACCOUNTANT)?ORDER_WAITING_ACCOUNTANT:ORDER_APPROVE;
        //     //                 break;
        //     //             case ACCOUNTANT:
        //     //                 $status = ($status==ORDER_WAITING_ACCOUNTANT)?ORDER_APPROVE:$status;
        //     //                 break;
        //     //             case ONGBAU:
        //     //             case DICHVUKHACHHANG:
        //     //             case TTPP:
        //     //                 $status = ($status<ORDER_WAITING_ACCOUNTANT)?ORDER_WAITING_ACCOUNTANT:$status;
        //     //                 break;
        //     //             default:
        //     //                 break;
        //     //         }
        //     //     }
        //     // }
        // }       
        $data = array(
            'id' => $id,
            'status' => $status
        );
        //log_message("debug", "aaaa: " . print_r($data, true));
        $dataApproveOrder = $this->site->approveOrder($data);
        if (!empty($dataApproveOrder)) {
            if (!empty($dataApproveOrder['message']) && $dataApproveOrder['message'] == 'OK') {
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Cập nhật trạng thái thành công'
                );
            }
        }
        $this->data["dataReturn"] = $dataReturn;
        echo json_encode($this->data);
    }

    function getAddressStore()
    {
        $shopid = $this->input->get("shopid");
        $listShop = $this->session->userdata('shops');
        $fulladdress = 'OBS';
        $array_search = array_search($shopid, array_column($listShop, 'SHOP_CODE'));
        if ($array_search !== false) {
            $fulladdress = $listShop[$array_search]['FULL_ADDRESS'];
        }
        $this->data['fulladdress'] = $fulladdress;
        echo json_encode($this->data);
    }


    function delivery()
    {
        $fromDate = '01-' . date("m-Y");
        $toDate = date("d-m-Y");
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $state = 'notdelivery';
        $type = 'delivery';


        // $fromDate   = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ym").'01';
        // $toDate     = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        // $shop_code  = $this->input->get('shop_code') ? $this->input->get('shop_code') : $this->session->userdata('shop_code');
        // $status  = $this->input->get('status') ? $this->input->get('status') : 5;
        // $model   = $this->input->get('model') ? $this->input->get('model') : $this->session->userdata('shop_model');
        // $group_id = $this->session->userdata('group_id');
        // $role = $this->session->userdata('role');
        // $orderStatusArray = array(
        //     -1 => 'Tất cả',
        //     5  => 'Chưa giao ',
        //     6  => 'Đã giao',
        // );
        // $shopModelArray = array(
        //     '' => 'Tất cả',
        //     'CSH'  => 'Chủ sở hữu',
        //     'NQ'  => 'Nhượng quyền',

        // );

        // $myShopModel = $this->session->userdata('shop_model');
        // //echo $myShopModel;die;
        // switch ($myShopModel) {
        //     case '02':
        //         $model = 'NQ';
        //         $shopModelArray = array('NQ'  => 'Nhượng quyền');
        //         break;
        //     case '01':
        //         $model = 'CSH';
        //         $shopModelArray = array('CSH'  => 'Chủ sở hữu');
        //         break;
        // }
        // if(null !== $role){
        //     switch ($role) {
        //         case 'BAC':
        //         case 'TRUNG':
        //         case 'NAM':
        //             $model = 'CSH';
        //             $shopModelArray = array('CSH'  => 'Chủ sở hữu');
        //             break;                  
        //     }
        // }
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['state'] = $state;
        $this->data['shopModel'] = $shopModel;
        $this->data['type'] = $type;
        $this->data['typeFilter']     = 1;


        $bc = array(
            array('link' => site_url('business/delivery'), 'page' => lang('business')),
            array('link' => '#', 'page' => lang('deliverys'))
        );
        $meta = array('page_title' => lang('deliverys'), 'bc' => $bc);
        $this->page_construct('business/delivery', $this->data, $meta);
    }

    function get_order_delivery()
    {
        $fromDate = $this->input->get('fromDate') ? $this->input->get('fromDate') : date("Ym") . '01';
        $toDate = $this->input->get('toDate') ? $this->input->get('toDate') : date("Ymd");
        $shop_code = $this->input->get('shop_code') ? $this->input->get('shop_code') : NULL;
        $status = $this->input->get('status') ? $this->input->get('status') : 5;
        $model = $this->input->get('model') ? $this->input->get('model') : NULL;
        $resultAPI = $this->site->getOrderDeliveryModel(['shop_code' => $shop_code, 'fromDate' => $fromDate, 'toDate' => $toDate, 'status' => $status, 'model' => $model]);

        if (isset($resultAPI)) {
            $data = $resultAPI;
        } else {
            $data = [];
        }

        $sOutput = array
        (
            'data' => $data,
        );

        echo json_encode($sOutput);
    }

    function getDeliveryDetails()
    {
        if ($this->input->get()) {
            $idParent = $this->input->get('idParent');
            $type = $this->input->get('type');
            if (!empty($idParent)) {
                $dataDeliveryDetail = $this->site->getDeliveryDetailsModel($idParent);
                if (!empty($dataDeliveryDetail) && is_array($dataDeliveryDetail)) {
                    $dataDetail = [];
                    $check = true;
                    if ($type != 'all') {
                        $j = 0;
                        foreach ($dataDeliveryDetail as $key => $valOrderDetail) {
                            if ($valOrderDetail['IDL_ID'] != 0) {
                                $dataDetail[$j] = $valOrderDetail;
                                $j++;
                            }
                        }
                        if (count($dataDetail) > 0) {
                            $dataDeliveryDetail = $dataDetail;
                            $check = false;
                        }
                    }
                    $sOutput = array
                    (
                        'data' => $dataDeliveryDetail,
                        'check' => $check,
                        'type' => $type
                    );
                    echo json_encode($sOutput);
                } else {
                    $dataReturn = array(
                        'error' => true,
                        'message' => 'Không có dữ liệu',
                    );
                    echo json_encode($dataReturn);
                }
            }
        }
    }

    //Employer
    function employee($action = '', $row_id = NULL)
    {
        if ($action == 'create') {
            $this->create_employee();
        } elseif ($action == 'edit') {
            $this->edit_employee($row_id);
        } elseif ($action == 'delete') {
            $this->delete_employee($row_id);
        } elseif ($action == 'submitCalendarEmployee') {
            $this->submitCalendarEmployee();
        } else {
            $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
            $option = $this->input->get('option') ? $this->input->get('option') : 'LIST';

            if ($shopCode == 'All') $shopCode = 'HEAD_OFFICE';
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

            // $this->data['listShop']     = $gp;
            $this->data['shopCode'] = $shopCode;
            $this->data['option'] = $option;
            //$bc = array(array('link' => '#', 'page' => lang('employer')));
            $bc = array(
                array('link' => site_url('business/employee'), 'page' => lang('business')),
                array('link' => '#', 'page' => lang('employer'))
            );
            $meta = array('page_title' => lang('employer'), 'bc' => $bc);
            $this->data['page_title'] = lang('employer');
            $this->page_construct('employer/index', $this->data, $meta);
        }

    }

    // function get_employee(){
    //     $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
    //     $option = $this->input->get('option') ? $this->input->get('option') : 'LIST';
    //     $data   = [];
    //     if($shopCode == 'All') $shopCode = 'HEAD_OFFICE';
    //     if($option == 'LIST'){
    //         $resultAPI = $this->site->getEMPLOYERByShopCode(['shop_code' => $shopCode]);
    //     }else{
    //         $resultAPI = $this->site->getListEmpDelByShopCode(['shop_code' => $shopCode]);
    //     }

    //     if(!empty($resultAPI)){
    //         if($resultAPI['status'] == 'OK' && $resultAPI['errorCode'] == 200){
    //             $data = $resultAPI['result'];
    //         }
    //     }
    //     $sOutput = array
    //     (
    //       'data'                => $data,
    //     );
    //     echo json_encode($sOutput);
    // }

    public function fullnameRegex($text)
    {
        if (preg_match('/^[a-z0-9]+$/', $text)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function create_employee()
    {
        if (!check_permission('employee:insert')) {
            $this->session->set_flashdata('error', 'Permission denied.');
            redirect('welcome');
        }
        $this->data['title'] = lang('add_user');
        $this->form_validation->set_rules('tel', lang("phone"), 'trim|required|numeric|min_length[10]|max_length[20]');

        $this->form_validation->set_rules('first_name', 'Tên', 'trim|xss_clean|required|min_length[2]|max_length[50]');

        $this->form_validation->set_rules('last_name', 'Họ', 'trim|xss_clean|required|min_length[2]|max_length[50]');

        $this->form_validation->set_rules('title', 'Chức danh', 'trim|xss_clean|required|min_length[4]|max_length[50]');

        $this->form_validation->set_rules('passport', 'Chứng minh nhân dân', 'numeric|min_length[5]|max_length[20]|trim');

        // $this->form_validation->set_rules('password', lang('password'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');

        // $this->form_validation->set_rules('confirm_password', lang('confirm_password'), 'required');

        $listEmpType = $this->site->getListEmpType();
        $listEmpTypeArr = [];
        if (!empty($listEmpType)) {
            if ($listEmpType['status'] == 200) {
                foreach ($listEmpType['metadata'] as $key => $value) {
                    $listEmpTypeArr[$value['id']] = $value['name'];
                }
            }
        }
        $this->data['listEmpTypeArr'] = $listEmpTypeArr;
        if ($this->form_validation->run() == true) {
            $additional_data = array(
                'employee_code' => $this->input->post('emp_code'),
                'department_id' => $this->input->post('type_code'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'title' => $this->input->post('title'),
                'phone' => $this->input->post('tel'),
                'address' => $this->input->post('address'),
                'id_card' => $this->input->post('passport'),
                'poi' => $this->input->post('passport_place'),
                'doi' => $this->input->post('passport_date'),
                // 'description' => $this->input->post('description'),
                // 'parent' => (!empty($this->input->post('parent'))) ? 1 : 0,
                'date_in' => $this->input->post('start_date'),
                'date_out' => $this->input->post('end_date'),
                'active' => 1
                // 'create_by' => $this->session->userdata('userId')
            );
        }
        if ($this->form_validation->run() == true && $dataReturn = $this->ion_auth->register_ntfpg($additional_data)) {
            $this->session->set_flashdata('message', lang('new_user_created'));
            redirect("business/employee/edit/" . $dataReturn['employee']['id']);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $this->data['page_title'] = lang('add_employer');
            $bc = array(array('link' => site_url('employer'), 'page' => lang('people')), array('link' => '#', 'page' => lang('add_employer')));
            $meta = array('page_title' => lang('add_employer'), 'bc' => $bc);
            $this->page_construct('employer/create_employee', $this->data, $meta);
        }
    }

    private function edit_employee($row_id = NULL)
    {
        if (!check_permission('employee:update')) {
            $this->session->set_flashdata('error', 'Permission denied.');
            redirect('welcome');
        }
        if (!$row_id || empty($row_id)) {
            redirect('welcome');
        }
        $user = $this->site->getEmployerByRowID($row_id);
        if (!empty($user)) {
            if ( $user['status'] == 200 && count($user['metadata']) == 1) {
                $dataReturn = $user['metadata'][0];
                $this->data['users'] = (isset($dataReturn)) ? $dataReturn : FALSE;
            } else redirect('business/employee');
        } else {
            redirect('business/employee');
        }
        $gp = [];
        $listShop = $this->session->userdata('shops');
        foreach ($listShop as $key => $value) {
            $gp[$value['shopCode']] = $value['shopName'];
        }
        $listEmpType = $this->site->getListEmpType();
        $listEmpTypeArr = [];
        if (!empty($listEmpType)) {
            if ($listEmpType['status'] == 200) {
                foreach ($listEmpType['metadata'] as $key => $value) {
                    $listEmpTypeArr[$value['id']] = $value['name'];
                }
            }
        }
        $this->data['listEmpTypeArr'] = $listEmpTypeArr;
        $this->data['listShop'] = $gp;
        $this->data['csrf'] = $this->_get_csrf_nonce();
        $this->form_validation->set_rules('tel', lang("phone"), 'numeric|min_length[10]|max_length[20]|trim');
        $this->form_validation->set_rules('first_name', 'Tên', 'trim|xss_clean|required|min_length[2]|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Họ', 'trim|xss_clean|required|min_length[2]|max_length[50]');

        $this->form_validation->set_rules('title', 'Chức danh', 'trim|xss_clean|required|min_length[4]|max_length[50]');

        $this->form_validation->set_rules('passport', 'Chứng minh nhân dân', 'numeric|min_length[5]|max_length[20]|trim');


        $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
        
        $this->data['dataListCalendarEmp'] = [];
        $dataListCalendarEmp = $this->site->getListCalendarEmp(['employee_id' => $row_id]);
        if (!empty($dataListCalendarEmp)) {
            if ($dataListCalendarEmp['status'] == 200) {
                $dataListCalendarEmp = $dataListCalendarEmp['metadata'];
                $this->data['dataListCalendarEmp'] = $dataListCalendarEmp;
            }
        }

        if (!empty($this->input->post('row_id'))) {
            $data = array(
                'row_id' => $this->input->post('row_id'),
                'employee_code' => $this->input->post('emp_code'),
                'department_id' => $this->input->post('type_code'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'title' => $this->input->post('title'),
                'phone' => $this->input->post('tel'),
                'address' => $this->input->post('address'),
                'id_card' => $this->input->post('passport'),
                'poi' => $this->input->post('passport_place'),
                'doi' => $this->input->post('passport_date'),
                // 'description' => $this->input->post('description'),
                // 'parent' => (!empty($this->input->post('parent'))) ? 1 : 0,
                'date_in' => $this->input->post('start_date'),
                'date_out' => $this->input->post('end_date'),
                'active' => 1
                // 'create_by' => $this->session->userdata('userId')
            );


            if ($this->input->post('new_password')) {
                $this->form_validation->set_rules('new_password', lang('new_password'), 'min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]');
                $this->form_validation->set_rules('confirm_password', lang('confirm_password'), 'required');
                $data['password'] = $this->input->post('new_password');
            }

            if ($this->form_validation->run() === TRUE && $resultAPI = $this->site->updateEmp($data)) {
                if (!empty($resultAPI)) {
                    if ($resultAPI['status'] == 200) {
                        $this->session->set_flashdata('message', lang('user_updated'));
                        redirect("business/employee");
                    } else if ($resultAPI['status'] == 'ERROR') {
                        $this->session->set_flashdata('error', lang('1106' . 'nongdanit'));
                    } else {
                        $this->session->set_flashdata('error', 'Hệ thống đang bận, vui lòng quay lại sau.');
                    }
                }
            }
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['row_id'] = $row_id;
        $bc = array(array('link' => '#', 'page' => lang('users')), array('link' => '#', 'page' => lang('employer_profile')));
        $meta = array('page_title' => lang('employer_profile'), 'bc' => $bc);
        $this->page_construct('employer/edit', $this->data, $meta);
    }

    private function submitCalendarEmployee()
    {
        $dataReturn = array(
            'error' => false,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
        );
        if (!check_permission('employee:update')) {
            $dataReturn['error'] = true;
            $dataReturn['message'] = 'Permission denied.';
            $this->data["dataReturn"] = $dataReturn;
            echo json_encode($this->data);
        } else {
            if ($this->input->post()) {
                $row_id = @$this->input->post("row_id");
                $shop_code = $this->input->post("shop_code");
                $start_date = $this->input->post("start_date");
                $end_date = $this->input->post("end_date");
                $username = $this->input->post('username');

                $data = array(
                    'row_id' => $row_id,
                    'shop_code' => $shop_code,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'username' => $username
                );
                if (!empty($row_id)) {
                    $resultAPI = $this->site->updateCalendarEmpWork($data);
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật thành công',
                    );
                } else {
                    $resultAPI = $this->site->addCalendarEmpWork($data);
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Thêm lịch thành công',
                    );
                }
                if (in_array($resultAPI['status'], [200, 201])) {
                    $dataReturn = $dataReturn;
                } else {
                    $dataReturn = array(
                        'error'     => false,
                        'message'   => $resultAPI['message'],
                    );
                }

                $this->data["dataReturn"] = array_merge($dataReturn, $data);
            }
            echo json_encode($this->data);
        }

    }

    private function delete_employee($id = NULL)
    {
        if (!check_permission('employee:delete')) {
            $this->session->set_flashdata('error', 'Permission denied.');
            redirect('welcome');
        }
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($this->site->deleteEmp(['row_id' => $id, 'update_by' => $this->session->userdata('userId')])) {
            $this->session->set_flashdata('message', lang('user_deleted'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    //Store
    function mbs_store($action = '', $id = NULL)
    {
        if ($action == 'create') {
            $this->create_stores();
        } elseif ($action == 'submit') {
            $this->submit_store();
        } elseif ($action == 'view') {
            $this->view_store($id);
        } else {
            $this->resetSession();
            # SHOP
            if ($this->session->has_userdata('imagenew')) {
                $arrImage = $this->session->userdata('imagenew');
                foreach ($arrImage as $key => $value) {
                    @unlink(SHOPS . $value);
                }
                $this->session->unset_userdata('imagenew');
            }
            @$this->session->unset_userdata('image');
            @$this->session->unset_userdata('image_del');
            @$this->session->unset_userdata('image_current');
            $status = $this->input->get('status') ? $this->input->get('status') : 'All';
            $province_code = $this->input->get('province_code') ? $this->input->get('province_code') : 'All';
            $district_code = $this->input->get('district_code') ? $this->input->get('district_code') : 'All';

            $this->data['page_title'] = lang('stores');
            $this->data['status'] = $status;
            $this->data['province_code'] = $province_code;
            $this->data['district_code'] = $district_code;
            //Danh sách tỉnh thành
            $getListProvince = $this->site->getListProvince();
            $listProvince = [];
            if ($getListProvince['status'] == 'OK' && $getListProvince['errorCode'] == 200) {
                $listProvince = $getListProvince['result'];
            }
            $this->data['listProvince'] = $listProvince;

            //Danh sách Quận/Huyện
            $getDistrict = $this->site->getDistrict($province_code);
            $listDistrict = [];
            if ($getDistrict['status'] == 'OK' && $getDistrict['errorCode'] == 200) {
                $listDistrict = $getDistrict['result'];
            }
            $this->data['listDistrict'] = $listDistrict;

            //Danh sách mô hình kinh doanh(shop_model)
            $getShopModel = $this->site->getShopModel(['status' => 1]);
            if ($getShopModel['status'] == 'OK' && $getShopModel['errorCode'] == 200) {
                $getListShopModel = $getShopModel['result'];
            }
            $this->data['listShopModel'] = $getListShopModel ? $getListShopModel : NULL;

            $dataListStatus = $this->site->getListStatus();
            if ($dataListStatus['status'] == 'OK' && $dataListStatus['errorCode'] == 200) {
                $this->data['listStatus'] = $dataListStatus['result'];
            } else $this->data['listStatus'] = [];
            //$bc = array(array('link' => '#', 'page' => lang('stores')));
            $bc = array(
                array('link' => site_url('business/mbs_store'), 'page' => lang('business')),
                array('link' => '#', 'page' => 'Khảo sát cửa hàng')
            );
            $meta = array('page_title' => lang('stores'), 'bc' => $bc);
            $this->page_construct('stores/index', $this->data, $meta);
        }

    }

    function get_stores()
    {
        $getListShop = $this->site->getListShop();
        if (isset($getListShop)) {
            if ($getListShop['status'] == 'OK' && $getListShop['errorCode'] == 200) {
                $data = $getListShop['result'];
            } else $data = [];
        } else {
            $data = [];
        }

        $sOutput = array
        (
            'data' => $data,
        );

        echo json_encode($sOutput);
    }


    private function create_stores()
    {
        $this->resetSession();
        # SHOP
        if ($this->session->has_userdata('imagenew')) {
            $arrImage = $this->session->userdata('imagenew');
            foreach ($arrImage as $key => $value) {
                @unlink(SHOPS . $value);
            }
            $this->session->unset_userdata('imagenew');
        }
        @$this->session->unset_userdata('image');
        @$this->session->unset_userdata('image_del');
        @$this->session->unset_userdata('image_current');
        // //Danh sách tỉnh thành
        // $getListProvince = $this->site->getListProvince();
        // $listProvince    = [];
        // if($getListProvince['status'] == 'OK' && $getListProvince['errorCode'] == 200){
        //     $listProvince = $getListProvince['result'];
        // }
        // $this->data['listProvince']     = $listProvince;
        // //Danh sách shop type
        $getShopType = $this->site->getShopType(['status' => 1]);
        if ($getShopType['status'] == 'OK' && $getShopType['errorCode'] == 200) {
            $getListShopType = $getShopType['result'];
        }
        $this->data['listShopType'] = $getListShopType ? $getListShopType : NULL;

        // //Danh sách mô hình kinh doanh(shop_model)
        $getShopModel = $this->site->getShopModel(['status' => 1]);
        if ($getShopModel['status'] == 'OK' && $getShopModel['errorCode'] == 200) {
            $getListShopModel = $getShopModel['result'];
        }
        $this->data['listShopModel'] = $getListShopModel ? $getListShopModel : NULL;

        $this->data['page_title'] = lang('add_store');
        $bc = array(array('link' => '', 'page' => 'Nghiệp vụ'), array('link' => '#', 'page' => lang('add_store')));
        $meta = array('page_title' => lang('add_store'), 'bc' => $bc);
        $this->page_construct('stores/create', $this->data, $meta);
    }

    private function submit_store()
    {
        $dataReturn = array(
            'error' => false,
            'message' => 'Shop mới đã được thêm',
            'url' => base_url('stores')
        );
        if ($this->input->post()) {
            $shop_name = $this->input->post("shop_name");
            $address = $this->input->post("address");
            $province_code = $this->input->post("province_code");
            $district_code = $this->input->post("district_code");
            $ward_code = $this->input->post("ward_code");
            $formtype = $this->input->post("formtype");
            $type_id = $this->input->post("type_id");
            $model_id = $this->input->post("model_id");
            $cus_name = $this->input->post("cus_name");
            $tel = $this->input->post("tel");
            $email = $this->input->post("email");
            $note_shop = $this->input->post("note_shop");
            $warehouse_id = $this->input->post("warehouse_id");
            $delivery_day_first = $this->input->post("delivery_day_first");
            $delivery_day = $this->input->post("delivery_day");
            $invoice_info = $this->input->post("invoice_info");
            $changemodel = $this->input->post("changemodel");
            $pre_model = $this->input->post("pre_model");
            $start_date = $this->input->post("start_date");

            if (isset($changemodel) && $changemodel == 0) {
                $shop_name_old = (!empty($pre_model)) ? $pre_model : "";
            } else $shop_name_old = "";

            $status = (!empty($this->input->post("status"))) ? $this->input->post("status") : OPEN;
            $shop_code = (!empty($this->input->post("shop_code"))) ? $this->input->post("shop_code") : "";

            $open_date = $this->input->post("open_date");
            if (!empty($open_date)) {
                $date = new DateTime($open_date);
                $open_date = $date->format('Y-m-d');
            } else {
                $open_date = NULL;
            }

            if (!empty($start_date)) {
                $date = new DateTime($start_date);
                $start_date = $date->format('Y-m-d');
            } else {
                $start_date = NULL;
            }

            $image = '';
            if ($this->session->has_userdata('imagenew')) {
                $imagenew = $this->session->userdata('imagenew');

                if ($this->session->has_userdata('image_current')) {
                    $image_current = $this->session->userdata('image_current');
                    $image = array_merge($image_current, $imagenew);
                } else $image = $imagenew;

                $image = array_unique($image, SORT_REGULAR);
                $dataImage = array(
                    'image' => json_encode($image)
                );
                $this->session->set_userdata($dataImage);
            }

            $image = '';
            if ($this->session->has_userdata('image')) {
                $image = $this->session->userdata("image");
            } else {
                $image = json_encode($this->session->userdata("image_current"));
            }

            $data = array(
                'shop_name' => $shop_name,
                'shop_code' => $shop_code,
                'cus_name' => (!empty($cus_name)) ? $cus_name : "",
                'tel' => (!empty($tel)) ? $tel : "",
                'email' => (!empty($email)) ? $email : "",
                'note_shop' => (!empty($note_shop)) ? $note_shop : "",
                'status' => $status,
                'address' => $address,
                'ward_code' => $ward_code,
                'district_code' => $district_code,
                'province_code' => $province_code,
                'lng' => 0.0,
                'lat' => 0.0,
                'type_id' => !empty($type_id) ? $type_id : 0,
                'open_date' => $open_date,
                'start_date' => $start_date,
                'model_id' => !empty($model_id) ? $model_id : 0,
                'image' => ($image == '[]') ? '' : $image,
                'warehouse_id' => !empty($warehouse_id) ? $warehouse_id : 0,
                'delivery_day_first' => !empty($delivery_day_first) ? $delivery_day_first : 0,
                'delivery_day' => !empty($delivery_day) ? $delivery_day : 0,
                'invoice_info' => !empty($invoice_info) ? $invoice_info : "",
                'shop_name_old' => !empty($shop_name_old) ? $shop_name_old : ""
            );

            if ($formtype == 'edit') {
                $id = $this->input->post("id");
                $getShopById = $this->site->getShopById($id);
                if ($getShopById['status'] == 'OK' && $getShopById['errorCode'] == 200) {
                    $stores = $getShopById['result'];
                }
                $this->data['stores'] = $stores ? $stores : NULL;
                if (empty($stores)) {
                    $dataReturn = array(
                        'error' => true,
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
                    );
                } else {
                    if ($this->input->post('update') !== null) {//Update shop
                        $data['row_id'] = $id;
                        $data['update_by'] = $this->session->userdata("userId");
                        $data['row_id_image'] = (empty($stores['SHOP_IMAGEs'][0]['ROW_ID'])) ? 0 : $stores['SHOP_IMAGEs'][0]['ROW_ID'];

                        #Update working detail
                        $dataUpdateWorkingDetail = [];
                        $dataUpdate = [];
                        foreach ($stores['AREA_SHOP_INFOs'] as $keyworking => $valueworking) {
                            $working_id = $valueworking['ROW_ID'];
                            $dataWorkingDetail = $this->site->getListWorkingDetailByWorkingId(['working_id' => $working_id]);
                            $flag = false;
                            $statusDetail = 1;
                            if (!empty($dataWorkingDetail)) {
                                if ($dataWorkingDetail['status'] == 'OK' && $dataWorkingDetail['errorCode'] == 200) {
                                    $getList = $dataWorkingDetail['result'];
                                    foreach ($getList as $key => $detail) {
                                        $input_type_web = json_decode($detail['INPUT_TYPE_WEB'], true);
                                        if ($input_type_web['attr']['layout'] == 1) {
                                            if ($input_type_web['type'] == 'text') {
                                                $content = $this->input->post('title_' . $detail['ROW_ID']);
                                                if (!empty($content)) {
                                                    $statusDetail = 5;
                                                    $flag = true;
                                                    if ($input_type_web['attr']['class'] == 'number') $content = str_replace(",", "", $content);

                                                    $content_json = json_encode(['value' => $content]);
                                                    $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                                }
                                            } elseif ($input_type_web['type'] == 'select') {
                                                $content = $this->input->post('input_exp_' . $detail['ROW_ID']);
                                                if (!empty($content)) {
                                                    $statusDetail = 5;
                                                    $flag = true;
                                                    $content_json = json_encode(['value' => (is_array($content)) ? $content : $content]);
                                                    $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                                }
                                            }
                                        } else {
                                            if ($input_type_web['type'] == 'text') {
                                                if (!empty($input_type_web['scope'])) {
                                                    $content = $this->input->post('title_' . $detail['ROW_ID']);
                                                    if ($input_type_web['attr']['class'] == 'number')
                                                        $content = str_replace(",", "", $content);

                                                    $content2 = $this->input->post('input_exp_' . $detail['ROW_ID']);
                                                    if (!empty($content) || !empty($content2)) {
                                                        $flag = true;
                                                        $statusDetail = 5;
                                                    }
                                                    $content_json = json_encode(['value' => $content, 'exp' => $content2]);
                                                    $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                                } else {
                                                    $content = $this->input->post('title_' . $detail['ROW_ID']);
                                                    $content2 = $this->input->post('title2_' . $detail['ROW_ID']);
                                                    if ($input_type_web['attr']['class'] == 'number') {
                                                        $content = str_replace(",", "", $content);
                                                        $content2 = str_replace(",", "", $content2);
                                                    }
                                                    if (!empty($content) || !empty($content2)) {
                                                        $flag = true;
                                                        $statusDetail = 5;
                                                    }
                                                    $content_json = json_encode(['value' => $content, 'value2' => $content2]);
                                                    $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                                }
                                            } elseif ($input_type_web['type'] == 'select') {
                                                $content = $this->input->post('input_exp_' . $detail['ROW_ID']);
                                                $note = $this->input->post('textarea_' . $detail['ROW_ID']);

                                                $statusDetail = 5;
                                                $flag = true;
                                                $content_json = json_encode(['value' => (is_array($content)) ? $content : $content]);
                                                $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'note' => !empty($note) ? $note : "", 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #END Update working detail

                        $lon = $this->input->post("lon");
                        $lat = $this->input->post("lat");
                        $data['lng'] = (!empty($lon)) ? $lon : 0.0;
                        $data['lat'] = (!empty($lat)) ? $lat : 0.0;

                        $dataUpdate = $this->site->updateShop($data, $dataUpdateWorkingDetail);
                        if (is_array($dataUpdate)) {
                            $dataReturn = array(
                                'error' => false,
                                'message' => 'Cập nhật thông tin thành công.',
                                'formtype' => $formtype,
                                'url' => base_url('business/mbs_store')
                            );
                        } else {
                            $dataReturn = array(
                                'error' => true,
                                'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
                                'dataUpdateWorkingDetail' => $dataUpdateWorkingDetail
                            );
                        }
                    }
                    if ($this->input->post('approve') !== null) { //Duyệt trạng thái
                        if ($this->site->approveWorking(['row_id' => $id, 'update_by' => $this->session->userdata("userId")])) {
                            $dataReturn = array(
                                'error' => false,
                                'message' => 'Mặt bằng đã được duyệt.',
                                'formtype' => $formtype,
                                'url' => base_url('business/mbs_store')
                            );
                        } else {
                            $dataReturn = array(
                                'error' => true,
                                'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
                            );
                        }
                    }
                }
            } else {
                $data['create_by'] = $this->session->userdata("userId");
                $insertShop = $this->site->insertShop($data);

                if (!empty($insertShop['status']) && $insertShop['status'] == 'OK' && $insertShop['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Thêm thành công.',
                        'formtype' => $formtype,
                        'url' => base_url('business/mbs_store/view/') . $insertShop['result']['ROW_ID']
                    );
                } elseif (!empty($insertShop['status']) && $insertShop['status'] == 'ERROR' && $insertShop['errorCode'] == 1104) {
                    $dataReturn = array(
                        'error' => true,
                        'message' => 'Tên mặt bằng đã tồn tại'
                    );
                } else {
                    $dataReturn = array(
                        'error' => true,
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
                    );
                }
            }
            if ($this->session->has_userdata('image_del')) {
                foreach ($this->session->userdata('image_del') as $key => $value) {
                    @unlink(SHOPS . $value);
                }
                $this->session->unset_userdata('image_del');
            }
            @$this->session->unset_userdata('image');
            @$this->session->unset_userdata('imagenew');
            @$this->session->unset_userdata('image_current');

            $this->data["dataReturn"] = array_merge($dataReturn, $data);
            echo json_encode($this->data);
        }
    }

    function upload_store()
    {
        sleep(1);
        $shopid = $this->input->post('shopid');
        if ($_FILES["files"]["name"] != '') {
            $output = '';
            $arrImage = [];
            for ($count = 0; $count < count($_FILES["files"]["name"]); $count++) {
                $config = array();
                $config['upload_path'] = SHOPS;
                $config["allowed_types"] = 'jpg|png|gif|jpeg';
                $config['max_size'] = '10048';
                $new_name = time() . $this->data_lib->inject($_FILES["files"]["name"][$count]);
                $config['file_name'] = $new_name;
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);


                $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
                $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
                $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
                $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
                $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
                if ($this->upload->do_upload('file')) {
                    $data = $this->upload->data();
                    $output .= '<div class="col-sm-3"><a shopid="' . $shopid . '" onclick="del_image(this)" name_image="' . $data["file_name"] . '" class="del_image" title="Xóa">x</a><img src="' . base_url() . URL_SHOPS . $data["file_name"] . '?r=' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" /></div>';
                    $arrImage[$count] = $data["file_name"];
                }
            }
            if (!empty($arrImage)) {
                $this->session->set_userdata(['imagenew' => $arrImage]);
            }

            if ($this->session->has_userdata('image_current')):
                $image_current = $this->session->userdata('image_current');
                foreach ($image_current as $key => $value) {
                    $output .= '<div class="col-sm-3"><a shopid="' . $shopid . '" onclick="del_image(this)" name_image="' . $value . '" class="del_image" title="Xóa">x</a><img src="' . base_url() . URL_SHOPS . $value . '?r=' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" /></div>';
                }
            endif;
            echo $output;
        }
    }

    function upload_competitors()
    {
        sleep(1);
        $competitorsid = $this->input->post('competitorsid');
        if ($_FILES["image_competitors"]["name"] != '') {
            $output = '';
            $arrImage = [];
            for ($count = 0; $count < count($_FILES["image_competitors"]["name"]); $count++) {
                $config = array();
                $config['upload_path'] = COMPETITORS;
                $config["allowed_types"] = 'jpg|png|gif|jpeg';
                $config['max_size'] = '10048';
                $new_name = time() . $this->data_lib->inject($_FILES["image_competitors"]["name"][$count]);
                $config['file_name'] = $new_name;
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);


                $_FILES["file"]["name"] = $_FILES["image_competitors"]["name"][$count];
                $_FILES["file"]["type"] = $_FILES["image_competitors"]["type"][$count];
                $_FILES["file"]["tmp_name"] = $_FILES["image_competitors"]["tmp_name"][$count];
                $_FILES["file"]["error"] = $_FILES["image_competitors"]["error"][$count];
                $_FILES["file"]["size"] = $_FILES["image_competitors"]["size"][$count];
                if ($this->upload->do_upload('file')) {
                    $data = $this->upload->data();
                    $output .= '
                    <div class="col-sm-3">
                    <a competitorsid="' . $competitorsid . '" onclick="del_image_competitors(this)" abc="' . $data["file_name"] . '" name_img="' . $data["file_name"] . '" class="del_image" title="Xóa">x</a>
                    <img src="' . base_url() . URL_COMPETITORS . $data["file_name"] . '?r=' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" />
                    </div>
                    ';
                    $arrImage[$count] = $data["file_name"];
                }
            }
            if (!empty($arrImage)) {
                $this->session->set_userdata(['image_competitors_new' => $arrImage]);
            }
            if ($this->session->has_userdata('image_competitors_current')):
                $image_current = $this->session->userdata('image_competitors_current');
                foreach ($image_current as $key => $value) {
                    $output .= '<div class="col-sm-3"><a competitorsid="' . $competitorsid . '" onclick="del_image_competitors(this)" dhd="' . $value . '" name_img="' . $value . '" class="del_image" title="Xóa">x</a><img src="' . base_url() . URL_COMPETITORS . $value . '?r=' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" /></div>';
                }
            endif;
            echo $output;
        }
    }

    function loadCompetitors()
    {
        $id = $this->input->get('id');
        $dataList = $this->site->getlistCompetitors(['working_detail_id' => $id]);
        $html = '';
        if (!empty($dataList)) {
            if ($dataList['status'] == 'OK' && $dataList['errorCode'] == 200) {
                $getList = $dataList['result'];
                foreach ($getList as $key => $data) {
                    $html .= '<tr>

                        <td>' . $data['TITLE'] . '<br>
                        (SP đặc trưng: ' . $data['CONTENT'] . ')
                        </td>
                        <td>' . $this->data_lib->formatNumber($data['RANGE']) . '</td>
                        <td>' . $this->data_lib->formatNumber($data['REVENUE_OF_GUEST']) . '</td>
                        <td>' . $this->data_lib->formatNumber($data['CUSTOMER_OF_DAY']) . '</td>
                        <td>' . $this->data_lib->formatNumber($data['REVENUE_OF_DAY']) . '</td>
                        <td><a href="javascript:void(0);" id=' . $data['ROW_ID'] . ' onclick="loadCompetitor(this);return false;" title="Update" class="tip btn btn-warning btn-xs"><i class="fa fa-edit"></i></a><a href="javascript:void(0);" idx=' . $data['WORKING_DETAIL_ID'] . ' id=' . $data['ROW_ID'] . ' onclick="deleteCompetitors(this);return false;"  title="Delete" class="tip btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>';
                }
            } elseif ($dataList['status'] == 'ERROR' && $dataList['errorCode'] == 400) {
                $html .= '<tr>
                    <td colspan="6">' . lang($dataList['errorCode'] . 'nongdanit') . '</td>
                </tr>';
            }
        } else {
            $html .= '<tr>
                <td colspan="6">Không có đối thủ cạnh tranh</td>
            </tr>';
        }
        echo $html;
    }

    function checkrowID()
    {
        $dataReturn = array(
            'error' => false,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
        );
        $data = [];
        if ($this->input->post()) {
            $rowid = $this->input->post('rowid');
            $shopid = $this->input->post('shopid');
            $position = $this->input->post('position');
            $parentid = $this->input->post('parentid');

            $dataWorkings = $this->site->checkSatatusWorking(['shop_id' => $shopid, 'position' => $position - 1, 'parent_id' => $parentid]);
            if (!empty($dataWorkings)) {
                if ($dataWorkings['status'] == 'ERROR') {
                    $data = $dataWorkings['result'];
                    $txt = '';
                    foreach ($data as $key => $value) {
                        $txt .= '<b>' . $value['TITLE'] . '</b><br>';
                    }
                    $dataReturn = array(
                        'error' => true,
                        'message' => $txt . ' cần được duyệt để thực hiện thao tác này',
                    );
                }
            }
        }
        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function checkeditworkingdetail()
    {
        $dataReturn = array(
            'error' => false,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
        );
        $data = [];
        if ($this->input->post()) {
            $row_id = $this->input->post('idfather');
            $dataWorkings = $this->site->getWorking(['row_id' => $row_id]);
            if (!empty($dataWorkings)) {
                if ($dataWorkings['status'] == 'OK' && $dataWorkings['errorCode'] == 200) {
                    $data = $dataWorkings['result'];
                    if ($data['STATUS'] > CANCEL && $data['STATUS'] < DOING) {
                        $dataReturn = array(
                            'error' => true,
                            'message' => '<b>' . $data['TITLE'] . '</b> phải ở trạng thái đang xử lý mới được thực hiện thao tác này',
                        );
                    }
                }
            }
        }
        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function reset()
    {
        $this->resetSession();
        echo true;
    }

    function deleteCompetitors()
    {
        if ($this->input->post('row_id')) {
            $row_id = $this->input->post('row_id');
            $working_detail_id = $this->input->post('working_detail_id');
            $dataCompetitors = $this->site->getCompetitorsByRowID(['row_id' => $row_id]);
            if (!empty($dataCompetitors)) {
                if ($dataCompetitors['status'] == 'OK' && $dataCompetitors['errorCode'] == 200) {
                    $data = json_decode($dataCompetitors['result'][0]['IMAGE']);
                }
            }
            $dataList = $this->site->deleteCompetitors(['row_id' => $row_id, 'working_detail_id' => $working_detail_id, 'delete_by' => $this->session->userdata('userId')]);
            $html = '';

            if (!empty($dataList)) {
                if ($dataList['status'] == 'OK' && $dataList['errorCode'] == 200) {
                    if (!empty($data)) {
                        foreach ($data as $key => $value) {
                            @unlink(COMPETITORS . $value);
                        }
                    }
                    $getList = $dataList['result'];
                    foreach ($getList as $key => $data) {
                        $html .= '<tr>

                            <td>' . $data['TITLE'] . '<br>
                            (SP đặc trưng: ' . $data['CONTENT'] . ')
                            </td>
                            <td>' . $data['RANGE'] . '</td>
                            <td>' . $data['REVENUE_OF_GUEST'] . '</td>
                            <td>' . $data['CUSTOMER_OF_DAY'] . '</td>
                            <td>' . $data['REVENUE_OF_DAY'] . '</td>
                            <td><a href=' . site_url('stores/edit/$1') . ' title="Update" class="tip btn btn-warning btn-xs"><i class="fa fa-edit"></i></a><a href="javascript:void(0);" idx=' . $data['WORKING_DETAIL_ID'] . ' id=' . $data['ROW_ID'] . ' onclick="deleteCompetitors(this);return false;"  title="Delete" class="tip btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>';
                    }
                } elseif ($dataList['status'] == 'ERROR' && $dataList['errorCode'] == 400) {
                    $html .= '<tr>
                        <td colspan="6">' . lang($dataList['errorCode'] . 'nongdanit') . '</td>
                    </tr>';
                }
            } else {
                $html .= '<tr>
                    <td colspan="6">Không có đối thủ cạnh tranh</td>
                </tr>';
            }
            echo $html;
        }
    }

    function getCompetitors()
    {
        $this->resetSession();
        $id = $this->input->get('id');
        $data = [];
        $dataList = $this->site->getCompetitorsByRowID(['row_id' => $id]);
        if (!empty($dataList)) {
            if ($dataList['status'] == 'OK' && $dataList['errorCode'] == 200) {
                if (!empty($dataList['result'][0]['IMAGE'])) {
                    $dataImage = array(
                        'image_competitors_current' => json_decode($dataList['result'][0]['IMAGE'])
                    );
                    $this->session->set_userdata($dataImage);
                }
                $dataList['result'][0]['IMAGE'] = json_decode($dataList['result'][0]['IMAGE']);
                $data = $dataList['result'][0];
            }
        }
        echo json_encode($data);
    }

    function delImageSession()
    {
        $shopid = $this->input->post('shopid');
        $name_img = $this->input->post('name_img');
        if ($this->session->has_userdata('image_current')) {
            $str_array = json_encode($this->session->userdata('image_current'));
            $str_array = str_replace($name_img, "", $str_array);
            $str_array = str_replace('"",', "", $str_array);
            $str_array = str_replace(',""', "", $str_array);
            $str_array = str_replace('""', "", $str_array);
            $this->session->unset_userdata('image_current');
        } else $str_array = json_encode([]);

        $image_arr_current = json_decode($str_array);

        if ($this->session->has_userdata('imagenew')) {
            $imagenew = $this->session->userdata('imagenew');
            $this->session->unset_userdata('imagenew');
            $str_arrayNew = json_encode($imagenew);
            $str_arrayNew = str_replace($name_img, "", $str_arrayNew);
            $str_arrayNew = str_replace('"",', "", $str_arrayNew);
            $str_arrayNew = str_replace(',""', "", $str_arrayNew);
            $str_arrayNew = str_replace('""', "", $str_arrayNew);
            $str_arrayNew = json_decode($str_arrayNew);
            $this->session->set_userdata(['imagenew' => $str_arrayNew]);
            $image_arr_current = array_unique(array_merge($image_arr_current, $str_arrayNew), SORT_REGULAR);
        }

        $dataImage = array(
            'image_current' => $image_arr_current
        );
        $this->session->set_userdata($dataImage);
        # Mảng xóa
        if ($this->session->has_userdata('image_del')) {
            $m = $this->session->userdata('image_del');
            $this->session->unset_userdata('image_del');
            array_push($m, $name_img);
            $this->session->set_userdata(['image_del' => $m]);
        } else {
            $arrname_img = [$name_img];
            $this->session->set_userdata(['image_del' => $arrname_img]);
        }

        $output = "";
        if (!empty($dataImage['image_current'])):
            foreach ($dataImage['image_current'] as $key => $value) {
                $output .= '
                <div class="col-sm-3">
                <a shopid="' . $shopid . '" onclick="del_image(this)" name_image="' . $value . '" class="del_image" title="Xóa">x</a>
                <img src="' . base_url() . URL_SHOPS . $value . '" class="img-responsive img-thumbnail" data-action="zoom" />
                </div>
                ';
            }
        endif;
        echo $output;
    }

    function delImageQCSession()
    {
        $qcid = $this->input->post('qcid');
        $name_img = $this->input->post('name_img');
        if ($this->session->has_userdata('image_current_qc' . $qcid)) {
            $str_array = implode("#", $this->session->userdata('image_current_qc' . $qcid));
            $str_array = str_replace($name_img, "", $str_array);
            $str_array = str_replace('##', "#", $str_array);
            $str_array = ltrim($str_array, '#');
            $str_array = rtrim($str_array, '#');
            $this->session->unset_userdata('image_current_qc' . $qcid);
            $str_array = explode("#", $str_array);
        } else $str_array = [];

        $image_arr_current = $str_array;

        if ($this->session->has_userdata('imagenew_qc' . $qcid)) {
            $imagenew = $this->session->userdata('imagenew_qc' . $qcid);
            $this->session->unset_userdata('imagenew_qc' . $qcid);
            $str_arrayNew = implode("#", $imagenew);
            $str_arrayNew = str_replace($name_img, "", $str_arrayNew);
            $str_arrayNew = str_replace('##', "#", $str_arrayNew);
            $str_arrayNew = ltrim($str_arrayNew, '#');
            $str_arrayNew = rtrim($str_arrayNew, '#');

            $str_arrayNew = explode("#", $str_arrayNew);
            $this->session->set_userdata(['imagenew_qc' . $qcid => $str_arrayNew]);

            $image_arr_current = array_unique(array_merge($image_arr_current, $str_arrayNew), SORT_REGULAR);
        }

        $dataImage = array(
            'image_current_qc' . $qcid => $image_arr_current
        );
        $this->session->set_userdata($dataImage);
        # Mảng xóa
        if ($this->session->has_userdata('image_del_qc' . $qcid)) {
            $m = $this->session->userdata('image_del_qc' . $qcid);
            $this->session->unset_userdata('image_del_qc' . $qcid);
            array_push($m, $name_img);
            $this->session->set_userdata(['image_del_qc' . $qcid => $m]);
        } else {
            $arrname_img = [$name_img];
            $this->session->set_userdata(['image_del_qc' . $qcid => $arrname_img]);
        }

        $output = "";
        if (!empty($dataImage['image_current_qc' . $qcid])):
            foreach ($dataImage['image_current_qc' . $qcid] as $key => $value) {
                if (!empty($value)):
                    $output .= '
                    <div class="col-sm-2">
                    <a style="float: right;" qcid="' . $qcid . '" onclick="del_image_qc(this)" name_image="' . $value . '" class="del_image" title="Xóa">x</a>
                    <img src="' . base_url() . URL_QC . $value . '" class="img-responsive img-thumbnail" data-action="zoom" />
                    </div>
                    ';
                endif;
            }
        endif;
        echo $output;
    }

    function delImageCompetitors()
    {
        $competitorsid = $this->input->post('competitorsid');
        $name_img = $this->input->post('name_img');
        if ($this->session->has_userdata('image_competitors_current')) {
            $str_array = json_encode($this->session->userdata('image_competitors_current'));
            $str_array = str_replace($name_img, "", $str_array);
            $str_array = str_replace('"",', "", $str_array);
            $str_array = str_replace(',""', "", $str_array);
            $str_array = str_replace('""', "", $str_array);
            $this->session->unset_userdata('image_competitors_current');
        } else $str_array = json_encode([]);
        $image_arr_current = json_decode($str_array);

        if ($this->session->has_userdata('image_competitors_new')) {
            $image_competitors_new = $this->session->userdata('image_competitors_new');
            $this->session->unset_userdata('image_competitors_new');
            $str_arrayNew = json_encode($image_competitors_new);
            $str_arrayNew = str_replace($name_img, "", $str_arrayNew);
            $str_arrayNew = str_replace('"",', "", $str_arrayNew);
            $str_arrayNew = str_replace(',""', "", $str_arrayNew);
            $str_arrayNew = str_replace('""', "", $str_arrayNew);
            $str_arrayNew = json_decode($str_arrayNew);
            $this->session->set_userdata(['image_competitors_new' => $str_arrayNew]);
            $image_arr_current = array_unique(array_merge($image_arr_current, $str_arrayNew), SORT_REGULAR);
        }

        $dataImage = array(
            'image_competitors_current' => $image_arr_current
        );
        $this->session->set_userdata($dataImage);
        # Mảng xóa
        if ($this->session->has_userdata('image_competitors_del')) {
            $m = $this->session->userdata('image_competitors_del');
            $this->session->unset_userdata('image_competitors_del');
            array_push($m, $name_img);
            $this->session->set_userdata(['image_competitors_del' => $m]);
        } else {
            $arrname_img = [$name_img];
            $this->session->set_userdata(['image_competitors_del' => $arrname_img]);
        }

        $output = "";
        if (!empty($dataImage['image_competitors_current'])):
            foreach ($dataImage['image_competitors_current'] as $key => $value) {
                if (!empty($value)) $output .= '<div class="col-sm-3"><a competitorsid="' . $competitorsid . '" onclick="del_image_competitors(this)" name_img="' . $value . '" class="del_image" title="Xóa">x</a><img src="' . base_url() . URL_COMPETITORS . $value . '?' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" /></div>';
            }
        endif;
        echo $output;
    }

    function submitCompetitors()
    {
        $dataReturn = array(
            'error' => false,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
        );
        $dataCompetitors = [];
        if ($this->input->post()) {
            $row_id = @$this->input->post("row_id");
            $title = $this->input->post("title");
            $content = $this->input->post("content");
            $range = (!empty($this->input->post("range"))) ? $this->input->post("range") : 0;
            $shop_model = (!empty($this->input->post("shop_model"))) ? $this->input->post("shop_model") : '';
            $revenue_of_guest = (!empty($this->input->post("revenue_of_guest"))) ? $this->input->post("revenue_of_guest") : 0;
            $customer_of_day = (!empty($this->input->post("customer_of_day"))) ? $this->input->post("customer_of_day") : 0;
            $revenue_of_day = (!empty($this->input->post("revenue_of_day"))) ? $this->input->post("revenue_of_day") : 0;
            $working_detail_id = (!empty($this->input->post("working_detail_id"))) ? $this->input->post("working_detail_id") : 0;
            $image = '';
            if ($this->session->has_userdata('image_competitors_new')) {
                $image_competitors_new = $this->session->userdata('image_competitors_new');

                if ($this->session->has_userdata('image_competitors_current')) {
                    $image_current = $this->session->userdata('image_competitors_current');
                    $image = array_unique(array_merge($image_current, $image_competitors_new), SORT_REGULAR);
                } else $image = $image_competitors_new;

                $image = array_unique($image);
                $dataImage = array(
                    'image_competitors' => json_encode($image)
                );
                $this->session->set_userdata($dataImage);
            }

            $image = '';
            if ($this->session->has_userdata('image_competitors')) {
                $image = $this->session->userdata("image_competitors");
            } else {
                $image = json_encode($this->session->userdata("image_competitors_current"));
            }

            $data = array(
                'title' => $title,
                'content' => $content,
                'range' => str_replace(",", "", $range),
                'shop_model' => $shop_model,
                'revenue_of_guest' => str_replace(",", "", $revenue_of_guest),
                'customer_of_day' => str_replace(",", "", $customer_of_day),
                'revenue_of_day' => str_replace(",", "", $revenue_of_day),
                'working_detail_id' => $working_detail_id,
                'image' => ($image == '[]') ? '' : $image,
                'create_by' => $this->session->userdata("userId")
            );
            if (!empty($row_id)) {
                $dataList = $this->site->getCompetitorsByRowID(['row_id' => $row_id]);
                if (!empty($dataList)) {
                    if ($dataList['status'] == 'OK' && $dataList['errorCode'] == 200) {
                        $dataCompetitors = $dataList['result'][0];
                    }
                }
                $data['row_id'] = $row_id;
                $data['update_by'] = $this->session->userdata("userId");
                if ($this->site->updateCompetitors($data)) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật thành công.'
                    );
                }
            } else {
                if ($this->site->insertCompetitors($data)) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Thêm thành công.'
                    );
                }
            }
            if ($this->session->has_userdata('image_competitors_del')) {
                foreach ($this->session->userdata('image_competitors_del') as $key => $value) {
                    @unlink(COMPETITORS . $value);
                }
                $this->session->unset_userdata('image_competitors_del');
            }
            @$this->session->unset_userdata('image_competitors');
            @$this->session->unset_userdata('image_competitors_new');
            @$this->session->unset_userdata('image_competitors_current');
            $this->data["dataReturn"] = array_merge($dataReturn, $data);
            echo json_encode($this->data);
        }
    }

    private function view_store($id = NULL)
    {
        $this->resetSession();
        # SHOP
        if ($this->session->has_userdata('imagenew')) {
            $arrImage = $this->session->userdata('imagenew');
            foreach ($arrImage as $key => $value) {
                @unlink(SHOPS . $value);
            }
            $this->session->unset_userdata('imagenew');
        }
        @$this->session->unset_userdata('image');
        @$this->session->unset_userdata('image_del');
        @$this->session->unset_userdata('image_current');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $getShopById = $this->site->getShopById($id);
        if ($getShopById['status'] == 'OK' && $getShopById['errorCode'] == 200) {
            $stores = $getShopById['result'];
        }
        $this->data['stores'] = $stores ? $stores : NULL;
        //Danh sách tỉnh thành
        $getListProvince = $this->site->getListProvince();
        $listProvince = [];
        if ($getListProvince['status'] == 'OK' && $getListProvince['errorCode'] == 200) {
            $listProvince = $getListProvince['result'];
        }
        $this->data['listProvince'] = $listProvince;

        //Danh sách Quận/Huyện
        $getDistrict = $this->site->getDistrict($stores['PROVINCE_CODE']);
        $listDistrict = [];
        if ($getDistrict['status'] == 'OK' && $getDistrict['errorCode'] == 200) {
            $listDistrict = $getDistrict['result'];
        }
        $this->data['listDistrict'] = $listDistrict;

        //Danh sách phường/xã
        $getWard = $this->site->getWard($stores['DISTRICT_CODE']);
        $listWard = [];
        if ($getWard['status'] == 'OK' && $getWard['errorCode'] == 200) {
            $listWard = $getWard['result'];
        }
        $this->data['listWard'] = $listWard;

        //Danh sách shop type
        $getShopType = $this->site->getShopType(['status' => 1]);
        if ($getShopType['status'] == 'OK' && $getShopType['errorCode'] == 200) {
            $getListShopType = $getShopType['result'];
        }
        $this->data['listShopType'] = $getListShopType ? $getListShopType : NULL;
        //Danh sách mô hình kinh doanh(shop_model)
        $getShopModel = $this->site->getShopModel(['status' => 1]);
        if ($getShopModel['status'] == 'OK' && $getShopModel['errorCode'] == 200) {
            $getListShopModel = $getShopModel['result'];
        }
        $this->data['listShopModel'] = $getListShopModel ? $getListShopModel : NULL;

        $dataListStatus = $this->site->getListStatus();
        if ($dataListStatus['status'] == 'OK' && $dataListStatus['errorCode'] == 200) {
            $this->data['listStatus'] = $dataListStatus['result'];
        } else $this->data['listStatus'] = [];

        $this->data['classbody'] = 'sidebar-collapse';
        $this->data['page_title'] = 'Thông tin cửa hàng ' . $stores['SHOP_NAME'];
        $bc = array(array('link' => site_url('stores'), 'page' => lang('stores')), array('link' => '#', 'page' => $this->data['page_title']));
        $meta = array('page_title' => 'Thông tin cửa hàng: ' . $stores['SHOP_NAME'], 'bc' => $bc);
        $this->page_construct('stores/view', $this->data, $meta);
    }

    # Lấy đơn hàng TTB và nguyên vật liệu
    function getOrderStoreByShopId()
    {
        $shop_id = $this->input->get('shop_id');
        $dataList = $this->site->getListTTBAndCSVCByShopId(['shop_id' => $shop_id]);
        $data = [];
        if (!empty($dataList)) {
            if ($dataList['status'] == 'OK' && $dataList['errorCode'] == 200) {
                $data = $dataList['result'];
            }
        }
        $sOutput = array
        (
            'data' => $data,
        );

        echo json_encode($sOutput);
    }

    function createOrderStore()
    {
        if ($this->input->post()) {
            $shopid = $this->input->post('shopid');
            $formula_id = $this->input->post('formula_id');
            $idfather = $this->input->post('idfather');
            if (!empty($idfather)) {
                $dataWorkings = $this->site->getWorking(['row_id' => $idfather]);
                if (!empty($dataWorkings)) {
                    if ($dataWorkings['status'] == 'OK' && $dataWorkings['errorCode'] == 200) {
                        $data = $dataWorkings['result'];
                        if ($data['STATUS'] > CANCEL && $data['STATUS'] < DOING) {
                            $dataReturn = array(
                                'error' => true,
                                'message' => '<b>' . $data['TITLE'] . '</b> phải ở trạng thái đang xử lý mới được thực hiện thao tác này',
                            );
                            // $sOutput = array('data'   => $dataReturn);
                            echo json_encode($dataReturn);
                        } else {
                            $dataRequest = [
                                'shop_id' => $shopid,
                                'formula_id' => $formula_id,
                                'create_by' => $this->session->userdata('userId')
                            ];
                            $requestTTBAndCSVC = $this->site->requestTTBAndCSVC($dataRequest);
                            if (!empty($requestTTBAndCSVC)) {
                                if ($requestTTBAndCSVC['status'] == 'OK' && $requestTTBAndCSVC['errorCode'] == 200) {
                                    $data = $requestTTBAndCSVC['result'];
                                }
                            }
                            $sOutput = array('data' => $data);
                            echo json_encode($sOutput);
                        }
                    }
                }
            }
        }
    }

    # Lấy lắp ráp trang thiết bị
    function getListLRTTBByShopId()
    {
        $shop_id = $this->input->get('shop_id');
        $dataList = $this->site->getListTTBXayDungByShopId(['shop_id' => $shop_id]);
        $data = [];
        if (!empty($dataList)) {
            if ($dataList['status'] == 'OK' && $dataList['errorCode'] == 200) {
                $data = $dataList['result'];
            }
        }
        $sOutput = array
        (
            'data' => $data,
        );

        echo json_encode($sOutput);
    }

    function getListClassTraning()
    {
        $shop_id = $this->input->get('shop_id');
        $dataList = $this->site->getListClassTraning(['shop_id' => $shop_id]);
        $data = [];
        if (!empty($dataList)) {
            if ($dataList['status'] == 'OK' && $dataList['errorCode'] == 200) {
                $data = $dataList['result'];
            }
        }
        $sOutput = array
        (
            'data' => $data,
        );

        echo json_encode($sOutput);
    }

    function createClassTrainning()
    {
        if ($this->input->post()) {
            $shopid = $this->input->post('shopid');
            $idfather = $this->input->post('idfather');
            if (!empty($idfather)) {
                $dataWorkings = $this->site->getWorking(['row_id' => $idfather]);
                if (!empty($dataWorkings)) {
                    if ($dataWorkings['status'] == 'OK' && $dataWorkings['errorCode'] == 200) {
                        $data = $dataWorkings['result'];
                        if ($data['STATUS'] > CANCEL && $data['STATUS'] < DOING) {
                            $dataReturn = array(
                                'error' => true,
                                'message' => '<b>' . $data['TITLE'] . '</b> phải ở trạng thái đang xử lý mới được thực hiện thao tác này',
                            );
                            echo json_encode($dataReturn);
                        } else {
                            $dataRequest = [
                                'shop_id' => $shopid,
                                'create_by' => $this->session->userdata('userId')
                            ];
                            $createClass = $this->site->createClassTrainning($dataRequest);
                            if (!empty($createClass)) {
                                if ($createClass['status'] == 'OK' && $createClass['errorCode'] == 200) {
                                    $data = $createClass['result'];
                                }
                            }
                            $sOutput = array
                            (
                                'data' => $data,
                            );
                            echo json_encode($sOutput);
                        }
                    }
                }
            }
        }
    }

    function actionShopFormula()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
        );
        if ($this->input->post()) {
            $row_id = @$this->input->post("row_id");
            $qty = $this->input->post("qty");
            $type = $this->input->post("type");

            if ($type == 1) {
                $data = array(
                    'qty' => $qty
                );
                if (!empty($row_id)) {
                    $data['row_id'] = $row_id;
                    $data['update_by'] = $this->session->userdata("userId");
                    $updateShopFormula = $this->site->updateShopFormula($data);
                    if (!empty($updateShopFormula)) {
                        if ($updateShopFormula['status'] == 'OK' && $updateShopFormula['errorCode'] == 200) {
                            $dataReturn = array(
                                'error' => false,
                                'message' => $type
                            );
                        }
                    }
                }
            } elseif ($type == 2) {
                $setupdate = $this->input->post("setupdate");
                $note = $this->input->post("note");
                if (!empty($setupdate)) {
                    $date = new DateTime($setupdate);
                    $setupdate = $date->format('Y-m-d');
                } else {
                    $setupdate = NULL;
                }
                $data = array(
                    'note' => $note,
                    'setupdate' => $setupdate,
                    'qty' => $qty
                );
                if (!empty($row_id)) {
                    $data['row_id'] = $row_id;
                    $data['update_by'] = $this->session->userdata("userId");
                    $updateShopFormula = $this->site->updateShopFormula($data);
                    if (!empty($updateShopFormula)) {
                        if ($updateShopFormula['status'] == 'OK' && $updateShopFormula['errorCode'] == 200) {
                            $dataReturn = array(
                                'error' => false,
                                'message' => $type
                            );
                        }
                    }
                }
            } elseif ($type == 3) {
                $trainer = $this->input->post("trainer");
                $qty_join = $this->input->post("qty_join");
                $trainingday = $this->input->post("trainingday");
                $note_training = $this->input->post("note_training");

                if (!empty($trainingday)) {
                    $date = new DateTime($trainingday);
                    $trainingday = $date->format('Y-m-d');
                } else {
                    $trainingday = NULL;
                }
                $note_training = $this->input->post("note_training");
                $data = array(
                    'trainer' => $trainer,
                    'trainingday' => $trainingday,
                    'qty_join' => $qty_join,
                    'note_training' => $note_training

                );
                if (!empty($row_id)) {
                    $data['row_id'] = $row_id;
                    $data['update_by'] = $this->session->userdata("userId");
                    $updateTrainingForShop = $this->site->updateTrainingForShop($data);
                    if (!empty($updateTrainingForShop)) {
                        if ($updateTrainingForShop['status'] == 'OK' && $updateTrainingForShop['errorCode'] == 200) {
                            $dataReturn = array(
                                'error' => false,
                                'message' => $type
                            );
                        }
                    }
                }
            }
            $this->data["dataReturn"] = $dataReturn;
            echo json_encode($this->data);
        }
    }

    function getLocation()
    {
        $dataReturn = array(
            'error' => true,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại',
        );
        $data = [];
        if ($this->input->post()) {
            $shopid = $this->input->post('shopid');
            $lng = $this->input->post('lng');
            $lat = $this->input->post('lat');
            // $radius          = $this->input->post('radius');
            $dataLocation = $this->site->getCheckLocationShop(['row_id' => $shopid, 'lng' => $lng, 'lat' => $lat]);
        }
        $this->data["dataReturn"] = array_merge($dataReturn, $dataLocation);
        echo json_encode($this->data);
    }

    function submitupdateworking()
    {
        $dataReturn = array(
            'error' => false,
            'message' => 'Cập nhật thành công',
        );
        $data = [];
        if ($this->input->post()) {
            $data = $this->input->post();
            $getWorking = $this->site->getWorking(['row_id' => $this->input->post('working_id')]);
            if ($getWorking['status'] == 'OK' && $getWorking['errorCode'] == 200) {
                $working = $getWorking['result'];
                if (!empty($working)) {
                    //Update working detail
                    $dataWorkingDetail = $this->site->getListWorkingDetailByWorkingId(['working_id' => $this->input->post('working_id')]);
                    $dataUpdate = [];
                    $flag = false;
                    $statusDetail = 1;
                    $dataUpdateWorkingDetail = [];
                    if (!empty($dataWorkingDetail)) {
                        if ($dataWorkingDetail['status'] == 'OK' && $dataWorkingDetail['errorCode'] == 200) {
                            $getList = $dataWorkingDetail['result'];
                            foreach ($getList as $key => $detail) {
                                $input_type_web = json_decode($detail['INPUT_TYPE_WEB'], true);
                                if ($input_type_web['attr']['layout'] == 1) {
                                    if ($input_type_web['type'] == 'text') {
                                        $content = $this->input->post('title_' . $detail['ROW_ID']);
                                        if (!empty($content)) {
                                            $statusDetail = 5;
                                            $flag = true;
                                            if ($input_type_web['attr']['class'] == 'number') $content = str_replace(",", "", $content);

                                            $content_json = json_encode(['value' => $content]);
                                            $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                            // $this->site->updateWorkingDetail();
                                        }
                                    } elseif ($input_type_web['type'] == 'select') {
                                        $content = $this->input->post('input_exp_' . $detail['ROW_ID']);
                                        if (!empty($content)) {
                                            $statusDetail = 5;
                                            $flag = true;
                                            $content_json = json_encode(['value' => (is_array($content)) ? $content : $content]);
                                            $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                            // $this->site->updateWorkingDetail();
                                        }
                                    }
                                } else {
                                    if ($input_type_web['type'] == 'text') {
                                        if (!empty($input_type_web['scope'])) {
                                            $content = $this->input->post('title_' . $detail['ROW_ID']);
                                            if ($input_type_web['attr']['class'] == 'number') $content = str_replace(",", "", $content);

                                            $content2 = $this->input->post('input_exp_' . $detail['ROW_ID']);
                                            if (!empty($content) || !empty($content2)) {
                                                $flag = true;
                                                $statusDetail = 5;
                                            }
                                            $content_json = json_encode(['value' => $content, 'exp' => $content2]);
                                            $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                            // $this->site->updateWorkingDetail();
                                        } else {
                                            $content = $this->input->post('title_' . $detail['ROW_ID']);
                                            $content2 = $this->input->post('title2_' . $detail['ROW_ID']);
                                            if ($input_type_web['attr']['class'] == 'number') {
                                                $content = str_replace(",", "", $content);
                                                $content2 = str_replace(",", "", $content2);
                                            }
                                            if (!empty($content) || !empty($content2)) {
                                                $flag = true;
                                                $statusDetail = 5;
                                            }
                                            $content_json = json_encode(['value' => $content, 'value2' => $content2]);
                                            $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                            // $this->site->updateWorkingDetail();
                                        }
                                    } elseif ($input_type_web['type'] == 'select') {
                                        $content = $this->input->post('input_exp_' . $detail['ROW_ID']);
                                        $note = $this->input->post('textarea_' . $detail['ROW_ID']);
                                        $statusDetail = 5;
                                        $flag = true;
                                        $content_json = json_encode(['value' => (is_array($content)) ? $content : $content]);
                                        $dataUpdateWorkingDetail[$detail['ROW_ID']] = ['row_id' => $detail['ROW_ID'], 'content' => $content_json, 'note' => !empty($note) ? $note : "", 'status' => $statusDetail, 'update_by' => $this->session->userdata('userId')];
                                        // $this->site->updateWorkingDetail();
                                    }
                                }
                            }
                        }
                    }
                    //Update Working
                    $update = false;
                    $status = @$this->input->post('status');
                    $start_date = !empty($this->input->post('start_date')) ? date('Y-m-d H:i:s', strtotime($this->input->post('start_date'))) : NULL;
                    $end_date = !empty($this->input->post('end_date')) ? date('Y-m-d H:i:s', strtotime($this->input->post('end_date'))) : NULL;
                    if ($working['FUNCTION_CODE'] == "HopDong" && $status == APPROVE) {
                        $shopID = $working['SHOP_ID'];
                        $getShopById = $this->site->getShopById($shopID);
                        $stores = $getShopById['result'];
                        $type_id = $stores['TYPE_ID'];
                        if (empty($type_id)) {
                            $dataReturn = array(
                                'error' => true,
                                'message' => 'Cần cập nhật loại cửa hàng mới có thể thực hiện thao tác này!',
                            );
                        } else {
                            $update = true;
                        }
                    } else {
                        $update = true;
                    }

                    if ($update) {
                        if (!in_array($status, [8, 10]) && ($start_date != NULL || $end_date != NULL || !empty($flag))) {
                            $status = 5;
                        }
                        $dataWorking = array(
                            'row_id' => $this->input->post('working_id'),
                            'assign' => (!empty($working['ASSIGN'])) ? $working['ASSIGN'] : 'NULL',
                            'start_date' => $start_date,
                            'end_date' => $end_date,
                            'status' => $status,
                            'update_by' => $this->session->userdata("userId")
                        );
                        $this->site->updateWorking($dataWorking, $dataUpdateWorkingDetail);
                    }
                }
            }
        }
        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        echo json_encode($this->data);
    }

    function delete_store($id = NULL)
    {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->session->set_flashdata('message', lang("answers_deleted"));
        redirect('stores');
    }

    # QC
    function qc()
    {
        $shop_code = $this->input->get('shop_code') ? $this->input->get('shop_code') : $this->session->userdata('shopCode');
        $shop_type = $this->input->get('shoptype') ? $this->input->get('shoptype') : 'all';
        $province_code = $this->input->get('province_code') ? $this->input->get('province_code') : '';
        $district_code = $this->input->get('district_code') ? $this->input->get('district_code') : '';

        $this->data['shop_code'] = $shop_code;
        $this->data['shop_type'] = $shop_type;
        $this->data['province_code'] = $province_code;
        $this->data['district_code'] = $district_code;


        $bc = array(
            array('link' => site_url('business/qc'), 'page' => lang('business')),
            array('link' => '#', 'page' => 'Đánh giá cửa hàng')
        );
        $meta = array('page_title' => 'Đánh giá cửa hàng', 'bc' => $bc);
        $this->page_construct('business/qc/list', $this->data, $meta);
    }

    function get_qc()
    {
        $dataGetQC = $this->site->getListQC();
        if ($dataGetQC['status'] == 'OK' && $dataGetQC['errorCode'] == 200) {
            $data = $dataGetQC['result'];
        } else $data = [];

        $sOutput = array
        (
            'data' => $data,
        );
        echo json_encode($sOutput);
    }

    function create_qc()
    {
        $shopList = $this->session->userdata('shops');
        $stores = [];
        foreach ($shopList as $key => $value) {
            $stores[$value['shopId']] = $value['shopName'];
        }
        $this->data['stores'] = $stores;
        $dataFormulaQC = $this->site->getFormulaQCNotNullItemModel();
        $formula = [];
        if ($dataFormulaQC['status'] == 'OK' && $dataFormulaQC['errorCode'] == 200) {
            foreach ($dataFormulaQC['result'] as $key => $value) {
                $formula[$value['ROW_ID']] = $value['NAME'] . ' - Version ' . $value['VERSION_ID'];
            }
        }
        $this->data['formula'] = $formula;

        $bc = array(array('link' => '#', 'page' => 'Thêm Đánh giá cửa hàng'));
        $meta = array('page_title' => 'Thêm Đánh giá cửa hàng', 'bc' => $bc);
        $this->page_construct('business/qc/create', $this->data, $meta);
    }

    function edit_qc($id = NULL)
    {
        if (!$id || empty($id)) {
            redirect('business/qc');
        }
        $dataQC = $this->site->getShopFormulaQCByRowID($id);
        // echo json_encode($dataQC);die;
        $dataShopFormulaQC = [$dataQC];
        if ($dataQC['status'] == 'OK' && $dataQC['errorCode'] == 200) {
            $dataShopFormulaQC = $dataQC['result'];
        } else {
            redirect('business/qc');
        }
        $this->data['dataShopFormulaQC'] = $dataShopFormulaQC;

        $shopList = $this->session->userdata('shops');
        $stores = [];
        foreach ($shopList as $key => $value) {
            $stores[$value['shopId']] = $value['shopName'];
        }
        $this->data['stores'] = $stores;
        $dataFormulaQC = $this->site->getFormulaQCNotNullItemModel();
        $formula = [];
        if ($dataFormulaQC['status'] == 'OK' && $dataFormulaQC['errorCode'] == 200) {
            foreach ($dataFormulaQC['result'] as $key => $value) {
                $formula[$value['ROW_ID']] = $value['NAME'] . ' - Version ' . $value['VERSION_ID'];
            }
        }
        $this->data['formula'] = $formula;

        $bc = array(array('link' => site_url('business/qc'), 'page' => 'Đánh giá cửa hàng'), array('link' => '#', 'page' => 'Cập nhật đánh giá'));
        $meta = array('page_title' => 'Cập nhật đánh giá', 'bc' => $bc);
        $this->page_construct('business/qc/edit', $this->data, $meta);
    }

    function submit_qc()
    {

        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại',
            'url' => base_url('business/qc')
        );
        if ($this->input->post()) {
            $formtype = $this->input->post("formtype");
            $shop_id = $this->input->post("shop_id");
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $formula_id = $this->input->post("formula_id");
            $note = $this->input->post("note");
            $review_name = $this->input->post("review_name");
            $review_date = $this->input->post("review_date");

            $data = array(
                'shop_id' => $shop_id,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'formula_id' => $formula_id,
                'note' => $note,
                'review_name' => $review_name,
                'review_date' => $review_date
            );

            if ($formtype == 'edit') {
                $id = $this->input->post("id");
                $data['row_id'] = $id;
                $data['update_by'] = $this->session->userdata('userId');
                $dataReturn = $this->site->updateShopFormulaQC($data);
                if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật đánh giá thành công',
                        'url' => base_url('business/qc')
                    );
                }

            } elseif ($formtype == 'create') {
                $data['create_by'] = $this->session->userdata('userId');
                $dataReturn = $this->site->insertShopFormulaQC($data);
                if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Thêm đánh giá thành công',
                        'url' => base_url('business/qc')
                    );
                }
            } elseif ($formtype == 'review') {
                $row_id = $this->input->post("row_id");
                $shop_formula_qc_id = $this->input->post("shop_formula_qc_id");
                $item_qc_id = $this->input->post("item_qc_id");
                $input_point = $this->input->post("input_point");
                $input_note = $this->input->post("input_note");
                $sourceImg = $this->input->post("sourceImg");
                $nameimage_qc = $this->input->post("nameimage_qc");
                $non_score = $this->input->post("non_score");


                $image = '';
                if ($this->session->has_userdata('imagenew_qc' . $row_id)) {
                    $imagenew = $this->session->userdata('imagenew_qc' . $row_id);

                    if ($this->session->has_userdata('image_current_qc' . $row_id)) {
                        $image_current = $this->session->userdata('image_current_qc' . $row_id);
                        $image = array_merge($image_current, $imagenew);
                    } else $image = $imagenew;

                    $image = array_unique($image, SORT_REGULAR);
                    $dataImage = array(
                        'image' => implode("#", $image)
                    );
                    $this->session->set_userdata($dataImage);
                }

                $image = '';
                if ($this->session->has_userdata('image')) {
                    $image = $this->session->userdata("image");
                } else {
                    $image = implode("#", $this->session->userdata('image_current_qc' . $row_id));
                }

                // $session_image_qc_arr= $this->session->userdata('imagenew_qc'.$row_id);
                // $session_image_qc_str= implode("#",$session_image_qc_arr);
                $data = array(
                    'formtype' => $formtype,
                    'row_id' => $row_id,
                    'shop_formula_qc_id' => $shop_formula_qc_id,
                    'item_qc_id' => $item_qc_id,
                    'input_point' => $input_point,
                    'input_note' => $input_note,
                    'image' => $image,
                    'non_score' => $non_score
                );
                /*
                if(!empty($sourceImg)){
                    $pos  = strpos($sourceImg, ';');
                    if($pos){
                        $imgExten = explode('/', substr($sourceImg, 0, $pos))[1];
                        $extens = ['jpg', 'jpeg', 'png' ];
                        if(in_array($imgExten, $extens)) {
                           $imgNewName = genhash(5). '.' . $imgExten;
                           $filepath = QC . $imgNewName;
                           $fileP = fopen($filepath, 'wb');
                           $imgCont = explode(',', $sourceImg);
                           fwrite($fileP, base64_decode($imgCont[1]));
                           fclose($fileP);
                           $data['image'] = $imgNewName;
                           @unlink(QC.$nameimage_qc);
                        }
                    }
                }*/
                $dataQC = $this->site->updateShopFormulaQCDetail($data);
                if ($dataQC['status'] == 'OK' && $dataQC['errorCode'] == 200) {

                    if ($this->session->has_userdata('image_del_qc' . $row_id)) {
                        foreach ($this->session->userdata('image_del_qc' . $row_id) as $key => $value) {
                            @unlink(QC . $value);
                        }
                        $this->session->unset_userdata('image_del_qc' . $row_id);
                    }
                    @$this->session->unset_userdata('imagenew_qc' . $row_id);
                    @$this->session->unset_userdata('image_current_qc' . $row_id);
                    @$this->session->unset_userdata('image');
                    $dataResult = $dataQC['result'];
                    $dataReturn = array(
                        'error' => false,
                        'message' => 'Cập nhật thành công',
                        'dataResult' => $dataResult
                    );
                }
            }
            $this->data["dataReturn"] = array_merge($dataReturn, $data);
            echo json_encode($this->data);
        }
    }

    //
    function upload_image_qc()
    {
        sleep(1);
        $qcid = $this->input->post('qcid');
        if ($_FILES["files"]["name"] != '') {
            $output = '';
            if ($this->session->has_userdata('image_current_qc' . $qcid)):
                $image_current = $this->session->userdata('image_current_qc' . $qcid);
                foreach ($image_current as $key => $value) {
                    $output .= '<div class="col-sm-2"><a style="float: right;" qcid="' . $qcid . '" onclick="del_image_qc(this)" name_image="' . $value . '" class="del_image" title="Xóa">x</a><img src="' . base_url() . URL_QC . $value . '?r=' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" /></div>';
                }
            endif;
            $arrImage = [];
            for ($count = 0; $count < count($_FILES["files"]["name"]); $count++) {
                $config = array();
                $config['upload_path'] = QC;
                $config["allowed_types"] = 'jpg|png|gif|jpeg';
                $config['max_size'] = '10048';
                $new_name = time() . $this->data_lib->inject($_FILES["files"]["name"][$count]);
                $config['file_name'] = $new_name;
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
                $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
                $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
                $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
                $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
                if ($this->upload->do_upload('file')) {
                    $data = $this->upload->data();
                    $output .= '<div class="col-sm-2"><a style="float: right;" qcid="' . $qcid . '" onclick="del_image_qc(this)" name_image="' . $data["file_name"] . '" class="del_image" title="Xóa">x</a><img src="' . base_url() . URL_QC . $data["file_name"] . '?r=' . time() . '" class="img-responsive img-thumbnail" data-action="zoom" /></div>';
                    $arrImage[$count] = $data["file_name"];
                }
            }
            if (!empty($arrImage)) {
                if ($this->session->has_userdata('imagenew_qc' . $qcid)) {
                    $imagenew = $this->session->userdata('imagenew_qc' . $qcid);
                    $arrImage = array_merge($arrImage, $imagenew);
                }
                $this->session->set_userdata(['imagenew_qc' . $qcid => $arrImage]);
            }

            echo $output;
        }
    }

    function review_qc($id = NULL)
    {
        if (!$id || empty($id)) {
            redirect('business/qc');
        }
        $dataQC = $this->site->getShopFormulaQCByRowID($id);
        $dataShopFormulaQC = [];
        if ($dataQC['status'] == 'OK' && $dataQC['errorCode'] == 200) {
            $dataShopFormulaQC = $dataQC['result'];
        } else {
            redirect('business/qc');
        }
        $this->data['dataShopFormulaQC'] = $dataShopFormulaQC;

        $this->data['classbody'] = 'sidebar-collapse';
        $bc = array(array('link' => site_url('business/qc'), 'page' => 'Đánh giá cửa hàng'), array('link' => '#', 'page' => 'Đánh giá của hàng ' . $dataShopFormulaQC['SHOP_NAME']));
        $meta = array('page_title' => 'Đánh giá của hàng ' . $dataShopFormulaQC['SHOP_NAME'], 'bc' => $bc);
        $this->page_construct('business/qc/edit', $this->data, $meta);
    }

    //nghiahv
    function orders()
    {
        $fromDate = '01-' . date("m-Y");
        $toDate = date("d-m-Y");
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->session->userdata('shopModel');
        $state = 'processing';
        $type = 'create';
        $orderTypeInv = [];
        foreach ($this->session->userdata('order_type_inv') as $value) {
            $orderTypeInv[$value['gc_code']] = $value['gen_code'];
        }


        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['state'] = $state;
        $this->data['type'] = $type;
        $this->data['orderTypeInv'] = $orderTypeInv;


        $bc = array(
            array('link' => site_url('business/orders'), 'page' => lang('business')),
            array('link' => '#', 'page' => 'Tạo đơn hàng')
        );
        $meta = array('page_title' => 'Tạo đơn hàng', 'bc' => $bc);
        $this->page_construct('business/orders', $this->data, $meta);
    }

    function promotions()
    {
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $shopCode = $this->session->userdata('shopCode');
        $shopType = 'All';
        $shopModel = $this->session->userdata('shopModel');
        $groupCode = 'All';

        $provinceCode = 'All';
        $districtCode = 'All';

        $resultApi = $this->data_lib->execute('/api/business/promotion/list', 'GET');
        $promotions = array();
        $promotions[''] = 'Chọn khuyến mãi';
        if ($resultApi['success']) {
            foreach ($resultApi['data'] as $key => $value) {
                $promotions[$value['PROMO_ID']] = $value['PROMO_NAME'];
            }
        }
        $this->data['promotionsList'] = $promotions;

        $this->data['toDate'] = $toDate;
        $this->data['fromDate'] = $fromDate;
        $this->data['shopCode'] = $shopCode;
        $this->data['shopType'] = $shopType;
        $this->data['shopModel'] = $shopModel;
        $this->data['provinceCode'] = $provinceCode;
        $this->data['districtCode'] = $districtCode;
        $this->data['groupCode'] = $groupCode;
        $bc = array(
            array('link' => site_url('business/promotions'), 'page' => lang('business')),
            array('link' => '#', 'page' => 'Khuyến mãi')
        );
        $meta = array('page_title' => 'Khuyến mãi', 'bc' => $bc);
        $this->page_construct('business/promotions', $this->data, $meta);
    }


    function inventory()
    {
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');

        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopModel'] = $shopModel;
        $this->data['stockType'] = '-1';
        $this->data['shopCode'] = $this->session->userdata('shopCode');
        //$this->data['data'] = $data;
        $bc = array(
            array('link' => site_url('business/inventory'), 'page' => lang('reports')),
            array('link' => '#', 'page' => 'Kiểm kê')
        );
        $meta = array('page_title' => 'Kiểm kê', 'bc' => $bc);
        $this->page_construct('business/inventory', $this->data, $meta);
    }

    function localtransfer()
    {
        $this->load->model('Shop_model','shopModel');
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");;
        $shopList = array();
        $allShopList = array();
        if (in_array($this->session->userdata('shopCode'), ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])) {
            $shopList['All'] = 'Tất cả';
        }
        $shops = $this->session->userdata('shops');
        foreach ($shops as $key => $value) {
            if ($value['shopModel'] == 'CSH')
                $shopList[$value['shopCode']] = $value['shopName'];
        }
        $result = $this->shopModel->findAll();
        foreach ($result['data'] as $key => $value) {
            if ($value['shopModel'] == 'CSH')
                $allShopList[$value['shopCode']] = $value['shopName'];
        }
        $this->data['shopList'] = $shopList;
        $this->data['allShopList'] = $allShopList;
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $this->session->userdata('shopCode');
        $bc = array(
            array('link' => site_url('business/localtransfer'), 'page' => lang('business')),
            array('link' => '#', 'page' => 'Điều chuyển HH nội bộ')
        );
        $meta = array('page_title' => 'Điều chuyển HH nội bộ', 'bc' => $bc);
        $this->page_construct('business/localtransfer', $this->data, $meta);
    }

    function deltransfer()
    {
        $permissions = [
            'read' => check_permission('delTransfer:read'),
            'insert' => check_permission('delTransfer:insert'),
            'update' => check_permission('delTransfer:update'),
            'delete' => check_permission('delTransfer:delete'),
            'export' => check_permission('delTransfer:export'),
        ];

        $this->load->model('Shop_model','shopModel');
        $fromDate = date("1-m-Y");
        $toDate = date("d-m-Y");;
        $shopList = array();
        $allShopList = array();
        if (in_array($this->session->userdata('shopCode'), ['HEAD_OFFICE', 'kienlonggroup', 'dongtamgroup'])) {
            $shopList['All'] = 'Tất cả';
        }
        $shops = $this->session->userdata('shops');
        foreach ($shops as $key => $value) {
            if ($value['shopModel'] == 'CSH')
                $shopList[$value['shopCode']] = $value['shopName'];
        }
        $result = $this->shopModel->findAll();
        foreach ($result['data'] as $key => $value) {
            if ($value['shopModel'] == 'CSH')
                $allShopList[$value['shopCode']] = $value['shopName'];
        }
       // echo json_encode($allShop);die;
        $this->data['shopList'] = $shopList;
        $this->data['allShopList'] = $allShopList;
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $this->session->userdata('shopCode');
        $this->data['permissions'] = $permissions;
        $bc = array(
            array('link' => site_url('business/deltransfer'), 'page' => lang('business')),
            array('link' => '#', 'page' => 'Hủy nguyên vật liệu')
        );
        $meta = array('page_title' => 'Hủy nguyên vật liệu', 'bc' => $bc);
        $this->page_construct('business/deltransfer', $this->data, $meta);
    }

    function purchase()
    {
        $this->load->model('Shop_model','shopModel');
        // $date = time() - 2 * 24 * 60 * 60;
        $fromDate = date("d-m-Y");
        $toDate = date("d-m-Y");
        $allShopList = array('' => 'Tất cả');

        $result = $this->shopModel->findAll();
        foreach ($result['data'] as $key => $value) {
            if ($value['shopModel'] == 'CSH')
                $allShopList[$value['shopCode']] = $value['shopName'];
        }

        $this->data['allShopList'] = $allShopList;
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $this->session->userdata('shopCode');
        $bc = array(
            array('link' => site_url('business/purchase'), 'page' => lang('business')),
            array('link' => '#', 'page' => 'Nhập hàng mua ngoài')
        );
        $meta = array('page_title' => 'Nhập hàng mua ngoài', 'bc' => $bc);
        $this->page_construct('business/purchase', $this->data, $meta);
    }

    function scrap(){
        $this->load->model('Shop_model','shopModel');
        $fromDate = date("d-m-Y");
        // $fromDate = '18-09-2023';
        $toDate = date("d-m-Y");
        $allShopList = array('' => 'Tất cả');

        $result = $this->shopModel->findAll();
        foreach ($result['data'] as $key => $value) {
            if ($value['shopModel'] == 'CSH')
                $allShopList[$value['shopCode']] = $value['shopName'];
        }

        $this->data['allShopList'] = $allShopList;
        $this->data['fromDate'] = $fromDate;
        $this->data['toDate'] = $toDate;
        $this->data['shopCode'] = $this->session->userdata('shopCode');
        $bc = array(
            array('link' => site_url('business/scrap'), 'page' => lang('business')),
            array('link' => '#', 'page' => 'Hủy hàng')
        );
        $meta = array('page_title' => 'Hủy hàng', 'bc' => $bc);
        $this->page_construct('business/scrap', $this->data, $meta);
    }
}

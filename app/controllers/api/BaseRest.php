<?php defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class BaseRest extends RestController{
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->loggedIn = $this->data_lib->logged_in();
        //$this->config->load('rest');
        if (!$this->loggedIn) {
            $response = array(
                'success' => false,
                'message' => 'Unauthorized',
            );
            $this->response($response, 401);
        }
        $this->controller = strtolower($this->router->fetch_class());
        $this->action = strtolower($this->router->fetch_method());
        $this->load->helper(array('form', 'url'));
//        $this->load->library('form_validation');
    }

    /**
     * [execute call api from OBS service]
     * @param  [type]  $url    [api url]
     * @param  [type]  $method [GET, POST, PUT, DELETE]
     * @param  array   $data   [data send server]
     * @param  boolean $OBS [false if call Thành api]
     * @return [type]          [json result]
     */
    function execute($url = null, $method = null, $data = array(), $OBS = true){
        $result = array(
            'success' => false,
            'code' => 500,
            'message' => '',
            'data' => [],
        );
        try {
            $ci = &get_instance();
            $BASEURL = $OBS ? URL_API : URL_API3;
            $TOKEN = ($OBS) ? 'OB ' . $ci->session->userdata('token') : 'OB ' . $ci->session->userdata('access_token');
            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'charset' => 'utf-8',
                'Authorization' => $TOKEN
            ];
            if (!$OBS) {
                $headers['x-api-key'] = X_api_key;
            }
            $client = new \GuzzleHttp\Client([
                'base_uri' => $BASEURL,
                'headers' => $headers,
                'defaults' => [
                    'exceptions' => false,
                ],
                'http_errors' => false,//Disable exceptions for Guzzle
            ]);
            $options = [];
            switch ($method) {
//            case HTTP_GET:
//            case HTTP_DELETE:
//                $options = ['query' => $data];
//                break;
                case HTTP_PUT:
                case HTTP_POST:
                case HTTP_GET:
                case HTTP_DELETE:
                    $options = ['json' => $data];
                    break;
            }
            if (!$OBS && $method === HTTP_GET && $url === 'attendance/details') {
                $options = ['query' => $data];
            }
            // $options['json'] = $data;
            // $response = $client->get($url, $options);
            $response = $client->request($method, $url, $options);
            $httpCode = $response->getStatusCode();
            $response = json_decode($response->getBody(), true);
            $result['code'] = $httpCode;
            $result['success'] = ( isset($response['success']) ) ? $response['success'] : true;
            $result['message'] = $response['message'] ?? '';
            $result['data'] = $response['data'] ?? [];
            // log_message("debug", "request: " . print_r($options, true));
            // log_message("debug", "response resultAPI: " . print_r($response, true));
        } catch (\Exception $e) {
            $result['message'] = $e->getMessage();
            log_message('debug', print_r($e->getMessage(), true));
        }
        return $result;
        
        
    }

    function badRequest($message = 'Bad request.'){
        $this->response(array(
                'success' => false,
                'message' => $message
        ), 400);
    }

    function permissionDenied(){
        $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
            ), 403);
    }

    function allowAccess($permission){
        if(!check_permission($permission)){
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data'    => []
            ), 403);
        }
    }


    private function getToken($HO_APP = true){
       return ($HO_APP) ? $this->session->userdata('token') : md5("Nutifood" . date("YmdH"));
    }

    function checkShopAccess($shopCode){
        $shopList = $this->session->userdata('shops');
        $shopIndex = array_search($shopCode, array_column($shopList, 'shopCode'));
        if($this->session->userdata('shop_code') != $shopCode && is_numeric($shopIndex) == false){
            $this->response(array(
                'success'   => false,
                'message'   => 'Không có quyền truy cập'
            ));
        }
    }
}
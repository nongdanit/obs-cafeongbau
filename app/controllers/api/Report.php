<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'BaseRest.php';

class Report extends BaseRest
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper('download');
    }

    function reportDaily_get()
    {
        $this->allowAccess('rptdaily:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $groupCode = $this->input->get('groupCode') ? $this->input->get('groupCode') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $role = $this->session->userdata('role');
        //if($shopModel == 'All') $shopModel = '00';

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode, $groupCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes
        );
        $url = '/api/reports/rptDaily';
        $resultAPI = $this->execute($url, 'GET', $data);
        $this->response($resultAPI);
    }

    function reportHour_get()
    {
        $this->allowAccess('rpthour:read');
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $role = $this->session->userdata('role');

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes
        );
        $resultAPI = $this->execute('/api/reports/rptHour', 'GET', $data);
        $this->response($resultAPI);
    }

    function reportItem_get()
    {
        $this->allowAccess('rptitem:read');
        $columns = array(
            0 => 'trans_date',
            1 => 'shop_code',
            2 => 'shop_name',
            3 => 'shop_type',
            4 => 'item_group_name',
            5 => 'item_code',
            6 => 'item_name',
            7 => 'quantity',
            8 => 'unit',
            9 => 'price',
            10 => 'discount',
            11 => 'amount'
        );

        $fromDate = $this->input->get('fromDate') ? date("Ymd", strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date("Ymd", strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $role = $this->session->userdata('role');
        $start = $this->input->get('start') ? $this->input->get('start') : 0;
        $limit = $this->input->get('length') ? $this->input->get('length') : 100;

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'start' => $start,
            'limit' => $limit,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
            'isExport' => false,
        );
        if (!empty($this->input->get('search')['value'])) {
            $dataPost['search'] = $this->input->get('search')['value'];
        }
        if (isset($this->input->get('order')[0]['column'])) {
            $dataPost['orderBy'] = $columns[$this->input->get('order')[0]['column']];
        }
        if (!empty($this->input->get('order')[0]['dir'])) {
            $dataPost['direction'] = $this->input->get('order')[0]['dir'];
        }
        $resultAPI = $this->execute("/api/reports/rptItem", "GET", $dataPost);
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $totalAmount = 0;
        $totalDiscount = 0;
        $totalQuantity = 0;
        $message = '';
        $data = [];
        if ($resultAPI['success'] && count($resultAPI['data'])) {
            $recordsTotal = $resultAPI['data'][0]['recordsTotal'];
            $recordsFiltered = $resultAPI['data'][0]['recordsTotal'];
            $totalAmount = $resultAPI['data'][0]['totalAmount'];
            $totalDiscount = $resultAPI['data'][0]['totalDiscount'];
            $totalQuantity = $resultAPI['data'][0]['totalQuantity'];
            $data = $resultAPI['data'];
        } else if (!$resultAPI['success']) {
            $message = $resultAPI['message'];
        }

        $sOutput = array(
            'draw' => intval($this->input->get('draw')),
            'recordsTotal' => intval($recordsTotal),
            'recordsFiltered' => intval($recordsFiltered),
            'totalQuantity' => $totalQuantity,
            'totalDiscount' => $totalDiscount,
            'totalAmount' => $totalAmount,
            'data' => $data,
            'message' => $message
        );
        $this->response($sOutput);
    }

    function reportItem_post()
    {
        $this->allowAccess('rptitem:read');
        $columns = array(
            0 => 'trans_date',
            1 => 'shop_code',
            2 => 'shop_name',
            3 => 'shop_type',
            4 => 'item_group_name',
            5 => 'item_code',
            6 => 'item_name',
            7 => 'quantity',
            8 => 'unit',
            9 => 'price',
            10 => 'discount',
            11 => 'amount'
        );

        $fromDate = $this->input->post('fromDate') ? date("Ymd", strtotime($this->input->post('fromDate'))) : date("Ymd");
        $toDate = $this->input->post('toDate') ? date("Ymd", strtotime($this->input->post('toDate'))) : date("Ymd");
        $shopCode = $this->input->post('shopCode') ? $this->input->post('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->post('shopModel') ? $this->input->post('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->post('shopType') ? $this->input->post('shopType') : 'All';
        $provinceCode = $this->input->post('provinceCode') ? $this->input->post('provinceCode') : 'All';
        $districtCode = $this->input->post('districtCode') ? $this->input->post('districtCode') : 'All';
        $role = $this->session->userdata('role');
        $start = $this->input->post('start') ? $this->input->post('start') : 0;
        $limit = $this->input->post('length') ? $this->input->post('length') : 100;

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'start' => $start,
            'limit' => $limit,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
            'isExport' => false,
        );
        if (!empty($this->input->post('search')['value'])) {
            $dataPost['search'] = $this->input->post('search')['value'];
        }
        if (isset($this->input->post('order')[0]['column'])) {
            $dataPost['orderBy'] = $columns[$this->input->post('order')[0]['column']];
        }
        if (!empty($this->input->post('order')[0]['dir'])) {
            $dataPost['direction'] = $this->input->post('order')[0]['dir'];
        }
        $resultAPI = $this->execute("/api/reports/rptItem", "GET", $dataPost);
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $totalAmount = 0;
        $totalDiscount = 0;
        $totalQuantity = 0;
        $message = '';
        $data = [];
        if ($resultAPI['success'] && count($resultAPI['data'])) {
            $recordsTotal = $resultAPI['data'][0]['recordsTotal'];
            $recordsFiltered = $resultAPI['data'][0]['recordsTotal'];
            $totalAmount = $resultAPI['data'][0]['totalAmount'];
            $totalDiscount = $resultAPI['data'][0]['totalDiscount'];
            $totalQuantity = $resultAPI['data'][0]['totalQuantity'];
            $data = $resultAPI['data'];
        } else if (!$resultAPI['success']) {
            $message = $resultAPI['message'];
        }

        $sOutput = array(
            'draw' => intval($this->input->post('draw')),
            'recordsTotal' => intval($recordsTotal),
            'recordsFiltered' => intval($recordsFiltered),
            'totalQuantity' => $totalQuantity,
            'totalDiscount' => $totalDiscount,
            'totalAmount' => $totalAmount,
            'data' => $data,
            'message' => $message
        );
        $this->response($sOutput);
    }

    function reportItemExport_get()
    {
        $this->allowAccess('rptitem:export');
        $fromDate = $this->input->get('fromDate') ? date("Ymd", strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date("Ymd", strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $role = $this->session->userdata('role');

        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';

        $type = $this->input->get('type') ? $this->input->get('type') : '1';
        $limit = $this->input->get('limit') ? $this->input->get('limit') : 0;
        $search = $this->input->get('search');

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'start' => 0,
            'limit' => 0,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
            'isExport' => true,
        );
        if (!empty($search)) {
            $dataPost['search'] = $search;
        }
//        $this->response($dataPost);
        $resultAPI = $this->execute("/api/reports/rptItem", "GET", $dataPost);
//        if ($resultAPI['success']) {
//            $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
//            force_download("Doanh số theo sản phẩm.xlsx", $data);
//        }
        $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
        force_download("Doanh số theo sản phẩm.xlsx", $data);
//        exit(0);
    }

    function reportTransDetail_post()
    {
        $this->allowAccess('rpttransdetails:read');
        $columns = array(
            1 => 'create_date',
            2 => 'shop_code',
            3 => 'shop_name',
            4 => 'shop_type',
            5 => 'trans_num',
            7 => 'pay_type',
            8 => 'discount_amt',
            9 => 'amount'
        );

        $fromDate = $this->input->post('fromDate') ? date("Ymd", strtotime($this->input->post('fromDate'))) : date("Ymd");
        $toDate = $this->input->post('toDate') ? date("Ymd", strtotime($this->input->post('toDate'))) : date("Ymd");
        $shopCode = $this->input->post('shopCode') ? $this->input->post('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->post('shopModel') ? $this->input->post('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->post('shopType') ? $this->input->post('shopType') : 'All';
        $provinceCode = $this->input->post('provinceCode') ? $this->input->post('provinceCode') : 'All';
        $districtCode = $this->input->post('districtCode') ? $this->input->post('districtCode') : 'All';
        $compareSync = $this->input->post('compareSync') ? 1 : 0;
        $status = $this->input->post('status') ? $this->input->post('status') : 0;
        $role = $this->session->userdata('role');

        $start = $this->input->post('start') ? $this->input->post('start') : 0;
        $limit = $this->input->post('length') ? $this->input->post('length') : 100;

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }
        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'start' => $start,
            'limit' => $limit,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
            'status' => $status,
            'compareSync' => $compareSync+0
        );

        if (!empty($this->input->post('search')['value'])) {
            $dataPost['search'] = $this->input->post('search')['value'];
        }
        if (isset($this->input->post('order')[0]['column'])) {
            $dataPost['orderBy'] = $columns[$this->input->post('order')[0]['column']];
        }
        if (!empty($this->input->post('order')[0]['dir'])) {
            $dataPost['direction'] = $this->input->post('order')[0]['dir'];
        }
        $resultAPI = $this->execute('/api/reports/rptTransDetails', 'GET', $dataPost);
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $totalDiscount = 0;
        $totalAmount = 0;
        $message = '';
        $data = [];
        if ($resultAPI['success'] && count($resultAPI['data'])) {
            $data = $resultAPI['data'];
            $recordsTotal = $resultAPI['data'][0]['recordsTotal'];
            $recordsFiltered = $resultAPI['data'][0]['recordsTotal'];
            $totalDiscount = $resultAPI['data'][0]['totalDiscount'];
            $totalAmount = $resultAPI['data'][0]['totalAmount'];
        } else if (!$resultAPI['success']) {
            $message = $resultAPI['message'];
        }
        $sOutput = array(
            'draw' => intval($this->input->post('draw')),
            'recordsTotal' => intval($recordsTotal),
            'recordsFiltered' => intval($recordsFiltered),
            'totalDiscount' => $totalDiscount,
            'totalAmount' => $totalAmount,
            'data' => $data,
            'message' => $message
        );
        $this->response($sOutput);
    }

    function updatePayType_put()
    {
        $this->allowAccess('rpttransdetails:update');
        $params = json_decode(trim(file_get_contents('php://input')), true);
        $transId = $params['transId'];
        $shopCode = $params['shopCode'];
        $payType = $params['payType'];

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $data = array(
            'id' => $transId,
            'shopCode' => $shopCode,
            'payType' => $payType,
            'modifyBy' => $this->session->userdata('userId'),
        );
        $resultAPI = $this->execute('/api/reports/updatePayType', 'PUT', $data);
        $this->response($resultAPI);
    }

    function reportTransDetail_get()
    {
        $this->allowAccess('rpttransdetails:read');
        $transId = $this->input->get('transId');
        $shopCode = $this->input->get('shopCode');

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $data = array(
            'id' => $transId,
            'shopCode' => $shopCode
        );
        $resultAPI = $this->execute('/api/reports/rptTransDetails/details', 'GET', $data);
        $this->response($resultAPI);
    }

    function reportTransDetailExport_get()
    {
        $this->allowAccess('rpttransdetails:export');
        $fromDate = $this->input->get('fromDate') ? date("Ymd", strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date("Ymd", strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $compareSync = $this->input->get('compareSync') ? $this->input->get('compareSync') : 0;
        if($compareSync == 'undefined'){
            $compareSync = 0;
        }
        $status = $this->input->get('status') ? $this->input->get('status') : 0;

        $search = $this->input->get('search');
        $limit = $this->input->get('limit');

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'start' => 0,
            'limit' => 0,
            'search' => $search,
            'orderBy' => 'create_date',
            'direction' => 'desc',
            'status' => $status,
            'compareSync' => $compareSync
        );
        $resultAPI = $this->execute('/api/reports/rptTransDetailsExport', 'GET', $dataPost);
        $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
        force_download("Doanh số theo bill.xlsx", $data);
    }

    function reportItemPayment_get()
    {
        $this->allowAccess('rptitembypayment:read');
        $columns = array(
            0 => 'trans_date',
            1 => 'pos_trans_id',
            2 => 'shop_code',
            3 => 'shop_name',
            4 => 'shop_address',
            5 => 'shop_type',
            6 => 'item_group_name',
            7 => 'item_code',
            8 => 'item_name',
            9 => 'quantity',
            10 => 'unit',
            11 => 'price',
            12 => 'discount',
            13 => 'amount',
            14 => 'AirPay',
            15 => 'Beamin',
            16 => 'Grab',
            17 => 'Momo',
            18 => 'NowFoody',
            19 => 'VNPay',
            20 => 'ZaloPay',
            21 => 'beFood',
            22 => 'Tiki',
            23 => 'Lazada',
            24 => 'Shopee',
        );

        $fromDate = $this->input->get('fromDate') ? date("Ymd", strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date("Ymd", strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $role = $this->session->userdata('role');
        $start = $this->input->get('start') ? $this->input->get('start') : 0;
        $limit = $this->input->get('length') ? $this->input->get('length') : 1000;

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'start' => $start,
            'limit' => $limit,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
        );
        if (!empty($this->input->get('search')['value'])) {
            $dataPost['search'] = $this->input->get('search')['value'];
        }
        if (isset($this->input->get('order')[0]['column'])) {
            $dataPost['orderBy'] = $columns[$this->input->get('order')[0]['column']];
        }
        if (!empty($this->input->get('order')[0]['dir'])) {
            $dataPost['direction'] = $this->input->get('order')[0]['dir'];
        };
        $resultAPI = $this->execute("/api/reports/rptItemByPayment", "GET", $dataPost);
        //log_message("debug", "test: " . print_r( $resultAPI, true));
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $totalAirPay = 0;
        $totalBeamin = 0;
        $totalGrab = 0;
        $totalMomo = 0;
        $totalNowFoody = 0;
        $totalVNPay = 0;
        $totalZaloPay = 0;
        $message = '';
        $data = [];
        if ($resultAPI['success'] && count($resultAPI['data'])) {
            $data = $resultAPI['data'];
            $recordsTotal = $resultAPI['data'][0]['recordsTotal'];
            $recordsFiltered = $resultAPI['data'][0]['recordsTotal'];
            // $recordsTotal   = $resultAPI['total'];
            // $recordsFiltered = $resultAPI['total'];
            // $totalAirPay = $resultAPI['totalAirPay'];
            // $totalBeamin = $resultAPI['totalBeamin'];    
            // $totalGrab = $resultAPI['totalGrab'];    
            // $totalMomo = $resultAPI['totalMomo'];    
            // $totalNowFoody = $resultAPI['totalNowFoody'];    
            // $totalVNPay = $resultAPI['totalVNPay'];    
            // $totalZaloPay = $resultAPI['totalZaloPay'];     

        } else if (!$resultAPI['success']) {
            $message = $resultAPI['message'];
        }
        $sOutput = array(
            'draw' => intval($this->input->get('draw')),
            'recordsTotal' => intval($recordsTotal),
            'recordsFiltered' => intval($recordsFiltered),
            'totalAirPay' => $totalAirPay,
            'totalBeamin' => $totalBeamin,
            'totalGrab' => $totalGrab,
            'totalMomo' => $totalMomo,
            'totalNowFoody' => $totalNowFoody,
            'totalVNPay' => $totalVNPay,
            'totalZaloPay' => $totalZaloPay,
            'data' => $data,
            'message' => $message
        );
        //log_message("debug", "api time: " . print_r($sOutput, true));
        $this->response($sOutput);
    }

    function reportItemPaymentExport_get()
    {
        $this->allowAccess('rptitembypayment:export');
        $fromDate = $this->input->get('fromDate') ? date("Ymd", strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date("Ymd", strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';

        $limit = $this->input->get('limit') ? $this->input->get('limit') : 300000;
        $type = $this->input->get('type') ? $this->input->get('type') : 1;

        $search = $this->input->get('search');

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'start' => 0,
            'limit' => $limit,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
        );

        $resultAPI = $this->execute('/api/reports/rptItemByPaymentExport', 'GET', $dataPost);
        $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
        force_download("Chi tiết SP theo HT thanh toán | OBS.xlsx", $data);
    }

    function reportShopinfo_get()
    {
        $this->allowAccess('rptshop_info:read');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : 'All';
        $status = null != $this->input->get('status') ? $this->input->get('status') : 'All';

        $shopFilter = shopFilter('All', $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'shopCodes' => $shopCodes,
            'status' => $status,
        );
        $resultAPI = $this->execute('/api/reports/rptShopInfo', 'GET', $dataPost);
        $this->response($resultAPI);
    }

    function reportMaterial_get()
    {
        $this->allowAccess('materials:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $type = 0;

        $shopFilter = shopFilter($shopCode, $shopModel);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
        );
        $url = '/api/reports/rptMaterial';
        $resultAPI = $this->execute($url, 'GET', $data);
        $this->response($resultAPI);
    }

    function reportMaterialDetail_post()
    {
        $this->allowAccess('materials:read');
        $columns = array(
            0 => 'trans_date',
            1 => 'shop_code',
            2 => 'shop_name',
            3 => 'shop_type',
            4 => 'mtlCode',
            5 => 'mtlName',
            6 => 'quantity',
            7 => 'unit',
            8 => 'qty',
            9 => 'unitPri',
        );
        $fromDate = $this->input->post('fromDate') ? date('Ymd', strtotime($this->input->post('fromDate'))) : date("Ymd");
        $toDate = $this->input->post('toDate') ? date('Ymd', strtotime($this->input->post('toDate'))) : date("Ymd");
        $shopModel = $this->input->post('shopModel') ? $this->input->post('shopModel') : $this->session->userdata('shopModel');
        $shopCode = $this->input->post('shopCode') ? $this->input->post('shopCode') : $this->session->userdata('shopCode');
        $role = $this->session->userdata('role');
        $start = $this->input->post('start') ? $this->input->post('start') : 0;
        $limit = $this->input->post('length') ? $this->input->post('length') : 1000;
        $type = 1;

        $shopFilter = shopFilter($shopCode, $shopModel);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'shopCodes' => $shopCodes,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'start' => $start,
            'limit' => $limit,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
            'isExport' => false
        );
        if (!empty($this->input->post('search')['value'])) {
            $data['search'] = $this->input->post('search')['value'];
        }
        if (isset($this->input->post('order')[0]['column'])) {
            $data['orderBy'] = $columns[$this->input->post('order')[0]['column']];
        }
        if (!empty($this->input->post('order')[0]['dir'])) {
            $data['direction'] = $this->input->post('order')[0]['dir'];
        }

        $url = '/api/reports/rptMaterialDetail';
        $resultAPI = $this->execute($url, 'GET', $data);
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $message = '';
        $data = [];
        if ($resultAPI['success'] && count($resultAPI['data'])) {
            $recordsTotal = $resultAPI['data'][0]['recordsTotal'];
            $recordsFiltered = $resultAPI['data'][0]['recordsTotal'];
            $data = $resultAPI['data'];
        } else if (!$resultAPI['success']) {
            $message = $resultAPI['message'];
        }
        $sOutput = array(
            'draw' => intval($this->input->post('draw')),
            'recordsTotal' => intval($recordsTotal),
            'recordsFiltered' => intval($recordsFiltered),
            'data' => $data,
            'message' => $message,
        );
        $this->response($sOutput);
    }

    function reportMaterialDetailExport_get()
    {
        $this->allowAccess('materials:export');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $role = $this->session->userdata('role');
        $search = $this->input->get('search');
        $start = $this->input->get('start') ? $this->input->get('start') : 0;
        $limit = $this->input->get('limit') ? $this->input->get('limit') : 1000;

        $shopFilter = shopFilter($shopCode, $shopModel);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'shopCodes' => $shopCodes,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'start' => 0,
            'limit' => 0,
            'search' => $search,
            'orderBy' => 'trans_date',
            'direction' => 'desc',
            'type' => 1,
            'isExport' => 1,
        );
        $url = '/api/reports/rptMaterialDetail';
        $resultAPI = $this->execute($url, 'GET', $data);
        $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
        force_download("tiêu hao NVL.xlsx", $data);

    }

    function reportLocalTransfer_get()
    {
        $this->allowAccess('rptLocalTransfer:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        //$stockType  = $this->input->get('stockType');
        $mtlGroup = $this->input->get('type');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopStatus = $this->input->get('shopStatus') ? $this->input->get('shopStatus') : $this->session->userdata('shopStatus');

        $shopFilter = shopFilter($shopCode, $shopModel);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'status' => $mtlGroup,
            'shopStatus' => $shopStatus+0,
        );
//         log_message("debug", "getOrderList resultAPI: " . print_r($data, true));
        $url = '/api/reports/rptLocalTransfer';

        $resultAPI = $this->execute($url, 'GET', $data);
//        log_message("debug", "getOrderList resultAPI: " . print_r($resultAPI, true));

        $this->response($resultAPI);
    }

    function reportInventory_get()
    {
        $this->allowAccess('rptInventory:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $type = $this->input->get('stockType') ? $this->input->get('stockType') : 'All';
        $status = $this->input->get('status');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');

        $shopFilter = shopFilter($shopCode, $shopModel);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'type' => ($type == 'All') ? -1 : $type,
            'status' => $status,
        );

        $resultAPI = $this->execute('/api/reports/rptInventory', 'GET', $data);
        $this->response($resultAPI);
    }

    function reportInventoryDetail_get()
    {
        $this->allowAccess('rptInventory:read');
        $stockId = $this->input->get('stockId') ? $this->input->get('stockId') : null;
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : null;

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) || count($shopFilter) > 1 || $stockId == null) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $data = array(
            'id' => $stockId,
            'shopCode' => $shopCode
        );
        $resultAPI = $this->execute('/api/reports/rptInventoryDetail', 'GET', $data);
        $this->response($resultAPI);
    }

    function updateStockStatus_put()
    {
        $this->allowAccess('rptInventory:update');
        $params = json_decode(trim(file_get_contents('php://input')), true);
        $stockId = $params['stockId'];
        $shopCode = $params['shopCode'];
        $status = $params['status'];

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) || count($shopFilter) > 1 || $stockId == null) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $data = array(
            'shopCode' => $shopCode,
            'id' => $stockId,
            'status' => $status,
            'modifyBy' => $this->session->userdata('userId'),
        );
        $url = '/api/reports/updateStockStatus';
        $result = $this->execute($url, 'PUT', $data);
        $this->response($result);
    }

    function reportInventoryExport_get()
    {
        $this->allowAccess('rptInventory:export');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $stockType = $this->input->get('stockType');
        $status = null != $this->input->get('status') ? $this->input->get('status') : 9;
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');

        $shopFilter = shopFilter($shopCode, $shopModel);

        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'type' => ($stockType == 'All') ? -1 : $stockType,
            'status' => $status,
        );
        $resultAPI = $this->execute('/api/reports/rptInventoryExport', 'GET', $data);
        $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
        force_download("Báo cáo kiểm kê.xlsx", $data);
    }

//    function rptShop_get()
//    {
//        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shop_code');
//        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
//        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
//        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
//        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : '00';
//        $status = null != $this->input->get('status') ? $this->input->get('status') : 'All';
//        $role = $this->session->userdata('role');
//
//        $shopFilter = shopFilter($shopCode, $shopModel, 'All', $provinceCode, $districtCode);
//
//        if (!count($shopFilter)) {
//            $this->response(array(
//                'success' => false,
//                'message' => 'Bad request.'
//            ), 400);
//        }
//
//        $dataPost = array(
//            'shopCode' => $shopCode,
//            'provinceCode' => $provinceCode,
//            'districtCode' => $districtCode,
//            'shopType' => $shopType,
//            'shopModel' => $shopModel,
//            'status' => $status,
//            'role' => $role
//        );
//        $resultAPI = $this->execute('/api/reports/rptShop', 'GET', $dataPost);
//        $this->response($resultAPI);
//    }

    function rptWareHouse_get()
    {
        $this->allowAccess('rptWareHouse:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ?? $this->session->userdata('shopCode');
        $shopType = $this->input->get('shopType') ?? 'All';
        $shopModel = $this->input->get('shopModel') ?? $this->session->userdata('shopModel');
        $groupCode = $this->input->get('groupCode') ?? 'All';
        $provinceCode = $this->input->get('provinceCode') ?? 'All';
        $districtCode = $this->input->get('districtCode') ?? 'All';
        $isExport = $this->input->get('isExport') ?? false;
        $role = $this->session->userdata('role');

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode, $groupCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'shopCodes' => $shopCodes,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'isExport' => $isExport,
        );
        $resultAPI = $this->execute('/api/reports/rptWareHouse', 'GET', $data);
//        $this->response($resultAPI);
        if($isExport){
            $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
            force_download("Nhập kho OBS.xlsx", $data);
        }else{
            $this->response($resultAPI);
        }

    }

    function rptWareHouseDetail_get()
    {
        $this->allowAccess('rptWareHouse:read');
        $invId = $this->input->get('invId') ? $this->input->get('invId') : null;
        $shopCode = $this->input->get('shopCode');

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $data = array(
            'id' => $invId,
            'shopCode' => $shopCode
        );
        $resultAPI = $this->execute('/api/reports/rptWareHouseDetail', 'GET', $data);
        $this->response($resultAPI);
    }

    //Thành API
    function lookupStore_get()
    {
        $this->allowAccess('lookupStore:read');
        $data = array(
            "TOKEN" => "WEBEMULATOR28X0X25X08899",
            "ADDRESS" => $this->get('address'),
            "LNG" => $this->get('lng') ? $this->get('lng') : 0,
            "LAT" => $this->get('lat') ? $this->get('lat') : 0,
            "DISTANCE" => $this->get('distance') ? $this->get('distance') : 0,
            "SHOP_MODEL" => strtoupper($this->get('shopModel')),
            "SHOP_TYPE" => strtoupper($this->get('shopType'))
        );
        $url = '/api/ob/web/shop/getListShopNearbyLngLatOrAddress';
        $result = $this->execute($url, 'POST', $data, false);
        $response = array(
            'data' => $result['result']
        );

        $this->response($response);
    }


    function checkincheckout_get()
    {
        $this->allowAccess('checkincheckout:read');
        $date = $this->input->get('date') ? $this->input->get('date') : date("d-m-Y");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');


        // if($shopCode == 'All') $shopCode = $this->session->userdata('shopCode');

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'data' => [],
            ));
        }


        $dataPost = array(
            'date' => date('Y-m-d', strtotime($date)),
            'shop_code' => $shopCode
        );


        $dataReturn = $this->site->getCheckIncheckOut($dataPost);
        if (isset($dataReturn)) {
            if ($dataReturn['status'] == 'OK' && $dataReturn['errorCode'] == 200) {
                $shopCodes = array_column($shopFilter, 'shopCode');
                $data = $dataReturn['result'];
                $data = array_filter($data, function ($shop) use ($shopCodes) {
                    return (in_array($shop['SHOP_CODE'], $shopCodes));
                });
                $data = array_values($data);
            } else {
                $data = [];
            }
        } else {
            $data = [];
        }
        $sOutput = array(
            'data' => $data,
        );
        $this->response($sOutput);
    }

    function checkincheckoutdetail_get()
    {
        $this->allowAccess('checkincheckout:read');
        $month = $this->input->get('month') ? $this->input->get('month') : '01-' . date("m-Y");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : 'All';

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'data' => [],
            ));
        }
        $shopCodes  = array_column($shopFilter, 'shopCode');

        $lenmonth = strlen($month);
        if ($lenmonth > 7) {
            $month = date('m-Y', strtotime($month));
        }

        $to_date = '25-' . $month;
        $from_date = date("d-m-Y", strtotime($to_date)) . " -1 month";
        $from_date = strtotime(date("d-m-Y", strtotime($from_date)) . " +1 day");
        $from_date = strftime("%Y-%m-%d", $from_date);

        $dataPost = array(
            'shop_code' => $shopCodes,
            'from_date' => $from_date,
            'to_date' => date('Y-m-d', strtotime($to_date))
        );
        $columns = [];
        $dataReturn = $this->site->getCheckIncheckOutDetail($dataPost);
        if (isset($dataReturn)) {
            if ($dataReturn['status'] == 200) {
                $data = $dataReturn['metadata'];
                if (count($data) > 0) {
                    if (!empty($data[0])) {
                        $arr_col = $data[0];
                        $i = 0;
                        foreach ($arr_col as $key => $valCol) {
                            $order = ($i > 0 && $i < 5) ? true : false;
                            $visible = ($i == 0 ) ? false : true;
                            $columns[] = ['visible' => $visible ,'title' => $key, 'data' => $key, 'orderable' => $order];
                            $i++;
                        }
                    }
                }
            } else {
                $data = [];
            }
        } else {
            $data = [];
        }
        $sOutput = array(
            'data' => $data,
            'columns' => $columns,
            'month' => $from_date
        );
        $this->response($sOutput);
    }

    function addAnnualLeave_post()
    {
        $this->allowAccess('checkincheckout:update');
        $shopFilter = shopFilter($this->input->post("shopCode"));
        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = array(
            'userName'      => $this->input->post("userName"),
            'shopCode'      => $this->input->post("shopCode"),
            'note'          => $this->input->post("note"),
            'user_id'       => $this->input->post('user_id'),
            'rowId'         => $this->input->post('rowId'),
            'date'          => $this->input->post('date'),
            'annual_leave'  => $this->input->post('annual_leave'),
            'paid_holiday'  => $this->input->post('paid_holiday'),
            'sick_leave'    => $this->input->post('sick_leave'),
            'birthday_leave'=> $this->input->post('birthday_leave'),
            'reason'        => $this->input->post('reason'),
            'reason_title'  => $this->input->post('reason_title')
        );
        $dataQC = $this->site->addAnnualLeave($data);
        if ($dataQC['status'] == 200 || $dataQC['status'] == 201) {
            $dataResult = $dataQC['metadata'];
            $dataReturn = array(
                'error' => false,
                'message' => 'Thành công',
                'dataResult' => $dataResult
            );
        } else {
            $dataReturn = array(
                'error' => true,
                'message' => $dataQC['message']
            );
        }

        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        $this->response($this->data);
    }

    function getInfoEmployeeWithGridTable_post()
    {
        $this->allowAccess('checkincheckout:read');
        $header_title = $this->input->post('header_title') ? date("Y-m-d", strtotime($this->input->post('header_title'))) : '';
        $username = $this->input->post('username') ? $this->input->post('username') : '';
        $shopcode = $this->input->post('shopcode') ? $this->input->post('shopcode') : '';
        $userId   = $this->input->post('user_id') ? $this->input->post('user_id') : '';
        $type     = $this->input->post('type') ? $this->input->post('type') : 1;
        $data     = [];

        if ($header_title != '' && $username != '' && $shopcode != '' && $userId != '') {
            if($type == 1) {
                $dataReturn = $this->site->checkInCheckOutByEmpCodeAndShopCodeAndDay(['username' => $username, 'shopcode' => $shopcode, 'datetime' => $header_title, 'userId' => $userId]);
                if (isset($dataReturn)) {
                    if ($dataReturn['status'] == 200) {
                        $data = (!empty($dataReturn['metadata']))?$dataReturn['metadata']:[];
                    }
                }
            } else {
                $data['balance'] = [];
                //Get reason
                $dataReturnReason   = $this->site->getReason();
                $dataReturn = $this->site->leaveBalance(['username' => $username, 'shopcode' => $shopcode, 'datetime' => $header_title, 'userId' => $userId]);
                if (isset($dataReturn)) {
                    if ($dataReturn['status'] == 200) {
                        $data['balance'] = (!empty($dataReturn['metadata'][0]))?$dataReturn['metadata'][0]:[];
                    }
                }
                $data['reason']     = $dataReturnReason['metadata'];
            }

        }
        $sOutput = array(
            'data' => $data
        );

        $this->response($sOutput);
    }

    function updateEmpCheckOut_post()
    {
        $this->allowAccess('checkincheckout:update');
        $dataReturn = array(
            'error' => true,
            'message' => 'Có lỗi xảy ra vui lòng thử lại'
        );
        $data = [];
        if ($this->input->post()) {
            $data = array(
                'username' => $this->input->post("username"),
                'shopcode' => $this->input->post("shopcode"),
                'timecheckout' => date('Y-m-d H:i:s', strtotime($this->input->post("date") . ' ' . $this->input->post("timecheckout"))),
                'note' => $this->input->post("note"),
                'update_by' => $this->session->userdata("userId"),
                'row_id' => $this->input->post('rowid')
            );
            $dataQC = $this->site->updateEmpCheckOut($data);
            if ($dataQC['status'] == 200) {
                $dataResult = $dataQC['metadata'];
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Cập nhật thành công',
                    'dataResult' => $dataResult
                );
            }
        }

        $this->data["dataReturn"] = array_merge($dataReturn, $data);
        $this->response($this->data);
    }


    // PROMOTION BY VIP CARD
    function reportPromotionByVipCard_post()
    {
        $this->allowAccess('rptPromotionByVipCard:read');
        $columns = array(
            0 => 'trans_date',
            1 => 'shop_code',
            2 => 'shop_name',
            3 => 'employee_code',
            4 => 'employee_name',
            5 => 'position',
            6 => 'card_number',
            7 => 'item_code',
            8 => 'item_name',
            9 => 'trans_num',
            10 => 'quantity',
            11 => 'price',
            12 => 'discount',
            13 => 'amount'
        );

        $fromDate = $this->input->post('fromDate') ? date("Ymd", strtotime($this->input->post('fromDate'))) : date("Ymd");
        $toDate = $this->input->post('toDate') ? date("Ymd", strtotime($this->input->post('toDate'))) : date("Ymd");
        $shopCode = $this->input->post('shopCode') ? $this->input->post('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->post('shopModel') ? $this->input->post('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->post('shopType') ? $this->input->post('shopType') : 'All';
        $provinceCode = $this->input->post('provinceCode') ? $this->input->post('provinceCode') : 'All';
        $districtCode = $this->input->post('districtCode') ? $this->input->post('districtCode') : 'All';
        $role = $this->session->userdata('role');
        $start = $this->input->post('start') ? $this->input->post('start') : 0;
        $limit = $this->input->post('length') ? $this->input->post('length') : 100;

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'startDate' => $fromDate,
            'endDate' => $toDate,
            'shopCode' => $shopCodes,
            'start' => $start,
            'limit' => $limit,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
            'isExport' => false,
        );

        if (!empty($this->input->post('search')['value'])) {
            $dataPost['search'] = $this->input->post('search')['value'];
        }
        if (isset($this->input->post('order')[0]['column'])) {
            $dataPost['orderBy'] = $columns[$this->input->post('order')[0]['column']];
        }
        if (!empty($this->input->post('order')[0]['dir'])) {
            $dataPost['direction'] = $this->input->post('order')[0]['dir'];
        }
        $resultAPI = $this->execute("/api/reports/rptVipCard", "GET", $dataPost);
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $totalAmount = 0;
        $totalDiscount = 0;
        $totalQuantity = 0;
        $message = '';
        $data = [];
        if ($resultAPI['success'] && count($resultAPI['data'])) {
            $recordsTotal = count($resultAPI['data']);
            $recordsFiltered = count($resultAPI['data']);
//            $totalAmount = $resultAPI['data'][0]['totalAmount'];
//            $totalDiscount = $resultAPI['data'][0]['totalDiscount'];
//            $totalQuantity = $resultAPI['data'][0]['totalQuantity'];
            $data = $resultAPI['data'];
        } else if (!$resultAPI['success']) {
            $message = $resultAPI['message'];
        }

        $sOutput = array(
            'draw' => intval($this->input->post('draw')),
            'recordsTotal' => intval($recordsTotal),
            'recordsFiltered' => intval($recordsFiltered),
            'totalQuantity' => $totalQuantity,
            'totalDiscount' => $totalDiscount,
            'totalAmount' => $totalAmount,
            'data' => $data,
            'message' => $message
        );
        $this->response($sOutput);
    }

    function reportPromotionByVipCardExport_get()
    {
        $this->allowAccess('rptPromotionByVipCard:export');
        $fromDate = $this->input->get('fromDate') ? date("Ymd", strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date("Ymd", strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $role = $this->session->userdata('role');

        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';

        $type = $this->input->get('type') ? $this->input->get('type') : '1';
        $limit = $this->input->get('limit') ? $this->input->get('limit') : 0;
        $search = $this->input->get('search');

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'startDate' => $fromDate,
            'endDate' => $toDate,
            'shopCode' => $shopCodes,
//            'start' => 0,
//            'limit' => 0,
            'search' => '',
            'orderBy' => 'trans_date',
            'direction' => 'desc',
            'isExport' => true,
        );
        if (!empty($search)) {
            $dataPost['search'] = $search;
        }
//        $this->response($dataPost);
        $resultAPI = $this->execute("/api/reports/rptVipCard", "GET", $dataPost);
//        if ($resultAPI['success']) {
//            $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
//            force_download("Doanh số theo sản phẩm.xlsx", $data);
//        }
        $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
        force_download("Doanh số theo thẻ VIP.xlsx", $data);
//        exit(0);
    }

    function reportAttendanceDetailExport_get()
    {
        $this->allowAccess('checkincheckout:export');
        $month = $this->input->get('month') ? date("Ymd", strtotime($this->input->get('month'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : '';

        // $shopCodes = array_column($shopFilter, 'shopCode');

        $monthCurrent =  date("Y-m", strtotime($month));
        $fromDate =  $monthCurrent . '-25';
        $monthPre = date('Y-m', strtotime($fromDate. ' - 1 month'));
        $toDate = $monthPre . '-26';

        $dataPost = array(
            'start_date' => $toDate,
            'end_date' => $fromDate,
            'shop_codes' => $shopCode
        );
        $resultAPI = $this->execute('attendance/details', 'GET', $dataPost, false);
        // log_message("debug", "attendance/details: " . print_r($resultAPI, true));
        $data = file_get_contents(URL_API_DOWNLOAD_FILE_ . '/' . $resultAPI['data']['fileName']);
        force_download("Báo cáo chấm công chi tiết.xlsx", $data);
    }

}
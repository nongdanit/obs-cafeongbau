<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'BaseRest.php';
class utils extends BaseRest {
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    function shopList_get(){
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $shopId = $this->input->get('shopId') ? $this->input->get('shopId') : NULL;
        $role = $this->session->userdata('role');
        switch ($shopModel) {
            case '01':
                $shopModel = 'CSH';
                break;
            case '02':
                $shopModel = 'NQ';
                break;
        }
        $shopList = shopFilter($shopCode, $shopModel,  $shopType, $provinceCode, $districtCode);
        $this->response($shopList);     
    }

    function districts_get(){
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districts = $this->session->userdata('districts');
        if($provinceCode != 'All'){
            $distrctList = array();
            foreach($districts as $value){
                if($value['provinceCode'] == $provinceCode){
                    $distrctList[] = $value;
                }
            }
            $this->response($distrctList);
        }
        $this->response($districts); 
    }

    function wards_get(){
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';

        $resultApi = $this->data_lib->execute('/api/utilities/wardList','GET', array( 'districtCode' => $districtCode)); 
        $wards = array();
        if($resultApi['success']){
            $wards= $resultApi['data'];
        }
        $this->response($wards);   
    }
}
<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'BaseRest.php';

class Business extends BaseRest
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper('download');
        $this->load->model('Utilities');
    }

    function checkOrderState($state)
    {
        return in_array($state, ['All', 'sended', 'processing', 'pending', 'approved', 'notdelivery', 'delivered']);
    }

    function checkOrderType($type)
    {
        return in_array($type, ['create', 'approve', 'delivery']);
    }

    function orderList_get()
    {
        if (!check_permission('orders:read') && !check_permission('delivery:read') && !check_permission('approve_order:read')) {
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data' => []
            ), 403);
        }

//        $this->load->library('form_validation');
//        $params = $this->input->get();
//        $this->form_validation->set_data($params);
//        $this->form_validation->set_rules('fromDate', '', 'trim|xss_clean|required|min_length[10]|max_length[10]');
//        $this->form_validation->set_rules('toDate', '', 'trim|xss_clean|required|min_length[10]|max_length[10]');
//        // $this->form_validation->set_rules('shopModel', '', 'trim|xss_clean|required|min_length[21]|max_length[21]');
//        $this->form_validation->set_rules('state', '', 'trim|xss_clean|required|callback_checkOrderState');
//        $this->form_validation->set_rules('type', '', 'trim|xss_clean|required|callback_checkOrderType');
//
//        if($this->form_validation->run() == FALSE){
//            $this->badRequest($this->form_validation->error_array());
//        }
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ym") . '01';
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $state = $this->input->get('state') ? $this->input->get('state') : 'processing';
        $typeOrder = $this->input->get('type') ? $this->input->get('type') : 'create';
        $shopCode = $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ?? $this->session->userdata('shopModel');
        $orderType = (!empty($this->input->get('orderType')))?$this->input->get('orderType'):'All';
        $typeFilter = $this->input->get('typeFilter') ?? 0;


//        echo $shopModel
        $shopFilter = shopFilter($shopCode, $shopModel);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        if (!empty($orderType) && $orderType != 'All'){
            $order_type_inv = $this->session->userdata('order_type_inv');
            $key = array_search($orderType, array_column($order_type_inv, 'gc_code'));
            $orderType =  $order_type_inv[$key]['order_type'];
        }

        $dataPost = array(
            'shopCodes' => $shopCodes,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'state' => $state,
            'typeOrder' => $typeOrder,
            'typeFilter'  => $typeFilter,
            'orderTypeInv'  => $orderType
        );
        // $this->response($dataPost);
        $resultAPI = $this->execute('/api/business/orderList', 'GET', $dataPost);

        $this->response($resultAPI);
    }

    function createOrder_post()
    {
        if (!check_permission('orders:insert') && !check_permission('approve_order:insert')) {
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data' => []
            ), 403);
        }
//        $params = $this->input->post();
//        $this->form_validation->set_data($params);
//        $this->form_validation->set_rules('shopCode', '', 'trim|xss_clean|required|min_length[10]|max_length[10]');
//        $this->form_validation->set_rules('orderDate', '', 'trim|xss_clean|required|min_length[10]|max_length[10]');
//        $this->form_validation->set_rules('orderCode', '', 'trim|xss_clean|required|min_length[21]|max_length[21]');
//        $this->form_validation->set_rules('orderNote', '', 'trim|xss_clean|min_length[0]|max_length[200]');
//        $this->form_validation->set_rules('orderStatus', '', 'trim|xss_clean|required|numeric');
//        if($this->form_validation->run() == FALSE){
//            $this->badRequest($this->form_validation->error_array());
//        }

        $shopCode = $this->input->post("shopCode");
        $invDate = $this->input->post("orderDate");
        $invCode = $this->input->post("orderCode");
        $note = $this->input->post("orderNote");
        $status = $this->input->post("orderStatus");
        $orderType = $this->input->post("orderType");
        $order_type_inv = $this->session->userdata('order_type_inv');
        $key = array_search($orderType, array_column($order_type_inv, 'gc_code'));
        $orderType =  $order_type_inv[$key]['order_type'];

        $shopFilter = shopFilter($shopCode);

        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }
        $data = array(
            'shopCode' => $shopCode,
            'invCode' => $invCode,
            'invDate' => date('Ymd', strtotime($invDate)),
            'invType' => 'BK',
            'note' => $note,
            'status' => $status,
            'userId' => $this->session->userdata('userId'),
            'orderType' => $orderType
        );
        $resultAPI = $this->execute('/api/business/order/create', 'POST', $data);
        $this->response($resultAPI);


    }

    function orderDetail_get()
    {
        if (!check_permission('orders:read') && !check_permission('delivery:read') && !check_permission('approve_order:read')) {
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data' => []
            ), 403);
        }
        $invId = $this->input->get('id');
        $shopCode = $this->input->get('shopCode');

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $data = array(
            'id' => $invId,
            'shopCode' => $shopCode
        );

        $resultAPI = $this->execute('/api/business/order/detail', 'GET', $data);
        $this->response($resultAPI);
    }

    function orderDetail_post()
    {
        if (!check_permission('orders:update') && !check_permission('delivery:update') && !check_permission('approve_order:update')) {
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data' => []
            ), 403);
        }
        $shopCode = $this->input->post('shopCode');
        $invId = $this->input->post('invId');
        $detailId = $this->input->post("detailId");
        $mtlCode = $this->input->post("mtlCode");
        $mtlConvertCode = $this->input->post("mtlConvertCode");
        $mtlName = $this->input->post("mtlName");
        $unit = $this->input->post("unit");
        $price = $this->input->post("price");
        $qtyOrder = $this->input->post("qtyOrder");
        $qtyApprove = $this->input->post("qtyApprove");
        $qtyDelivery = $this->input->post("qtyDelivery");
        $state = $this->input->post("state");
        $userId = $this->session->userdata('userId');

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $data = array(
            'shopCode' => $shopCode,
            'invId' => $invId,
            'detailId' => $detailId,
            'mtlCode' => $mtlCode,
            'mtlConvertCode' => $mtlConvertCode,
            'mtlName' => $mtlName,
            'unit' => $unit,
            'price' => $price,
            'qtyOrder' => $qtyOrder,
            'qtyApprove' => $qtyApprove,
            'qtyDelivery' => $qtyDelivery,
            'userId' => $userId,
            'state' => $state
        );
        $resultAPI = $this->execute('/api/business/order/detail/create', 'POST', $data);
        $this->response($resultAPI);
    }


    function activeOrder_put()
    {
        if (!check_permission('orders:update') && !check_permission('delivery:update') && !check_permission('approve_order:update')) {
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data' => []
            ), 403);
        }
        $params = json_decode(trim(file_get_contents('php://input')), true);


        $id = $params['id'];
        $shopCode = $params['shopCode'];
        $status = $params['status'];
        $state = $params['state'];
        $priceId = $params['priceId'] ?? 0;
        $approveNote = $params['approveNote'] ?? '';
        $approveOrgCode = $params['approveOrgCode'] ?? '';

        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $response = $this->Utilities->getErpPrice();
        $erpPrices = $response['data'];

        $key = array_search($priceId, array_column($erpPrices, 'rowid'));
        $listHeaderId =  $erpPrices[$key]['listHeaderId'];

        $data = array(
            'id' => $id,
            'shopCode' => $shopCode,
            'status' => $status,
            'state' => $state,
            'priceId' => $listHeaderId,
            'approveNote' => $approveNote,
            'approveOrgCode' => $approveOrgCode,
            'userId' => $this->session->userdata('userId')
        );
        // $this->response($data);
        $resultAPI = $this->execute('/api/business/order/status', 'PUT', $data);
        $this->response($resultAPI);
    }

    function stores_get()
    {
        $this->allowAccess('mbs_store:read');
        $getListShop = $this->site->getListShop();
        if (isset($getListShop)) {
            if ($getListShop['status'] == 'OK' && $getListShop['errorCode'] == 200) {
                $data = $getListShop['result'];
            } else $data = [];
        } else {
            $data = [];
        }

        $sOutput = array
        (
            'data' => $data,
        );

        $this->response($sOutput);
    }

    function promotionShop_get()
    {
        $this->allowAccess('promotions:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ym") . '01';
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');

        $shopFilter = shopFilter($shopCode, $shopModel);

        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $shopCodes = array_column($shopFilter, 'shopCode');

        $dataPost = array(
            'shopCodes' => $shopCodes,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        );
        $resultAPI = $this->execute('/api/business/promotion/shop', 'GET', $dataPost);
        $this->response($resultAPI);
    }

    function approvePromotion_post()
    {
        $this->allowAccess('promotions:insert');
        $shopList = $this->session->userdata('shops');
        $promoId = $this->input->post('promoId');
        $shopCode = $this->input->post('shopCode');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $rowId = $this->input->post('rowid');

        $shopFilter = shopFilter($shopCode);

        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $data = array();
        $shopIndex = array_search($shopCode, array_column($shopList, 'shopCode'));
        $shop = $shopList[$shopIndex];
        $data = array(
            'shopId' => $shop['shopId'],
            'promoId' => $promoId,
            'shopCode' => $shopCode,
            'startDate' => date("Ymd", strtotime($startDate)),
            'endDate' => date("Ymd", strtotime($endDate)),
            'ob' => 1,
            'createBy' => $this->session->userdata('userId'),
        );
        $resultAPI = $this->execute('/api/business/promotion/shop/create', 'POST', $data);
        $this->response($resultAPI);
    }

    function approvePromotion_put()
    {
        $this->allowAccess('promotions:update');
        $params = json_decode(trim(file_get_contents('php://input')), true);
        $promoId = $params['id'];
        $shopCode = $params['shopCode'];
        $startDate = $params['startDate'];
        $endDate = $params['endDate'];

        $shopFilter = shopFilter($shopCode);

        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $data = array(
            'promoId' => $promoId,
            'shopCode' => $shopCode,
            'startDate' => date("Ymd", strtotime($startDate)),
            'endDate' => date("Ymd", strtotime($endDate)),
            'createBy' => $this->session->userdata('userId'),
        );
        $resultAPI = $this->execute('/api/business/promotion/shop/update', 'PUT', $data);
        $this->response($resultAPI);
    }

    function transferList_get()
    {
        $this->allowAccess('localTransfer:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $status = $this->input->get('status') ?? -1;
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $invType = $this->input->get('invType') ?? 'NB';
        $shopFilter = shopFilter($shopCode);
        // log_message("debug", "transferList_get: ".print_r($shopFilter, true));
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        if ($status == 'All') $status = -1;

        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'status' => $status,
            'invType' => $invType
        );
        $resultApi = $this->execute('/api/business/localtransfer/list', 'GET', $data);
        $this->response($resultApi);
    }

    function transferDetail_get()
    {
        $this->allowAccess('localTransfer:read');
        $invId = $this->input->get('id') ? $this->input->get('id') : 0;
        $shopCode = $this->input->get('shopCode');

        $shopFilter = shopFilter($shopCode);

        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $data = array(
            'id' => $invId,
            'shopCode' => $shopCode
        );
        //var_dump($data);die;
        $resultApi = $this->execute('/api/business/localtransfer/detail', 'GET', $data);
        $this->response($resultApi);
    }

    function transferCreate_post()
    {
//        $this->allowAccess('localTransfer:insert');
        $dataPost = json_decode(trim(file_get_contents('php://input')), true);
        $invType = $dataPost['ticketType'];

        $shopFilter = shopFilter($dataPost['fromShop']);

        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }
        if (!in_array($invType, ['PX', 'NB'])) {
            $shopFilter = shopFilter($dataPost['toShop']);

            if (!count($shopFilter) || count($shopFilter) > 1) {
                $this->response(array(
                    'success' => false,
                    'message' => 'Bad request.'
                ));
            }
        }

        if ($dataPost['ticketId'] == '') $dataPost['ticketId'] = 0;

        $data = array(
            'id' => $dataPost['ticketId'],
            'fromShopCode' => $dataPost['fromShop'],
            'toShopCode' => $dataPost['toShop'],
            'invCode' => $dataPost['ticketCode'],
            'invDate' => date('Ymd', strtotime($dataPost['ticketDate'])),
            'note' => $dataPost['ticketNote'] ? $dataPost['ticketNote'] : null,
            'status' => $dataPost['ticketStatus'] ? $dataPost['ticketStatus'] : 0,
            'details' => $dataPost['ticketDetails'],
            'createBy' => $this->session->userdata('userId'),
            'invType' => $invType,
        );
        $resultAPI = $this->execute('/api/business/localtransfer/create', 'POST', $data);
        $this->response($resultAPI);
    }

    function transferExport_get()
    {
        $this->allowAccess('localTransfer:export');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $status = $this->input->get('status') ?? -1;
        $invType = $this->input->get('invType') ?? 'NB';
        $shopCode = $this->session->userdata('shopCode');

        $shopFilter = shopFilter($shopCode);

        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        if($status == 'All') $status = -1;

//        switch ($state) {
//            case 'All':
//                $state = -1;
//                break;
//            case 'processing':
//                $state = 0;
//                break;
//            case 'completed':
//                $state = 9;
//                break;
//            default:
//                $state = -1;
//                break;
//        }

        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'status' => $status,
            'invType' => $invType,
        );
        $resultAPI = $this->execute('/api/business/localtransfer/export', 'GET', $data);
        if ($resultAPI['success']) {
            set_time_limit(0);
            http_response_code(200);
            ob_clean();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="Báo cáo điều chuyển hàng.xlsx"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            // header('Content-Length: ' . filesize($filepath));
            flush();
            readfile(URL_API . '/' . $resultAPI['data']['fileName']);
        }
        exit(0);
    }

    function materialList_get()
    {
        // $this->allowAccess('inventory:read');
        $type = $this->input->get('type');
        $data = array(
            'type' => $type
        );
        $resultAPI = $this->execute('/api/business/material/list', 'GET', $data);
        $this->response($resultAPI);
    }

    function stockCreate_post()
    {
        $this->allowAccess('inventory:insert');
        $dataPost = json_decode(trim(file_get_contents('php://input')), true);

        $shopFilter = shopFilter($dataPost['shopCode']);

        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $data = array(
            'id' => $dataPost['stockId'] ? $dataPost['stockId'] : 0,
            'shopCode' => $dataPost['shopCode'],
            'stockCode' => $dataPost['stockCode'],
            'stockDate' => date('Ymd', strtotime($dataPost['stockDate'])),
            'stockType' => $dataPost['stockType'],
            'stockStatus' => $dataPost['stockStatus'],
            'note' => $dataPost['stockNote'] ? $dataPost['stockNote'] : null,
            'userId' => $this->session->userdata('userId'),
            'details' => $dataPost['stockDetails'],
        );
        //log_message("debug", "api test: " . print_r($data, true));
        $resultAPI = $this->execute('/api/business/stock/create', 'POST', $data);
        $this->response($resultAPI);
    }

    function employee_get()
    {
        $data = array();
        $sOutput = array(
            'data' => &$data,
        );
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $option = $this->input->get('option') ? $this->input->get('option') : 'LIST';
        $data = [];
        if ($shopCode == 'All') $shopCode = '';

        // $shopFilter = shopFilter($shopCode);

        // if (!count($shopFilter)) {
        //     $this->response($sOutput);
        // }
        // $shopCodes = array_column($shopFilter, 'shopCode');
        // array_push($shopCodes, "POS01CUONG", "POS01");

        if ($option == 'LIST') {
            $resultAPI = $this->site->getEMPLOYERByShopCode(['shop_code' => $shopCode]);
        } else {
            $resultAPI = $this->site->getListEmpDelByShopCode(['shop_code' => $shopCode]);
        }
        if (!empty($resultAPI)) {
            if ($resultAPI['status'] == 200 ) {
                // $shopCodes = array_column($shopFilter, 'shopCode');
                $data = $resultAPI['metadata'];
                // $data = array_filter($data, function ($shop) use ($shopCodes) {
                //     return (in_array($shop['shop_code'], $shopCodes) || $shop['shop_code'] == null);
                // });
                $data = array_values($data);
            }
        }
        $this->response($sOutput);
    }

    function erpPrice_get()
    {
        $finalResponse = array(
            'success' => false,
            'code' => 400,
            'message' => '',
            'data' => [],
        );
        $response = $this->execute('/api/business/price/erpPrice', HTTP_GET, []);
        $finalResponse['data'] = $response['data'];
        $finalResponse['message'] = $response['message'];
        $finalResponse['code'] = $response['code'];
        $finalResponse['success'] = $response['success'];
        $this->response($finalResponse);

    }

    function businessApproveOrderExport_get()
    {
        $this->allowAccess('approve_order:export');
        $fromDate = $this->input->get('fromDate') ? date("Ymd", strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date("Ymd", strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $role = $this->session->userdata('role');

        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';

        $search = $this->input->get('search');
        $invId = $this->input->get('invId') ? $this->input->get('invId') : '';

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $get_invId = rtrim($invId,",");
        $convert_invId = explode(",", $get_invId);

        if(!empty($invId)){
            $dataPost = array(
                'invIds' => $convert_invId,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
            );
        }
        if (!empty($search)) {
            $dataPost['search'] = $search;
        }
        $resultAPI = $this->execute("/api/business/order/detailsAll", "GET", $dataPost);

        $data = file_get_contents(URL_API . '/' . $resultAPI['data']['fileName']);
        force_download("Tất Cả Chi Tiết Duyệt ĐƠn Hàng.xlsx", $data);
//        exit(0);
    }

    function purchaseList_get()
    {
        $this->allowAccess('purchase:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $status = $this->input->get('status') ?? -1;
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $invType = $this->input->get('invType') ?? 'MN';
        $shopFilter = shopFilter($shopCode);
        // log_message("debug", "purchaseList_get: ".print_r($shopFilter, true));
        if (!count($shopFilter) ) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        // array_push($shopCodes, "POS01HUNG", "TES01");
        if ($status == 'All') $status = -1;

        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'status' => $status,
            'invType' => $invType
        );
        $resultApi = $this->execute('/api/business/purchase/list', 'GET', $data);
        $this->response($resultApi);
    }

    function updateStatusPurchase_put()
    {
        if ( !check_permission('purchase:Update') ) {
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data' => []
            ), 403);
        }
        $params = json_decode(trim(file_get_contents('php://input')), true);

        $id = $params['id'];
        $status = $params['status'];

        $data = array(
            'id' => $id,
            'status' => $status,
            'userId' => $this->session->userdata('userId')
        );
        $resultAPI = $this->execute('/api/business/purchase/status', 'PUT', $data);
        $this->response($resultAPI);
    }

    function purchaseDetail_get()
    {
        $this->allowAccess('purchase:read');
        $invId = $this->input->get('id') ? $this->input->get('id') : 0;
        $shopCode = $this->input->get('shopCode');

        $shopFilter = shopFilter($shopCode);

        if (!count($shopFilter) || count($shopFilter) > 1) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $data = array(
            'id' => $invId,
            'shopCode' => $shopCode
        );
        //var_dump($data);die;
        $resultApi = $this->execute('/api/business/purchase/detail', 'GET', $data);
        $this->response($resultApi);
    }

    function purchaseCreate_post()
    {
       $this->allowAccess('purchase:insert');
        $dataPost = json_decode(trim(file_get_contents('php://input')), true);
        $invType = $dataPost['ticketType'];
        if ($dataPost['ticketId'] == '') $dataPost['ticketId'] = 0;

        $data = array(
            'invId'     => $dataPost['ticketId'],
            'shopCode'  => $dataPost['toShop'],
            'invCode'   => $dataPost['ticketCode'],
            'invDate'   => date('Ymd', strtotime($dataPost['ticketDate'])),
            'invType'   => $invType,
            'status'    => $dataPost['ticketStatus'] ? $dataPost['ticketStatus'] : 0,
            'note'      => $dataPost['ticketNote'] ? $dataPost['ticketNote'] : null,
            'details'   => $dataPost['ticketDetails'],
            'userId'    => $this->session->userdata('userId'),
        );
        $resultAPI = $this->execute('/api/business/purchase/create', 'POST', $data);
        $this->response($resultAPI);
    }

    function purchaseExport_get()
    {
        $this->allowAccess('purchase:export');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $status = $this->input->get('status') ?? -1;
        $invType = $this->input->get('invType') ?? 'MN';
        $shopCode = $this->session->userdata('shopCode');

        $shopFilter = shopFilter($shopCode);

        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        if($status == 'All') $status = -1;

        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'status' => $status,
            'invType' => $invType,
        );
        $resultAPI = $this->execute('/api/business/purchase/export', 'GET', $data);
        if ($resultAPI['success']) {
            set_time_limit(0);
            http_response_code(200);
            ob_clean();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="Báo cáo điều chuyển hàng.xlsx"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            // header('Content-Length: ' . filesize($filepath));
            flush();
            readfile(URL_API . '/' . $resultAPI['data']['fileName']);
        }
        exit(0);
    }

    function scrapList_get()
    {
        $this->allowAccess('scrap:read');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $status = $this->input->get('status') ?? -1;
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopFilter = shopFilter($shopCode);
        if (!count($shopFilter) ) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        // array_push($shopCodes, "POS01");
        if ($status == 'All') $status = -1;
        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'status' => $status
        );
        // log_message("debug", "scrapList_get: ".print_r($data, true));
        $resultApi = $this->execute('/api/business/scrap/list', 'GET', $data);
        $this->response($resultApi);
    }

    function scrapDetail_get()
    {
        $this->allowAccess('scrap:read');
        $invId = $this->input->get('id') ? $this->input->get('id') : 0;

        $data = array(
            'id' => $invId
        );
        //var_dump($data);die;
        $resultApi = $this->execute('/api/business/scrap/detail', 'GET', $data);
        $this->response($resultApi);
    }

    function updateStatusScrap_put()
    {
        if ( !check_permission('scrap:Update') ) {
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data' => []
            ), 403);
        }
        $params = json_decode(trim(file_get_contents('php://input')), true);

        $id = $params['id'];
        $status = $params['status'];

        $data = array(
            'id' => $id,
            'status' => $status,
            'userId' => $this->session->userdata('userId')
        );
        $resultAPI = $this->execute('/api/business/scrap/status', 'PUT', $data);
        $this->response($resultAPI);
    }

    function scrapCreate_post()
    {
       $this->allowAccess('scrap:insert');
        $dataPost = json_decode(trim(file_get_contents('php://input')), true);
        $invType = $dataPost['ticketType'];
        if ($dataPost['ticketId'] == '') $dataPost['ticketId'] = null;

        $data = array(
            'shopCode'  => $dataPost['toShop'],
            'invCode'   => $dataPost['ticketCode'],
            'invDate'   => date('Ymd', strtotime($dataPost['ticketDate'])),
            'invType'   => $invType,
            'status'    => $dataPost['ticketStatus'] ? $dataPost['ticketStatus'] : 0,
            'note'      => $dataPost['ticketNote'] ? $dataPost['ticketNote'] : null,
            'userId'    => $this->session->userdata('userId'),
            'id'        => $dataPost['ticketId'],
            'invId'     => $dataPost['ticketId'],
            'details'   => $dataPost['ticketDetails']
        );
        $resultAPI = $this->execute('/api/business/scrap/create', 'POST', $data);
        $this->response($resultAPI);
    }

    function scrapExport_get()
    {
        $this->allowAccess('scrap:export');
        $fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $status = $this->input->get('status') ?? -1;
        $shopCode = $this->session->userdata('shopCode');

        $shopFilter = shopFilter($shopCode);

        if (!count($shopFilter)) {
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ));
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        // array_push($shopCodes, "POS01");
        if($status == 'All') $status = -1;

        $data = array(
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'shopCodes' => $shopCodes,
            'status' => $status
        );
        // log_message("debug", "payload: ".print_r($data, true));
        $resultAPI = $this->execute('/api/business/scrap/export', 'GET', $data);
        // log_message("debug", "reponse: ".print_r($resultAPI, true));
        if ($resultAPI['success']) {
            set_time_limit(0);
            http_response_code(200);
            ob_clean();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="Báo cáo điều chuyển hàng.xlsx"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            // header('Content-Length: ' . filesize($filepath));
            flush();
            readfile(URL_API . '/' . $resultAPI['data']['fileName']);
        }
        exit(0);
    }


    /**
     * Chấm công 
     */
    function deleteCalendarEmployee_post()
    {
        if ( !check_permission('employee:delete') ) {
            $this->response(array(
                'success' => false,
                'message' => 'Permission denied',
                'data' => []
            ), 403);
        }
        $dataReturn = array(
            'error' => true,
            'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
        );
        $row_id = $this->input->post('row_id');

        $resultAPI = $this->site->deleteCalendarEmpWork(['row_id' => $row_id]);
        if (isset($resultAPI)) {
            if ($resultAPI['status'] == 200) {
                $dataReturn = array(
                    'error' => false,
                    'message' => 'Đã xóa lịch làm việc thành công'
                );
            }
        }
        $this->data["dataReturn"] = $dataReturn;
        $this->response($this->data);
    }

}
<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'BaseRest.php';

class Export extends BaseRest {
	function __construct(){
        // Construct the parent class
        parent::__construct();
    }

    function orderDetailInfo_get(){
    	$fromDate = $this->input->get('fromDate') ? date('Ymd', strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date('Ymd', strtotime($this->input->get('toDate'))) : date("Ymd");
        $parentCode = $this->session->userdata('shop_code');
        $data = array(
        	'parentCode' => $parentCode,
        	'fromDate' => $fromDate,
        	'toDate' => $toDate
        );
        $url = '/api/exports/orderDetailInfo';
        $result = $this->data_lib->executeRestApi($url,'GET', $data, $this->session->userdata('token'), TRUE);
        if($result['success']){
            set_time_limit(0);
            http_response_code(200);
            ob_clean();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="Đơn đặt hàng.xlsx"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            // header('Content-Length: ' . filesize($filepath));
            flush();
            readfile(URL_API . '/'. $result['fileName']);
        }
        exit(0); 
    }
}
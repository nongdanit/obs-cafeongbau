<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'BaseRest.php';

class Bi extends BaseRest
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    function aop_get()
    {
        $year = $this->input->get('year') ?? date("Y");
        $url = '/api/powerbi/aop/' . $year;
        $result = executeApi(HTTP_GET, $url);
        $this->response($result);
    }

    function nso_get()
    {
        $year = $this->input->get('year') ?? date("Y");
        $url = '/api/powerbi/nso/' . $year;
        $result = executeApi(HTTP_GET, $url);
        $this->response($result);
    }

    function real_get()
    {
        $year = $this->input->get('year') ?? date("Y");
        $url = '/api/powerbi/real/' . $year;
        $result = executeApi(HTTP_GET, $url);
        $this->response($result);
    }

    function aop_post()
    {
//        echo 'test';die;
//        $dataPost = json_decode(trim(file_get_contents('php://input')), true);
//        $shopCode = $this->input->post("shopCode");
//        $invDate = $this->input->post("orderDate");
//        $invCode = $this->input->post("orderCode");
//        $note = $this->input->post("orderNote");
//        $status = $this->input->post("orderStatus");
        $allShop = $this->session->userdata('shops');
        $dataPost = $this->input->post();
        $searchedValue = null;
        foreach ($allShop as $shop) {
            if ($shop['shopCode'] == $dataPost['shopCode']) {
                $searchedValue = $shop;
                break;
            }
        }
        if ($searchedValue != null) {
            $dataPost['shopId'] = $searchedValue['shopId'];
            $dataPost['createBy'] = $this->session->userdata('userId');
        }

//        $data = $this->input->post();
        $url = '/api/powerbi/aop/new';
        $result = executeApi(HTTP_POST, $url, $dataPost);
        $this->response($result);
    }

    function aop_put()
    {
        $params = json_decode(trim(file_get_contents('php://input')), true);
        $url = '/api/powerbi/aop/' . $params['id'];
        $params['modifyBy'] = $this->session->userdata('userId');
        $result = executeApi(HTTP_PUT, $url, $params);
        $this->response($result);
    }

    public function import_post($type)
    {
        $config['upload_path'] = "./uploads/files/";
        $config['allowed_types'] = 'xlsx';
        $config['encrypt_name'] = TRUE;
//        $type = $this->request->post('type');
        $this->load->library('upload', $config);
        if ($this->upload->do_upload("file")) {
            $data = array('upload_data' => $this->upload->data());
            $result = sendFile($data['upload_data']['full_path'], '/api/powerbi/aop/import/' . $type);
            $this->response($result);
        }
    }
}
<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'BaseRest.php';

class Dashboard extends BaseRest{

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    function summaryShop_get(){
        $this->allowAccess('dashboard:read');
    	$fromDate      = $this->get('fromDate') ? date("Ymd", strtotime($this->get('fromDate'))) : date("Ymd");
    	$toDate        = $this->get('toDate') ? date("Ymd", strtotime($this->get('toDate'))) : date("Ymd");
    	$shopCode      = $this->get('shopCode') ? $this->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel     = $this->get('shopModel') ? $this->get('shopModel') : $this->session->userdata('shopModel');
        $shopType      = $this->get('shopType') ? $this->get('shopType') : 'All';
        $provinceCode  = $this->get('provinceCode') ? $this->get('provinceCode'): 'All';
        $districtCode  = $this->get('districtCode') ? $this->get('districtCode'): 'All';
//        $role = $this->session->userdata('role');

    	$shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if(!count($shopFilter)){
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes = array_column($shopFilter, 'shopCode');
        $data = array(
            'fromDate'  => $fromDate,
            'toDate'    => $toDate,
            'shopCodes' => $shopCodes
        );
    	$url = '/api/dashboard/summary';
        $resultAPI = $this->execute($url, 'GET', $data);

        $response = array(
            'success' => true,
            'data' => array(
                'totalAmount' => 0,
                'totalBill' => 0,
                'totalQuantity' => 0,
                'totalActive' => 0,
                'totalPending' => 0,
                'totalStop' => 0,
            )
        );
        if($resultAPI['success']){
            $shopList = $this->session->userdata('shops'); 
            $countActive = $countDeactive = $countPending = 0;
            foreach ($shopList as $k => &$val) {
                switch ($val['activeStatus']) {
                    case 'Đang hoạt động':
                        $countActive++;
                        break;
                    case 'Sắp khai trương':
                        $countPending++;
                        break;
                    case 'Đã đóng cửa':
                        $countDeactive++;
                        break;    
                }
            }
            $response['data']['totalActive'] = $countActive;
            $response['data']['totalPending'] = $countPending;
            $response['data']['totalStop'] = $countDeactive;
            $response['data']['totalBill'] = $resultAPI['data']['totalBill'];
            $response['data']['totalAmount'] = $resultAPI['data']['totalAmount'];
            $response['data']['totalQuantity'] = $resultAPI['data']['totalQuantity'];
        }
    	$this->response($response);
    }

    function summaryChart_get(){
        $this->allowAccess('dashboard:read');
        $fromDate = $this->input->get('fromDate') ? date("Ymd", strtotime($this->input->get('fromDate'))) : date("Ymd");
        $toDate = $this->input->get('toDate') ? date("Ymd", strtotime($this->input->get('toDate'))) : date("Ymd");
        $shopCode = $this->input->get('shopCode') ? $this->input->get('shopCode') : $this->session->userdata('shopCode');
        $shopModel  = $this->input->get('shopModel') ? $this->input->get('shopModel') : $this->session->userdata('shopModel');
        $shopType  = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';   
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode') : 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode') : 'All';
        $checkFull = 1;

        $shopFilter = shopFilter($shopCode, $shopModel, $shopType, $provinceCode, $districtCode);
        if(!count($shopFilter)){
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }
        
        $shopCodes = array_column($shopFilter, 'shopCode');
        $dataPost = array(
            'fromDate'  => $fromDate,
            'toDate'    => $toDate,
            'shopCodes' => $shopCodes
        );
        $resultAPI = $this->execute('/api/dashboard/summaryItem', 'GET', $dataPost);

        $response = array(
            'success' => true,
            'data' => []
        );
        $dataChart = $data = [];
        if($resultAPI && !empty($resultAPI['data']) && count($resultAPI['data']) > 3){

            $resultAPI = array_values($resultAPI['data']);
            if($fromDate == $toDate){
                $txt = "Dữ liệu ngày ".date('d-m-Y', strtotime($toDate));
            }else{
                $txt = "Dữ liệu ngày từ ".date('d-m-Y', strtotime($fromDate))." đến ".date('d-m-Y', strtotime($toDate));
            }
            $data['title'] = $txt;
            if($checkFull){
                $categories = array('00-01','01-02','02-03','03-04','04-05','05-06','06-07','07-08','08-09','09-10','10-11','11-12','12-13','13-14','14-15','15-16','16-17','17-18','18-19','19-20','20-21','21-22','22-23','23-24');
            } else {
                $categories = array('06-07','07-08','08-09','09-10','10-11','11-12','12-13','13-14','14-15','15-16','16-17','17-18','18-19','19-20','20-21','21-22');
            }
            $data['categories'] = $categories;
            $dataChart = $this->groupByPartAndType($resultAPI, $checkFull);
            $i = 0;
            foreach ($dataChart as $key => $times) {
                $data['data'][$i]['name'] = $key;
                $data['data'][$i]['data'] = array_values($times);
                $i++;
            }
            $response['data'] = $data;
        }
        $this->response($response);
    }

    private function groupByPartAndType($input, $checkFull): array
    {
        $output = Array();
        if($checkFull){
            # 24h
            foreach($input as $key => $value) {
                //if($value['00-01'] != null){
                $output_element[$key] = &$output[$value['Nhóm']];
                !isset($output_element[$key]['00-01']) && $output_element[$key]['00-01'] = 0;
                $output_element[$key]['00-01'] += $value['00-01'];
                !isset($output_element[$key]['01-02']) && $output_element[$key]['01-02'] = 0;
                $output_element[$key]['01-02'] += $value['01-02'];
                !isset($output_element[$key]['02-03']) && $output_element[$key]['02-03'] = 0;
                $output_element[$key]['02-03'] += $value['02-03'];
                !isset($output_element[$key]['03-04']) && $output_element[$key]['03-04'] = 0;
                $output_element[$key]['03-04'] += $value['03-04'];
                !isset($output_element[$key]['04-05']) && $output_element[$key]['04-05'] = 0;
                $output_element[$key]['04-05'] += $value['04-05'];
                !isset($output_element[$key]['05-06']) && $output_element[$key]['05-06'] = 0;
                $output_element[$key]['05-06'] += $value['05-06'];
                !isset($output_element[$key]['06-07']) && $output_element[$key]['06-07'] = 0;
                $output_element[$key]['06-07'] += $value['06-07'];
                !isset($output_element[$key]['07-08']) && $output_element[$key]['07-08'] = 0;
                $output_element[$key]['07-08'] += $value['07-08'];
                !isset($output_element[$key]['08-09']) && $output_element[$key]['08-09'] = 0;
                $output_element[$key]['08-09'] += $value['08-09'];
                !isset($output_element[$key]['09-10']) && $output_element[$key]['09-10'] = 0;
                $output_element[$key]['09-10'] += $value['09-10'];
                !isset($output_element[$key]['10-11']) && $output_element[$key]['10-11'] = 0;
                $output_element[$key]['10-11'] += $value['10-11'];
                !isset($output_element[$key]['11-12']) && $output_element[$key]['11-12'] = 0;
                $output_element[$key]['11-12'] += $value['11-12'];
                !isset($output_element[$key]['12-13']) && $output_element[$key]['12-13'] = 0;
                $output_element[$key]['12-13'] += $value['12-13'];
                !isset($output_element[$key]['13-14']) && $output_element[$key]['13-14'] = 0;
                $output_element[$key]['13-14'] += $value['13-14'];
                !isset($output_element[$key]['14-15']) && $output_element[$key]['14-15'] = 0;
                $output_element[$key]['14-15'] += $value['14-15'];
                !isset($output_element[$key]['15-16']) && $output_element[$key]['15-16'] = 0;
                $output_element[$key]['15-16'] += $value['15-16'];
                !isset($output_element[$key]['16-17']) && $output_element[$key]['16-17'] = 0;
                $output_element[$key]['16-17'] += $value['16-17'];
                !isset($output_element[$key]['17-18']) && $output_element[$key]['17-18'] = 0;
                $output_element[$key]['17-18'] += $value['17-18'];
                !isset($output_element[$key]['18-19']) && $output_element[$key]['18-19'] = 0;
                $output_element[$key]['18-19'] += $value['18-19'];
                !isset($output_element[$key]['19-20']) && $output_element[$key]['19-20'] = 0;
                $output_element[$key]['19-20'] += $value['19-20'];
                !isset($output_element[$key]['20-21']) && $output_element[$key]['20-21'] = 0;
                $output_element[$key]['20-21'] += $value['20-21'];
                !isset($output_element[$key]['21-22']) && $output_element[$key]['21-22'] = 0;
                $output_element[$key]['21-22'] += $value['21-22'];
                !isset($output_element[$key]['22-23']) && $output_element[$key]['22-23'] = 0;
                $output_element[$key]['22-23'] += $value['22-23'];
                !isset($output_element[$key]['23-24']) && $output_element[$key]['23-24'] = 0;
                $output_element[$key]['23-24'] += $value['23-24'];
            //}
            }
        } else {
            foreach($input as $key => $value) {
                $output_element[$key] = &$output[$value['Nhóm']];
                !isset($output_element[$key]['06-07']) && $output_element[$key]['06-07'] = 0;
                $output_element[$key]['06-07'] += $value['06-07'];
                !isset($output_element[$key]['07-08']) && $output_element[$key]['07-08'] = 0;
                $output_element[$key]['07-08'] += $value['07-08'];
                !isset($output_element[$key]['08-09']) && $output_element[$key]['08-09'] = 0;
                $output_element[$key]['08-09'] += $value['08-09'];
                !isset($output_element[$key]['09-10']) && $output_element[$key]['09-10'] = 0;
                $output_element[$key]['09-10'] += $value['09-10'];
                !isset($output_element[$key]['10-11']) && $output_element[$key]['10-11'] = 0;
                $output_element[$key]['10-11'] += $value['10-11'];
                !isset($output_element[$key]['11-12']) && $output_element[$key]['11-12'] = 0;
                $output_element[$key]['11-12'] += $value['11-12'];
                !isset($output_element[$key]['12-13']) && $output_element[$key]['12-13'] = 0;
                $output_element[$key]['12-13'] += $value['12-13'];
                !isset($output_element[$key]['13-14']) && $output_element[$key]['13-14'] = 0;
                $output_element[$key]['13-14'] += $value['13-14'];
                !isset($output_element[$key]['14-15']) && $output_element[$key]['14-15'] = 0;
                $output_element[$key]['14-15'] += $value['14-15'];
                !isset($output_element[$key]['15-16']) && $output_element[$key]['15-16'] = 0;
                $output_element[$key]['15-16'] += $value['15-16'];
                !isset($output_element[$key]['16-17']) && $output_element[$key]['16-17'] = 0;
                $output_element[$key]['16-17'] += $value['16-17'];
                !isset($output_element[$key]['17-18']) && $output_element[$key]['17-18'] = 0;
                $output_element[$key]['17-18'] += $value['17-18'];
                !isset($output_element[$key]['18-19']) && $output_element[$key]['18-19'] = 0;
                $output_element[$key]['18-19'] += $value['18-19'];
                !isset($output_element[$key]['19-20']) && $output_element[$key]['19-20'] = 0;
                $output_element[$key]['19-20'] += $value['19-20'];
                !isset($output_element[$key]['20-21']) && $output_element[$key]['20-21'] = 0;
                $output_element[$key]['20-21'] += $value['20-21'];
                !isset($output_element[$key]['21-22']) && $output_element[$key]['21-22'] = 0;
                $output_element[$key]['21-22'] += $value['21-22'];
            }
        }
        return ($output);
    }
}
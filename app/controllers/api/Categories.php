<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'BaseRest.php';

class Categories extends BaseRest {
	
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    function priceList_get(){
        $this->allowAccess('prices:read');
        $url = '/api/categories/prices';
        $resultAPI = $this->execute($url, 'GET');
        $this->response($resultAPI);
    }

    function storeByPrice_get(){
        $priceId = $this->input->get('priceId');
        $data = array('priceId' => $priceId );

        $url = '/api/categories/prices/storeByPrice';
        $resultAPI = $this->execute($url, 'GET', $data);
        $this->response($resultAPI);
    }

    function itemByPrice_get(){
        $priceId = $this->input->get('priceId');
        $data = array('priceId' => $priceId );

        $url = '/api/categories/prices/itemByPrice';
        $resultAPI = $this->execute($url, 'GET', $data);
        $this->response($resultAPI);
    }

    function priceDetail_get(){
        $this->allowAccess('prices:read');
        $priceId = $this->input->get('priceId');
        $data = array('priceId' => $priceId );

        $url = '/api/categories/prices/detail';
        $resultAPI = $this->execute($url, 'GET', $data);
        $this->response($resultAPI);
    }

    function itemList_get(){
        $this->allowAccess('qc_items:read');
        $url = '/api/categories/item/list';
        $resultAPI = $this->execute($url, 'GET');
        $this->response($resultAPI);
    }

    function stores_get(){
        $this->allowAccess('shop_info:read');
        $shopType  = $this->input->get('shopType') ? $this->input->get('shopType') : 'All';
        $provinceCode = $this->input->get('provinceCode') ? $this->input->get('provinceCode'): 'All';
        $districtCode = $this->input->get('districtCode') ? $this->input->get('districtCode'): 'All';
        $shopModel = $this->input->get('shopModel') ? $this->input->get('shopModel'): $this->session->userdata('shopModel');
        $status =  null != $this->input->get('status') ? $this->input->get('status'): 'All';

        $shopFilter = shopFilter('All', $shopModel, $shopType, $provinceCode, $districtCode);
        
        if(!count($shopFilter)){
            $this->response(array(
                'success' => false,
                'message' => 'Bad request.'
            ), 400);
        }

        $shopCodes  = array_column($shopFilter, 'shopCode');

        $dataPost =  array(
            'shopCodes' => $shopCodes,
            'status'    => $status
        );
        //log_message("debug", "aaaa resultAPI: " . print_r($dataPost, true));
        $resultAPI = $this->execute('/api/categories/shop/list','GET', $dataPost);
        $this->response($resultAPI);
    }

    function recipe_get(){
        $this->allowAccess("recipe:read");
        $url = '/api/categories/recipe';
        $result = $this->execute($url, 'GET');
        $response = array(
            'groups' => array(),
            'items' => array(),
            'data' => array()
        );
        $groups = array();
        $items = array('Tất cả');
        
        if(!empty($result) && $result['success']){
            $allObject = $result['data'];
            $allGroup = array_values(array_unique(array_column($allObject, 'mtlGroup')));
            $allItem = array_values(array_unique(array_column($allObject, 'itemName')));
            usort($allGroup, function($a,$b){
                $coll = collator_create('utf8mb4_vietnamese_ci');
                return collator_compare( $coll, strtolower($a), strtolower($b));
            });
            usort($allItem, function($a,$b){
                $coll = collator_create('utf8mb4_vietnamese_ci');
                return collator_compare( $coll, strtolower($a), strtolower($b));
            });
            array_push(
                $groups, 
                array(
                    'id' => 'Tất cả', 
                    'text' => 'Tất cả', 
                    'child' => array_merge(array('Tất cả'), array_values(array_unique(array_column($allObject, 'itemName'))))
                )
            );
            foreach ($allGroup as $key => $value) {
                $new_array = array_filter($allObject, function($obj) use ($value){
                    if($obj['mtlGroup'] == $value){
                        return true;
                    }
                });
                array_push(
                    $groups, 
                    array(
                        'id' => $value, 
                        'text' => $value, 
                        'child' => array_merge(array('Tất cả'), array_values(array_unique(array_column($new_array, 'itemName')))))
                    );
                //array_push($groups,$value);
            }
            
            $response['groups'] = $groups;
            $response['items'] = $items;
            $response['data'] = $allObject;
        }    
        $this->response($response);
    }
}